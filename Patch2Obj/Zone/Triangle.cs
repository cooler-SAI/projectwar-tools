﻿namespace UnexpectedBytes.War.Terrain
{
    public class Triangle
    {
        public System.Int32 A { get; set; }
        public System.Int32 B { get; set; }
        public System.Int32 C { get; set; }

        public Triangle()
        {
            A = 0;
            B = 0;
            C = 0;
        }
    }
}
