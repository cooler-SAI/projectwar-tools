﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class Heightmap
    {
        public List<UInt16> data;
        public Int32 Width = 131;
        public Int32 Height = 131;

        public void Read(BinaryReader reader)
        {
            for (var y = 0; y < 131; y++)
            {
                for (var x = 0; x < 131; x++)
                {
                    data.Add(reader.ReadUInt16());
                }
            }
        }
    }
}
