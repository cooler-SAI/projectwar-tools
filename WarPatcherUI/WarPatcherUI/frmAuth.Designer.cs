﻿using System.Drawing;

namespace WarPatcherUI
{
    partial class frmAuth
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAuth));
            this.lblFile = new WarPatcherUI.MLabel();
            this.lblTotal = new WarPatcherUI.MLabel();
            this.btnCancel = new WarPatcherUI.MButton();
            this.btnPlay = new WarPatcherUI.MButton();
            this.mButton1 = new WarPatcherUI.MButton();
            this.mLabel1 = new WarPatcherUI.MLabel();
            this.txtName = new WarPatcherUI.MTextBox();
            this.mTextBox1 = new WarPatcherUI.MTextBox();
            this.lblError = new WarPatcherUI.MLabel();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.alphaFormTransformer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // alphaFormTransformer1
            // 
            this.alphaFormTransformer1.Controls.Add(this.lblError);
            this.alphaFormTransformer1.Controls.Add(this.mTextBox1);
            this.alphaFormTransformer1.Controls.Add(this.txtName);
            this.alphaFormTransformer1.Controls.Add(this.mLabel1);
            this.alphaFormTransformer1.Controls.Add(this.mButton1);
            this.alphaFormTransformer1.Controls.Add(this.btnCancel);
            this.alphaFormTransformer1.Controls.Add(this.btnPlay);
            this.alphaFormTransformer1.Controls.Add(this.lblFile);
            this.alphaFormTransformer1.Controls.Add(this.lblTotal);
            this.alphaFormTransformer1.Size = new System.Drawing.Size(607, 407);
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile.ForeColor = System.Drawing.Color.White;
            this.lblFile.LayoutName = "nametextlabel";
            this.lblFile.Location = new System.Drawing.Point(74, 230);
            this.lblFile.Margin = new System.Windows.Forms.Padding(0);
            this.lblFile.Name = "lblFile";
            this.lblFile.PadLeft = 0;
            this.lblFile.PadTop = 0;
            this.lblFile.ResourceName = null;
            this.lblFile.Size = new System.Drawing.Size(174, 16);
            this.lblFile.TabIndex = 0;
            this.lblFile.Text = "[STR_PROMPT_LOGIN]";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.White;
            this.lblTotal.LayoutName = "passwordtextlabel";
            this.lblTotal.Location = new System.Drawing.Point(51, 266);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.PadLeft = 0;
            this.lblTotal.PadTop = 0;
            this.lblTotal.ResourceName = null;
            this.lblTotal.Size = new System.Drawing.Size(216, 16);
            this.lblTotal.TabIndex = 2;
            this.lblTotal.Text = "[STR_PROMPT_PASSWORD]";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.LayoutName = "cancelbutton";
            this.btnCancel.Location = new System.Drawing.Point(317, 301);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.ResourceName = "authbutton";
            this.btnCancel.Size = new System.Drawing.Size(119, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "[STR_CANCEL]";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlay.LayoutName = "okbutton";
            this.btnPlay.Location = new System.Drawing.Point(451, 301);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.ResourceName = "authbutton";
            this.btnPlay.Size = new System.Drawing.Size(104, 23);
            this.btnPlay.TabIndex = 4;
            this.btnPlay.Text = "[STR_OK]";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // mButton1
            // 
            this.mButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mButton1.LayoutName = "createbutton";
            this.mButton1.Location = new System.Drawing.Point(289, 330);
            this.mButton1.Name = "mButton1";
            this.mButton1.ResourceName = "createaccountbutton";
            this.mButton1.Size = new System.Drawing.Size(104, 23);
            this.mButton1.TabIndex = 6;
            this.mButton1.Text = "[STR_CREATE_ACCOUNT]";
            this.mButton1.UseVisualStyleBackColor = true;
            this.mButton1.Click += new System.EventHandler(this.mButton1_Click);
            // 
            // mLabel1
            // 
            this.mLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mLabel1.ForeColor = System.Drawing.Color.White;
            this.mLabel1.LayoutName = "titletext";
            this.mLabel1.Location = new System.Drawing.Point(120, 146);
            this.mLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel1.Name = "mLabel1";
            this.mLabel1.PadLeft = 0;
            this.mLabel1.PadTop = 0;
            this.mLabel1.ResourceName = null;
            this.mLabel1.Size = new System.Drawing.Size(177, 16);
            this.mLabel1.TabIndex = 12;
            this.mLabel1.Text = "[STR_APPWINDOW_TITLE]";
            this.mLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtName
            // 
            this.txtName.LayoutName = "nameinput";
            this.txtName.Location = new System.Drawing.Point(213, 227);
            this.txtName.MaxLength = 25;
            this.txtName.Multiline = false;
            this.txtName.Name = "txtName";
            this.txtName.Padding = new System.Windows.Forms.Padding(2, 3, 0, 0);
            this.txtName.PasswordChar = '\0';
            this.txtName.ReadOnly = false;
            this.txtName.ResourceName = null;
            this.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtName.SelectionLength = 0;
            this.txtName.SelectionStart = 0;
            this.txtName.Size = new System.Drawing.Size(164, 24);
            this.txtName.TabIndex = 1;
            this.txtName.WordWrap = false;
            // 
            // mTextBox1
            // 
            this.mTextBox1.LayoutName = "passwordinput";
            this.mTextBox1.Location = new System.Drawing.Point(213, 263);
            this.mTextBox1.MaxLength = 25;
            this.mTextBox1.Multiline = false;
            this.mTextBox1.Name = "mTextBox1";
            this.mTextBox1.Padding = new System.Windows.Forms.Padding(2, 3, 0, 0);
            this.mTextBox1.PasswordChar = '*';
            this.mTextBox1.ReadOnly = false;
            this.mTextBox1.ResourceName = null;
            this.mTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mTextBox1.SelectionLength = 0;
            this.mTextBox1.SelectionStart = 0;
            this.mTextBox1.Size = new System.Drawing.Size(164, 24);
            this.mTextBox1.TabIndex = 3;
            this.mTextBox1.WordWrap = false;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Firebrick;
            this.lblError.LayoutName = "messagetext";
            this.lblError.Location = new System.Drawing.Point(93, 193);
            this.lblError.Margin = new System.Windows.Forms.Padding(0);
            this.lblError.Name = "lblError";
            this.lblError.PadLeft = 0;
            this.lblError.PadTop = 0;
            this.lblError.ResourceName = null;
            this.lblError.Size = new System.Drawing.Size(117, 13);
            this.lblError.TabIndex = 15;
            this.lblError.Text = "[STR_ENTER_LOGIN]";
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmAuth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 407);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmAuth";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.frmAuth_Load);
            this.Shown += new System.EventHandler(this.frmAuth_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAuth_KeyDown);
            this.alphaFormTransformer1.ResumeLayout(false);
            this.alphaFormTransformer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MLabel lblFile;
        private MLabel lblTotal;
        private MButton btnCancel;
        private MButton btnPlay;
        private MButton mButton1;
        private MLabel mLabel1;
        private MTextBox txtName;
        private MTextBox mTextBox1;
        private MLabel lblError;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
    }
}