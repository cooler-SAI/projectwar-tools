﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WarPatcherUI.Types;
using WarPatcherWrapper;

#pragma warning disable 414

namespace WarPatcherUI
{
    public class Patcher
    {
        private const int SW_RESTORE = 9;

        #region imports
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        #endregion

        #region events
        public delegate void StateChangeDelegate(Patcher patcher, PatcherState state);
        public delegate void StringDelegate(Patcher patcher, string msg);
        public event StateChangeDelegate OnStateChanged;
        public event StringDelegate OnError;
        public event StringDelegate OnMOTD;
        #endregion

        #region members
        private HashSet<MythicPackage> _updatedArchives = new HashSet<MythicPackage>();
        private List<ManifestItem> _missingItems = new List<ManifestItem>();
        private readonly Config Config;
        private ClientPatcher _clientPatcher;
        private Downloader _downloader;
        private REQUEST_FILE_MANIFEST _manifest;
        private PatcherState _state = PatcherState.GetServerStatus;
        private bool _initialized = false;
        private bool _playRequested = false;
        private string _username;
        private string _password;
        Thread _updateThread = null;
        #endregion

        #region properties
        public long TotalSize { get; private set; }
        public long TotalCurrent { get; private set; }
        public long CurrentSize { get; private set; }
        public long Current { get; private set; }
        public string CurrentItemName { get; private set; } = "";
        public string CurrentArchive = "";
        public int Rate { get; private set; }
        public float CurrentPercent => CurrentSize > 0 ? ((float)Current * 100f / (float)CurrentSize) : 0;
        public float TotalPercent => TotalSize > 0 ? ((float)TotalCurrent * 100f / (float)TotalSize) : 0;
 
        public PatcherState State
        {
            get
            {
                return _state;
            }
            private set
            {
                bool changed = false;
                if (value != _state)
                    changed = true;

                _state = value;
                if (changed && OnStateChanged != null)
                    OnStateChanged(this, _state);
            }
        }
        #endregion

        public Patcher(Config config, string username, string password, ClientPatcher clientPatcher, Downloader downloader)
        {
            _initialized = false;
            Config = config;
            _clientPatcher = clientPatcher;
            _downloader = downloader;
            _username = username;
            _password = password;

            _downloader.OnDownloadQueueFinished = OnDownloadQueueFinished;
            _downloader.OnFileDownloaded = OnFileDownloaded;
            _downloader.OnAssetDownloaded = OnAssetDownloaded;
            _clientPatcher.OnMYPLoadProgress += Patcher_OnMYPLoadProgress;
            _downloader.OnDownloadProgress = ProgressDelegate;
        }

        #region methods
        private void ProgressDelegate(Downloader downloader, string asset, long current, long currentTotal, long overallCurrent, long overallTotal, int rate)
        {
            CurrentItemName = asset;
            TotalSize = overallTotal;
            TotalCurrent = overallCurrent;
            Current = current;
            CurrentSize = currentTotal;
            Rate = rate;
        }

        public async Task UpdateState()
        {
            if (State == PatcherState.GetServerStatus)
            {
                await GetServerStatus();
                State = PatcherState.InitializeArchives;
                await UpdateState();
            }
            else if (State == PatcherState.InitializeArchives)
            {
                await InitializeArchives();
                State = PatcherState.RequestManifest;
                await UpdateState();
            }
            else if (State == PatcherState.RequestManifest)
            {
                await RequestManifest();
                State = PatcherState.ValidateFiles;
                await UpdateState();
            }
            else if (State == PatcherState.ValidateFiles)
            {
                _missingItems.Clear();
                _updatedArchives.Clear();

                await ValidateFiles();
                if (_missingItems.Count > 0)
                {
                    State = PatcherState.RequestAssets;
                    _missingItems.ForEach(e => _downloader.AddAsset(e));
                    await _downloader.ProcessQueue();
                }
                else
                {
                    State = PatcherState.Ready;
                    _updateThread = null;
                    if (_playRequested)
                    {
                        Launch();
                    }
                }
            }
        }

        private void Patcher_OnMYPLoadProgress(string filename, int index, int total)
        {
            TotalSize = total;
            TotalCurrent = index;
            Current = index;
            CurrentSize = total;
            CurrentItemName = Path.GetFileName(filename);
        }

        private Task OnAssetDownloaded(ManifestAssetItem asset)
        {
            _clientPatcher.UpdateAsset((int)asset.Archive, asset.Hash, asset.Compressed, asset.Data, asset.CRC);

            if (!_updatedArchives.Contains(asset.Archive))
                _updatedArchives.Add(asset.Archive);

            return Task.CompletedTask;
        }

        private Task OnFileDownloaded(ManifestFileItem asset)
        {
            Log.Info(asset.Name + " downloaded");

            var file = Path.Combine(Config.WarFolder, asset.Name);

            if (File.Exists(file))
            {
                Log.Info($"File {file} already exists. Deleting");
                File.Delete(file);
            }

            File.Move(asset.TempFileName, file);

            if (!file.ToLower().EndsWith(".myp"))
            {
                var crc = Util.Adler32(0, File.ReadAllBytes(file));
                if (crc != asset.CRC)
                {
                    Log.Info($"Required file '{file}' has invalid hash. (Current CRC:{crc} Server CRC:{asset.CRC})");
                    Error("Corrupted file.");
                }
            }

            var s = File.GetAccessControl(file);

            SecurityIdentifier cu = WindowsIdentity.GetCurrent().User;
            s.SetOwner(cu);
            s.SetAccessRule(new FileSystemAccessRule(cu, FileSystemRights.FullControl, AccessControlType.Allow));

            //Update the Access Control on the File
            File.SetAccessControl(file, s);

            return Task.CompletedTask;
        }

        private Task OnDownloadQueueFinished()
        {
            State = PatcherState.ValidateFiles;

            foreach (var archive in _updatedArchives)
            {
                _clientPatcher.SaveArchive((int)archive);
            }

            return UpdateState();
        }

        private async Task GetServerStatus()
        {
            var status = await HttpUtil.RequestAsync<SERVER_STATUS>(Config.PatcherServer);
            if (OnMOTD != null)
                OnMOTD(this, status.MOTD);
        }

        private async Task RequestManifest()
        {
            Log.Info("Requesting file manifest");
            _manifest = await HttpUtil.RequestAsync<REQUEST_FILE_MANIFEST>(Config.PatcherServer);

            foreach (var m in _manifest.Items.Where(e => e.Name.ToUpper().EndsWith(".MYP")))
            {
                Log.Info($"MYP {m.Name} HashHash:{m.CRC}");
            }
        }

        private void CheckArchive(MythicPackage package, List<PatcherAsset> list)
        {
            Log.Info($"Checking archive  {package}' (AssetCount:{list.Count})");
            var server = list.ToDictionary(e => e.Hash, e => e);
            var current = _clientPatcher.GetAssetHashes((int)package);

            //check missing assets
            foreach (var asset in server.Values)
            {
                if (!current.ContainsKey(asset.Hash))
                {
                    //Log.Info($"Asset missing { asset.Hash.ToString()} in {package}");
                    _missingItems.Add(new ManifestAssetItem()
                    {
                        Archive = package,
                        Hash = asset.Hash,
                        Size = (int)asset.Size,
                        Compressed = asset.Compressed,
                        CRC = asset.CRC32,
                        CompressedSize = (int)asset.CompressedSize,
                    });
                }
            }

            //check deleted assets
            foreach (var asset in current.Values)
            {
                if (!server.ContainsKey(asset.Hash))
                {
                    Log.Info($"Asset should not exist {asset.Hash} in {package}");
                    _clientPatcher.DeleteAsset((int)package, asset.Hash);
                    if (!_updatedArchives.Contains(package))
                        _updatedArchives.Add(package);
                }
            }

            ////check updated assets
            foreach (var asset in server.Values)
            {
                if (current.ContainsKey(asset.Hash))
                {
                    if (asset.CRC32 != current[asset.Hash].CRC || asset.Size != current[asset.Hash].Size)
                    {
                        Log.Info($"Asset out of date {asset.Hash.ToString()} in {package}");
                        _missingItems.Add(new ManifestAssetItem()
                        {
                            Archive = asset.Archive,
                            Hash = asset.Hash,
                            Size = (int)asset.Size,
                            Compressed = asset.Compressed,
                            CRC = asset.CRC32,
                            CompressedSize = (int)asset.CompressedSize,
                        });
                    }
                }
            }
        }

        private Task ValidateFiles()
        {
            return Task.Run(() =>
            {
                Log.Info($"Validate Files (files:{_manifest.Items.Count})");

                foreach (var file in _manifest.Items)
                {
                    string path = Path.Combine(Config.WarFolder, file.Name);
                    string folder = Path.GetDirectoryName(path);
                    if (!Directory.Exists(folder))
                    {
                        Log.Info($"Directory '{folder}' does not exist. Creating");
                        Directory.CreateDirectory(folder);
                    }

                    if (!File.Exists(path))
                    {

                        Log.Info($"Required file '{path}' missing. Adding to download queue.");
                        if (!path.ToUpper().EndsWith(".MYP"))
                            _missingItems.Add(file);
                        else
                        {
                            var archive = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileName(file.Name.ToUpper().Replace(".MYP", "")));
                            _clientPatcher.CreateArchive(path, (int)archive);
                        }

                    }
                    else if (!path.ToLower().EndsWith(".myp"))
                    {
                        var crc = Util.Adler32(0, File.ReadAllBytes(path));
                        if (crc != file.CRC)
                        {
                            Log.Info($"Required file '{path}' has invalid hash. (Current CRC:{crc} Server CRC:{file.CRC}) Added to download queue.");
                            _missingItems.Add(file);
                        }
                    }
                    else
                    {
                        var archive = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileName(file.Name.ToUpper().Replace(".MYP", "")));
                    }
                }

                var hashes = _clientPatcher.GetArchiveHashes();
                var archives = _manifest.Items.Where(e => e.Name.ToLower().EndsWith(".myp")).ToDictionary(e => ((MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(e.Name).ToUpper())), e => e);
                var reqList = new List<MythicPackage>();

                foreach (var m in hashes)
                {
                    var archive = (MythicPackage)m.Key;
                    if (!archives.ContainsKey(archive))
                        continue;

                    if (archives.ContainsKey(archive) && archives[archive].CRC != m.Value)
                    {
                        Log.Info($"Archive out of date '{archive}' hash (Current:{m.Value} Server:{archives[archive].CRC })");
                        //if (archive != MythicPackage.DEV) 
                        reqList.Add(archive);
                    }
                }

                if (reqList.Count > 0)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        HttpUtil.RequestStream(Config.PatcherServer, new REQUEST_ARCHIVE_MANIFEST() { Archives = reqList }, ms, (current, total, read) => { });
                        ms.Position = 0;
                        BinaryReader reader = new BinaryReader(ms);

                        int count = reader.ReadByte();
                        for (int i = 0; i < count; i++)
                        {
                            var package = (MythicPackage)reader.ReadByte();
                            var size = reader.ReadInt32();
                            var data = Util.DeCompress(reader.ReadBytes(size));
                            var list = new List<PatcherAsset>();
                            if (size > 0)
                            {
                                using (var ms2 = new MemoryStream(data))
                                {
                                    var reader2 = new BinaryReader(ms2);
                                    int assetCount = reader2.ReadInt32();
                                    for (int c = 0; c < assetCount; c++)
                                    {
                                        var asset = new PatcherAsset()
                                        {
                                            Hash = reader2.ReadInt64(),
                                            CRC32 = (uint)reader2.ReadUInt32(),
                                            CompressedSize = reader2.ReadInt32(),
                                            Size = reader2.ReadUInt32(),
                                            Compressed = reader2.ReadByte() > 0,
                                            Archive = package

                                        };
                                        list.Add(asset);
                                    }
                                }

                                CheckArchive(package, list);
                            }
                        }
                    }
                }
            });
        }

        public static void PatchExe(string warPath)
        {
            if (!IsWarRunning(warPath))
            {
                using (Stream stream = new FileStream(warPath, FileMode.OpenOrCreate))
                {

                    int encryptAddress = (0x00957FBE + 3) - 0x00400000;
                    stream.Seek(encryptAddress, SeekOrigin.Begin);
                    stream.WriteByte(0x01); //cmp byte ptr[edi+22h],0 -> cmp byte ptr[edi+22h],1



                    byte[] decryptPatch1 = { 0x90, 0x90, 0x90, 0x90, 0x57, 0x8B, 0xF8, 0xEB, 0x32 };//NOP | NOP | NOP | NOP | push ei | mov edi,eax | JUMP short 958106
                    int decryptAddress1 = (0x009580CB) - 0x00400000;
                    stream.Seek(decryptAddress1, SeekOrigin.Begin);
                    stream.Write(decryptPatch1, 0, 9);

                    byte[] decryptPatch2 = { 0x90, 0x90, 0x90, 0x90, 0xEB, 0x08 }; //NOP NOP NOP NOP JUMP short 958159
                    int decryptAddress2 = (0x0095814B) - 0x00400000;
                    stream.Seek(decryptAddress2, SeekOrigin.Begin);
                    stream.Write(decryptPatch2, 0, 6);

                    //stream.WriteByte(0x01);
                }
            }
        }

        private static void StartGame(string auth, string user, string path)
        {
            Process Pro = new Process();

            Pro.StartInfo.FileName = path;
            Pro.StartInfo.Arguments = " --acctname=" + System.Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(user)) + " --sesstoken=" + auth + " --loadlog";
            Pro.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            if (Pro.Start())
            {
                System.Threading.Thread.Sleep(2000);
                var handle = Pro.MainWindowHandle;
                SetForegroundWindow(Pro.MainWindowHandle);
            }

        }

        public static bool IsWarRunning(string path)
        {
            foreach (Process p in Process.GetProcesses())
            {
                if (p.ProcessName == "WAR")
                    return true;
            }
            return false;
        }

        public void Play()
        {
            if (_updateThread != null)
                return;

            if (!IsWarRunning(Path.Combine(Config.WarFolder, "war.exe")))
            {
                _playRequested = true;
                State = PatcherState.InitializeArchives;
                _updateThread = new Thread(() =>
                {
                    UpdateState().Wait();
                });

                _updateThread.IsBackground = true;
                _updateThread.Start();
            }
            else
                Launch();
        }

        private void Launch()
        {
            string warExePath = Path.Combine(Config.WarFolder, "war.exe");
            if (!File.Exists(warExePath))
            {
                Error($"{Path.GetFileName(warExePath.ToUpper())} does not exist at {Path.GetDirectoryName(warExePath.ToUpper())}");
            }
            else
            {
                if (_initialized)
                    CloseArchives();

                Directory.SetCurrentDirectory(Config.WarFolder);
                PatchExe(warExePath);
                StartGame(_password, _username, warExePath);
                State = PatcherState.Playing;
            }
        }

        private void CloseArchives()
        {
            _initialized = false;
            _clientPatcher.CloseManager();
        }

        private Task InitializeArchives()
        {
            return Task.Run(() =>
            {
                uint results = 0;
                results = _clientPatcher.InitManager(Config.WarFolder);
                this._initialized = true;
                for (int i = 0; i < 31; i++)
                {
                    MythicPackage archive = (MythicPackage)i;
                    if ((int)(results >> i & 1) > 0)
                    {
                        Log.Info(archive + " loaded");
                    }
                    else
                    {
                        var path = Path.Combine(Config.WarFolder, archive.ToString() + ".myp");
                        if (File.Exists(path))
                        {
                            try
                            {
                                Log.Info(archive + " is corrupt. Deleting.");
                                File.Delete(path);
                            }
                            catch (Exception e)
                            {
                                Log.Exception("Error deleting corrupt acrhive: ", e);
                            }
                        }
                    }
                }
            });
        }

        private void Error(string msg)
        {
            State = PatcherState.Error;

            if (OnError != null)
                OnError(this, msg);
        }

        #endregion
    }
}
