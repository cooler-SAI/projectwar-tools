﻿namespace WarPatcherUI.Types
{
    public enum LoginStatus
    {
        OK,
        INVALID_USER,
        CREATED,
        USERNAME_ALREADY_EXISTS,
        EMAIL_ALREADY_EXISTS,
        INVALID_EMAIL,
        INVALID_LOGIN,
    }
}
