﻿namespace WarPatcherUI.Types
{
    public class ArchiveAsset
    {
        public long Hash { get; set; }
        public uint CRC { get; set; }
        public long Size { get; set; }
        public bool Compressed { get; set; }
        public string Name { get; set; }
    }
}
