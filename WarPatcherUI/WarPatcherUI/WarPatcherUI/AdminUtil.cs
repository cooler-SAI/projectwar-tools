﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarPatcherUI
{
    public static class TaskExtensions
    {
        public static void RunInBackground(this Task task)
        {
            task.ContinueWith(t => { }, TaskContinuationOptions.OnlyOnFaulted);
        }


        public static async Task<T> Timeout<T>(this Task<T> task, int ms)
        {
            var source = new System.Threading.CancellationTokenSource();
            var completed = await Task.WhenAny(task, Task.Delay(ms, source.Token));
            if (completed == task)
            {
                source.Cancel();
                return await task;
            }
            throw new TimeoutException();
        }


        public static async Task Timeout(this Task task, int ms)
        {
            var source = new System.Threading.CancellationTokenSource();
            var completed = await Task.WhenAny(task, Task.Delay(ms, source.Token));
            if (completed == task)
            {
                source.Cancel();
                await task;
            }
            else
                throw new TimeoutException();
        }

    }

    public static class AdminUtil
    {
        public static TaskScheduler UIThread { get; set; }

        public static void RunAsync(this Task task)
        {
            task.ContinueWith(t => { }, TaskContinuationOptions.OnlyOnFaulted);
        }

        public static Task UITask(Action<Task> a)
        {

            return Task.Run(() =>
            {
            }).ContinueWith(a, UIThread);

        }


        public static List<int> CSVToIntList(string csv, char[] splitTokens = null)
        {
            var list = new List<int>();
            if (splitTokens == null)
                splitTokens = new char[] { ',', ' ', '|' }; ;

            var tokens = csv.Split(splitTokens, StringSplitOptions.RemoveEmptyEntries);
            foreach (var token in tokens)
            {
                int value = 0;
                if (int.TryParse(token, out value))
                    list.Add(value);
            }
            return list;
        }


        public static List<string> CSVToStrList(string csv, char[] splitTokens = null)
        {
            var list = new List<string>();
            if (splitTokens == null)
                splitTokens = new char[] { ',', ' ', '|' }; ;

            var tokens = csv.Split(splitTokens, StringSplitOptions.RemoveEmptyEntries);
            foreach (var token in tokens)
            {
                list.Add(token);
            }
            return list;
        }




    }
}
