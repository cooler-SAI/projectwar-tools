﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarPatcherUI
{
    public partial class frmPatcher : MythicForm
    {
       public override string _bgName { get; set; } = "mainbackground";
       public override string _windowName { get; set; } = "mainwindow";


        private string _currentFileMsg = "";
        private string _totalMsg = "";
        private int _currentPercent = 0;
        private int _totalPercent;
        private bool _changed = false;
        private List<string> _log = new List<string>();
        public frmPatcher()
        {
            AdminUtil.UIThread = TaskScheduler.FromCurrentSynchronizationContext();


            InitializeComponent();

            txtLog.Width = 0;
            txtLog.Height = 0;

            InitResource(
                "WarPatcherUI.patch.txt", 
                "WarPatcherUI.9420659275846915044.xml", 
                "WarPatcherUI.patcher.png",
                "WarPatcherUI.ageofreckoning.ttf");

     
            Program.Patcher.OnLog += Patcher_OnLog;
            Program.Patcher.OnMYPLoadProgress += Patcher_OnMYPLoadProgress;
            lblTotal.Text = "";
            lblFile.Text = "";
            Task.Run(() =>
            {
                Program.Patcher.InitManager(@"c:\warhammer2");
            }).RunInBackground();
        }

        private async void Patcher_OnMYPLoadProgress(string filename, int index, int total)
        {
            _totalPercent = index * 100 / total;
            _totalMsg = $"Loading {Path.GetFileName(filename)} ({_totalPercent}%)";

            if (index == total - 1)
            {
                _totalPercent = 0;
                _totalMsg = "Loading myps completed";
            }
            _changed = true;
        }


        private async void Patcher_OnLog(WarPatcherWrapper.LogType type, WarPatcherWrapper.LogLevel level, string msg)
        {
            lock (_log)
            {
                _log.Add($"{DateTime.Now.ToString("hh:mm:ss")}: {msg}\r\n");
                _changed = true;
            }
   
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {

        }

        private void frmPatcher_Load(object sender, EventArgs e)
        {
            mBrowser1.ScriptErrorsSuppressed = true;
            mBrowser1.DocumentCompleted += MBrowser1_DocumentCompleted;
            mBrowser1.AllowNavigation = false;
      
            try
            {
                mBrowser1.Navigate(@"file:///C:/Warhammer/notes/english/readme.html");
            }
            catch (Exception ee)
            {
            }
  
            alphaFormTransformer1.TransformForm2(255);

          
        }

        private void MBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            mBrowser1.Document.Body.Style = "overflow:hidden";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        int incr = 1;
        int incr2 = 3;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_changed)
            {
                mTotalProgress.Value = _totalPercent;
                lblTotal.Text = _totalMsg;
                mTotalProgress.Paint();

                if (_log.Count > 0)
                {
                    var list = new List<string>();
                    lock (_log)
                    {
                        list = _log.ToList();
                        _log.Clear();
                    }
                    StringBuilder builder = new StringBuilder();
                    foreach (var line in list)
                    {
                        builder.Append(line);
                }
                    foreach (var line in list)
                    {
                        txtLog.Text += line;
                        txtLog.SelectionStart = txtLog.TextLength;
                        txtLog.ScrollToCaret();
                    }
         

                }
      
   
            }
            _changed = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void lblShowLog_Click(object sender, EventArgs e)
        {
        
        }

        private void lblShowLog_Click_1(object sender, EventArgs e)
        {
            if (txtLog.Width == 0)
            {
                txtLog.Visible = true;
                lblShowLog.Text = "Hide Log";


                txtLog.Left = this.mBrowser1.Left;
                txtLog.Top = this.mBrowser1.Top;
                txtLog.Width = this.mBrowser1.Width;
                txtLog.Height = this.mBrowser1.Height;
            }
            else
            {
                txtLog.Width = 0;
                txtLog.Height = 0;

                txtLog.Visible = false;
                lblShowLog.Text = "Show Log";
            }
        }
    }
}
