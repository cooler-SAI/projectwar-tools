﻿namespace WarPatcherUI
{
    partial class frmPatcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPatcher));
            this.btnPlay = new WarPatcherUI.MButton();
            this.btnCancel = new WarPatcherUI.MButton();
            this.pbFile = new WarPatcherUI.MProgressBar();
            this.mTotalProgress = new WarPatcherUI.MProgressBar();
            this.lblFile = new WarPatcherUI.MLabel();
            this.lblTotal = new WarPatcherUI.MLabel();
            this.btnClose = new WarPatcherUI.MButton();
            this.btnMinimize = new WarPatcherUI.MButton();
            this.lblTitle = new WarPatcherUI.MLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mLabel2 = new WarPatcherUI.MLabel();
            this.lblStatus = new WarPatcherUI.MLabel();
            this.mLabel4 = new WarPatcherUI.MLabel();
            this.mLabel5 = new WarPatcherUI.MLabel();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.patchNotes = new WarPatcherUI.RTextBox();
            this.alphaFormTransformer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // alphaFormTransformer1
            // 
            this.alphaFormTransformer1.BackColor = System.Drawing.SystemColors.Control;
            this.alphaFormTransformer1.BackgroundImage = null;
            this.alphaFormTransformer1.Controls.Add(this.patchNotes);
            this.alphaFormTransformer1.Controls.Add(this.mLabel4);
            this.alphaFormTransformer1.Controls.Add(this.mLabel5);
            this.alphaFormTransformer1.Controls.Add(this.lblStatus);
            this.alphaFormTransformer1.Controls.Add(this.mLabel2);
            this.alphaFormTransformer1.Controls.Add(this.lblTitle);
            this.alphaFormTransformer1.Controls.Add(this.btnMinimize);
            this.alphaFormTransformer1.Controls.Add(this.btnClose);
            this.alphaFormTransformer1.Controls.Add(this.mTotalProgress);
            this.alphaFormTransformer1.Controls.Add(this.pbFile);
            this.alphaFormTransformer1.Controls.Add(this.btnCancel);
            this.alphaFormTransformer1.Controls.Add(this.btnPlay);
            this.alphaFormTransformer1.Controls.Add(this.lblFile);
            this.alphaFormTransformer1.Controls.Add(this.lblTotal);
            this.alphaFormTransformer1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.alphaFormTransformer1.Size = new System.Drawing.Size(599, 507);
            // 
            // btnPlay
            // 
            this.btnPlay.Enabled = false;
            this.btnPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlay.LayoutName = "playbutton";
            this.btnPlay.Location = new System.Drawing.Point(332, 391);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.ResourceName = "bigbutton";
            this.btnPlay.Size = new System.Drawing.Size(104, 23);
            this.btnPlay.TabIndex = 0;
            this.btnPlay.Text = "[STR_PLAY]";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.LayoutName = "cancelbutton";
            this.btnCancel.Location = new System.Drawing.Point(442, 391);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.ResourceName = "bigbutton";
            this.btnCancel.Size = new System.Drawing.Size(119, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "[STR_CANCEL]";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pbFile
            // 
            this.pbFile.LayoutName = "fileprogress";
            this.pbFile.Location = new System.Drawing.Point(104, 304);
            this.pbFile.Name = "pbFile";
            this.pbFile.ResourceName = "progressbar";
            this.pbFile.Size = new System.Drawing.Size(100, 23);
            this.pbFile.TabIndex = 3;
            this.pbFile.Value = 0F;
            // 
            // mTotalProgress
            // 
            this.mTotalProgress.LayoutName = "totalprogress";
            this.mTotalProgress.Location = new System.Drawing.Point(73, 230);
            this.mTotalProgress.Name = "mTotalProgress";
            this.mTotalProgress.ResourceName = "progressbar";
            this.mTotalProgress.Size = new System.Drawing.Size(100, 23);
            this.mTotalProgress.TabIndex = 4;
            this.mTotalProgress.Value = 0F;
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblFile.LayoutName = "filetext";
            this.lblFile.Location = new System.Drawing.Point(222, 240);
            this.lblFile.Margin = new System.Windows.Forms.Padding(0);
            this.lblFile.Name = "lblFile";
            this.lblFile.PadLeft = 0;
            this.lblFile.PadTop = 0;
            this.lblFile.ResourceName = null;
            this.lblFile.Size = new System.Drawing.Size(111, 13);
            this.lblFile.TabIndex = 5;
            this.lblFile.Text = "dev.myp [3MB/11MB]";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblTotal.LayoutName = "totaltext";
            this.lblTotal.Location = new System.Drawing.Point(218, 293);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.PadLeft = 0;
            this.lblTotal.PadTop = 0;
            this.lblTotal.ResourceName = null;
            this.lblTotal.Size = new System.Drawing.Size(127, 13);
            this.lblTotal.TabIndex = 6;
            this.lblTotal.Text = "Patching files... [3/11MB]";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.LayoutName = "closebutton";
            this.btnClose.Location = new System.Drawing.Point(457, 86);
            this.btnClose.Name = "btnClose";
            this.btnClose.ResourceName = "closebutton";
            this.btnClose.Size = new System.Drawing.Size(104, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.LayoutName = "minimizebutton";
            this.btnMinimize.Location = new System.Drawing.Point(347, 86);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.ResourceName = "minimizebutton";
            this.btnMinimize.Size = new System.Drawing.Size(104, 23);
            this.btnMinimize.TabIndex = 9;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.LayoutName = "titletext_left";
            this.lblTitle.Location = new System.Drawing.Point(27, 32);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.PadLeft = 0;
            this.lblTitle.PadTop = 0;
            this.lblTitle.ResourceName = null;
            this.lblTitle.Size = new System.Drawing.Size(221, 16);
            this.lblTitle.TabIndex = 11;
            this.lblTitle.Text = "-";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mLabel2
            // 
            this.mLabel2.AutoSize = true;
            this.mLabel2.ForeColor = System.Drawing.Color.White;
            this.mLabel2.LayoutName = "";
            this.mLabel2.Location = new System.Drawing.Point(472, 224);
            this.mLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel2.Name = "mLabel2";
            this.mLabel2.PadLeft = 0;
            this.mLabel2.PadTop = 0;
            this.mLabel2.ResourceName = null;
            this.mLabel2.Size = new System.Drawing.Size(43, 13);
            this.mLabel2.TabIndex = 12;
            this.mLabel2.Text = "Status: ";
            this.mLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.ForeColor = System.Drawing.Color.Lime;
            this.lblStatus.LayoutName = "";
            this.lblStatus.Location = new System.Drawing.Point(548, 224);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.PadLeft = 0;
            this.lblStatus.PadTop = 0;
            this.lblStatus.ResourceName = null;
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 13;
            this.lblStatus.Text = "Online";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mLabel4
            // 
            this.mLabel4.AutoSize = true;
            this.mLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.mLabel4.LayoutName = "";
            this.mLabel4.Location = new System.Drawing.Point(548, 240);
            this.mLabel4.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel4.Name = "mLabel4";
            this.mLabel4.PadLeft = 0;
            this.mLabel4.PadTop = 0;
            this.mLabel4.ResourceName = null;
            this.mLabel4.Size = new System.Drawing.Size(13, 13);
            this.mLabel4.TabIndex = 15;
            this.mLabel4.Text = "0";
            this.mLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mLabel5
            // 
            this.mLabel5.AutoSize = true;
            this.mLabel5.ForeColor = System.Drawing.Color.White;
            this.mLabel5.LayoutName = "";
            this.mLabel5.Location = new System.Drawing.Point(472, 240);
            this.mLabel5.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel5.Name = "mLabel5";
            this.mLabel5.PadLeft = 0;
            this.mLabel5.PadTop = 0;
            this.mLabel5.ResourceName = null;
            this.mLabel5.Size = new System.Drawing.Size(44, 13);
            this.mLabel5.TabIndex = 14;
            this.mLabel5.Text = "Players:";
            this.mLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            // 
            // patchNotes
            // 
            this.patchNotes.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patchNotes.LayoutName = "browserwindow";
            this.patchNotes.Location = new System.Drawing.Point(112, 405);
            this.patchNotes.MaxLength = 32767;
            this.patchNotes.Multiline = false;
            this.patchNotes.Name = "patchNotes";
            this.patchNotes.ReadOnly = false;
            this.patchNotes.ResourceName = "browserwindow";
            this.patchNotes.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Both;
            this.patchNotes.SelectionLength = 0;
            this.patchNotes.SelectionStart = 0;
            this.patchNotes.Size = new System.Drawing.Size(200, 100);
            this.patchNotes.TabIndex = 16;
            this.patchNotes.Text = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Lucida Cons" +
    "ole;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this.patchNotes.WordWrap = true;
            // 
            // frmPatcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(599, 507);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPatcher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "War Patcher";
            this.Load += new System.EventHandler(this.frmPatcher_Load);
            this.alphaFormTransformer1.ResumeLayout(false);
            this.alphaFormTransformer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MButton btnPlay;
        private MButton btnCancel;
        private MProgressBar pbFile;
        private MProgressBar mTotalProgress;
        private MLabel lblFile;
        private MLabel lblTotal;
        private MButton btnClose;
        private MButton btnMinimize;
        private MLabel lblTitle;
        private System.Windows.Forms.Timer timer1;
        private MLabel mLabel2;
        private MLabel lblStatus;
        private MLabel mLabel4;
        private MLabel mLabel5;
        private System.Windows.Forms.Timer timer2;
        private RTextBox patchNotes;
    }
}