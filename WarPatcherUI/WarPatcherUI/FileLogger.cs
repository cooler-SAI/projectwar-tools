﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace WarPatcherUI
{
    public enum LogType
    {
        INFO = 1,
        IMPORTANT = 2,
        WARN = 4,
        ERROR = 8,
    }

    public class LogEntry
    {
        public DateTime Timestamp { get; set; }
        public string Msg { get; set; }
        public LogType Type { get; set; }
        public string Context { get; set; }
    }

    public interface ILog
    {
        void Info(string msg);
        void Error(string msg);
        void Exception(string msg, Exception e);
        void Warning(string msg);
        void Shutdown();
    }

    public class FileLogger : ILog
    {
        private List<LogEntry> _entries = new List<LogEntry>();
        private Thread _thread = null;
        private string _filename;
        private CancellationTokenSource _token;

        private Dictionary<LogType, (ConsoleColor, ConsoleColor)> _colors = new Dictionary<LogType, (ConsoleColor, ConsoleColor)>()
        {
            {LogType.INFO, (ConsoleColor.Gray, ConsoleColor.Black) },
            {LogType.IMPORTANT, (ConsoleColor.White, ConsoleColor.Black) },
            {LogType.WARN, (ConsoleColor.Yellow, ConsoleColor.Black) },
            {LogType.ERROR, (ConsoleColor.Yellow, ConsoleColor.Red) },
        };

        public FileLogger(CancellationTokenSource token, string file)
        {
            _token = token;
            _filename = file;
            _thread = new Thread(Save);
            _thread.IsBackground = true;
            _thread.Start();
        }


        public void Info(string msg)
        {
            Append(new LogEntry()
            {
                Type = LogType.INFO,
                Timestamp = DateTime.Now,
                Msg = msg,
            });
        }

        public void Important(string msg)
        {
            Append(new LogEntry()
            {
                Type = LogType.IMPORTANT,
                Timestamp = DateTime.Now,
                Msg = msg,
            });
        }

        public void Error(string msg)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();
#endif

            Append(new LogEntry()
            {
                Type = LogType.ERROR,
                Timestamp = DateTime.Now,
                Msg = msg,
            });
        }


        public void Exception(string msg, Exception e)
        {
            Error(msg + "/n" + e.ToString());
        }

        public void Warning(string msg)
        {
            Append(new LogEntry()
            {
                Type = LogType.WARN,
                Timestamp = DateTime.Now,
                Msg = msg
            });
        }

        private void Append(LogEntry entry)
        {
            lock (_entries)
            {
                string msg = entry.Timestamp.ToString("mm:ss:fff") + "[" + entry.Type + "]: " + entry.Msg;
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debug.WriteLine(msg);

                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write(entry.Timestamp.ToString("mm:ss:fff") + "[");
                Console.ForegroundColor = _colors[entry.Type].Item1;
                Console.BackgroundColor = _colors[entry.Type].Item2;
                Console.Write(entry.Type.ToString());
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write("]: ");
                Console.ForegroundColor = _colors[entry.Type].Item1;
                Console.BackgroundColor = _colors[entry.Type].Item2;
                Console.WriteLine(entry.Msg);

                _entries.Add(entry);
            }
        }

        private void Save()
        {
            while (!_token.IsCancellationRequested)
            {
                Flush();
                Thread.Sleep(10);
            }
        }

        public void Shutdown()
        {
            Info("Shutdown");
            Flush();
        }

        public void Flush()
        {
            if (_entries.Count > 0)
            {
                List<LogEntry> entries = _entries;
                lock (_entries)
                {
                    _entries = new List<LogEntry>();
                }

                using (var fs = new FileStream(_filename, FileMode.Append, FileAccess.Write))
                {
                    using (var writer = new StreamWriter(fs))
                    {
                        foreach (var entry in entries)
                        {
                            string msg = entry.Timestamp.ToString("mm:ss:fff") + " [" + entry.Type + "]: " + entry.Msg;
                            writer.WriteLine(msg);
                        }
                    }
                }
            }
        }
    }

    public static class Log
    {
        public static ILog Instance;

        public static void Error(string msg)
        {
            Instance.Error(msg);
        }

        public static void Exception(string msg, Exception e)
        {
            Instance.Error(msg + "/n" + e.ToString());
        }

        public static void Info(string msg)
        {
            Instance.Info(msg);
        }

        public static void Warning(string msg)
        {
            Instance.Warning(msg);
        }

        public static void Shutdown()
        {
            Instance.Shutdown();
        }
    }
}
