﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WarData;
using WarShared;
using WarShared.Data;
using WarShared.Protocol.Patcher;

namespace WarPatcherServer
{
    public class PatcherServer : HttpServerBase, ITimerOwner
    {
        #region classes
        private class ArchiveInfo
        {
            public uint CRC { get; set; }
            public Dictionary<long, PatcherAsset> Assets { get; set; } = new Dictionary<long, PatcherAsset>();
        }
        #endregion

        #region members
        private DataProvider Data;
        private REQUEST_FILE_MANIFEST _manifest = new REQUEST_FILE_MANIFEST();
        private EnvironmentData _sharedEnv;
        private Dictionary<long, PatcherFile> _files = new Dictionary<long, PatcherFile>();
        private Dictionary<long, EnvironmentData> _envs = new Dictionary<long, EnvironmentData>();
        private Dictionary<MythicPackage, byte[]> _archiveManifest = new Dictionary<MythicPackage, byte[]>();
        private Dictionary<MythicPackage, MYP> _myps = new Dictionary<MythicPackage, MYP>();
        private Dictionary<MythicPackage, ArchiveInfo> _assets = new Dictionary<MythicPackage, ArchiveInfo>();
        private ServerInfoData _serverInfo;
        private SemaphoreSlim _lock = new SemaphoreSlim(1);

        #endregion

        public PatcherServer(DataProvider data, ServerInfoData info, CancellationTokenSource tokenSource) : base(tokenSource, "Patcher", Program.Config.ServerID)
        {
            Data = data;
            _serverInfo = info;
        }

        #region methods
        protected override Task Start()
        {
            return Initialize();
        }

        private async Task Initialize()
        {
            Log.Info("Assets load begin");
            await _lock.WaitAsync();
            try
            {
                _archiveManifest.Clear();
                _assets.Clear();

                _serverInfo = Data.DB.Query<ServerInfoData>($"select * from ServerInfo where ID={_serverInfo.ID}").FirstOrDefault();
                _envs = (await Data.DB.QueryAsync<EnvironmentData>($"SELECT * FROM Environment")).ToDictionary(e => e.ID, e => e);
                _sharedEnv = _envs.Values.Where(e => e.Name == "Shared").FirstOrDefault();

                Validate.NotNull(_envs, nameof(_envs));


                var sharedFiles = (await Data.DB.QueryAsync<PatcherFile>($"SELECT * FROM PatcherFile Where  EnvironmentID={_sharedEnv.ID}")).ToDictionary(e => e.ID, e => e);
                _files  = (await Data.DB.QueryAsync<PatcherFile>($"SELECT * FROM PatcherFile Where EnvironmentID={_serverInfo.EnvironmentID}")).ToDictionary(e => e.ID, e => e);
                var assets = await Data.DB.QueryAsync<PatcherAsset>($"SELECT * FROM PatcherAsset Where EnvironmentID={_serverInfo.EnvironmentID} or EnvironmentID={_sharedEnv.ID} order by PatcherFileID, Hash");

                //replace shared files with environment specific ones
                foreach (var file in sharedFiles.Values)
                {
                    if (!_files.Values.Any(e => e.Filename == file.Filename))
                    {
                        _files[file.ID] = file;
                    }
                }
                _manifest = new REQUEST_FILE_MANIFEST();

                foreach (var asset in assets)
                {
                    if (!_assets.ContainsKey(asset.Package))
                        _assets[asset.Package] = new ArchiveInfo();

                    _assets[asset.Package].Assets[asset.Hash] = asset;
                    _assets[asset.Package].CRC = Util.Adler32(_assets[asset.Package].CRC, asset.CRC32);
                    _assets[asset.Package].CRC = Util.Adler32(_assets[asset.Package].CRC, (uint)asset.Size);

                }

                foreach (var archive in _assets.Keys)
                {
                    MemoryStream ms = new MemoryStream();
                    BinaryWriter writer = new BinaryWriter(ms);
                    writer.Write((int)_assets[archive].Assets.Count);
                    foreach (var asset in _assets[archive].Assets.Values)
                    {
                        writer.Write((long)asset.Hash);
                        writer.Write((uint)asset.CRC32);
                        writer.Write((uint)asset.CompressedSize);
                        writer.Write((uint)asset.Size);
                        writer.Write((byte)(asset.Compressed ? 1 : 0));
                    }
                    _archiveManifest[archive] = Util.Compress(ms.ToArray());

                }
                foreach (var file in _files.Values)
                {

                    var env = _envs[file.EnvironmentID];
                    Validate.NotNull(env, nameof(env));

                    string guidName = file.Guid.ToString().Replace("-", "");
                    string path = Path.Combine(Path.Combine(Program.Config.Files, env.Name), guidName);

                    if (file.PatcherFileID.HasValue)
                    {
                        _files[file.PatcherFileID.Value].Files.Add(file);
                        file.Parent = _files[file.PatcherFileID.Value];
                    }


                    if (file.Size > 0)
                    {
                        uint crc = 0;
                        if (!file.Filename.ToUpper().EndsWith(".MYP"))
                            crc = Util.Adler32(0, File.ReadAllBytes(path));
                        else
                        {
                            MythicPackage p = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(file.Filename).ToUpper());
                            if (_assets.ContainsKey(p))
                            {
                                crc = _assets[p].CRC;
                                _myps[p] = new MYP(Log, p);
                                _myps[p].LoadFromFile(path);
                            }
                        }

                        
                        _manifest.Items.Add(new ManifestFileItem()
                        {
                            ID = file.ID,
                            Size = new FileInfo(path).Length,
                            CRC = crc,
                            Name = file.Path,
                            Required = file.Required
                        });
                    }
                }


            }
            finally
            {
                _lock.Release();
            }
            Log.Info("Assets loaded");
        }

        [HttpRoute("LOGIN")]
        public Task LOGIN(HttpContext context,[FromJsonBody]LOGIN login)
        {
            string username = login.Username.Replace("'", "''").Trim().ToLower();
            if (username.Length < 4)
                return ResponseJson(context, new LOGIN_RESPONSE() { Status = LoginStatus.INVALID_USER });

            //TODO: move to data server
            var user = Data.DB.Query<AccountData>($"select * from account where Username='{username}'").FirstOrDefault();
            if (user == null)
                return ResponseJson(context, new LOGIN_RESPONSE() { Status = LoginStatus.INVALID_USER });

            if(!BCrypt.Net.BCrypt.Verify(login.Password, user.PasswordHash))
                return ResponseJson(context, new LOGIN_RESPONSE() { Status = LoginStatus.INVALID_LOGIN });

            user.LoginToken = Guid.NewGuid();
            Data.DB.Update(user);

            return ResponseJson(context, new LOGIN_RESPONSE() { Status = LoginStatus.OK, LoginToken = user.LoginToken });
        }

        [HttpRoute("LOGIN_CREATE")]
        public Task LOGIN_CREATE(HttpContext context, [FromJsonBody]LOGIN_CREATE login)
        {
            string username = login.Username.Replace("'", "''").Trim().ToLower();
            if(username.Length < 4)
                return ResponseJson(context, new LOGIN_RESPONSE() { Status = LoginStatus.INVALID_USER });

            var user = Data.DB.Query<AccountData>($"select * from account where Username='{username}'").FirstOrDefault();
            if(user != null)
                return ResponseJson(context, new LOGIN_RESPONSE() { Status = LoginStatus.USERNAME_ALREADY_EXISTS });

            Data.DB.Insert(new AccountData()
            {
                Username = username,
                Email = login.Email,
                PasswordHash = BCrypt.Net.BCrypt.HashPassword(login.Password)
            });

            return ResponseJson(context, new LOGIN_RESPONSE() { Status = LoginStatus.OK });
        }

        [HttpRoute("REQUEST_FILE_MANIFEST")]
        public Task REQUEST_MANIFEST(HttpContext context)
        {
            return ResponseJson(context, _manifest);
        }

        [HttpRoute("SERVER_STATUS")]
        public Task SERVER_STATUS(HttpContext context)
        {
            return ResponseJson(context, new SERVER_STATUS() { Status = ServerStatus.Online, MOTD=_serverInfo.MOTD });
        }

        [HttpRoute("REQUEST_ARCHIVE_MANIFEST")]
        public Task REQUEST_ARCHIVE_MANIFEST(HttpContext context, [FromJsonBody]REQUEST_ARCHIVE_MANIFEST manifest)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryWriter writer = new BinaryWriter(ms);

                writer.Write((byte)manifest.Archives.Count);
                foreach (var p in manifest.Archives)
                {
                    writer.Write((byte)p);

                    if (_archiveManifest.ContainsKey(p))
                    {
                        writer.Write((int)_archiveManifest[p].Length);
                        ms.Write(_archiveManifest[p], 0, _archiveManifest[p].Length);
                    }
                    else
                    {
                        writer.Write((int)0);
                    }
                }
                ms.Position = 0;
                context.Response.ContentLength = ms.Length;
                ms.CopyTo(context.Response.Body);
            }
            return Task.CompletedTask;
        }


        [HttpRoute("REQUEST_FILE")]
        public Task REQUEST_FILE(HttpContext context, [FromJsonBody]REQUEST_FILE rfile)
        {
            if (_files.ContainsKey(rfile.ID))
            {
                var file = _files[rfile.ID];
                var env = _envs[file.EnvironmentID];
                Validate.NotNull(env, nameof(env));

                string guidName = file.Guid.ToString().Replace("-", "");
                string path = Path.Combine(Path.Combine(Program.Config.Files, env.Name), guidName);

                Validate.IsTrue(File.Exists(path), $"File does not exist {path}");
                using (var fs = File.OpenRead(path))
                {
                    context.Response.ContentLength = fs.Length;
                    fs.CopyTo(context.Response.Body);
                }
            }
            return Task.CompletedTask;
        }

        [HttpRoute("RELOAD_ASSETS")]
        public Task RELOAD_ASSETS(HttpContext context)
        {
            return Initialize();
        }

        [HttpRoute("REQUEST_ASSET")]
        public Task REQUEST_ASSET(HttpContext context, [FromJsonBody]REQUEST_ASSET aaset)
        {

            if (_myps.ContainsKey(aaset.Archive) && _myps[aaset.Archive].Assets.ContainsKey((long)aaset.Hash))
            {
                var data = _myps[aaset.Archive].GetAssetDataRaw((long)aaset.Hash);

                Validate.IsTrue(data.Length == _myps[aaset.Archive].Assets[aaset.Hash].CompressedSize, nameof(data.Length));

                context.Response.ContentLength = data.Length;
                var ms = new MemoryStream(data);
                ms.WriteTo(context.Response.Body);
            }
            return Task.CompletedTask;
        }

        #endregion
    }
}
