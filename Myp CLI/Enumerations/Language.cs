﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    public enum Language : Int32
    {
        English = 1,
        French = 2,
        German = 3,
        Italian = 4,
        Spanish = 5,
        Korean = 6,
        S_Chinese = 7,
        T_Chinese = 8,
        Japanese = 9,
        Russian = 10
    }
}