﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FileTableHeader
    {
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 FileCount { get; set; }
        [field: MarshalAs(UnmanagedType.U8)] public UInt64 NextFileOffset { get; set; }
    }
}