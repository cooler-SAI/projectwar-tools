﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FileTableEntry
    {
        [field: MarshalAs(UnmanagedType.U8)] public UInt64 Offset { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 HeaderSize { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 CompressedSize { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 DecompressedSize { get; set; }
        [field: MarshalAs(UnmanagedType.I8)] public Int64 Hash { get; set; }
        [field: MarshalAs(UnmanagedType.U4)] public UInt32 CRC32 { get; set; }
        [field: MarshalAs(UnmanagedType.U1)] public Byte IsCompressed { get; set; }
        [field: MarshalAs(UnmanagedType.U1)] public Byte Unk1 { get; set; }
    }
}