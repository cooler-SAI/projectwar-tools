﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    public class CSV
    {
        public Dictionary<String, List<String>> _keyedRows = new Dictionary<String, List<String>>();
        public List<List<String>> Lines = new List<List<String>>();

        private Int32 _rowindex;
        private String _commentsection = null;

        public Int32 RowIndex { get => _rowindex; set => _rowindex = value; }
        public String Commentsection { get => _commentsection; set => _commentsection = value; }

        public List<String> Row => ReadRow();
        public Boolean EOF => RowIndex == Lines.Count - 1;

        public CSV() => RowIndex = 0;

        public String GetKeyedValue(Object rowID, Int32 colIndex, String defaultValue = "")
        {
            String key = rowID.ToString();
            if(_keyedRows.ContainsKey(key) && colIndex < _keyedRows[key].Count)
                return _keyedRows[key][colIndex];
            return defaultValue;
        }
        public static List<String> QuotedCSVToList(String line, Char quote = '"', Char splitter = ',')
        {
            var list = new List<String>();

            Char[] chars = line.ToCharArray();
            Boolean inQuote = false;
            String current = "";
            for(Int32 i = 0; i < chars.Length; i++)
            {
                if(chars[i] == quote && !inQuote)
                {

                    inQuote = true;
                    //    continue;
                } else if(chars[i] == quote && inQuote)
                {
                    inQuote = false;
                    // continue;
                } else if(chars[i] == splitter && !inQuote)
                {
                    list.Add(current);
                    current = "";
                } else if(inQuote)
                {
                    current += chars[i];
                } else
                {
                    current += chars[i];
                }

            }
            if(current.Length > 0)
                list.Add(current);

            return list;
        }


        public CSV(String text, String commentSection = null, Boolean quoted = false, Int32 idCol = -1)
        {
            Commentsection = commentSection;
            String comment = "";
            foreach(String row in text.Split(new Char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList())
            {
                if(Commentsection != null && row.StartsWith(Commentsection))
                {
                    comment = row.Replace(",", "").Replace(";", "");

                }


                if(row.StartsWith(";"))
                    continue;

                List<String> cols = new List<String>();
                if(quoted)
                {
                    cols = QuotedCSVToList(row);
                } else
                {
                    foreach(String col in row.Split(new Char[] { ',' }))
                    {
                        cols.Add(col);
                    }
                }

                if(Commentsection != null && comment != "")
                    cols.Add(comment);
                Lines.Add(cols);
                if(idCol > -1 && idCol < cols.Count)
                    _keyedRows[cols[idCol]] = cols;
            }
            RowIndex = 0;
        }


        public Dictionary<Int32, Int32> ToDictionary(Int32 IDCol, Int32 valueCol)
        {
            var results = new Dictionary<Int32, Int32>();
            while(!EOF)
            {
                List<String> row = ReadRow();
                results[ReadInt32(IDCol)] = ReadInt32(valueCol);
                NextRow();
            }
            return results;
        }

        public void MoveToEnd()
        {
            RowIndex = Lines.Count - 1;
        }

        public void NewRow()
        {
            Lines.Insert(RowIndex + 1, new List<String>());
        }

        public void RemoveRow()
        {
            Lines.RemoveAt(RowIndex);
        }
        public void Remove(List<Int32> rows)
        {
            List<List<String>> lines = new List<List<String>>();
            for(Int32 i = 0; i < Lines.Count; i++)
            {
                if(!rows.Contains(i))
                    lines.Add(Lines[i]);
            }
            Lines = lines;
            if(RowIndex > Lines.Count - 1)
                RowIndex = lines.Count - 1;
        }

        public void WriteCol(Int32 col, String value)
        {
            List<String> line = Lines[RowIndex];
            if(line.Count - 1 < col)
            {
                Int32 count = col - line.Count;
                for(Int32 i = 0; i <= count; i++)
                    line.Add("");
            }
            line[col] = value;
        }


        public List<String> ReadRow()
        {
            return Lines[RowIndex];
        }

        public void NextRow()
        {
            RowIndex++;
        }

        public Int32 ReadInt32(Int32 col, Int32 defaultValue = 0)
        {
            Int32 result = 0;
            if(col < Lines[RowIndex].Count && Int32.TryParse(Lines[RowIndex][col], out result))
            {
                return result;
            }
            return defaultValue;
        }

        public Int32 PeakInt32(Int32 row, Int32 col, Int32 defaultValue = -1)
        {
            if(row < Lines.Count)
            {
                Int32 result = 0;
                Int32.TryParse(Lines[row][col], out result);
                return result;
            }
            return defaultValue;
        }
        public Single ReadFloat(Int32 col)
        {
            Single result = 0;

            if(col < Lines[RowIndex].Count && Single.TryParse(Lines[RowIndex][col], out result))
                return result;
            return 0;
        }

        public String ReadString(Int32 col)
        {
            return Lines[RowIndex][col];
        }

        public String ToText()
        {
            StringBuilder builder = new StringBuilder();
            foreach(List<String> row in Lines)
            {
                for(Int32 i = 0; i < row.Count; i++)
                {
                    if(i < row.Count - 1)
                        builder.Append(row[i] + ",");
                    else
                        builder.Append(row[i]);
                }
                builder.Append("\n");
            }
            return builder.ToString();
        }

    }
}
