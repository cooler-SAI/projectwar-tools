﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    internal class Program
    {
        private static AssemblyFileVersionAttribute version;
        private static AssemblyTitleAttribute title;
        private static String exe;

        public static Dictionary<Int64, String> AssetHash2Path = new Dictionary<Int64, String>();
        public static Dictionary<String, Int64> AssetPath2Hash = new Dictionary<String, Int64>();

        [STAThread]
        private static void Main(String[] args)
        {
            version = Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyFileVersionAttribute>();
            title = Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyTitleAttribute>();
            exe = System.AppDomain.CurrentDomain.FriendlyName;

            if(File.Exists("hashes_slim.tab")) {
                Byte[] hashes_slim = File.ReadAllBytes("hashes_slim.tab");
                Byte[] hashes_slim_dat = Compress(hashes_slim);
                File.WriteAllBytes("hashes_slim.dat", hashes_slim_dat);
                Console.WriteLine("Constructed hashes_slim.dat");
            }

            Console.WriteLine($"{title.Title} {version.Version}");

            String cmd = (args == null || args.Length == 0 || args[0] == null) ? "" : args[0].ToUpper();

            LoadAssetHashes();

            switch(cmd)
            {
                case "-e":
                    goto case "extract";
                case "extract":
                    Extract(args);
                    break;
                case "-pd":
                    goto case "pack-directory";
                case "packfolder":
                    goto case "pack-directory";
                case "pack-directory":
                    PackFolder(args);
                    break;
                case "packfile":
                    goto case "pack-file";
                case "-pf":
                    goto case "pack-file";
                case "pack-file":
                    PackFile(args);
                    break;
                case "edit":
                    goto case "edit-csv";
                case "-ecsv":
                    goto case "edit-csv";
                case "edit-csv":
                    EditCSVFile(args);
                    break;
                case "-h":
                    goto case "help";
                case "help":
                    Help();
                    break;
                default:
                    Help();
                    break;
            }
        }

        private static void EDIT(String[] args)
        {
            if(args.Length < 3)
            {
                Help();
                return;
            }
            if(!File.Exists(args[1]))
            {
                Console.WriteLine("Myp not found");
                return;
            }

            /*PackageType p = (PackageType)Enum.Parse(typeof(PackageType), Path.GetFileNameWithoutExtension(args[1].ToUpper()));*/

            using(Myp myp = new Myp(args[1], true))
            {
                String assetName = args[2].ToLower().Replace("\\", "/").ToLower();
                Byte[] asset = myp.GetAssetData(Myp.HashWAR(assetName));
                if(asset == null)
                {
                    Console.WriteLine("Asset not found");
                    return;
                }
                frmCSVViewer viewer = new frmCSVViewer();
                viewer.LoadAsset(myp, assetName, asset);
                viewer.ShowDialog();
                if(viewer.Changed)
                {
                    myp.Save();
                }
            }
        }

        private static void Extract(String[] args)
        {
            if(args.Length < 3)
            {
                Console.WriteLine("Usage: EXTRACT [myps folder/myp file] [dest folder/asset path]");
                return;
            }

            if(Directory.Exists(args[1]))
            {
                Console.WriteLine($"Extracting all .myp to {args[1]}");
                foreach(String mypName in Enum.GetNames(typeof(PackageType)))
                {
                    String name = Path.Combine(args[1], mypName + ".myp");
                    PackageType p = (PackageType)Enum.Parse(typeof(PackageType), mypName);
                    ExtractMyp(p, name, args[2]);
                }
            } else if(File.Exists(args[1]))
            {
                String name = args[1].ToUpper();
                PackageType p = (PackageType)Enum.Parse(typeof(PackageType), Path.GetFileNameWithoutExtension(name));
                ExtractMyp(p, name, args[2]);
            } else
            {
                Console.WriteLine($"Nothing to extract {args[1]}");
            }
        }

        private static void PackFolder(String[] args)
        {
            if(args.Length < 2)
            {
                Help();
                return;
            }

            if(!Directory.Exists(args[1]))
            {
                Console.WriteLine($"Myps folder not found {args[1]}");
                return;
            }

            String mypName = Path.GetFileName(args[2]);

            PackageType p = PackageType.NONE;
            if(Enum.TryParse<PackageType>(mypName, out p))
            {
                Console.WriteLine($"Destination myp name is limited to existing names");
                return;
            }

            if(!File.Exists(args[2]))
            {
                Console.WriteLine($"Creating myp ({mypName})");
                using(Myp myp = new Myp(p, args[2], true))
                { }
            }

            using(Myp myp = new Myp(p, args[2], true))
            {
                foreach(String file in Directory.GetFiles(args[1]))
                {
                    String assetName = file.Replace(args[1], "").Replace("\\", "/").Substring(1).ToLower();
                    Console.WriteLine("Packing " + Path.GetFileName(file));
                    myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }

                foreach(String folder in Directory.GetDirectories(args[1]))
                {
                    PackFolder(args[1], folder, myp);
                }
                Console.WriteLine("Writing " + args[2]);
                myp.Save();
                Console.WriteLine("Done");
            }
        }

        private static void PACKFILE(String[] args)
        {
            if(args.Length < 3)
            {
                Help();
                return;
            }

            if(!File.Exists(args[1]))
            {
                Console.WriteLine($"Asset to pack not found {args[1]}");
                return;
            }

            String mypName = Path.GetFileName(args[3]);

            PackageType p = PackageType.None;
            if(Enum.TryParse<PackageType>(mypName, out p))
            {
                Console.WriteLine($"Destination myp name is limited to existing Myp archive names");
                return;
            }

            if(!File.Exists(args[3]))
            {
                Console.WriteLine($"Creating myp ({mypName})");
                using(Myp myp = new Myp(p, args[3], true))
                { }
            }

            using(Myp myp = new Myp(p, args[3], true))
            {

                String assetName = args[2].Replace("\\", "/").ToLower();
                Console.WriteLine("Packing [" + assetName + "]");
                myp.UpdateAsset(assetName, File.ReadAllBytes(args[1]), CanCompress(args[1]));

                Console.WriteLine("Writing " + args[2]);
                myp.Save();
                Console.WriteLine("Done");
            }
        }

        private static void PackFolder(String root, String currentFolder, Myp myp)
        {
            foreach(String folder in Directory.GetDirectories(currentFolder))
            {
                PackFolder(root, folder, myp);
            }

            foreach(String file in Directory.GetFiles(currentFolder))
            {
                Console.WriteLine("Packing " + Path.GetFileName(file));
                String assetName = file.Replace(root, "").Replace("\\", "/").Substring(1).ToLower();

                if(assetName.Contains("no_dehash"))
                {
                    Int64 hash = (Int64)UInt64.Parse(Path.GetFileNameWithoutExtension(file));
                    myp.UpdateAsset(hash, File.ReadAllBytes(file), false);
                } else
                {
                    myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }
            }
        }

        private static Boolean CanCompress(String filename, Boolean defaultValue = true)
        {
            String ext = Path.GetExtension(filename).ToLower().Replace(".", "");

            if(ext == "bik")
            {
                return false;
            }

            if(ext == "bin")
            {
                return true;
            }

            if(ext == "dds")
            {
                return false;
            }

            if(ext == "h")
            {
                return false;
            }

            if(ext == "inc")
            {
                return false;
            }

            if(ext == "lmp")
            {
                return false;
            }

            if(ext == "mp3")
            {
                return false;
            }

            if(ext == "psh")
            {
                return false;
            }

            if(ext == "stx")
            {
                return false;
            }

            if(ext == "txt")
            {
                return true;
            }

            if(ext == "vsh")
            {
                return false;
            }

            if(ext == "wav")
            {
                return true;
            }

            if(ext == "csv")
            {
                return true;
            }

            return defaultValue;
        }

        static void ExtractMyp(String name, String dest)
        {
            if(File.Exists(name))
            {
                try
                {
                    using(var myp = new Myp(p, name, false))
                    {
                        Console.WriteLine($"Processing {name} containing {myp.Assets.Count} assets.");

                        Int32 count = 0;

                        Task.Run(() =>
                        {
                            while(true)
                            {
                                unchecked
                                {
                                    Single delta_progress = 100.0f / myp.Assets.Count;
                                    Int32 percent = (Int32)(delta_progress * count);

                                    Console.Title = $"WAaaAGHhhH TOolz WOT iZ 4 MeKAniks =>< {count}/{myp.Assets.Count} assets {percent}% extracted from {name.ToLower()}";
                                }

                                if(count == myp.Assets.Count)
                                    break;

                                Thread.Yield();
                                Thread.Sleep(1000);
                            }
                        });

                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        Parallel.ForEach(myp.Assets, (asset) =>
                        {
                            Int64 hash = asset.Key;
                            Byte[] data = myp.GetAssetData(hash);
                            String path = String.Empty;

                            if(AssetHash2Path.ContainsKey(hash))
                            {
                                path = Path.Combine(dest, AssetHash2Path[hash].ToLower().Replace("/", "\\").Replace(".stx", ".stx.dds"));
                                String folder = Path.GetDirectoryName(path);

                                if(!Directory.Exists(folder))
                                    Directory.CreateDirectory(folder);

                            } else {
                                String type = myp.GetExtension(data);

                                String folder = Path.Combine(dest, "unknown_hashes", type);

                                if(!Directory.Exists(folder))
                                    Directory.CreateDirectory(folder);

                                UInt64 uhash = (UInt64)hash;
                                uhash = (uhash >> 32) | (uhash << 32);                                                  // swap adjacent 32-bit blocks
                                uhash = ((uhash & 0xFFFF0000FFFF0000) >> 16) | ((uhash & 0x0000FFFF0000FFFF) << 16);    // swap adjacent 16-bit blocks
                                uhash = ((uhash & 0xFF00FF00FF00FF00) >> 8) | ((uhash & 0x00FF00FF00FF00FF) << 8);      // swap adjacent  8-bit blocks

                                Byte[] ubytes = BitConverter.GetBytes(uhash);
                                var ustringBuilder = new StringBuilder(ubytes.Length);

                                foreach(Byte byteValue in ubytes)
                                    ustringBuilder.Append(byteValue.ToString("x2"));

                                path = Path.Combine(folder, ustringBuilder.ToString() + "." + type.ToLower());
                            }
                            File.WriteAllBytes(path, data);

                            FileAttributes attributes = File.GetAttributes(path);

                            // Console.WriteLine($"{path} C[{asset.Value.Compressed}]");

                            if(asset.Value.Compressed == 1)
                                attributes |= FileAttributes.Archive;
                            else
                                attributes &= ~FileAttributes.Archive;

                            File.SetAttributes(path, attributes);

                            ++count;

                        });
                        watch.Stop();

                        Double elapsed_seconds = Math.Round(watch.ElapsedMilliseconds / 1000.0f, 2);
                        Console.WriteLine($"Extraction completed in {elapsed_seconds} seconds.");
                    }
                } catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static Byte[] Compress(Byte[] data)
        {
            MemoryStream outB = new MemoryStream();

            using(DeflateStream archive = new DeflateStream(outB, CompressionLevel.Optimal, true))
            {
                archive.Write(data, 0, data.Length);
            }
            outB.Position = 0;
            return outB.ToArray();
        }

        public static Byte[] DeCompress(Byte[] data, Boolean gzip = false)
        {
            MemoryStream outB = new MemoryStream(data);
            if(gzip)
            {
                outB.Position += 2;
            }

            using(DeflateStream archive = new DeflateStream(outB, CompressionMode.Decompress, true))
            {
                MemoryStream dest = new MemoryStream();
                archive.CopyTo(dest);
                return dest.ToArray();
            }
        }

        private static void Help()
        {
            Console.WriteLine($"{title.Title} {version.Version}");
            Console.WriteLine("  ");
            Console.WriteLine($" Usage: {exe.ToLower()} <command> <source> <destination> <asset string or hash> (optional) -verbose");
            Console.WriteLine("  ");
            Console.WriteLine("  <commands>");
            Console.WriteLine("  -e    : Extracts either all .myp files in the <source> directory or extract the .myp <source> file to the <destination> directory.");
            Console.WriteLine("  -pd   : Recursively packs all files below the <source> directory in to the <destination> .myp file creating or modifying as requried.");
            Console.WriteLine("  -pf   : Packs a single <source> file in to the <destination> .myp file optionally providing an <asset string or hash(override)>");
            Console.WriteLine("  -csv  : Unpacks a csv file from the <source> .myp file and a <asset string or hash> to be edited and re-saved.");
            Console.WriteLine("  -h    : Displays this message.");
        }
    }
}
