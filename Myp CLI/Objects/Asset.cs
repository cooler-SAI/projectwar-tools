﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    public class Asset
    {
        public Int64 Hash { get; set; }

        public Int64 Filepath { get; set; }

        public PackageType Package { get; set; }


        public override String ToString() => Hash;
    }
}