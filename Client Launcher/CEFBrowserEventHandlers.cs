﻿using CefSharp;
using CefSharp.Internals;
using CefSharp.WinForms;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Launcher
{
    [DesignTimeVisible(false)]
    public partial class CEFBrowserForm
    {
        #region Event handlers
        protected virtual void BrowserInitializedEvent(object sender, IsBrowserInitializedChangedEventArgs e)
        {
#if DEBUG
            browser.ShowDevTools();
#endif

            var bindingOptions = new BindingOptions()
            {
                Binder = BindingOptions.DefaultBinder.Binder,
                MethodInterceptor = new MethodInterceptorLogger() // intercept .net methods calls from js and log it
            };

            CefSharpSettings.WcfEnabled = true;

            JavascriptObjectRepository.ResolveObject += (sender2, e2) =>
            {
                IJavascriptObjectRepository repo = e2.ObjectRepository;
                if (e2.ObjectName == "boundAsync2")
                {
                    repo.Register("boundAsync2", new AsyncBoundObject(), isAsync: true, options: bindingOptions);
                }
                else if (e2.ObjectName == "bound")
                {
                    JavascriptObjectRepository.Register("bound", new BoundObject(), isAsync: false, options: BindingOptions.DefaultBinder);
                }
                else if (e2.ObjectName == "boundAsync")
                {
                    JavascriptObjectRepository.Register("boundAsync", new AsyncBoundObject(), isAsync: true, options: bindingOptions);
                }
            };


            JavascriptObjectRepository.ObjectBoundInJavascript += (sender2, e2) =>
            {
                var name = e2.ObjectName;

                Debug.WriteLine($"Object {e2.ObjectName} was bound successfully.");
            };


            Debug.WriteLine($"IsBrowserInitialized changed to {e.IsBrowserInitialized}");
        }

        protected virtual void TitleChangedEvent(object sender, TitleChangedEventArgs e)
        {
            Debug.WriteLine($"Title changed to {e.Title}");
        }

        protected virtual void AddressChangedEvent(object sender, AddressChangedEventArgs e)
        {
            Debug.WriteLine($"Address changed to {e.Address}");
        }

        protected virtual void LoadingStateChangedEvent(object sender, LoadingStateChangedEventArgs e)
        {
            Debug.WriteLine("Loading state changed to {0}.", IsLoading = e.IsLoading);
        }

        protected virtual void ConsoleMessageEvent(object sender, ConsoleMessageEventArgs e)
        {
            Debug.WriteLine("Console message recieved : {0} : {1} ({2}).", e.Message, e.Source, e.Line);
        }

        protected virtual void StatusMessageEvent(object sender, StatusMessageEventArgs e)
        {
            Debug.WriteLine("Status changed to {0}.", e.Value);
        }

        protected virtual void FrameLoadStartEvent(object sender, FrameLoadStartEventArgs e)
        {
            if (e.TransitionType == TransitionType.ClientRedirect)
            {
                OnClientRedirect(this, new RedirectEventArgs(e.Url));
            }
            if (e.TransitionType == TransitionType.FormSubmit)
            {
                OnFormSubmit(this, new RedirectEventArgs(e.Url));
            }
            Debug.WriteLine("Frame started loading '{0}' from transition {1}.", e.Url, e.TransitionType.ToString());
        }

        protected virtual void FrameLoadEndEvent(object sender, FrameLoadEndEventArgs e)
        {
            Debug.WriteLine("Frame {0} finished loading with status code {1}.", e.Url, e.HttpStatusCode);
            Task.Factory.StartNew(async () =>
            {
                string html = await this.GetMainFrame().GetSourceAsync();
                OnSourceReady(this, new SourceReadyEventArgs(html));
            }).ConfigureAwait(false);
        }

        protected virtual void LoadErrorEvent(object sender, LoadErrorEventArgs e)
        {
            Debug.WriteLine("Error {0}: {1} while loading '{2}'.", e.ErrorCode, e.ErrorText, e.FailedUrl);
        }

        protected virtual void OnDownloadUpdatedFired(object sender, DownloadItem e)
        {
            if (e != null)
            {
                if (e.IsInProgress)
                {
                    Debug.WriteLine("Download {0} {1}% complete {2}kbs {3}/{4} bytes transfered.", e.Url, e.PercentComplete, e.CurrentSpeed, e.ReceivedBytes, e.TotalBytes);
                }
                if (e.IsComplete)
                {
                    Debug.WriteLine("Download {0} completed at {0}.", e.EndTime);
                }
                if (e.IsCancelled)
                {
                    Debug.WriteLine("Download {0} cancelled.");
                }
            }
            else
            {
                Debug.WriteLine("Download update recieved.");
            }
        }

        protected virtual void OnBeforeDownloadFired(object sender, DownloadItem e)
        {
            if (e != null)
            {
                Debug.WriteLine("Before download {0}.", e.Url);
            }
            else
            {
                Debug.WriteLine("Before download.");
            }
        }

        protected virtual void GetAuthCredentialsEvent(object sender, GetAuthCredentialsEventArgs e)
        {
            Debug.WriteLine("Get credentials.");

            if (!e.Callback.IsDisposed)
            {
                Launcher.Native.GetCredentials(e.Host, out System.Net.NetworkCredential credential);
                e.Callback.Continue(credential.UserName, credential.Password);
                e.ContinueAsync = true;
            }
            e.ContinueAsync = false;
        }

        protected virtual void OnCertificateErrorEvent(object sender, OnCertificateErrorEventArgs e)
        {
            Debug.WriteLine("Certificate error {0} at '{1}'", e.SSLInfo.CertStatus, e.RequestUrl);

            if (!e.Callback.IsDisposed)
            {
                e.Callback.Continue(true);
                e.ContinueAsync = true;
            }
            e.ContinueAsync = false;
        }

        protected virtual void OnRenderViewReadyEvent(object sender, OnRenderViewReadyEventArgs e)
        {
            Debug.WriteLine("Render view ready.");
        }

        protected virtual void OnRenderProcessTerminatedEvent(object sender, OnRenderProcessTerminatedEventArgs e)
        {
            Debug.WriteLine("Render process terminated, Status {0}.", e.Status);
        }

        protected virtual void OnBeforeBrowseEvent(object sender, OnBeforeBrowseEventArgs e)
        {
            Debug.WriteLine("Before browse, Cancel:  {0}, Redirect: {1}.", e.CancelNavigation.ToString(), e.IsRedirect.ToString());
        }

        protected virtual void OnProtocolExecutionEvent(object sender, OnProtocolExecutionEventArgs e)
        {
            Debug.WriteLine("Protocol execution attempted for {0}.", e.AttemptExecution);
            e.AttemptExecution = e.Url.Contains("mailto:");
        }

        protected virtual void OnResourceLoadCompleteEvent(object sender, OnResourceLoadCompleteEventArgs e)
        {
            Debug.WriteLine("Resource load completed, Status {0}", e.Status);
        }

        protected virtual void GetResourceResponseFilterEvent(object sender, GetResourceResponseFilterEventArgs e)
        {
            Debug.WriteLine("Get resource response filter.");
        }

        protected virtual void OnResourceResponseEvent(object sender, OnResourceResponseEventArgs e)
        {
            Debug.WriteLine("Resource response, Retry or redirect {0}.", e.RedirectOrRetry);
        }

        protected virtual void OnResourceRedirectEvent(object sender, OnResourceRedirectEventArgs e)
        {
            Debug.WriteLine("Resource redirected to '{0}'.", e.NewUrl);
            OnClientRedirect(this, new RedirectEventArgs(e.NewUrl));
        }

        protected virtual void OnBeforeResourceLoadEvent(object sender, OnBeforeResourceLoadEventArgs e)
        {
            Debug.WriteLine("Before resource load.");

            if (!e.Callback.IsDisposed)
            {
                e.Callback.Continue(true);
                e.ContinuationHandling = CefReturnValue.ContinueAsync;
            }
            e.ContinuationHandling = CefReturnValue.Continue;
        }

        protected virtual void OnClientRedirectEvent(object sender, RedirectEventArgs e)
        {
            Debug.WriteLine("Client redirected to '{0}'.", e.Location);
        }

        protected virtual void OnFormSubmitEvent(object sender, RedirectEventArgs e)
        {
            Debug.WriteLine("Form submitted to '{0}'.", e.Location);
        }
        #endregion
    }
}

