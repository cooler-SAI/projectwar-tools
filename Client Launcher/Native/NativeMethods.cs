namespace Launcher
{
    public static partial class Native
    {
        /// <summary>
        /// The w m_ move
        /// </summary>
        public const int WM_MOVE = 0x3;
        /// <summary>
        /// The w m_ moving
        /// </summary>
        public const int WM_MOVING = 0x216;
        /// <summary>
        /// The w m_ activate
        /// </summary>
        public const int WM_ACTIVATE = 0x6;
    }
}
