namespace Launcher
{
    public struct CallbackResponseStruct
    {
        public string Response;

        public CallbackResponseStruct(string response)
        {
            Response = response;
        }
    }
}
