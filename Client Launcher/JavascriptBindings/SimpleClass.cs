using CefSharp;
using System.Collections.Generic;

namespace Launcher
{
    public class SimpleClass
    {
        public IJavascriptCallback Callback { get; set; }
        public string TestString { get; set; }

        public IList<SimpleSubClass> SubClasses { get; set; }
    }
}
