﻿namespace Launcher
{
    public partial class CEFBrowserForm
    {
        public System.ComponentModel.IContainer components = null;
        
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CEFBrowserForm));
            this.MoveGrip = new TransparentEventCatcher();
            this.SuspendLayout();
            // 
            // MoveGrip
            // 
            this.MoveGrip.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MoveGrip.Location = new System.Drawing.Point(0, 0);
            this.MoveGrip.Name = "MoveGrip";
            this.MoveGrip.Size = new System.Drawing.Size(812, 20);
            this.MoveGrip.TabIndex = 0;
            this.MoveGrip.TabStop = false;
            this.MoveGrip.MouseDown += new System.Windows.Forms.MouseEventHandler(this.InvisibleButtonMouseDown);
            this.MoveGrip.MouseMove += new System.Windows.Forms.MouseEventHandler(this.InvisibleButtonMouseMove);
            this.MoveGrip.MouseUp += new System.Windows.Forms.MouseEventHandler(this.InvisibleButtonMouseUp);
            // 
            // CEFBrowserForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(912, 513);
            this.ControlBox = false;
            this.Controls.Add(this.MoveGrip);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CEFBrowserForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.ResumeLayout(false);

        }
        #endregion

        public TransparentEventCatcher MoveGrip;
    }
}

