﻿using CefSharp;
using System;
using System.Security.Cryptography.X509Certificates;

namespace Launcher
{
    /// <summary>
    ///     To use this class, check <see cref="IRequestHandler" /> for more information about the event parameters.
    ///     Often you will find MANDATORY information on how to work with the parameters or which thread the call comes from.
    ///     Simply check out the interface' method the event was named by.
    ///     (e.g <see cref="RequestEventHandler.OnCertificateErrorEvent" /> corresponds to
    ///     <see cref="IRequestHandler.OnCertificateError" />)
    ///     inspired by:
    ///     https://github.com/cefsharp/CefSharp/blob/fa41529853b2527eb0468a507ab6c5bd0768eb59/CefSharp.Example/RequestHandler.cs
    /// </summary>
    public class RequestEventHandler : IRequestHandler
    {
        public event EventHandler<OnBeforeBrowseEventArgs> OnBeforeBrowseEvent;
        public event EventHandler<OnOpenUrlFromTabEventArgs> OnOpenUrlFromTabEvent;
        public event EventHandler<OnCertificateErrorEventArgs> OnCertificateErrorEvent;
        public event EventHandler<OnPluginCrashedEventArgs> OnPluginCrashedEvent;
        public event EventHandler<OnBeforeResourceLoadEventArgs> OnBeforeResourceLoadEvent;
        public event EventHandler<GetAuthCredentialsEventArgs> GetAuthCredentialsEvent;
        public event EventHandler<OnRenderProcessTerminatedEventArgs> OnRenderProcessTerminatedEvent;
        public event EventHandler<OnQuotaRequestEventArgs> OnQuotaRequestEvent;
        public event EventHandler<OnResourceRedirectEventArgs> OnResourceRedirectEvent;

        /// <summary>
        ///     SECURITY WARNING: YOU SHOULD USE THIS EVENT TO ENFORCE RESTRICTIONS BASED ON SCHEME, HOST OR OTHER URL ANALYSIS
        ///     BEFORE ALLOWING OS EXECUTION.
        /// </summary>
        public event EventHandler<OnProtocolExecutionEventArgs> OnProtocolExecutionEvent;
        public event EventHandler<OnRenderViewReadyEventArgs> OnRenderViewReadyEvent;
        public event EventHandler<OnResourceResponseEventArgs> OnResourceResponseEvent;
        public event EventHandler<GetResourceResponseFilterEventArgs> GetResourceResponseFilterEvent;
        public event EventHandler<OnResourceLoadCompleteEventArgs> OnResourceLoadCompleteEvent;

        public bool CanGetCookies(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request)
        {
            return true;
        }

        public bool CanSetCookie(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, Cookie cookie)
        {
            return true;
        }

        public bool OnBeforeBrowse(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool userGesture, bool isRedirect)
        {
            OnBeforeBrowseEventArgs args = new OnBeforeBrowseEventArgs(chromiumWebBrowser, browser, frame, request, isRedirect);
            ExecuteEventHandler<OnBeforeBrowseEventArgs>(OnBeforeBrowseEvent, args);
            return args.CancelNavigation;
        }

        public bool OnOpenUrlFromTab(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, string targetUrl, WindowOpenDisposition targetDisposition, bool userGesture)
        {
            OnOpenUrlFromTabEventArgs args = new OnOpenUrlFromTabEventArgs(chromiumWebBrowser, browser, frame, targetUrl, targetDisposition, userGesture);
            ExecuteEventHandler<OnOpenUrlFromTabEventArgs>(OnOpenUrlFromTabEvent, args);
            return args.CancelNavigation;
        }

        public bool OnCertificateError(IWebBrowser chromiumWebBrowser, IBrowser browser, CefErrorCode errorCode, string requestUrl, ISslInfo sslInfo, IRequestCallback callback)
        {
            OnCertificateErrorEventArgs args = new OnCertificateErrorEventArgs(chromiumWebBrowser, browser, errorCode, requestUrl, sslInfo, callback);
            ExecuteEventHandler<OnCertificateErrorEventArgs>(OnCertificateErrorEvent, args);

            EnsureCallbackDisposal(callback);
            return args.ContinueAsync;
        }

        public void OnPluginCrashed(IWebBrowser chromiumWebBrowser, IBrowser browser, string pluginPath)
        {
            OnPluginCrashedEventArgs args = new OnPluginCrashedEventArgs(chromiumWebBrowser, browser, pluginPath);
            ExecuteEventHandler<OnPluginCrashedEventArgs>(OnPluginCrashedEvent, args);
        }

        public CefReturnValue OnBeforeResourceLoad(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IRequestCallback callback)
        {
            OnBeforeResourceLoadEventArgs args = new OnBeforeResourceLoadEventArgs(chromiumWebBrowser, browser, frame, request, callback);
            ExecuteEventHandler<OnBeforeResourceLoadEventArgs>(OnBeforeResourceLoadEvent, args);

            EnsureCallbackDisposal(callback);
            return args.ContinuationHandling;
        }

        public bool GetAuthCredentials(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, bool isProxy, string host, int port, string realm, string scheme, IAuthCallback callback)
        {
            GetAuthCredentialsEventArgs args = new GetAuthCredentialsEventArgs(chromiumWebBrowser, browser, frame, isProxy, host, port, realm, scheme, callback);
            ExecuteEventHandler<GetAuthCredentialsEventArgs>(GetAuthCredentialsEvent, args);

            EnsureCallbackDisposal(callback);
            return args.ContinueAsync;
        }

        public void OnRenderProcessTerminated(IWebBrowser chromiumWebBrowser, IBrowser browser, CefTerminationStatus status)
        {
            OnRenderProcessTerminatedEventArgs args = new OnRenderProcessTerminatedEventArgs(chromiumWebBrowser, browser, status);
            ExecuteEventHandler<OnRenderProcessTerminatedEventArgs>(OnRenderProcessTerminatedEvent, args);
        }

        public bool OnQuotaRequest(IWebBrowser chromiumWebBrowser, IBrowser browser, string originUrl, long newSize, IRequestCallback callback)
        {
            OnQuotaRequestEventArgs args = new OnQuotaRequestEventArgs(chromiumWebBrowser, browser, originUrl, newSize, callback);
            ExecuteEventHandler<OnQuotaRequestEventArgs>(OnQuotaRequestEvent, args);

            EnsureCallbackDisposal(callback);
            return args.ContinueAsync;
        }

        public void OnResourceRedirect(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response, ref string newUrl)
        {
            OnResourceRedirectEventArgs args = new OnResourceRedirectEventArgs(chromiumWebBrowser, browser, frame, request, response, newUrl);
            ExecuteEventHandler<OnResourceRedirectEventArgs>(OnResourceRedirectEvent, args);
            if (!Equals(newUrl, args.NewUrl))
            {
                newUrl = args.NewUrl;
            }
        }

        public bool OnProtocolExecution(IWebBrowser chromiumWebBrowser, IBrowser browser, string url)
        {
            OnProtocolExecutionEventArgs args = new OnProtocolExecutionEventArgs(chromiumWebBrowser, browser, url);
            ExecuteEventHandler<OnProtocolExecutionEventArgs>(OnProtocolExecutionEvent, args);
            return args.AttemptExecution;
        }

        public void OnRenderViewReady(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
            OnRenderViewReadyEventArgs args = new OnRenderViewReadyEventArgs(chromiumWebBrowser, browser);
            ExecuteEventHandler<OnRenderViewReadyEventArgs>(OnRenderViewReadyEvent, args);
        }

        public bool OnResourceResponse(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response)
        {
            OnResourceResponseEventArgs args = new OnResourceResponseEventArgs(chromiumWebBrowser, browser, frame, request, response);
            ExecuteEventHandler<OnResourceResponseEventArgs>(OnResourceResponseEvent, args);
            return args.RedirectOrRetry;
        }

        public IResponseFilter GetResourceResponseFilter(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response)
        {
            GetResourceResponseFilterEventArgs args = new GetResourceResponseFilterEventArgs(chromiumWebBrowser, browser, frame, request, response);
            ExecuteEventHandler<GetResourceResponseFilterEventArgs>(GetResourceResponseFilterEvent, args);
            return args.ResponseFilter;
        }

        public void OnResourceLoadComplete(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response, UrlRequestStatus status, long receivedContentLength)
        {
            OnResourceLoadCompleteEventArgs args = new OnResourceLoadCompleteEventArgs(chromiumWebBrowser, browser, frame, request, response, status, receivedContentLength);
            ExecuteEventHandler<OnResourceLoadCompleteEventArgs>(OnResourceLoadCompleteEvent, args);
        }

        public bool OnSelectClientCertificate(IWebBrowser chromiumWebBrowser, IBrowser browser, bool isProxy, string host, int port, X509Certificate2Collection certificates, ISelectClientCertificateCallback callback)
        {
            //TODO: Someone please contribute an implementation of this
            throw new NotImplementedException();
        }

        private static void EnsureCallbackDisposal(IRequestCallback callbackToDispose)
        {
            if (callbackToDispose != null && !callbackToDispose.IsDisposed)
            {
                callbackToDispose.Dispose();
            }
        }

        private static void EnsureCallbackDisposal(IAuthCallback callbackToDispose)
        {
            if (callbackToDispose != null && !callbackToDispose.IsDisposed)
            {
                callbackToDispose.Dispose();
            }
        }

        private void ExecuteEventHandler<T>(EventHandler<T> handler, T args)
        {
            handler?.Invoke(this, args);
        }
    }
}
