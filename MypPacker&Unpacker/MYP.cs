﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace WarClientTool
{
    public enum MythicPackage : Int32
    {
        NONE = 0,
        MFT = 1,
        ART = 2,
        ART2 = 3,
        ART3 = 4,
        AUDIO = 5,
        DATA = 6,
        WORLD = 7,
        INTERFACE = 8,
        VIDEO = 9,
        BLOODHUNT = 10,
        PATCH = 11,
        VO_ENGLISH = 12,
        VO_FRENCH = 13,
        VIDEO_FRENCH = 14,
        VO_GERMAN = 15,
        VIDEO_GERMAN = 16,
        VO_ITALIAN = 17,
        VIDEO_ITALIAN = 18,
        VO_SPANISH = 19,
        VIDEO_SPANISH = 20,
        VO_KOREAN = 21,
        VIDEO_KOREAN = 22,
        VO_CHINESE = 23,
        VIDEO_CHINESE = 24,
        VO_JAPANESE = 25,
        VIDEO_JAPANESE = 26,
        VO_RUSSIAN = 27,
        VIDEO_RUSSIAN = 28,
        WARTEST = 29,
        NDA = 30,
        DEV = 31,
    }

    public interface ILog
    {
        void Info(String msg, String context = "");
        void Error(String msg, String context = "");
        void Exception(String msg, Exception e, String context = "");
        void Warning(String msg, String context = "");
    }

    public class AssetInfo
    {
        public MythicPackage Package { get; set; }
        public Int64 Hash { get; set; }
        public UInt32 CompressedSize { get; set; }
        public UInt32 UncompressedSize { get; set; }
        public Boolean Compressed { get; set; }
        public String Name { get; set; }
        public UInt32 CRC { get; set; }
        public Int64 ID { get; set; }
        public Boolean Changed { get; set; }
        public Byte[] Data { get; set; }
    }

    public class Myp : IDisposable
    {

        public Dictionary<Int64, PatcherAsset> Assets { private set; get; } = new Dictionary<Int64, PatcherAsset>();

        //  private static ILog Log;

        private IntPtr _Mypptr = IntPtr.Zero;
        public MythicPackage Package { get; private set; }
        public MypManager Manager { get; set; }
        public Boolean Changed { get; set; } = false;

        #region IMPORTS

        [SuppressUnmanagedCodeSecurity]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void LogDelegate(Int32 type, Int32 level, String msg);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern IntPtr LoadMyp(IntPtr log, String path);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern IntPtr CreateMyp(IntPtr log, String path);


        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern void UnloadMyp(IntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UInt32 GetAssetSize(IntPtr ptr, String assetPath);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UInt32 GetAssetSizeByHash(IntPtr ptr, Int64 hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UInt32 GetAssetSizeCompressedByHash(IntPtr ptr, Int64 hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetCount(IntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetData(IntPtr ptr, String assetPath, [In, Out]Byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetDataByHash(IntPtr ptr, Int64 hash, [In, Out]Byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetDataByHashRaw(IntPtr ptr, Int64 hash, [In, Out]Byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 UpdateAssetByHash(IntPtr ptr, Int64 hash, [In, Out]Byte[] data, UInt32 size, Boolean compressed);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Boolean Save(IntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Boolean DeleteAssetByHash(IntPtr ptr, Int64 hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"Myp64.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"Myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetsInfo(IntPtr ptr, [In, Out]Int64[] hashes, [In, Out]UInt32[] CompressedSize,
            [In, Out]UInt32[] UnCompressedSize, [In, Out]UInt32[] compressed, [In, Out]UInt32[] crc);

        #endregion

        public static void MypLog(Int32 type, Int32 level, String msg)
        {
            Console.WriteLine(msg);
            //if (Log != null)
            //{
            //    if (level == 8)
            //        Log.Error(msg);
            //    else
            //        Log.Info(msg);
            //}
        }

        public void Dispose()
        {
            if (_Mypptr != IntPtr.Zero) {
                UnloadMyp(_Mypptr);
                _Mypptr = IntPtr.Zero;
            }
        }

        //   private IntPtr _logDelegate;
        public String Filename { private set; get; }

        //    private bool _loaded = false;
        //   private bool _closed = true;
        private readonly LogDelegate _callback = new LogDelegate(MypLog);
        private readonly IntPtr _callbackhandle;

        public Myp()
        {
            try {
                _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                GC.KeepAlive(_callback);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public Myp(MythicPackage package)
        {
            try {
                _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                GC.KeepAlive(_callback);
                Package = package;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public Myp(MythicPackage package, String path)
        {
            try {
                _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                GC.KeepAlive(_callback);
                Package = package;
                LoadFromFile(_callbackhandle, path);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
        
        public Myp(MythicPackage package, String path, Boolean create)
        {
            try {
                _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                GC.KeepAlive(_callback);
                Package = package;
                LoadFromFile(_callbackhandle, path, create);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
        
        public Myp(String path, Boolean create)
        {
            try {
                _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                GC.KeepAlive(_callback);
                LoadFromFile(_callbackhandle, path, create);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public void LoadFromFile(IntPtr _callbackhandle, String filename, Boolean create = true)
        {
            Filename = filename;

            if (create && !File.Exists(filename))
                _Mypptr = CreateMyp(_callbackhandle, filename);
            else
                _Mypptr = LoadMyp(_callbackhandle, filename);

            lock (Assets) {

                if (_Mypptr == IntPtr.Zero)
                    throw new Exception($"Error loading '{filename}'");


                Int32 count = GetAssetCount(_Mypptr);
                Int64[] hashes = new Int64[(Int32)count];
                UInt32[] csize = new UInt32[(Int32)count];
                UInt32[] uscise = new UInt32[(Int32)count];
                UInt32[] crc = new UInt32[(Int32)count];
                UInt32[] c = new UInt32[(Int32)count];

                GetAssetsInfo(_Mypptr, hashes, csize, uscise, c, crc);
                for (Int32 i = 0; i < count; i++) {
                    var asset = new PatcherAsset() {
                        Package = Package,
                        Hash = hashes[i],
                        CompressedSize = csize[i],
                        Size = uscise[i],
                        Compressed = c[i],
                        CRC32 = crc[i],
                    };
                    Assets[asset.Hash] = asset;
                }
                //_loaded = true;
            }
        }


        public void UpdateAsset(PatcherAsset asset)
        {
            lock (Assets) {
                Changed = true;
                Assets[asset.Hash] = asset;
                UpdateAssetByHash(_Mypptr, asset.Hash, asset.Data, (UInt32)asset.Data.Length, asset.Compressed == 1);
            }
        }

        public void UpdateAsset(Int64 hash, Byte[] data, Boolean compress)
        {
            lock (Assets) {
                Changed = true;
                UpdateAssetByHash(_Mypptr, hash, data, (UInt32)data.Length, compress);
            }
        }

        public void UpdateAsset(String path, Byte[] data, Boolean compress)
        {
            lock (Assets) {
                Int64 hash = HashWAR(path);
                Changed = true;
                UpdateAssetByHash(_Mypptr, hash, data, (UInt32)data.Length, compress);
            }
        }

        public Boolean Save()
        {
            // _dataChanges.Clear();

            lock (Assets) {
                if (Save(_Mypptr)) {

                    Int32 count = GetAssetCount(_Mypptr);
                    Int64[] hashes = new Int64[(Int32)count];
                    UInt32[] csize = new UInt32[(Int32)count];
                    UInt32[] uscise = new UInt32[(Int32)count];
                    UInt32[] crc = new UInt32[(Int32)count];
                    UInt32[] c = new UInt32[(Int32)count];

                    GetAssetsInfo(_Mypptr, hashes, csize, uscise, c, crc);
                    for (Int32 i = 0; i < count; i++) {
                        if (Assets.ContainsKey(hashes[i])) {
                            Assets[hashes[i]].CompressedSize = csize[i];
                            Assets[hashes[i]].CRC32 = crc[i];
                            Assets[hashes[i]].Compressed = c[i];
                        }
                    }

                    Changed = false;
                    return true;
                }
            }
            return false;
        }

        public Boolean Delete(PatcherAsset asset)
        {
            lock (Assets) {
                if (Assets.ContainsKey(asset.Hash) && DeleteAssetByHash(_Mypptr, asset.Hash)) {
                    Assets.Remove(asset.Hash);
                    Changed = true;
                    return true;
                }
            }
            return false;
        }

        public Byte[] GetAssetData(String name) => GetAssetData(HashWAR(name));

        public Byte[] GetAssetData(Int64 hash)
        {
            //if (_dataChanges.ContainsKey(hash))
            //    return _dataChanges[hash].Item2;
            lock (Assets) {
                UInt32 assetSize = GetAssetSizeByHash(_Mypptr, hash);
                if (assetSize > 0) {
                    Byte[] data = new Byte[(Int32)assetSize];
                    GetAssetDataByHash(_Mypptr, hash, data);
                    return data;
                }
                return null;
            }
        }

        public Byte[] GetAssetDataRaw(Int64 hash)
        {
            lock (Assets) {
                UInt32 assetSize = GetAssetSizeCompressedByHash(_Mypptr, hash);
                if (assetSize > 0) {
                    Byte[] data = new Byte[(Int32)assetSize];
                    GetAssetDataByHashRaw(_Mypptr, hash, data);
                    return data;
                }
            }
            return null;
        }

        public static Int64 HashWAR(String s)
        {
            UInt32 edx, eax, esi, ebx, edi, ecx;

            eax = _ = _ = ebx = 0;
            ebx = edi = esi = (UInt32)s.Length + 0xDEADBEEF;

            Int32 i = 0;

            for (i = 0; i + 12 < s.Length; i += 12) {
                edi = (UInt32)((s[i + 7] << 24) | (s[i + 6] << 16) | (s[i + 5] << 8) | s[i + 4]) + edi;
                esi = (UInt32)((s[i + 11] << 24) | (s[i + 10] << 16) | (s[i + 9] << 8) | s[i + 8]) + esi;
                edx = (UInt32)((s[i + 3] << 24) | (s[i + 2] << 16) | (s[i + 1] << 8) | s[i]) - esi;

                edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
                esi += edi;
                edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
                edx += esi;
                esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
                edi += edx;
                ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
                esi += edi;
                edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
                ebx += esi;
                esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
                edi += ebx;
            }

            if (s.Length - i > 0) {
                switch (s.Length - i) {
                    case 12:
                        esi += (UInt32)s[i + 11] << 24;
                        goto case 11;
                    case 11:
                        esi += (UInt32)s[i + 10] << 16;
                        goto case 10;
                    case 10:
                        esi += (UInt32)s[i + 9] << 8;
                        goto case 9;
                    case 9:
                        esi += s[i + 8];
                        goto case 8;
                    case 8:
                        edi += (UInt32)s[i + 7] << 24;
                        goto case 7;
                    case 7:
                        edi += (UInt32)s[i + 6] << 16;
                        goto case 6;
                    case 6:
                        edi += (UInt32)s[i + 5] << 8;
                        goto case 5;
                    case 5:
                        edi += s[i + 4];
                        goto case 4;
                    case 4:
                        ebx += (UInt32)s[i + 3] << 24;
                        goto case 3;
                    case 3:
                        ebx += (UInt32)s[i + 2] << 16;
                        goto case 2;
                    case 2:
                        ebx += (UInt32)s[i + 1] << 8;
                        goto case 1;
                    case 1:
                        ebx += s[i];
                        break;
                }

                esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
                ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
                edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
                esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
                edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
                edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
                eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

                return ((Int64)edi << 32) + eax;
            }
            return ((Int64)esi << 32) + eax;
        }

        public static String GetExtension(Byte[] buffer)
        {
            String header = System.Text.Encoding.ASCII.GetString(buffer, 0, 8);

            if (header.StartsWith("bdLF")) {
                return "db";
            } else if (header.StartsWith("gsLF")) {
                return "geom";
            } else if (header.StartsWith("idLF")) {
                return "diffuse";
            } else if (header.StartsWith("psLF")) {
                return "specular";
            } else if (header.StartsWith("amLF")) {
                return "mask";
            } else if (header.StartsWith("ntLF")) {
                return "tint";
            } else if (header.StartsWith("lgLF")) {
                return "glow";
            } else if (header.StartsWith("RÍ")) {
                return "zmft";
            } else if (header.StartsWith("Gamebryo File Format")) {
                return "nif";
            } else if (header.StartsWith("NetImmerse File")) {
                return "nif";
            } else if (header.StartsWith("particlesystem")) {
                return "ems";
            } else if (header.StartsWith("<!DOCTYPE html")) {
                return "html";
            } else if (header.StartsWith("<region")) {
                return "layout";
            } else if (buffer[0] == 0x42 && buffer[1] == 0x49 && buffer[2] == 0x4b) {
                return "jpeg";
            } else if (buffer[0] == 0x42 && buffer[1] == 0x49 && buffer[2] == 0x4b) {
                return "bik";
            } else if (buffer[0] == 0xd0 && buffer[1] == 0xcf && buffer[2] == 0x11 && buffer[3] == 0xe0 && buffer[4] == 0xa1 && buffer[5] == 0xb1 && buffer[6] == 0x1a && buffer[7] == 0xe1) {
                return "doc";
            } else if (buffer[0] == 0x00 && buffer[1] == 0x01 && buffer[2] == 0x00 && buffer[3] == 0x00 && buffer[4] == 0x00 && buffer[5] == 0x00 && buffer[6] == 0x00 && buffer[7] == 0x00) {
                return "ttf";
            } else if (buffer[0] == 0x00 && buffer[1] == 0x00 && buffer[2] == 0x0a && buffer[3] == 0x00 && buffer[4] == 0x00 && buffer[5] == 0x00 && buffer[6] == 0x00 && buffer[7] == 0x00) {
                return "tga";
            } else if (buffer[0] == 0x00 && buffer[1] == 0x00 && buffer[2] == 0x03 && buffer[3] == 0x00 && buffer[4] == 0x00 && buffer[5] == 0x00 && buffer[6] == 0x00 && buffer[7] == 0x00) {
                return "tga";
            } else if (buffer[0] == 0x00 && buffer[1] == 0x00 && buffer[2] == 0x02 && buffer[3] == 0x00 && buffer[4] == 0x00 && buffer[5] == 0x00 && buffer[6] == 0x00 && buffer[7] == 0x00) {
                return "tga";
            } else if (buffer[0] == 0x49 && buffer[1] == 0x44 && buffer[2] == 0x33 && buffer[3] == 0x03 && buffer[4] == 0x00 && buffer[5] == 0x00 && buffer[6] == 0x00 && buffer[7] == 0x00) {
                return "mp3";
            } else if (buffer[0] == 0x0a && buffer[1] == 0x23 && buffer[2] == 0x54 && buffer[3] == 0x49 && buffer[4] == 0x54 && buffer[5] == 0x32 && buffer[6] == 0x00 && buffer[7] == 0x00) {
                return "mp3";
            } else if (buffer[0] == 0x00 && buffer[1] == 0x01 && buffer[2] == 0x01 && buffer[3] == 0x00 && buffer[4] == 0x00) {
                return "tga";
            } else if (buffer[0] == 0x42 && buffer[1] == 0x49 && buffer[2] == 0x4b && buffer[3] > 0x60 && buffer[3] < 0x70) {
                return "bik";
            } else if (buffer[0] == 0x00 && buffer[1] == 0x01 && buffer[2] == 0x0a && buffer[3] == 0x00) {
                return "tga";
            } else if (buffer[0] == 0x0a && buffer[1] == 0x05 && buffer[2] == 0x01 && buffer[3] == 0x08) {
                return "pcx";
            } else if (buffer[0] == 0x50 && buffer[1] == 0x4b && buffer[2] == 0x03 && buffer[3] == 0x04) {
                return "zip";
            } else if (buffer[0] == 0x01 && buffer[1] == 0x4c && buffer[2] == 0x75 && buffer[3] == 0x61) {
                return "luac";
            } else if (buffer[0] == 0x00 && buffer[1] == 0x00 && buffer[2] == 0xFE && buffer[3] == 0xFF) {
                return "txt";
            } else if (buffer[0] == 0xFF && buffer[1] == 0xFE && buffer[2] == 0x00 && buffer[3] == 0x00) {
                return "txt";
            } else if (buffer[0] == 0x42 && buffer[1] == 0x49 && buffer[2] == 0x4b) {
                return "txt";
            } else if (buffer[0] == 0xEF && buffer[1] == 0xBB && buffer[2] == 0xBF) {
                return "txt";
            } else if (buffer[0] == 0x42 && buffer[1] == 0x49 && buffer[2] == 0x4b) {
                return "txt";
            } else if (buffer[0] == 0xEF && buffer[1] == 0xBB && buffer[2] == 0xBF) {
                return "txt";
            } else if (buffer[0] == 0xFE && buffer[1] == 0xFF) {
                return "txt";
            } else if (buffer[0] == 0xFF && buffer[1] == 0xFE) {
                return "txt";
            }

            String footer = System.Text.Encoding.ASCII.GetString(buffer, buffer.Length - 32, 32);

            if (footer.Contains("TRUEVISION-XFILE")) {
                return "vsh";
            }

            String text = System.Text.Encoding.ASCII.GetString(buffer, 0, buffer.Length);

            if (text.Contains("target vs_3_0")) {
                return "vsh";
            } else if (text.Contains("target vs_2_0")) {
                return "vsh";
            } else if (text.Contains("target vs_1_0")) {
                return "vsh";
            } else if (text.Contains("target vs_1_1")) {
                return "vsh";
            } else if (text.Contains("target ps_3_0")) {
                return "psh";
            } else if (text.Contains("target ps_2_0")) {
                return "psh";
            } else if (text.Contains("target ps_2_b")) {
                return "psh";
            } else if (text.Contains("target ps_2_a")) {
                return "psh";
            } else if (text.Contains("target ps_1_0")) {
                return "psh";
            } else if (header.Contains("SCPT")) {
                return "scpt";
            } else if (header.Contains("DDS")) {
                return "dds";
            } else if (header.Contains("XSM")) {
                return "xsm";
            } else if (header.Contains("XAC")) {
                return "xac";
            } else if (header.Contains("8BPS")) {
                return "8bps";
            } else if (header.Contains("PNG")) {
                return "png";
            } else if (header.Contains("AMX")) {
                return "amx";
            } else if (header.Contains("SIDS")) {
                return "sids";
            } else if (header.Contains("MZ")) {
                return "exe";
            } else if (header.StartsWith("RIFF") && Encoding.ASCII.GetString(buffer, 8, 4) == "riff") {
                return "riff";
            } else if (header.StartsWith("RIFF") && Encoding.ASCII.GetString(buffer, 8, 4) == "wave") {
                return "wav";
            } else if (text.Contains("lua") && text.Contains("lua")) {
                return "lua";
            } else if (text.Contains("WMPHOTO")) {
                return "lmp";
            } else if (text.Contains("<?xml")) {
                return "<?xml";
            }

            String csv = text.Replace(",", "");
            Int32 commaCount = text.Length - csv.Length;
            Int32 commaThreshold = Math.Max(buffer.Length / 22, 16);

            if (commaCount >= commaThreshold) {
                return "csv";
            }

            return "txt";
        }
    }
}
