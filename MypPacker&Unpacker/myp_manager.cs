﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarClientTool
{
    public class PatcherAsset
    {



        public Int64 EnvironmentID { get; set; }


        public Int64 PatcherFileID { get; set; }


        public MythicPackage Package { get; set; }


        public Int64 Size { get; set; }
        public Int64 CompressedSize { get; set; }
        public Int64 Hash { get; set; }
        public UInt32 CRC32 { get; set; }

        public String Name { get; set; }
        public UInt32 Compressed { get; set; }
        public Byte[] Data;
        public override String ToString() => Name;
        public PatcherAsset()
        {
        }
    }
    public class StringTable
    {
        public List<String> List = new List<String>();
        public Dictionary<String, String> _keyed = new Dictionary<String, String>();
        public StringTable()
        {
        }

        public void Load(Byte[] data)
        {
            String str = System.Text.UnicodeEncoding.Unicode.GetString(data);
            Int32 row = 0;
            foreach (String line in str.Split(new String[] { "\r\n" }, StringSplitOptions.None))
            {
                Int32 index = line.IndexOf('\t');
                String key = line;

                if (index > 0)
                {
                    key = line.Substring(0, index).ToLower().Trim();
                    String value = line.Substring(index + 1);
                    _keyed[key] = value;
                    List.Add(value);
                }
                else
                {
                    _keyed[key] = "";
                    List.Add("");
                }
                row++;
            }
        }

        public String GetKeyedValue(String row) => _keyed.ContainsKey(row) ? _keyed[row] : "";

        public String GetValue(Int32 row) => row < List.Count ? List[row] : "";

        public void SetValue(Int32 row, String value)
        {
            if (row > List.Count)
            {
                Int32 count = row - List.Count + 1;

                for (Int32 i = 0; i < count; i++)
                {
                    List.Add("");
                }
            }
            _keyed[row.ToString()] = value;
            List[row] = value;

        }

        public String ToDocument()
        {
            var builder = new StringBuilder();

            for (Int32 i = 0; i < List.Count; i++)
            {
                builder.Append(i + "\t" + List[i] + "\r\n");
            }


            return builder.ToString();
        }
    }
    public enum Language
    {
        english = 1,
        french = 2,
        german = 3,
        italian = 4,
        spanish = 5,
        korean = 6,
        s_chinese = 7,
        t_chinese = 8,
        japanese = 9,
        russian = 10,
    }

    public class Folder
    {
        public MythicPackage Package { get; internal set; }
        public String Name => Path == null ? Package.ToString().ToLower() : System.IO.Path.GetFileName(Path);
        public Folder Parent { get; internal set; }
        public MypManager Manager { get; internal set; }
        public Int64 PatcherFileID = 0;
        public Folder Root
        {
            get
            {
                Folder r = Parent;
                while (r != null)
                {
                    if (r.Parent == null)
                    {
                        return r;
                    }

                    r = r.Parent;
                }
                return this;
            }
        }

        public Dictionary<String, Folder> Folders = new Dictionary<String, Folder>();
        public Dictionary<Int64, PatcherAsset> Files = new Dictionary<Int64, PatcherAsset>();
        public Boolean Dehashed = true;
        public String Path { get; internal set; }

        public override String ToString() => Name;

        public Folder(MypManager manager) => Manager = manager;

        public List<PatcherAsset> GetAssets(MythicPackage packageFilter = MythicPackage.NONE)
        {
            var list = new List<PatcherAsset>();
            GetAssetsRecursive(list, packageFilter);
            return list;
        }

        public List<MythicPackage> GetPackages()
        {
            var list = new List<PatcherAsset>();
            GetAssetsRecursive(list);
            return list.GroupBy(e => e.Package).Select(e => e.Key).ToList();
        }
        private void GetAssetsRecursive(List<PatcherAsset> assets, MythicPackage packageFilter = MythicPackage.NONE)
        {

            assets.AddRange(Files.Values.Where(e => packageFilter == MythicPackage.NONE || e.Package == packageFilter));
            foreach (Folder folder in Folders.Values)
            {
                folder.GetAssetsRecursive(assets);
            }
        }
    }

    public class MypManager : IDisposable
    {
        private readonly Dictionary<MythicPackage, Myp> _packages = new Dictionary<MythicPackage, Myp>();
        private readonly String _Mypfolder;
        public readonly Dictionary<Int64, String> HashToString = null;
        public Dictionary<MythicPackage, Folder> PackageFolders = new Dictionary<MythicPackage, Folder>();
        private readonly Dictionary<Language, Dictionary<String, StringTable>> _strings = new Dictionary<Language, Dictionary<String, StringTable>>();

        public Boolean Updated
        {
            get
            {

                foreach (Myp package in _packages.Values)
                {
                    if (package.Changed)
                    {
                        return true;
                    }
                }

                return false;
            }
        }


        public void Close()
        {
            foreach (Myp archive in _packages.Values)
            {
                archive.Dispose();
            }
        }

        public MypManager(String MypFolder, Dictionary<Int64, String> dehashes)
        {
            HashToString = dehashes;
            _Mypfolder = MypFolder;
        }


        protected async Task LoadInternal(Boolean overlay, params MythicPackage[] include)
        {
            var tasks = new List<Task>();

            foreach (MythicPackage archive in Enum.GetValues(typeof(MythicPackage)))
            {
                //if (archive == MythicPackage.MFT)
                //    continue;

                if (include == null || !include.Contains(archive))
                {
                    continue;
                }

                String file = Path.Combine(_Mypfolder, archive.ToString().ToLower() + ".Myp");
                if (File.Exists(file))
                {
                    tasks.Add(Task.Run(() => LoadPackage(file)));
                }
            }
            await Task.WhenAll(tasks);
        }

        public Dictionary<Int64, PatcherAsset> GetAssets(MythicPackage package) => _packages[package].Assets;

        public List<MythicPackage> GetPackages() => _packages.Keys.ToList();

        public PatcherAsset GetAsset(MythicPackage package, Int64 hash)
        {
            if (_packages.ContainsKey(package) && _packages[package].Assets.ContainsKey(hash))
            {
                if (_packages[package].Assets[hash].Data == null)
                {
                    _packages[package].Assets[hash].Data = _packages[package].GetAssetData(hash);
                }

                return _packages[package].Assets[hash];
            }
            return null;
        }

        private void LoadPackage(String file)
        {
            var package = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(file).ToLower());

            if (File.Exists(file))
            {
                try
                {
                    var m = new Myp(package)
                    {
                        Manager = this
                    };
                    _packages[package] = m;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }

        public void Save()
        {
            foreach (Myp Myp in _packages.Values.ToList())
            {
                if (Myp.Changed)
                {
                    Console.WriteLine($"Saving archive '{Myp.Filename}'");
                    Myp.Save();
                }
            }

        }

        public void UpdateAsset(PatcherAsset asset) => _packages[asset.Package].UpdateAsset(asset);

        public void InsertAsset(PatcherAsset asset) => _packages[asset.Package].UpdateAsset(asset);

        public void DeleteAsset(PatcherAsset asset) => _packages[asset.Package].Delete(asset);

        public Boolean HasAsset(MythicPackage package, Int64 hash) => _packages.ContainsKey(package) && _packages[package].Assets.ContainsKey(hash);

        public void Dispose()
        {
            foreach (Myp Myp in _packages.Values.ToList())
            {
                Myp.Dispose();
            }
        }


        public String GetAssetStringASCII(String assetName, MythicPackage? archive = null)
        {
            Int64 hash = Myp.HashWAR(assetName);
            if (archive == null)
            {
                foreach (MythicPackage package in GetPackages())
                {

                    if (HasAsset(package, hash))
                    {
                        return System.Text.Encoding.ASCII.GetString(GetAsset(package, hash).Data);
                    }
                }
            }
            else if (GetPackages().Contains(archive.Value))
            {
                return System.Text.Encoding.ASCII.GetString(GetAsset(archive.Value, hash).Data);
            }

            return "";
        }
        public String GetAssetStringUnicode(String assetName, MythicPackage? archive = null)
        {
            Int64 hash = Myp.HashWAR(assetName);
            if (archive == null)
            {
                foreach (MythicPackage package in GetPackages())
                {

                    if (HasAsset(package, hash))
                    {
                        return System.Text.Encoding.Unicode.GetString(GetAsset(package, hash).Data);
                    }
                }
            }
            else if (GetPackages().Contains(archive.Value))
            {
                return System.Text.Encoding.Unicode.GetString(GetAsset(archive.Value, hash).Data);
            }

            return "";
        }

        public PatcherAsset GetAsset(String assetName) => GetAsset(Myp.HashWAR(assetName));

        public PatcherAsset GetAsset(PatcherAsset asset)
        {
            asset.Data = GetAsset(asset.Package, asset.Hash).Data;
            return asset;
        }

        public PatcherAsset FindAsset(String assetName, MythicPackage package)
        {
            PatcherAsset asset = GetAsset(assetName, package);
            return asset ?? GetAsset(Myp.HashWAR(assetName));
        }

        public PatcherAsset GetAsset(String assetName, MythicPackage package)
        {
            Int64 hash = Myp.HashWAR(assetName);
            return HasPackage(package) && HasAsset(package, hash) ? GetAsset(package, hash) : null;
        }

        public PatcherAsset GetAsset(Int64 hash)
        {
            foreach (MythicPackage package in GetPackages())
            {
                PatcherAsset asset = GetAsset(package, hash);
                if (asset != null)
                {
                    return asset;
                }
            }
            return null;
        }

        public Boolean HasPackage(MythicPackage package) => GetPackages().Contains(package);

        public async Task Load(Boolean overlay, params MythicPackage[] include)
        {
            await LoadInternal(overlay, include);
            foreach (MythicPackage package in GetPackages())
            {
                foreach (PatcherAsset asset in GetAssets(package).Values)
                {
                    AddToFolder(package, asset);
                }
            }
        }

        protected void AddToFolder(MythicPackage package, PatcherAsset entry)
        {
            _ = package.ToString().ToLower();
            if (!PackageFolders.ContainsKey(package))
            {
                PackageFolders[package] = new Folder(this)
                {
                    Package = package,
                    Path = ""
                };
            }
            Folder folder = PackageFolders[package];

            if (!String.IsNullOrEmpty(entry.Name))
            {
                String[] folders = entry.Name.Split(new Char[] { '/' });
                String path = "";


                for (Int32 i = 0; i < folders.Length - 1; i++)
                {
                    path += folders[i];
                    if (!folder.Folders.ContainsKey(path))
                    {
                        folder.Folders[path] = new Folder(this)
                        {
                            Package = package,
                            Path = path,
                            PatcherFileID = entry.PatcherFileID,
                            Parent = folder,
                        };
                    }

                    folder = folder.Folders[path];
                    path += '/';
                }
                _ = folders[folders.Length - 1];

                folder.Files[entry.Hash] = entry;
            }
            else
            {

                if (!folder.Folders.ContainsKey("NO_DEHASH"))
                {
                    folder.Folders["NO_DEHASH"] = new Folder(this)
                    {
                        Package = package,
                        Path = "NO_DEHASH",
                        Dehashed = false,
                        PatcherFileID = entry.PatcherFileID,
                    };
                }

                folder.Folders["NO_DEHASH"].Files[entry.Hash] = entry;
            }
        }

        public void UpdateAsset(MythicPackage package, String path, Byte[] data)
        {
            PatcherAsset asset = GetAsset(path, package);
            asset.Data = data;
            UpdateAsset(asset);
        }

        public String GetStringByKey(Language language, String tableName, String key, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            String result = "";
            if (!_strings.ContainsKey(language))
            {
                _strings[language] = new Dictionary<String, StringTable>();
            }

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset("data/strings/" + language + "/" + path, defaulPackage);
                if (asset == null)
                {
                    asset = GetAsset("data/strings/" + language + "/" + path);
                }

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            return _strings[language].ContainsKey(path) ? _strings[language][path].GetKeyedValue(key) : result;
        }

        public String GetString(Language language, String tableName, Int32 row, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            String result = "";
            if (!_strings.ContainsKey(language))
            {
                _strings[language] = new Dictionary<String, StringTable>();
            }

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset("data/strings/" + language + "/" + path, defaulPackage);
                if (asset == null)
                {
                    asset = GetAsset("data/strings/" + language + "/" + path);
                }

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            return _strings[language].ContainsKey(path) ? _strings[language][path].GetValue(row) : result;
        }

        public String GetString(Language language, String tableName)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            if (!_strings.ContainsKey(language))
            {
                _strings[language] = new Dictionary<String, StringTable>();
            }

            if (!_strings[language].ContainsKey(path))
            {
                GetStringTable(language, path);
            }

            return _strings[language].ContainsKey(path) ? _strings[language][path].ToDocument() : "";
        }

        public Byte[] GetStringUnicode(Language language, String tableName)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
            {
                _strings[language] = new Dictionary<String, StringTable>();
            }

            if (!_strings[language].ContainsKey(path))
            {
                GetStringTable(language, path);
            }

            return _strings[language].ContainsKey(path)
                ? System.Text.UnicodeEncoding.Unicode.GetPreamble().Concat(System.Text.UnicodeEncoding.Unicode.GetBytes(_strings[language][path].ToDocument())).ToArray()
                : System.Text.UnicodeEncoding.Unicode.GetPreamble().Concat(System.Text.UnicodeEncoding.Unicode.GetBytes("")).ToArray();
        }

        public List<String> GetStringTable(Language language, String tableName, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
            {
                _strings[language] = new Dictionary<String, StringTable>();
            }

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset($"data/strings/{language}/{path}", defaulPackage);
                if (asset == null)
                {
                    asset = GetAsset("data/strings/" + language + "/" + path);
                }

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            return _strings[language].ContainsKey(path) ? _strings[language][path].List : new List<String>();

        }

        public StringTable GetStringTable2(Language language, String tableName, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
            {
                _strings[language] = new Dictionary<String, StringTable>();
            }

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset($"data/strings/{language}/{path}", defaulPackage);
                if (asset == null)
                {
                    asset = GetAsset("data/strings/" + language + "/" + path);
                }

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            return _strings[language].ContainsKey(path) ? _strings[language][path] : null;

        }

        public void SetString(Language language, String tableName, Int32 label, String value, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            if (!_strings.ContainsKey(language))
            {
                _strings[language] = new Dictionary<String, StringTable>();
            }

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset(path, defaulPackage);
                if (asset == null)
                {
                    asset = GetAsset(path);
                }

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
            {
                _strings[language][path].SetValue(label, value);
            }

        }

        public String GetStringFormat(Language language, String label, params Object[] vals)
        {
            String str = GetStringByKey(language, "/default.txt", label);

            for (Int32 i = 0; i < vals.Length; i++)
            {
                str = str.Replace("<<" + (i + 1).ToString() + ">>", vals[i].ToString());
            }
            return str.Replace("<BR>", "\r\n");
        }

        public Boolean HasDehash(String path) => HashToString.ContainsKey(Myp.HashWAR(path));
    }

}
