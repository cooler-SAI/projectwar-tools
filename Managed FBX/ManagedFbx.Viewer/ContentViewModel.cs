﻿using HelixToolkit.Wpf;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Animations;
using HelixToolkit.Wpf.SharpDX.Assimp;
using HelixToolkit.Wpf.SharpDX.Controls;
using HelixToolkit.Wpf.SharpDX.Model.Scene;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using OrthographicCamera = HelixToolkit.Wpf.SharpDX.OrthographicCamera;
using PerspectiveCamera = HelixToolkit.Wpf.SharpDX.PerspectiveCamera;

namespace ManagedFbx.Viewer
{
    // https://social.msdn.microsoft.com/Forums/vstudio/en-US/cc5b0c73-4992-4e1b-b782-b50aa7834255/fbx-3d-scene-in-wpf-app?forum=wpf
    // https://www.c-sharpcorner.com/uploadfile/mheydlauf/creating-a-simple-3d-scene-in-wpf/
    // https://stackoverflow.com/questions/3127753/displaying-3d-models-in-wpf

    public class ContentViewModel
    {
        #region Constructors

        public ContentViewModel(string fileName)
        {
            FileName = fileName;
            EffectsManager = new DefaultEffectsManager();
            Camera = new OrthographicCamera()
            {
                LookDirection = new System.Windows.Media.Media3D.Vector3D(0, -10, -10),
                Position = new System.Windows.Media.Media3D.Point3D(0, 10, 10),
                UpDirection = new System.Windows.Media.Media3D.Vector3D(0, 1, 0),
                FarPlaneDistance = 5000,
                NearPlaneDistance = 0.1f
            };
            ResetCameraCommand = new DelegateCommand(() =>
            {
                (Camera as OrthographicCamera).Reset();
                (Camera as OrthographicCamera).FarPlaneDistance = 5000;
                (Camera as OrthographicCamera).NearPlaneDistance = 0.1f;
            });
            ExportCommand = new DelegateCommand(() => { ExportFile(); });
            EnvironmentMap = LoadFileToMemory("fi.0.0.ch_aof_star_burn_detail.stx.dds");
        }

        #endregion Constructors

        #region Members

        private ContentView view;
        [Dependency]
        public ContentView View
        {
            get { return view; }

            set
            {
                view = value;
                if (view != null)
                    view.DataContext = this;
            }
        }

        public HelixToolkitScene Scene { get; set; }
        public SceneNodeGroupModel3D CurrentModel { get; set; } = new SceneNodeGroupModel3D();

        public IEffectsManager EffectsManager { get; set; }

        public const string Orthographic = "Orthographic Camera";
        public const string Perspective = "Perspective Camera";
        private HelixToolkit.Wpf.SharpDX.Camera _Camera;
        public HelixToolkit.Wpf.SharpDX.Camera Camera
        {
            get { return _Camera; }
            set
            {
                _Camera = value;
                CameraModel = value is PerspectiveCamera ? Perspective : value is OrthographicCamera ? Orthographic : null;
            }
        }

        public List<string> CameraModelCollection { get; private set; }

        private string _CameraModel;
        public string CameraModel
        {
            get { return _CameraModel; }
            set
            {
                _CameraModel = value;
                OnCameraModelChanged();
            }
        }

        protected OrthographicCamera defaultOrthographicCamera = new OrthographicCamera { Position = new System.Windows.Media.Media3D.Point3D(0, 0, 5), LookDirection = new System.Windows.Media.Media3D.Vector3D(-0, -0, -5), UpDirection = new System.Windows.Media.Media3D.Vector3D(0, 1, 0), NearPlaneDistance = 1, FarPlaneDistance = 100 };

        protected PerspectiveCamera defaultPerspectiveCamera = new PerspectiveCamera { Position = new System.Windows.Media.Media3D.Point3D(0, 0, 5), LookDirection = new System.Windows.Media.Media3D.Vector3D(-0, -0, -5), UpDirection = new System.Windows.Media.Media3D.Vector3D(0, 1, 0), NearPlaneDistance = 0.5, FarPlaneDistance = 150 };

        public event EventHandler CameraModelChanged;

        public ICommand ResetCameraCommand { set; get; }
        public ICommand ExportCommand { private set; get; }

        private bool _EnableAnimation = false;
        public bool EnableAnimation
        {
            set
            {
                _EnableAnimation = value;
                if (_EnableAnimation)
                    StartAnimation();
                else
                    StopAnimation();
            }
            get { return _EnableAnimation; }
        }
        public ObservableCollection<Animation> Animations { get; } = new ObservableCollection<Animation>();

        private Animation _SelectedAnimation = null;
        public Animation SelectedAnimation
        {
            set
            {
                _SelectedAnimation = value;
                StopAnimation();

                if (_SelectedAnimation != null)
                    _AnimationUpdater = new NodeAnimationUpdater(value);
                else
                    _AnimationUpdater = null;
                if (_EnableAnimation)
                    StartAnimation();
            }
            get
            {
                return _SelectedAnimation;
            }
        }

        public TextureModel EnvironmentMap { get; }

        public string FileName { get; set; } = string.Empty;

        private string OpenFileFilter = $"{HelixToolkit.Wpf.SharpDX.Assimp.Importer.SupportedFormatsString}";
        private string ExportFileFilter = $"{HelixToolkit.Wpf.SharpDX.Assimp.Exporter.SupportedFormatsString}";
        private SynchronizationContext _Context = SynchronizationContext.Current;
        private NodeAnimationUpdater _AnimationUpdater;
        private List<BoneSkinMeshNode> _BoneSkinNodes = new List<BoneSkinMeshNode>();
        private List<BoneSkinMeshNode> _SkeletonNodes = new List<BoneSkinMeshNode>();
        private CompositionTargetEx _CompositeHelper = new CompositionTargetEx();

        #endregion Members

        #region Functions

        public void CreateViewPort()
        {
            CurrentModel.AddNode(Scene.Root);
        }
        
        public static MemoryStream LoadFileToMemory(string filePath)
        {
            using (var file = new FileStream(filePath, FileMode.Open))
            {
                var memory = new MemoryStream();
                file.CopyTo(memory);
                return memory;
            }
        }

        protected virtual void OnCameraModelChanged()
        {
            CameraModelChanged?.Invoke(this, new EventArgs());
        }

        public void StartAnimation()
        {
            _CompositeHelper.Rendering += CompositeHelper_Rendering;
        }

        public void StopAnimation()
        {
            _CompositeHelper.Rendering -= CompositeHelper_Rendering;
        }

        private void CompositeHelper_Rendering(object sender, System.Windows.Media.RenderingEventArgs e)
        {
            if (_AnimationUpdater != null)
            {
                _AnimationUpdater.Update(Stopwatch.GetTimestamp(), Stopwatch.Frequency);
            }
        }

        private int SaveFileDialog(string filter, out string path)
        {
            var d = new SaveFileDialog
            {
                Filter = filter
            };
            if (d.ShowDialog() == true)
            {
                path = d.FileName;
                return d.FilterIndex - 1; //This is tarting from 1. So must minus 1
            }
            else
            {
                path = string.Empty;
                return -1;
            }
        }

        private void ExportFile()
        {
            var index = SaveFileDialog(ExportFileFilter, out var path);
            if (!string.IsNullOrEmpty(path) && index >= 0)
            {
                var id = HelixToolkit.Wpf.SharpDX.Assimp.Exporter.SupportedFormats[index].FormatId;
                var exporter = new HelixToolkit.Wpf.SharpDX.Assimp.Exporter();
                exporter.ExportToFile(path, Scene, id);
                return;
            }
        }

        #endregion Functions
    }
}
