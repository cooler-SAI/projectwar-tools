#pragma once

// Including SDKDDKVer.h defines the highest available platform.
// To support older versions, Include WinSDKVer.h, define _WIN32_WINNT to the target platform and then include SDKDDKVer.h.
#include <SDKDDKVer.h>

#include <WinSock2.h>
#include <MSWSock.h>
#include <Windows.h>

#include <type_traits>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "zlib.h"
#include "brotli/decode.h"

#include "EnumClassOps.h"

#ifdef _DLL
#define LIBRARY_API __declspec(dllexport)
#else
#define LIBRARY_API __declspec(dllimport)
#endif

namespace  WarhammerOnline::DataMining
{
    namespace sfs = std::filesystem;

    #pragma region Integer type defenitions
    typedef unsigned char      byte;
    typedef signed char        int8_t;
    typedef short              int16_t;
    typedef int                int32_t;
    typedef long long          int64_t;
    typedef unsigned char      uint8_t;
    typedef unsigned short     uint16_t;
    typedef unsigned int       uint32_t;
    typedef unsigned long long uint64_t;
    #pragma endregion

    #pragma region Enumerations
    enum class LogLevel : unsigned int
    {
        /*Normally disabled by default this category is both verbose and potentially includes sensitive data (not recommended for production environments).*/
        Trace = (1 << 0x00),

        /*Information for us by developers, primarily containing information useful for debugging.*/
        Debug = (1 << 0x01),

        /*For following flow of execution amongst other content such as routine event dispatch and handling.*/
        Information = (1 << 0x02),

        /*Notifcaiton of abnormal or unexpected logic, control and data, problems that are remediable and can be worked around.*/
        Warning = (1 << 0x03),

        /*Failures that may stop flow of execution, generally indicitive of localised flaws in the current activity rather than application wide.*/
        Error = (1 << 0x04),

        /*Very important application, library or external catastrophic failures and crashes that are both irrecoverable and usually cause loss of data.*/
        Critical = (1 << 0x05),

        /*.This category does not write any messages.*/
        None = (1 << 0x06)
    };
    template<> struct EnableAllEnumOperators<LogLevel> { static constexpr bool value = true; };
    #pragma endregion

    #pragma region Error handling and logging functions
    inline static void LogWriteLine(LogLevel &&level, const std::string &&s) {
        std::string message = s + "\n";
        std::cout << message;
        OutputDebugStringA(message.c_str());
    };

    inline static void LogStatus(const std::string& s) {
        std::string message = s + "\n";
        std::cout << message;
        OutputDebugStringA(message.c_str());
    };

    inline static void LogError(const std::string& s) {
        std::string message = s + "\n";
        std::cout << message;
        OutputDebugStringA(message.c_str());
    };

    inline static void LogWarning(const std::string& s) {
        std::string message = s + "\n";
        std::cout << message;
        OutputDebugStringA(message.c_str());
    };

    inline static void LogDebugInfo(const std::string& s) {
        std::string message = s + "\n";
        std::cout << message;
        OutputDebugStringA(message.c_str());
    };

    inline static void HandleException(std::exception &&aException, std::string &&aLogMessage = "") {
        LogError(std::string("System error : " + aLogMessage));
        if (nullptr != aException.what())
            LogError(std::move(std::string(aException.what())));
    }
    #pragma endregion

    #pragma region Utility functions
    template<typename T, typename S>
    class StringStreamMemoryBuffer : public std::streambuf
    {
    public:
        StringStreamMemoryBuffer(T data, S size) { this->setg(data, data, data + size); }
    };
    template<> struct StringStreamMemoryBuffer<const char *, size_t>;

    template<typename T>
    class StaticMemberVariableEmitter
    {
    public:
        std::string message;
        StaticMemberVariableEmitter(T &&s) : message(std::forward<T>(std::remove_reference<T>(s))) { std::cout << "Created " << message << std::endl; }
        ~StaticMemberVariableEmitter() { std::cout << "Destroyed " << message << std::endl; }
    };
    template<> struct StaticMemberVariableEmitter<const std::string>;

    inline static bool FileExists(const std::string& fileName) {
        try { /* RAII ensures the destructor is called for objects as they go out of scope implicitly closing the stream. */
            std::ifstream f(fileName.c_str());
            return f.good();
        } catch (std::fstream::failure& e) {
            HandleException(std::move(e));
        }
        return false;
    };

    inline static uint64_t FileSize(const std::string& fileName) {
        try {
            return sfs::file_size(fileName);
        } catch (sfs::filesystem_error& e) {
            HandleException(std::move(e));
        }
        return 0;
    };

    inline static std::string to_hex(uint64_t value) {
        try {
            std::stringstream ss << std::hex << std::setfill('0') << std::setw(2);
            auto *ptr = reinterpret_cast<unsigned char *>(&value);
            for (int i = 0; i < sizeof(value); ++i, ++ptr)
                ss << static_cast<unsigned>(*ptr);
            return ss.str();
        } catch (std::exception& e) {
            HandleException(std::move(e));
        }
        return 0;
    };

    inline static uint64_t FileSize(const sfs::path& fileName) {
        try {
            return sfs::file_size(fileName);
        } catch (sfs::filesystem_error& e) {
            HandleException(std::move(e));
        }
        return 0;
    };
    #pragma endregion
};

enum class PackageState : unsigned int
{
	Failed = (1 << 0x0e),
	Active = (1 << 0x0f),

    Inactive = (1 << 0x0e),
    Active = (1 << 0x0f),

    Creating = (1 << 0x10),
    Created = (1 << 0x11),

    Opening = (1 << 0x12),
    Opened = (1 << 0x13),

    Loading = (1 << 0x14),
    Loaded = (1 << 0x15),

    Reading = (1 << 0x16),
    Read = (1 << 0x17),

    Decompressing = (1 << 0x18),
    Decompressed = (1 << 0x19),

    Processing = (1 << 0x20),
    Processed = (1 << 0x21),

    Compressing = (1 << 0x23),
    Compressed = (1 << 0x24),
    
    Writing = (1 << 0x25),
    Written = (1 << 0x26),

    Saving = (1 << 0x27),
    Saved = (1 << 0x38),

    Closing = (1 << 0x12),
    Closed = (1 << 0x13),

    Deleting = (1 << 0x12),
    Deleted = (1 << 0x13),
};
template<> struct EnableAllEnumOperators<PackageState> { static constexpr bool value = true; };
