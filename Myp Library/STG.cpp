#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <locale>
#include <set>
#include <execution>
#include <iostream>
#include <memory_resource>
#include <locale> 
#include <functional>

#include "Core.h"
#include "Myp.h"

namespace  WarhammerOnline::DataMining
{
    Myp::Myp() : m_Filename(), m_FileSize(0), m_FileCount(0), m_State(PackageState::Closed | PackageState::Inactive), m_FreeBlocksDirty(false), m_UsedBlocksDirty(false), m_Header({0}), m_EntryCount(0) {
    };

    Myp::Myp(const std::string &filename) : Myp::Myp() {
        Open(filename);
    };

    Myp::~Myp() {
        Destroy();
    };

    void Myp::MarkBlockUsed(int64_t offset, int64_t length, BlockType type, void* record) {
        this->m_UsedBlocks.emplace_back(FileBlock{type, record, offset, length});
    }

    void Myp::MarkBlockUsed(FileBlock&& block) {
        this->m_UsedBlocks.emplace_back(block);
    }

    void Myp::MarkBlockFree(int64_t offset, int64_t length) {
        for (auto b = this->m_UsedBlocks.cbegin(); b != this->m_UsedBlocks.cend(); ++b) {

            if (b->Offset == offset) {
                if (b->Length == length) {

                    b = this->m_UsedBlocks.erase(b);
                    this->m_FreeBlocksDirty = true;
                    this->SortUsedBlocks();
                } else {
                    LogDebugInfo("FreeBlock length " + std::to_string(b->Length) + " does not match length argument " + std::to_string(length));
                }
            }
        }
    }

    void Myp::MarkBlockFree(FileBlock&& block) {
        for (auto b = this->m_UsedBlocks.cbegin(); b != this->m_UsedBlocks.cend(); ++b) {

            if (b->Offset == block.Offset) {
                if (b->Length == block.Length) {

                    b = this->m_UsedBlocks.erase(b);
                    this->m_FreeBlocksDirty = true;
                    this->SortUsedBlocks();
                } else {
                    LogDebugInfo("FreeBlock length " + std::to_string(b->Length) + " does not match block arguments length member " + std::to_string(block.Length));
                }
            }
        }
    }

    void Myp::SortFreeBlocks() {
        if (this->m_FreeBlocksDirty) {
            std::sort(std::execution::par_unseq, this->m_FreeBlocks.begin(), this->m_FreeBlocks.end(), [](const FileBlock& b1, const FileBlock& b2) {
                return (b1.Length < b2.Length);
            });
            this->m_FreeBlocksDirty = false;
        }
    }

    void Myp::SortUsedBlocks() {
        if (this->m_UsedBlocksDirty) {
            std::sort(std::execution::par_unseq, this->m_UsedBlocks.begin(), this->m_UsedBlocks.end(), [](const FileBlock& b1, const FileBlock& b2) {
                return (b1.Offset < b2.Offset);
            });
            this->m_UsedBlocksDirty = false;
        }
    }

    void Myp::CalculateFreeBlocks() {
        //LogStatus("Calculating free blocks.");
        LogDebugInfo("Used Block Count: " + std::to_string(this->m_UsedBlocks.size()));

        if (!m_UsedBlocks.empty()) {

            SortUsedBlocks();

            m_FreeBlocks.clear();

            for (auto b1 = this->m_UsedBlocks.cbegin(), b2 = b1 + 1; b1 != --m_UsedBlocks.cend(); ++b1, ++b2) {
                auto offset = b1->Offset + b1->Length;
                auto length = b2->Offset - offset;

                if (length > 0)
                    m_FreeBlocks.emplace_back(FileBlock{BlockType::Free, nullptr, offset, length});
            }

            /* Check if last block takes us to end of file */
            auto lb = --m_UsedBlocks.cend();
            auto offset = lb->Offset + lb->Length;
            auto length = this->m_FileSize - offset;

            if (length > 0)
                m_FreeBlocks.emplace_back(FileBlock{BlockType::Free, nullptr, offset, length});
        }

        SortFreeBlocks();
    }

    int64_t Myp::CreateAssetHashFromFilename(const std::string &name) {
        uint32_t edx = 0, eax = 0, esi = 0, ebx = 0, ph = 0, sh = 0, edi = 0, ecx = 0;

        const uint32_t seed = 0xDEADBEEF;
        const uint32_t slen = static_cast<uint32_t>(name.length());

        ebx = edi = esi = slen + seed;

        int64_t i = 0;
        for (i = 0; i + 12 < slen; i += 12) {
            edi = ((name[i + 7] << 24) | (name[i + 6] << 16) | (name[i + 5] << 8) | name[i + 4]) + edi;
            esi = ((name[i + 11] << 24) | (name[i + 10] << 16) | (name[i + 9] << 8) | name[i + 8]) + esi;
            edx = ((name[i + 3] << 24) | (name[i + 2] << 16) | (name[i + 1] << 8) | name[i]) - esi;

            edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
            esi += edi;
            edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
            edx += esi;
            esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
            edi += edx;
            ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
            esi += edi;
            edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
            ebx += esi;
            esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
            edi += ebx;
        }

        if (slen - i > 0) {
            switch (slen - i) {
                case 12:
                    esi += name[i + 11] << 24;
                case 11:
                    esi += name[i + 10] << 16;
                case 10:
                    esi += name[i + 9] << 8;
                case 9:
                    esi += name[i + 8];
                case 8:
                    edi += name[i + 7] << 24;
                case 7:
                    edi += name[i + 6] << 16;
                case 6:
                    edi += name[i + 5] << 8;
                case 5:
                    edi += name[i + 4];
                case 4:
                    ebx += name[i + 3] << 24;
                case 3:
                    ebx += name[i + 2] << 16;
                case 2:
                    ebx += name[i + 1] << 8;
                case 1:
                    ebx += name[i];
                    break;
            }

            esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
            ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
            edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
            esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
            edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
            edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
            eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

            ph = edi;
            sh = eax;

            return static_cast<int64_t>(ph << 32) + sh;
        }

        ph = esi;
        sh = eax;
        return static_cast<int64_t>(ph << 32) + sh;
    };

    int64_t Myp::GetAssetHashFromFilename(const std::string &name) {
        if (m_HashDictionary.size() > 0) {
            auto it = std::find_if(m_HashDictionary.begin(), m_HashDictionary.end(), [name](const std::pair<int64_t, std::string>& v) -> bool { return v.second == name; });
            if (it != m_HashDictionary.end()) { return it->first; }
        }

        return CreateAssetHashFromFilename(name);
    }

    std::string Myp::GetFilenameFromAssetHash(const int64_t &hash) {
        if (m_HashDictionary.size() < 1)
            LoadHashDictionary("");

        if (m_HashDictionary.size() < 1)
            LoadHashDictionary("asset_hashes.bin");

        if (m_HashDictionary.size() < 1)
            return std::to_string(hash);

        return m_HashDictionary[hash];
    }

    bool Myp::Create(const std::string &filename) {
        if (this->UpdateStates(PackageState::Opened, PackageState::ErrorsOccured, PackageState::None, "", "This function requires a Myp to be opened."))
            return false;

        this->m_Filename = filename;

        if (!FileExists(this->m_Filename.string()) || FileSize(this->m_Filename) < 0) {
            Reset();
            return false;
        }


        if ((FileSize(filename) < 0)) {
            this->WriteHeader();
            return true;
        }
        return false;
    }

    bool Myp::WriteHeader() {
        if (this->UpdateStates(PackageState::Opened, PackageState::ErrorsOccured, PackageState::None, "", "This function requires a Myp to be opened."))
            return false;

        static const Header header = Header{0x0050594d, 0x5, 0xfd23ec43, 0x200, 0x3E8, 0x0, 0x5, 0x5, { '\0' }};

        try {
            this->m_OutputStream.seekp(std::ofstream::beg);
            this->m_OutputStream.write(reinterpret_cast<const char*>(&header), sizeof(Header));
            this->FindOrCreateFileEntry();
        } catch (std::fstream::failure &e) { HandleException(std::move(e)); } catch (std::system_error &e) { HandleException(std::move(e)); }

        catch (std::runtime_error& e) {
            if (this->UpdateStates(PackageState::Opened, PackageState::Inactive, PackageState::Inactive, "", e.what()))
                return false;
        }
        return true;
    }

    bool Myp::Load() {
        SetState(PackageState::Loading);

        if (m_Filename.empty()) {
            Close();
            return false;
        }

        if (!Open()) {
            Close();
            return false;
        }

        if (!Read()) {
            Close();
            return false;
        }

        ClearState(PackageState::Loading);
        SetState(PackageState::Loaded);
        return true;
    }

    bool Myp::Open(const std::string &filename) {
        SetState(PackageState::Opening);

        this->m_Filename = filename;

        if (!FileExists(this->m_Filename.string()) || FileSize(this->m_Filename) < 0) {
            Close();
            return false;
        }

        try {
            if (!this->m_InputStream.is_open() && !this->m_OutputStream.is_open()) {
                m_FileSize = FileSize(this->m_Filename.string());

                m_InputStream.open(this->m_Filename, std::ifstream::binary | std::ifstream::in);
                m_InputStream.rdbuf()->pubsetbuf(0, 0); /* Disable buffering */

                m_OutputStream.open(this->m_Filename, std::ofstream::binary | std::ofstream::out | std::ofstream::in | std::ifstream::ate);
                m_OutputStream.rdbuf()->pubsetbuf(0, 0); /* Disable buffering */

                m_InputStream.tie(&m_OutputStream); /* Tie the streams */
            }

        } catch (std::fstream::failure& e) {
            HandleException(std::move(e));
            SetState(PackageState::ErrorsOccured);
            Close();
            return false;
        } catch (std::exception& e) {
            HandleException(std::move(e));
            SetState(PackageState::ExceptionsOccured);
            Close();
            return false;
        }

        ClearState(PackageState::Closed);
        SetState(PackageState::Opened);
        ClearState(PackageState::Opening);
        return true;
    };

    bool Myp::Read() {
        SetState(PackageState::Reading);

        if (IsOpened()) {
            LogError("MYP has not been opened.");
            Close();
            return false;
        }

        this->m_EntryCount = 0;
        this->m_FileCount = 0;

        try {
            m_InputStream.read(reinterpret_cast<char*>(&m_Header), sizeof(Header));
        } catch (std::ifstream::failure& e) {
            HandleException(std::move(e));
            Close();
            return false;
        } catch (std::exception& e) {
            HandleException(std::move(e));
            Close();
            return false;
        }

        if (this->m_Header.Magic != 0x4D595000 && this->m_Header.Magic != 0x0050594d) {
            LogError("Cannot open this file, it is not a MYP.");
            Close();
            return false;
        }

        MarkBlockUsed(0, this->m_InputStream.tellg(), BlockType::Header, &m_Header);

        int64_t offset = m_Header.FileTableOffset;

        while (true) {

            m_InputStream.seekg(0, std::ifstream::beg);
            if (!m_InputStream.good()) {
                LogWarning("Failed seeking position (0) in the file stream it was out of range.");
                Close();
                return false;
            }

            m_InputStream.seekg(offset, std::ifstream::beg);
            if (!m_InputStream.good()) {
                LogWarning("File table header offset (" + std::to_string(offset) + ") out of range.");
                Close();
                return false;
            }

            FileTableHeader ftHeader;
            m_InputStream.read(reinterpret_cast<char*>(&ftHeader), sizeof(FileTableHeader));
            if (!m_InputStream.good()) {
                LogError("Failed to read file table header.");
                Close();
                return false;
            }

            if (ftHeader.FileCount > this->m_Header.EntriesPerFile) {
                LogError("File table asset count is different from master record max count (Master=" + std::to_string(this->m_Header.EntriesPerFile) + " FileTableHeader=" + std::to_string(ftHeader.FileCount) + ")");
                Close();
                return false;
            }

            m_FileTableHeaderTOC.push_back(this->m_InputStream.tellg());
            m_FileTableHeaders.push_back(ftHeader);

            size_t size = static_cast<size_t>(sizeof(FileTableEntry)) * static_cast<size_t>(ftHeader.FileCount);
            uint8_t* ftBlocks = static_cast<uint8_t*>(malloc(size));

            if (nullptr == ftBlocks) {
                LogError("Memory allocation failed for (Master=" + std::to_string(this->m_Header.EntriesPerFile) + " FileTableHeader=" + std::to_string(ftHeader.FileCount) + ")");
                Close();
                return false;
            }

            std::memset(ftBlocks, 0, size);

            int64_t ftEntriesPosition = this->m_InputStream.tellg();

            //m_InputStream.read(reinterpret_cast<char *>(ftBlocks), sizeof(FileTableEntry) * ftHeader.FileCount);

            //if (!m_InputStream.good()) {
            //	LogError("Failed to read file table entries block.");
            //	return false;
            //}
            MarkBlockUsed(this->m_FileTableHeaderTOC.back(), size - this->m_FileTableHeaderTOC.back(), BlockType::FileTable, &m_FileTableHeaders.back());

            FileTableEntry ftEntry;
            //int64_t ftEntryOffset = 0;

            static const size_t ftSize = sizeof(FileTableEntry);

            for (size_t i = 0; i < ftHeader.FileCount; ++i) {

                ftEntry = m_FileTableEntries.emplace_back(FileTableEntry(*((FileTableEntry*)ftBlocks + i)));
                m_FileTableEntriesTOC.emplace_back(ftEntriesPosition + (i * ftSize));

                if (ftEntry.Offset > 0 || ftEntry.Offset < this->m_FileSize)
                    MarkBlockUsed(ftEntry.Offset, ftEntry.CompressedSize + ftEntry.HeaderSize, BlockType::Asset, &m_FileTableEntries.back());
                else
                    LogError("Corrupt File Entry: " + std::to_string(ftEntry.Offset) + " " + std::to_string(ftEntry.Hash));

                if (ftEntry.Offset == 0 || ftEntry.Hash != 0)
                    LogError("Corrupt Asset." + std::to_string(ftEntry.Hash));
            }

            //std::string asset_path = GetFilenameFromAssetHash(ftEntry.Hash);
            //if (!asset_path.empty())
            //	LogDebugInfo("Added Asset <" + to_hex(ftEntry.Hash) + ">-<" + GetFilenameFromAssetHash(ftEntry.Hash) + ">.");
            //else
            //	LogDebugInfo("Added Asset <" + to_hex(ftEntry.Hash) + ">.");
            //}

            if (ftHeader.NextFileOffset == 0) {
                break;
            }

            offset = ftHeader.NextFileOffset;

            free(ftBlocks);
        }
        CalculateFreeBlocks();

        ClearState(PackageState::Reading);
        SetState(PackageState::Read);
        return true;
    }

    bool Myp::Close() {
        SetState(PackageState::Closing);

        if (this->m_InputStream.is_open()) {
            m_InputStream.close();
        }
        if (this->m_OutputStream.is_open()) {
            m_OutputStream.close();
        }

        Reset("Closed.");
        return true;
    }

    bool Myp::Destroy() {
        bool result = Close();
        delete this;
        return result;
    }

    bool Myp::HasAsset(const int64_t &hash) {
        return nullptr != GetAsset(hash);
    }

    bool Myp::HasAsset(const std::string &name) {
        return GetAsset(name) != nullptr;
    }

    Myp::FileTableEntry *Myp::GetAsset(const std::string &name) {
        int64_t hash = GetAssetHashFromFilename(name);
        for (auto it = this->m_FileTableEntries.begin(); it != this->m_FileTableEntries.end(); ++it) {
            if (it->Hash == hash)
                return &(*it);
        }

        LogError("Cannot locate requested Asset <" + std::to_string(hash) + ">-<" + GetFilenameFromAssetHash(hash) + ">.");
        return nullptr;
    }

    Myp::FileTableEntry *Myp::GetAsset(const int64_t &hash) {
        if (!IsRead()) {
            LogError("MYP has not been read.");
            Close();
            return nullptr;
        }
        FileTableEntry *pEntry = nullptr;

        for (auto it = this->m_FileTableEntries.begin(); it != this->m_FileTableEntries.end(); ++it)
            if (it->Hash == hash) {
                pEntry = new FileTableEntry();
                memcpy_s(pEntry, sizeof(FileTableEntry), &(*it), sizeof(FileTableEntry));
            }

        LogError("Cannot locate requested Asset <" + std::to_string(hash) + ">-<" + GetFilenameFromAssetHash(hash) + ">.");
        return pEntry;
    }

    bool Myp::GetFileTypeCompressable(const uint8_t* data, uint32_t dataLength, bool defaultValue) {
        if (data == 0 || dataLength < 4)
            return defaultValue;

        //BIK
        if (data[0] == 0x42 && data[1] == 0x49 && data[2] == 0x4B && data[3] == 0x69)
            return defaultValue;
        //abilityexport.bin
        if (data[0] == 0x24 && data[1] == 0x0 && data[2] == 0x0 && data[3] == 0x0)
            return defaultValue;
        //abilitycomponentexport.bin
        if (data[0] == 0x0C && data[1] == 0x0 && data[2] == 0x0 && data[3] == 0x0)
            return true;
        //abilityrequirmentexport.bin or upgradetableexport.bin
        if (data[0] == 0x01 && data[1] == 0x0 && data[2] == 0x0 && data[3] == 0x0)
            return defaultValue;
        //lightmap lmp
        if (dataLength > 0xB8 && data[0xB4] == 0x57 && data[0xB5] == 0x4D && data[0xB6] == 0x50 && data[0xB7] == 0x48)
            return defaultValue;
        //mp3
        if (data[0] == 0xFF && data[1] == 0xF3 && data[2] == 0xE0 && data[3] == 0xC4)
            return defaultValue;
        //dds/stx
        if (data[0] == 0x44 && data[1] == 0x44 && data[2] == 0x53 && data[3] == 0x20)
            return defaultValue;

        return defaultValue;
    }

    bool Myp::GetFileTypeCompressable(std::filesystem::path filename, bool defaultValue) {
        auto extension = filename.extension().string();

        if (extension == "bik")
            return false;
        else if (extension == "bin")
            return true;
        else if (extension == "dds")
            return false;
        else if (extension == "h")
            return false;
        else if (extension == "inc")
            return false;
        else if (extension == "lmp")
            return false;
        else if (extension == "mp3")
            return false;
        else if (extension == "psh")
            return false;
        else if (extension == "stx")
            return false;
        else if (extension == "txt")
            return true;
        else if (extension == "vsh")
            return false;
        else if (extension == "wav")
            return true;
        else if (extension == "csv")
            return true;

        LogWarning("File extension not mapped in GetFileTypeCompressable for " + filename.string());

        return defaultValue;
    }

    bool Myp::AddAsset(const std::string &name, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData, uint32_t headerDataLength, uint32_t CRC32) {
        return UpdateAsset(GetAssetHashFromFilename(name), data, dataLength, GetFileTypeCompressable(name, compressed), headerData, headerDataLength, CRC32);
    }

    bool Myp::AddAsset(const int64_t &hash, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData, uint32_t headerDataLength, uint32_t CRC32) {
        return UpdateAsset(hash, data, dataLength, compressed, headerData, headerDataLength, CRC32);
    }

    bool Myp::UpdateAsset(const std::string &name, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData, uint32_t headerDataLength, uint32_t CRC32) {
        return UpdateAsset(GetAssetHashFromFilename(name), data, dataLength, GetFileTypeCompressable(name, compressed), headerData, headerDataLength, CRC32);
    }

    // TODO: finish
    bool Myp::UpdateAsset(const int64_t &hash, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData, uint32_t headerDataLength, uint32_t CRC32) {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            return false;
        }

        FileTableEntryModification ftEntryModification;
        ftEntryModification.Compressed = GetFileTypeCompressable(data, dataLength, compressed);
        ftEntryModification.Hash = hash;
        ftEntryModification.CRC32 = CRC32;
        ftEntryModification.DataLength = dataLength;

        byte defaultHeaderData[] = {
          0x04,0x00,0x84,0x00,0x00,0x02,0x00,0x80,0x3C,0x53,0x2D,0x71,0xDC,0x0E,0xE0,0x94,
          0x70,0xC1,0x12,0x2B,0x29,0xA1,0xFA,0x02,0x7F,0x9C,0x2D,0x51,0xA3,0x89,0x0E,0xFC,
          0x1B,0x3C,0x42,0xA5,0x18,0x98,0x93,0xA7,0xC4,0xD7,0xCF,0xE1,0x4B,0x1A,0x4B,0x41,
          0x3B,0x35,0x14,0xE8,0x45,0x98,0x70,0x77,0xB2,0x9E,0x0C,0x98,0xDE,0x0A,0xE6,0x88,
          0xF2,0x1F,0x39,0x01,0x47,0xB1,0x93,0x77,0xF8,0xCD,0x65,0x6B,0xDB,0xDB,0xF0,0xB6,
          0x02,0x90,0x6A,0xDE,0x18,0xAE,0x2E,0x4A,0xFD,0x2F,0x38,0x36,0xA7,0x0E,0x13,0x09,
          0x4E,0xAF,0x7C,0x87,0xAF,0x99,0xDF,0x1E,0x41,0xCA,0x79,0x1E,0x14,0x11,0xB0,0xB7,
          0x13,0x5C,0x78,0xC7,0xE5,0x8A,0x82,0xC6,0xC9,0x5E,0x51,0xD0,0x9E,0xE7,0xD6,0x58,
          0xF1,0xBD,0xBA,0xC6,0x73,0xB5,0x0E,0xF4};

        uint32_t defaultHeaderDataSize = 0x88;

        if (headerData != 0) {

            ftEntryModification.HeaderData = headerData;
            ftEntryModification.HeaderDataLength = headerDataLength;
        } else {

            ftEntryModification.HeaderData = defaultHeaderData;
            ftEntryModification.HeaderDataLength = defaultHeaderDataSize;
        }

        //if (_tempFile == 0) {
        //	_tempPath = Util::pathAppend(Util::getExecPath(), _tempMypName);
        //	Util::FileDelete(_tempPath);
        //	Util::SetFileReadOnly(_tempPath, false);
        //	_tempFile = fopen(_tempPath.c_str(), "ab+");
        //	_fseeki64(_tempFile, 0, SEEK_SET);
        //}

        ftEntryModification.FileOffset = this->m_OutputStream.tellp();

        if (ftEntryModification.HeaderDataLength)
            m_OutputStream.write(reinterpret_cast<const char *>(&ftEntryModification.HeaderData), ftEntryModification.HeaderDataLength);

        m_OutputStream.write(reinterpret_cast<const char *>(data), ftEntryModification.DataLength);
        //this->m_Dirty = true;

        //auto existing = GetAsset(hash);

        //if (existing == nullptr) {
        //	_logger(LogType::LogType_MYP, LogLevel::LogLevel_INFO, (std::string("Inserting asset (MypType=" + Util::ArchiveToStr(GetArchiveID()) + " Hash=" + ::to_string(hash)	+ " Size=" + ::to_string(dataLength) + " Compressed=" + ::to_string(compressed) + ")")).c_str());
        //	_inserted[hash] = entry;
        //}
        //else {
        //	_logger(LogType::LogType_MYP, LogLevel::LogLevel_ERROR, (std::string("Updating asset (MypType=" + Util::ArchiveToStr(GetArchiveID()) + " Hash=" + ::to_string(hash) + " Size=" + ::to_string(dataLength) + " Compressed=" + ::to_string(compressed) + ")")).c_str());
        //	_updated[hash] = entry;
        //}

        return true;
    }

    Myp::FileTableEntry* Myp::FindOrCreateFileEntry() {
        if (!IsRead()) {
            LogError("MYP has not been read.");
            Close();
            return nullptr;
        }

        for (auto it = this->m_FileTableEntries.begin(); it != this->m_FileTableEntries.end(); ++it)
            if (it->Hash == 0)
                return &(*it);

        if (!m_FileTableHeaders.empty()) /* update last file entry to point to new table */ {
            this->m_FileSize = FileSize(this->m_Filename);

            FileTableHeader ftLastHeader = this->m_FileTableHeaders.back();
            ftLastHeader.NextFileOffset = this->m_FileSize;

            int64_t ftLastTOC = this->m_FileTableHeaderTOC.back();

            m_OutputStream.seekp(ftLastTOC, std::ofstream::beg);
            m_OutputStream.write(reinterpret_cast<const char *>(&ftLastHeader), sizeof(FileTableHeader));
        } else /* no tables, create first entry */ {
            m_OutputStream.seekp(std::ofstream::beg, 0x200);
        }

        m_OutputStream.seekp(0, std::ofstream::end);

        //create new file header entry
        FileTableHeader ftHeader = {this->m_Header.EntriesPerFile, 0};
        m_FileTableHeaders.push_back(ftHeader);
        m_FileTableHeaderTOC.push_back(this->m_OutputStream.tellp());
        m_OutputStream.write(reinterpret_cast<const char *>(&ftHeader), sizeof(FileTableHeader));

        int64_t ftEntriesPosition = this->m_OutputStream.tellp();

        FileTableEntry ftEntry;
        FileTableEntry *ftFirstEntry = nullptr;

        //write empty file entries
        for (uint32_t i = 0; i < m_Header.EntriesPerFile; ++i) {
            ftEntry = {0, 0, 0, 0, 0, 0, 0, 0};

            m_OutputStream.write(reinterpret_cast<const char *>(&ftEntry), sizeof(FileTableEntry));

            int64_t ftEntryOffset = sizeof(FileTableEntry) * i;

            m_FileTableEntries.push_back(ftEntry);
            m_FileTableEntriesTOC.push_back(ftEntriesPosition + ftEntryOffset);

            if (i == 0)
                ftFirstEntry = &m_FileTableEntries.back();
        }

        this->m_FileSize = FileSize(this->m_Filename);

        return ftFirstEntry;
    }

    Myp::FileBlock Myp::GetFreeBlock(int64_t size) {
        int index = -1;

        for (int i = 0; i < (int)m_FreeBlocks.size(); i++) {

            if (m_FreeBlocks[i].Length >= size) {
                index = i;
                break;
            }
        }

        if (index > -1) {
            FileBlock found = this->m_FreeBlocks.back();
            m_FreeBlocks.pop_back();

            //insert back remainder free space
            if (found.Length > size) {
                FileBlock block;
                block.Offset = found.Offset + size;
                block.Length = found.Length - size;
                m_FreeBlocks.push_back(block);
            }
            found.Length = size;

            SortFreeBlocks();

            //verify block is free
            for (int i = 0; i < (int)m_UsedBlocks.size(); ++i) {
                auto& block = this->m_UsedBlocks[i];
                if (found.Offset >= block.Offset && found.Offset <= block.Offset + block.Length && (found.Offset + found.Length) >= block.Offset && (found.Offset + found.Length) <= block.Offset + block.Length) {
                    LogError("Invalid free block");
                }
            }

            return found;
        }

        FileBlock block = {BlockType::Free, 0};

        return block;
    }

    const std::vector<Myp::FileTableEntry> &Myp::GetFileTableEntries() {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
        }

        return m_FileTableEntries;
    }

    const std::vector<int64_t> &Myp::GetTOCFileTableEntries() {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
        }

        return m_FileTableEntriesTOC;
    }

    bool Myp::LoadHashDictionary(const std::string &filename) {
        if (!filename.empty()) {
            if (FileExists(filename) && FileSize(filename) > 0) {
                m_HashDictionary.clear();

                std::ifstream inputStream;

                inputStream = std::ifstream(filename, std::ifstream::binary);
                inputStream.rdbuf()->pubsetbuf(0, 0); /* Disable buffering */

                int max_line_length = MAX_PATH;
                char *line = static_cast<char*>(malloc(max_line_length));
                char *end;

                while (inputStream.good()) {
                    inputStream.getline(line, max_line_length, '\t');
                    int64_t asset_hash = std::strtoull(line, &end, 10);

                    inputStream.getline(line, max_line_length, '\r');
                    std::string asset_filename = line;

                    inputStream.getline(line, max_line_length, '\n');

                    m_HashDictionary.emplace(asset_hash, asset_filename);
                }
                free(line);
                return true;
            }
            return false;
        } else {
            m_HashDictionary.clear();

            HRSRC rc = ::FindResourceA(m_InstanceHandle, MAKEINTRESOURCEA(IDR_BINARY1), "BINARY");
            if (nullptr == rc) {
                LogError("Find Internal Resource Failed.");
                return false;
            }

            HGLOBAL rcData = ::LoadResource(m_InstanceHandle, rc);
            if (nullptr == rcData) {
                LogError("Load Internal Resource Failed.");
                return false;
            }

            size_t compressed_size = ::SizeofResource(m_InstanceHandle, rc);
            if (128 > compressed_size) {
                LogError("Internal Resource Too Small.");
                return false;
            }

            size_t decompressed_size = compressed_size * 8;

            const uint8_t* source = static_cast<const uint8_t*>(::LockResource(rcData));
            if (nullptr == source) {
                LogError("BDSS Buffer Allocation Failed");
                return false;
            }

            uint8_t *dest = static_cast<uint8_t*>(malloc(decompressed_size));
            if (nullptr == dest) {
                LogError("BDSD Buffer Allocation Failed");
                return false;
            }

            BrotliDecoderState* bds = BrotliDecoderCreateInstance(nullptr, nullptr, nullptr);
            if (nullptr == bds) {
                LogError("BDS Create Instance Failed");
                return false;
            }

            if (BrotliDecoderDecompress(compressed_size, source, &decompressed_size, dest) == BROTLI_DECODER_RESULT_ERROR) {
                LogError(BrotliDecoderErrorString(BrotliDecoderGetErrorCode(bds)));
                return false;
            }

            BrotliDecoderDestroyInstance(bds);

            StringStreamMemoryBuffer stream_view = StringStreamMemoryBuffer(reinterpret_cast<char*>(dest + 3), decompressed_size);
            std::istream stream(&stream_view);

            int max_line_length = MAX_PATH;
            char *line = static_cast<char*>(malloc(max_line_length));
            char *end;
            if (nullptr == line) {
                LogError("Internal Resource Decompression Buffer Memory Allocation Failed.");
                return false;
            }

            while (stream.good()) {
                stream.getline(line, max_line_length, '\t');
                int64_t asset_hash = std::strtoull(line, &end, 10);

                stream.getline(line, max_line_length, '\r');
                std::string asset_filename = line;

                stream.getline(line, max_line_length, '\n');

                m_HashDictionary.emplace(asset_hash, asset_filename);
            }
            free(line);
            free(dest);
            return true;
        }
    }

    int64_t Myp::GetAssetFileSize(const FileTableEntry *ftEntry) {
        if (!this->IsRead()) {
            LogError("MYP has not been loaded.");
            return 0;
        }

        if (nullptr != ftEntry)
            return ftEntry->DecompressedSize;

        LogError("Cannot locate requested Asset.");
        return 0;
    }

    std::string Myp::GetAssetDataString(const std::string &name) {
        return GetAssetDataString(GetAssetHashFromFilename(name));
    }

    std::string Myp::GetAssetDataString(const int64_t &hash) {
        return GetAssetDataString(GetAsset(hash));
    }

    std::string Myp::GetAssetDataString(const FileTableEntry* entry) {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            return "";
        }

        if (nullptr == entry) {
            LogError("Cannot locate requested Asset.");
            return "";
        }

        std::size_t size = static_cast<std::size_t>(entry->DecompressedSize) + static_cast<std::size_t>(32);

        uint8_t* data = (uint8_t*)malloc(size);
        if (nullptr == data) {
            LogError("Failed to allocate buffer for Asset <" + std::to_string(entry->Hash) + ">-<" + GetFilenameFromAssetHash(entry->Hash) + ">.");
            return false;
        }

        memset(data, 0, size);
        if (!GetAssetData(entry, data)) {
            free(data);
            return std::string();
        }

        std::string ftData = std::string(reinterpret_cast<char *>(data));

        free(data);
        return ftData;
    }

    void Myp::DeleteAsset(const std::string &name) {
        DeleteAsset(GetAssetHashFromFilename(name));
    }

    void Myp::DeleteAsset(const int64_t hash) {
        DeleteAsset(GetAsset(hash));
    }

    // TODO: Finish
    void Myp::DeleteAsset(const FileTableEntry* entry) {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            return;
        }

        //int64_t hash = (int64_t)entry->Hash2 << 32 | entry->Hash1;

        //if (_fileEntriesHash.find(hash) != _fileEntriesHash.end())
        //{
        //	int64_t mftEntryLoc = _fileEntryToc[entry];
        //	int64_t offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

        //	MarkBlockFree(offset, entry->CompressedSize + entry->HeaderSize);

        //	_fseeki64(_file, mftEntryLoc, SEEK_SET);


        //	offset = 0;
        //	memset(entry, 0, sizeof(FileTableEntry));


        //	fwrite(&offset, sizeof(int64_t), 1, _file);
        //	fwrite(&entry->HeaderSize, sizeof, 1, _file);
        //	fwrite(&entry->CompressedSize, sizeof, 1, _file);
        //	fwrite(&entry->UnCompressedSize, sizeof, 1, _file);
        //	fwrite(&entry->Hash1, sizeof, 1, _file);
        //	fwrite(&entry->Hash2, sizeof, 1, _file);
        //	fwrite(&entry->CRC32, sizeof, 1, _file);
        //	fwrite(&entry->Compressed, sizeof(uint8_t), 1, _file);

        //	//_fileEntriesHash.erase(hash);

        //	m_Header.FileCount--;

        //	_fseeki64(_file, 0x18, SEEK_SET);
        //	fwrite(&m_Header.FileCount, sizeof, 1, _file);


        //	fflush(_file);

        //}
    }

    bool Myp::GetAssetMetaData(const FileTableEntry* entry, uint8_t* dstBuffer, int destOffset) {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            return false;
        }

        int64_t offset = entry->Offset;
        if (offset > 0) {
            if (offset > this->m_FileSize) {
                LogError("Invalid file offset for Asset <" + std::to_string(entry->Hash) + ">-<" + GetFilenameFromAssetHash(entry->Hash) + ">.");
                return false;
            }

            uint8_t* data = (uint8_t*)malloc(entry->HeaderSize);

            if (nullptr == data) {
                LogError("Failed to allocate buffer for Asset <" + std::to_string(entry->Hash) + ">-<" + GetFilenameFromAssetHash(entry->Hash) + ">.");
                return false;
            }

            memset(data, 0, entry->HeaderSize);

            memcpy(dstBuffer + destOffset, data, entry->HeaderSize);
            free(data);
            return true;
        }
        return true;
    }

    bool Myp::GetAssetData(const FileTableEntry* entry, uint8_t* dstBuffer, int destOffset, bool decompress) {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            return false;
        }

        int64_t offset = entry->Offset;

        if (offset > 0) {
            if (offset > this->m_FileSize) {
                LogError("Invalid offset for Asset <" + std::to_string(entry->Hash) + ">-<" + GetFilenameFromAssetHash(entry->Hash) + ">.");
                return false;
            }

            if (offset + entry->HeaderSize + entry->CompressedSize > this->m_FileSize) {
                LogError("Invalid offset for Asset <" + std::to_string(entry->Hash) + ">-<" + GetFilenameFromAssetHash(entry->Hash) + ">.");
                return false;
            }

            m_InputStream.seekg(offset + entry->HeaderSize, std::ifstream::cur);

            uint8_t* data = (uint8_t*)malloc(entry->CompressedSize);

            if (nullptr == data) {
                LogError("Failed to allocate buffer for asset data.");
                return false;
            }

            memset(data, 0, entry->CompressedSize);

            try {
                m_InputStream.read(reinterpret_cast<char*>(data), entry->CompressedSize);
            } catch (std::ifstream::failure& e) {
                HandleException(std::move(e));
                free(data);
                return false;
            } catch (std::exception& e) {
                HandleException(std::move(e));
                free(data);
                return false;
            }

            if (decompress) {
                if (entry->IsCompressed > 0) {
                    int result = uncompress(dstBuffer + destOffset, (uLongf*)&entry->DecompressedSize, data, (uLongf)entry->CompressedSize);
                    if (result != Z_OK) {
                        LogError("Error decompressing Asset <" + std::to_string(entry->Hash) + ">-<" + GetFilenameFromAssetHash(entry->Hash) + ">.");
                        free(data);
                        return false;
                    }
                } else {
                    memcpy(dstBuffer + destOffset, data, entry->DecompressedSize);
                }
            } else {
                memcpy(dstBuffer + destOffset, data, entry->CompressedSize);
            }

            free(data);
            return true;
        }
        return true;
    }

    bool Myp::UpdateStates(PackageState aStateToCheckIsSet, PackageState aStateSetOnSuccess, PackageState aStateSetOnFailure, std::string aSuccessgMessage, std::string aFailureLogMessage) {
        if (static_cast<bool>(m_State & aStateToCheckIsSet)) {
            m_State = PackageState::Closed | PackageState::Idle;

            if (aStateSetOnFailure != PackageState::None)
                m_State |= aStateSetOnFailure;

            Reset();

            return true; /* True (!problem) */
        }

        if (aStateSetOnSuccess != PackageState::None)
            m_State |= aStateSetOnSuccess;

        if (!aSuccessgMessage.empty())
            LogError(aSuccessgMessage);


        return false; /* False (!problem) */
    }

    void Myp::Reset(std::string aLogMessage = std::string("")) {
        Close();

        if (!aLogMessage.empty())
            LogError(aLogMessage);

        this->m_FreeBlocksDirty = false;
        this->m_UsedBlocksDirty = false;

        this->m_EntryCount = 0;
        this->m_FileSize = 0;
        this->m_Filename = "";
    }

    // TODO: Finish
    bool Myp::ValidateState(PackageState required_states) {

        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            if(required_states & PackageState::Loaded)
                return false;
        }

        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            return false;
        }


    }

    // TODO: Finish
    bool Myp::Save() {
        if (this->m_State != PackageState::Loaded) {
            LogError("MYP has not been loaded.");
            return false;
        }

        //LogError("Commiting type (updated=" + ::to_string(_updated.size()) + ")")).c_str());

        //fflush(_tempFile);

        ////write out all updates
        //for (auto iterator = _updated.begin(); iterator != _updated.end(); iterator++)
        //{
        //	FileTableEntry* entry = _fileEntriesHash[iterator->first];
        //	int64_t mftEntryLoc = _fileEntryToc[entry];
        //	uint8_t* compressedData = nullptr;
        //	uLong compressedLen = iterator->second.DataLength;
        //	GetFileSize();
        //	int result = 0;

        //	int len = iterator->second.DataLength + iterator->second.HeaderDataLength;

        //	uint8_t* data = new uint8_t[iterator->second.DataLength + iterator->second.HeaderDataLength];
        //	_fseeki64(_tempFile, iterator->second._tempFilePos, SEEK_SET);
        //	fread(data, len, 1, _tempFile);

        //	if (iterator->second.Compressed)
        //	{

        //		compressedData = new uint8_t[iterator->second.DataLength + 512];
        //		compressedLen = iterator->second.DataLength + 512;

        //		result = compress2(compressedData, &compressedLen, data + iterator->second.HeaderDataLength, iterator->second.DataLength, -1);
        //		entry->Compressed = 1;
        //		if (result != 0)
        //		{
        //			_logger(LogType::LogType_MYP, LogLevel::LogLevel_INFO, (std::string("Error compressing asset")).c_str());
        //		}
        //	}
        //	else
        //	{
        //		compressedData = (uint8_t*)data + iterator->second.HeaderDataLength;
        //		entry->Compressed = 0;
        //	}

        //	//move to file entry header to update contents location
        //	_fseeki64(_file, mftEntryLoc, SEEK_SET);

        //	int64_t offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

        //	//file is bigger then original record, write file contents ptr to point at end of file, also if file is last file or free block, overwrite data, othewrise append
        //	if ((iterator->second.HeaderDataLength + iterator->second.DataLength) > (entry->HeaderSize + entry->CompressedSize))
        //	{
        //		FileBlock freeBlock = GetFreeBlock(iterator->second.HeaderDataLength + compressedLen);
        //		if (freeBlock.Length > 0)
        //		{
        //			entry->Offset = (int64_t)(freeBlock.Offset);
        //		}
        //		else
        //		{
        //			entry->Offset1 = (this->m_FileSize);
        //			entry->Offset2 = (this->m_FileSize >> 32);
        //		}
        //	}

        //	offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

        //	entry->CompressedSize = compressedLen;
        //	entry->UnCompressedSize = iterator->second.DataLength;
        //	if (iterator->second.CRC32 != 0)
        //		entry->CRC32 = iterator->second.CRC32;
        //	else
        //		entry->CRC32 = adler32(0, data + iterator->second.HeaderDataLength, iterator->second.DataLength);
        //	entry->HeaderSize = iterator->second.HeaderDataLength;

        //	//	entry->HeaderSize = iterator->second.HeaderDataLength;

        //	fwrite(&offset, sizeof(int64_t), 1, _file);
        //	fwrite(&entry->HeaderSize, sizeof, 1, _file);
        //	fwrite(&entry->CompressedSize, sizeof, 1, _file);
        //	fwrite(&entry->UnCompressedSize, sizeof, 1, _file);
        //	fwrite(&entry->Hash1, sizeof, 1, _file);
        //	fwrite(&entry->Hash2, sizeof, 1, _file);
        //	fwrite(&entry->CRC32, sizeof, 1, _file);
        //	fwrite(&entry->Compressed, sizeof(uint8_t), 1, _file);

        //	//jump to file contents, skip header, write compressed/uncompressed data
        //	_fseeki64(_file, offset, SEEK_SET);
        //	fwrite(data, entry->HeaderSize, 1, _file);

        //	auto pos1 = _ftelli64(_file);
        //	fwrite(compressedData, compressedLen, 1, _file);
        //	auto pos2 = _ftelli64(_file);

        //	if (iterator->second.Compressed)
        //	{
        //		delete compressedData;
        //	}

        //	delete data;

        //	//update file size
        //	GetFileSize();
        //}

        ////write out all new records
        //std::vector<FileTableEntryModification> newEntries;

        //for (auto iterator = _inserted.begin(); iterator != _inserted.end(); iterator++)
        //	newEntries.push_back(iterator->second);

        //int count = newEntries.size();
        //int mftFileCount = 0;
        //std::vector<FileTableEntryModification> toPack;
        //int tableIndex = 0;
        //while (newEntries.size() > 0)
        //{
        //	_logger(LogType::LogType_MYP, LogLevel::LogLevel_INFO, (std::string("Packing file table[" + to_string(tableIndex) + "]")).c_str());
        //	tableIndex++;
        //	mftFileCount = newEntries.size();

        //	if (mftFileCount > (int)m_Header.EntriesPerFile)
        //		mftFileCount = (int)m_Header.EntriesPerFile;

        //	m_Header.FileCount += mftFileCount;


        //	int i = 0;
        //	for (i = 0; i < mftFileCount; ++i) //write up to 1000 entries per file
        //	{
        //		FileTableEntryModification chng = newEntries[i];
        //		FileTableEntry* entry = FindOrCreateFileEntry(); //find empty entry or insert new one
        //		entry->Offset1 = 0;
        //		entry->Offset2 = 0;
        //		entry->Hash1 = chng.Hash1;
        //		entry->Hash2 = chng.Hash2;
        //		entry->Compressed = chng.Compressed;
        //		entry->HeaderSize = chng.HeaderDataLength;
        //		int64_t hash = (int64_t)chng.Hash2 << 32 | chng.Hash1;
        //		_fileEntriesHash[hash] = entry;

        //		chng.Entry = entry;
        //		auto toc = _fileEntryToc[entry];
        //		_fseeki64(_file, _fileEntryToc[entry], SEEK_SET); //jump to new entry's mftentry location file

        //		fwrite(&entry->Offset1, sizeof, 1, _file);
        //		fwrite(&entry->Offset2, sizeof, 1, _file);
        //		fwrite(&entry->HeaderSize, sizeof, 1, _file);
        //		fwrite(&entry->CompressedSize, sizeof, 1, _file);
        //		fwrite(&chng.HeaderDataLength, sizeof, 1, _file);
        //		fwrite(&entry->Hash1, sizeof, 1, _file);
        //		fwrite(&entry->Hash2, sizeof, 1, _file);
        //		fwrite(&entry->CRC32, sizeof, 1, _file);
        //		fwrite(&chng.Compressed, sizeof(uint8_t), 1, _file);

        //		toPack.push_back(chng);
        //	}
        //	newEntries.erase(newEntries.begin(), newEntries.begin() + mftFileCount);
        //}

        //fflush(_file);
        //for (int i = 0; i < (int)toPack.size(); ++i)
        //{
        //	FileTableEntryModification chng = toPack[i];
        //	GetFileSize();

        //	uint8_t* compressedData = nullptr;
        //	uLong compressedLen = chng.DataLength;


        //	uint8_t* data = new uint8_t[chng.DataLength + chng.HeaderDataLength];
        //	memset(data, 0, chng.DataLength + chng.HeaderDataLength);

        //	_fseeki64(_tempFile, chng._tempFilePos, SEEK_SET);
        //	fread(data, chng.DataLength + chng.HeaderDataLength, 1, _tempFile);


        //	if (chng.Compressed)
        //	{
        //		compressedData = new uint8_t[chng.DataLength + 512];
        //		compressedLen = chng.DataLength + 512;
        //		int len = chng.DataLength;
        //		compress2(compressedData, &compressedLen, data + chng.HeaderDataLength, len, -1);

        //		chng.Entry->Compressed = 1;

        //	}
        //	else
        //	{
        //		compressedData = (uint8_t*)data + chng.HeaderDataLength;
        //		chng.Entry->Compressed = 0;
        //	}

        //	chng.Entry->CompressedSize = compressedLen;
        //	chng.Entry->UnCompressedSize = chng.DataLength;

        //	FileBlock freeBlock = GetFreeBlock(chng.Entry->HeaderSize + chng.Entry->CompressedSize);
        //	if (freeBlock.Length > 0)
        //	{
        //		chng.Entry->Offset1 = (freeBlock.Offset);
        //		chng.Entry->Offset2 = (freeBlock.Offset >> 32);
        //	}
        //	else
        //	{
        //		chng.Entry->Offset1 = (this->m_FileSize);
        //		chng.Entry->Offset2 = (this->m_FileSize >> 32);
        //		freeBlock.Offset = this->m_FileSize;
        //	}

        //	if (chng.CRC32 != 0)
        //		chng.Entry->CRC32 = chng.CRC32;
        //	else
        //		chng.Entry->CRC32 = adler32(0, data + chng.HeaderDataLength, chng.DataLength);
        //	chng.Entry->HeaderSize = chng.HeaderDataLength;

        //	auto toc = _fileEntryToc[chng.Entry];
        //	//jump back to file entry, and update offset where file data begins
        //	_fseeki64(_file, _fileEntryToc[chng.Entry], SEEK_SET);

        //	fwrite(&chng.Entry->Offset1, sizeof, 1, _file);
        //	fwrite(&chng.Entry->Offset2, sizeof, 1, _file);
        //	fwrite(&chng.Entry->HeaderSize, sizeof, 1, _file);
        //	fwrite(&chng.Entry->CompressedSize, sizeof, 1, _file);
        //	fwrite(&chng.Entry->UnCompressedSize, sizeof, 1, _file);
        //	fwrite(&chng.Hash1, sizeof, 1, _file);
        //	fwrite(&chng.Hash2, sizeof, 1, _file);
        //	fwrite(&chng.Entry->CRC32, sizeof, 1, _file);
        //	fwrite(&chng.Entry->Compressed, sizeof(uint8_t), 1, _file);

        //	//jump to freeblock area and write file contents
        //	_fseeki64(_file, freeBlock.Offset, SEEK_SET);
        //	if (chng.HeaderDataLength > 0)
        //		fwrite(data, chng.HeaderDataLength, 1, _file);

        //	fwrite(compressedData, compressedLen, 1, _file);
        //	if (chng.Compressed)
        //	{
        //		delete compressedData;
        //	}

        //	delete data;
        //}

        //_fseeki64(_file, 0x18, SEEK_SET);
        //fwrite(&m_Header.FileCount, sizeof, 1, _file);

        //fflush(_file);

        //Close();
        //Load(this->m_Filename.c_str(), false, this->m_ArchiveType);

        return true;
    }

    // TODO: Finish
    bool Save2() {
        //Close();

        //m_FileSize = 0;
        //_updated.clear();
        //_inserted.clear();
        //_fileEntryToc.clear();

        //if (_tempFile != nullptr)
        //{
        //	fclose(_tempFile);
        //	_tempFile = 0;
        //	Util::FileDelete(_tempPath);
        //}

        //for (int i = 0; i < (int)_fileBlockMemory.size(); ++i)
        //	free(_fileBlockMemory[i]);

        //m_State = false;
        //_fileEntriesHash.clear();

        //for (int i = 0; i < (int)m_FileTableHeader.size(); ++i)
        //	delete this->m_FileTableHeader[i];

        //for (int i = 0; i < (int)_insertedFileEntries.size(); ++i)
        //	delete _insertedFileEntries[i];

        //_insertedFileEntries.clear();
        //m_FileTableHeader.clear();
        //_fileBlockMemory.clear();
        //m_FileTableEntries.clear();
        //m_FileHeaderTOC.clear();

        return true;
    }

    //
    HMODULE Myp::m_InstanceHandle = 0;
};
