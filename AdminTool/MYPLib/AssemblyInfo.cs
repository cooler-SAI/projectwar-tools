﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Admin Tool")]
[assembly: AssemblyDescription("https://www.rogueworxstudios.com/")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WAR: Apocalypse Project")]
[assembly: AssemblyProduct("WAaaAGHhhH TOolz WOT iZ 4 MeKAniks")]
[assembly: AssemblyCopyright("Copyright © WAR: Apocalypse Project 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fa2bedc0-52d3-42eb-b10d-5ee3e3cd2e7f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number

//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguage("en-GB")]

