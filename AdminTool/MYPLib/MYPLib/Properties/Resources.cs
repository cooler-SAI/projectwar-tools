﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.Properties.Resources
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace MYPLib.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (MYPLib.Properties.Resources.resourceMan == null)
          MYPLib.Properties.Resources.resourceMan = new ResourceManager("MYPLib.Properties.Resources", typeof (MYPLib.Properties.Resources).Assembly);
        return MYPLib.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return MYPLib.Properties.Resources.resourceCulture;
      }
      set
      {
        MYPLib.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
