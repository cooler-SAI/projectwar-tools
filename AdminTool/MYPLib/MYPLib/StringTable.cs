﻿// Decompiled with JetBrains decompiler
// Type: MypLib.StringTable
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.Collections.Generic;
using System.Text;

namespace MypLib
{
  public class StringTable
  {
    public System.Collections.Generic.List<string> List = new System.Collections.Generic.List<string>();
    public Dictionary<string, string> _keyed = new Dictionary<string, string>();

    public void Load(byte[] data)
    {
      string str1 = Encoding.Unicode.GetString(data);
      int num1 = 0;
      string[] separator = new string[1]{ "\r\n" };
      int num2 = 0;
      foreach (string str2 in str1.Split(separator, (StringSplitOptions) num2))
      {
        int length = str2.IndexOf('\t');
        if (length > 0)
        {
          string index = str2.Substring(0, length).ToLower().Trim();
          string str3 = str2.Substring(length + 1);
          this.List.Add(str3);
          this._keyed[index] = str3;
        }
        ++num1;
      }
    }

    public string GetKeyedValue(string row)
    {
      if (this._keyed.ContainsKey(row))
        return this._keyed[row];
      return "";
    }

    public string GetValue(int row)
    {
      if (row < this.List.Count)
        return this.List[row];
      return "";
    }

    public void SetValue(int row, string value)
    {
      if (row > this.List.Count)
      {
        int num = row - this.List.Count + 1;
        for (int index = 0; index < num; ++index)
          this.List.Add("");
      }
      this._keyed[row.ToString()] = value;
      this.List[row] = value;
    }

    public string ToDocument()
    {
      StringBuilder stringBuilder = new StringBuilder();
      for (int index = 0; index < this.List.Count; ++index)
        stringBuilder.Append(index.ToString() + "\t" + this.List[index] + "\r\n");
      return stringBuilder.ToString();
    }
  }
}
