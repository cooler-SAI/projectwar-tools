﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.AbilityRequirmentBin
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.IO;

namespace MYPLib
{
  public class AbilityRequirmentBin
  {
    public List<ExtData> A02 = new List<ExtData>();

    public ushort ID { get; set; }

    public ExtData D1
    {
      get
      {
        if (this.A02.Count > 0)
          return this.A02[0];
        return new ExtData();
      }
    }

    public ExtData D2
    {
      get
      {
        if (this.A02.Count > 1)
          return this.A02[1];
        return new ExtData();
      }
    }

    public ExtData D3
    {
      get
      {
        if (this.A02.Count > 2)
          return this.A02[2];
        return new ExtData();
      }
    }

    public ExtData D4
    {
      get
      {
        if (this.A02.Count > 3)
          return this.A02[3];
        return new ExtData();
      }
    }

    public ExtData D5
    {
      get
      {
        if (this.A02.Count > 4)
          return this.A02[4];
        return new ExtData();
      }
    }

    public ExtData D6
    {
      get
      {
        if (this.A02.Count > 5)
          return this.A02[5];
        return new ExtData();
      }
    }

    public ExtData D7
    {
      get
      {
        if (this.A02.Count > 6)
          return this.A02[6];
        return new ExtData();
      }
    }

    public ExtData D8
    {
      get
      {
        if (this.A02.Count > 7)
          return this.A02[7];
        return new ExtData();
      }
    }

    public ExtData D9
    {
      get
      {
        if (this.A02.Count > 8)
          return this.A02[8];
        return new ExtData();
      }
    }

    public AbilityRequirmentBin(Stream stream)
    {
      this.ID = PacketUtil.GetUint16R(stream);
      for (int index = 0; index < 6; ++index)
      {
        if (PacketUtil.GetUint32R(stream) == 2863311530U)
          this.A02.Add(new ExtData()
          {
            Val1 = (int) PacketUtil.GetUint32R(stream),
            Val2 = (int) PacketUtil.GetUint32R(stream),
            Val3 = (int) PacketUtil.GetUint32R(stream),
            Val4 = (int) PacketUtil.GetUint32R(stream),
            Val5 = (int) PacketUtil.GetUint32R(stream),
            Val6 = (int) PacketUtil.GetUint32R(stream),
            Val7 = (int) PacketUtil.GetUint32R(stream),
            Val8 = (int) PacketUtil.GetUint32R(stream),
            Val9 = PacketUtil.GetUint8(stream)
          });
      }
    }

    public void Save(Stream stream)
    {
      PacketUtil.WriteUInt16R(stream, this.ID);
      for (int index = 0; index < 6; ++index)
      {
        if (index < this.A02.Count)
        {
          PacketUtil.WriteUInt32R(stream, 2863311530U);
          this.A02[index].Save(stream);
        }
        else
          PacketUtil.WriteUInt32R(stream, 0U);
      }
    }
  }
}
