﻿// Decompiled with JetBrains decompiler
// Type: MypLib.MythicWater
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace MypLib
{
  public class MythicWater
  {
    public static PointF ParsePointF(string csv)
    {
      string[] strArray = csv.Split(',');
      if (strArray.Length > 1)
        return new PointF(float.Parse(strArray[0]), float.Parse(strArray[1]));
      return new PointF();
    }

    public MythicWater.Water WaterSystem { get; set; }

    public static SurfaceType GetLiquidType(string type)
    {
      string lower = type.ToLower();
      if (lower.Contains("poison"))
        return SurfaceType.WATER_POISON;
      if (lower.Contains("river"))
        return SurfaceType.WATER_RIVER;
      if (lower.Contains("marsh"))
        return SurfaceType.WATER_MARSH;
      if (lower.Contains("_tar_"))
        return SurfaceType.TAR;
      if (lower.Contains("muck"))
        return SurfaceType.WATER_MUCK;
      if (lower.Contains("dirty"))
        return SurfaceType.WATER_DIRTY;
      if (lower.Contains("lava"))
        return SurfaceType.LAVA;
      if (lower.Contains("magma"))
        return SurfaceType.LAVA_MAGMA;
      if (lower.Contains("ocean"))
        return SurfaceType.WATER_OCEAN;
      if (lower.Contains("hotspring"))
        return SurfaceType.WATER_HOTSPRING;
      if (lower.Contains("ice") || lower.Contains("icy"))
        return SurfaceType.WATER_ICY;
      if (lower.Contains("lake"))
        return SurfaceType.WATER_LAKE;
      if (lower.Contains("bog"))
        return SurfaceType.WATER_BOG;
      if (lower.Contains("stream"))
        return SurfaceType.WATER_STREAM;
      if (lower.Contains("tainted"))
        return SurfaceType.WATER_TAINTED;
      return lower.Contains("death") ? SurfaceType.INSTANT_DEATH : SurfaceType.WATER_GENERIC;
    }

    public static MythicWater Load(Stream stream)
    {
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.Load(stream);
      XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("Fringes");
      if (elementsByTagName != null)
      {
        foreach (XmlNode xmlNode in elementsByTagName)
          xmlNode.AppendChild((XmlNode) xmlDocument.CreateElement("WaterFringe"));
      }
      XmlElement xmlElement = (XmlElement) xmlDocument.GetElementsByTagName("WaterSystem")[0];
      if (xmlElement != null)
        xmlElement.AppendChild((XmlNode) xmlDocument.CreateElement("WaterBody"));
      MythicWater mythicWater = (MythicWater) JsonConvert.DeserializeObject<MythicWater>(Regex.Replace(JsonConvert.SerializeXmlNode((XmlNode) xmlDocument, (Newtonsoft.Json.Formatting) 1), "(\\s*)\"@(.*)\":", "$1\"$2\":", RegexOptions.IgnoreCase));
      if (mythicWater.WaterSystem.WaterBody != null)
        mythicWater.WaterSystem.WaterBody.RemoveAt(mythicWater.WaterSystem.WaterBody.Count - 1);
      if (mythicWater.WaterSystem.WaterBody == null)
        return (MythicWater) null;
      int num = 1;
      foreach (MythicWater.WaterBody waterBody in mythicWater.WaterSystem.WaterBody)
      {
        if (waterBody != null)
        {
          waterBody.ID = num;
          if (waterBody.Fringes != null && waterBody.Fringes.WaterFringe != null)
            waterBody.Fringes.WaterFringe.RemoveAt(waterBody.Fringes.WaterFringe.Count - 1);
          ++num;
        }
      }
      return mythicWater;
    }

    public class Water
    {
      public MythicWater.HEADER Header { get; set; }

      public List<MythicWater.WaterBody> WaterBody { get; set; }
    }

    public class FringeControlPoint
    {
      public int index { get; set; }

      public string pos { get; set; }

      public PointF Pos
      {
        get
        {
          return MythicWater.ParsePointF(this.pos);
        }
      }
    }

    public class WaterFringe
    {
      public string name { get; set; }

      public string type { get; set; }

      public string looped { get; set; }

      public MythicWater.FringeControlPoints ControlPoints { get; set; }
    }

    public class Fringes
    {
      public List<MythicWater.WaterFringe> WaterFringe { get; set; }
    }

    public class WaterBodyPointPair
    {
      public int index { get; set; }

      public string left { get; set; }

      public string right { get; set; }

      public PointF Left
      {
        get
        {
          return MythicWater.ParsePointF(this.left);
        }
      }

      public PointF Right
      {
        get
        {
          return MythicWater.ParsePointF(this.right);
        }
      }
    }

    public class WaterControlPoints
    {
      public List<MythicWater.WaterBodyPointPair> WaterBodyPointPair { get; set; }
    }

    public class FringeControlPoints
    {
      public List<MythicWater.FringeControlPoint> FringeControlPoint { get; set; }
    }

    public class WaterBody
    {
      public int ID;

      public string name { get; set; }

      public string type { get; set; }

      public float height { get; set; }

      public int interior { get; set; }

      public SurfaceType LiquidType
      {
        get
        {
          if (this.type != null)
            return MythicWater.GetLiquidType(this.type);
          return SurfaceType.WATER_GENERIC;
        }
      }

      public MythicWater.WaterControlPoints ControlPoints { get; set; }

      public MythicWater.Fringes Fringes { get; set; }
    }

    public class HEADER
    {
      public string version { get; set; }
    }
  }
}
