﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmProgress
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class frmProgress : Form
  {
    private string _currentFile = "";
    private int _current = 0;
    private int _total = 0;
    private bool _shown = false;
    private IContainer components = (IContainer) null;
    private ProgressBar progressBar1;
    private Label label1;
    private Timer timer1;

    public frmProgress()
    {
      this.InitializeComponent();
    }

    public void UpdateCurrent(string file, int current, int total)
    {
      if (!this._shown)
        return;
      this.BeginInvoke((Delegate) new frmProgress.UpdateDelegate(this.UpdateCurrentInternal), (object) file, (object) current, (object) total);
    }

    public void UpdateCurrentInternal(string file, int current, int total)
    {
      this._currentFile = file;
      this._current = current;
      this._total = total;
      this.label1.Text = file;
      if ((uint) total > 0U)
        return;
      this.timer1.Enabled = false;
      this.progressBar1.Value = 100;
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      if (this._total <= 0)
        return;
      this.progressBar1.Value = (int) ((double) this._current * 100.0 / (double) this._total);
    }

    private void frmProgress_Load(object sender, EventArgs e)
    {
      this._shown = true;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.progressBar1 = new ProgressBar();
      this.label1 = new Label();
      this.timer1 = new Timer(this.components);
      this.SuspendLayout();
      this.progressBar1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.progressBar1.Location = new Point(1, 62);
      this.progressBar1.Name = "progressBar1";
      this.progressBar1.Size = new Size(543, 23);
      this.progressBar1.TabIndex = 0;
      this.label1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.label1.Location = new Point(4, 10);
      this.label1.Name = "label1";
      this.label1.Size = new Size(540, 45);
      this.label1.TabIndex = 1;
      this.label1.Text = "label1";
      this.label1.TextAlign = ContentAlignment.MiddleLeft;
      this.timer1.Enabled = true;
      this.timer1.Interval = 30;
      this.timer1.Tick += new EventHandler(this.timer1_Tick);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(545, 134);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.progressBar1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Name = nameof (frmProgress);
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "Progress";
      this.Load += new EventHandler(this.frmProgress_Load);
      this.ResumeLayout(false);
    }

    public delegate void UpdateDelegate(string file, int current, int total);
  }
}
