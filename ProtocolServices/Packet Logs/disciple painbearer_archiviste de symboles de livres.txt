
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 05 06 00 2B 00 00 00 01 00 05 60 D5 00 00 |.....+......`...|
|00 00 00 00 00 00 00 00 51 16 00 00 1E 19 99 67 |........Q......g|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 05 60 D5 7C A6 32 B4 00 00 00 00 00 |.....`.|.2......|
|00 51 16 00 00 1E 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 107 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 68 72 12 45 00 00 0C C1 43 16 00 06 8F CF 00 |.hr.E....C......|
|02 0A A4 00 00 04 45 32 22 81 00 00 00 00 00 00 |......E2".......|
|00 01 00 03 E8 00 00 00 00 BA 0D 00 02 00 00 01 |................|
|19 00 50 75 72 73 75 69 6E 67 20 46 6C 65 73 68 |..Pursuing Flesh|
|68 6F 75 6E 64 5E 6D 00 00 00 00 08 01 0A 00 00 |hound^m.........|
|00 12 05 00 12 45 4F CF 8A A4 43 16 64 00 A1 00 |.....EO...C.d...|
|00 00 00 08 C1 0C 12 45 00 00 00                |...........     |
-------------------------------------------------------------------
[Client] packet : (0xD2) F_INTERACT  Size = 24 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 05 0D 00 2B 00 00 00 D2 00 00 02 0D 00 00 |.....+..........|
|00 00 00 00 00 00 00 00                         |........        |
-------------------------------------------------------------------
[Server] packet : (0x7E) F_SET_ABILITY_TIMER  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 7E 00 01 01 00 00 00 00 00 00 00 00 00    |............... |
-------------------------------------------------------------------
[Server] packet : (0xE9) F_INTERACT_RESPONSE  Size = 11 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 08 E9 00 02 0D 00 00 00 02 00                |...........     |
-------------------------------------------------------------------
[Client] packet : (0xD2) F_INTERACT  Size = 24 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 05 0F 00 2B 00 00 00 D2 00 00 02 0D 00 09 |.....+..........|
|00 00 00 00 00 00 FB 6D                         |........        |
-------------------------------------------------------------------
[Server] packet : (0xBF) F_INIT_STORE  Size = 1918 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|07 7B BF 00 00 00 0A 01 01 02 00 00 03 00 00 00 |.{..............|
|00 00 00 03 20 F2 10 F4 00 00 00 00 00 00 00 00 |.... ...........|
|0F 18 00 00 00 00 00 01 00 40 00 00 00 00 08 00 |.........@......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 |................|
|00 00 00 00 00 00 00 00 00 00 00 00 14 44 61 67 |.............Dag|
|67 65 72 20 6F 66 20 43 6F 72 72 75 70 74 69 6F |ger of Corruptio|
|6E 00 00 00 00 00 00 23 41 20 74 72 6F 70 68 79 |n......#A trophy|
|20 62 6F 75 67 68 74 20 77 69 74 68 20 42 65 73 | bought with Bes|
|74 69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 |tial Tokens.....|
|06 20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |. ..............|
|00 00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 |.........8....Be|
|73 74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 |stial Token.....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 |................|
|03 00 00 00 00 00 00 03 20 F4 10 F6 00 00 00 00 |........ .......|
|00 00 00 00 0F 18 00 00 00 00 00 01 00 40 00 00 |.............@..|
|00 00 08 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 01 00 01 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|0F 54 68 65 20 50 6C 61 69 6E 20 50 6F 75 63 68 |.The Plain Pouch|
|00 00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 |......#A trophy |
|62 6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 |bought with Best|
|69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 |ial Tokens......|
|20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | ...............|
|00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 |........8....Bes|
|74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 |tial Token......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 01 |................|
|00 00 00 00 00 00 03 20 FA 10 FC 00 00 00 00 00 |....... ........|
|00 00 00 0F 18 00 00 00 00 00 01 00 10 00 00 00 |................|
|00 00 80 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|01 00 01 00 00 00 00 00 00 00 00 00 00 00 00 10 |................|
|54 68 65 20 4F 72 62 20 6F 66 20 4E 69 67 68 74 |The Orb of Night|
|00 00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 |......#A trophy |
|62 6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 |bought with Best|
|69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 |ial Tokens......|
|20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | ...............|
|00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 |........8....Bes|
|74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 |tial Token......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 03 01 |................|
|00 00 00 00 00 00 03 20 FB 10 FD 00 00 00 00 00 |....... ........|
|00 00 00 0F 18 00 00 00 00 00 01 00 10 00 00 00 |................|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|01 00 01 00 00 00 00 00 00 00 00 00 00 00 00 0F |................|
|43 6F 72 73 61 69 72 27 73 20 54 6F 6F 6C 73 00 |Corsair's Tools.|
|00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 62 |.....#A trophy b|
|6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 69 |ought with Besti|
|61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 20 |al Tokens...... |
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 74 |.......8....Best|
|69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 00 |ial Token.......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 04 01 00 |................|
|00 00 00 00 00 03 20 FC 10 FE 00 00 00 00 00 00 |...... .........|
|00 00 0F 18 00 00 00 00 00 01 00 10 00 00 00 00 |................|
|08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 |................|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 13 54 |...............T|
|68 65 20 4C 6F 61 74 68 73 6F 6D 65 20 50 68 69 |he Loathsome Phi|
|61 6C 00 00 00 00 00 00 23 41 20 74 72 6F 70 68 |al......#A troph|
|79 20 62 6F 75 67 68 74 20 77 69 74 68 20 42 65 |y bought with Be|
|73 74 69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 |stial Tokens....|
|03 06 20 00 00 00 00 00 00 00 00 00 00 00 00 00 |.. .............|
|00 00 00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 |..........8....B|
|65 73 74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 |estial Token....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|05 01 00 00 00 00 00 00 03 21 09 11 0B 00 00 00 |.........!......|
|00 00 00 00 00 0F 18 00 00 00 00 00 01 00 06 00 |................|
|00 00 00 08 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 01 00 01 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 0D 52 61 76 65 6E 27 73 20 53 6B 75 6C 6C 00 |..Raven's Skull.|
|00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 62 |.....#A trophy b|
|6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 69 |ought with Besti|
|61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 20 |al Tokens...... |
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 74 |.......8....Best|
|69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 00 |ial Token.......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 06 01 00 |................|
|00 00 00 00 00 03 21 0A 11 0C 00 00 00 00 00 00 |......!.........|
|00 00 0F 18 00 00 00 00 00 01 00 06 00 00 00 00 |................|
|08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 |................|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 0C 57 |...............W|
|6F 6C 66 27 73 20 53 6B 75 6C 6C 00 00 00 00 00 |olf's Skull.....|
|00 23 41 20 74 72 6F 70 68 79 20 62 6F 75 67 68 |.#A trophy bough|
|74 20 77 69 74 68 20 42 65 73 74 69 61 6C 20 54 |t with Bestial T|
|6F 6B 65 6E 73 01 01 00 00 03 06 20 00 00 00 00 |okens...... ....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|01 00 01 38 81 01 EC 0D 42 65 73 74 69 61 6C 20 |...8....Bestial |
|54 6F 6B 65 6E 00 01 00 00 00 00 00 00 00 00 00 |Token...........|
|00 00 00 00 00 00 00 00 00 07 03 00 00 00 00 00 |................|
|00 03 21 0D 11 0F 00 00 00 00 00 00 00 00 0F 18 |..!.............|
|00 00 00 00 00 01 00 56 00 00 00 00 08 00 00 00 |.......V........|
|00 00 00 00 00 00 00 00 00 00 00 01 00 01 00 00 |................|
|00 00 00 00 00 00 00 00 00 00 0D 55 6E 73 68 61 |...........Unsha|
|76 65 6E 20 48 65 61 64 00 00 00 00 00 00 23 41 |ven Head......#A|
|20 74 72 6F 70 68 79 20 62 6F 75 67 68 74 20 77 | trophy bought w|
|69 74 68 20 42 65 73 74 69 61 6C 20 54 6F 6B 65 |ith Bestial Toke|
|6E 73 01 01 00 00 03 06 20 00 00 00 00 00 00 00 |ns...... .......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 |................|
|38 81 01 EC 0D 42 65 73 74 69 61 6C 20 54 6F 6B |8....Bestial Tok|
|65 6E 00 01 00 00 00 00 00 00 00 00 00 00 00 00 |en..............|
|00 00 00 00 00 00 08 03 00 00 00 00 00 00 03 21 |...............!|
|55 15 C0 00 00 00 00 00 00 00 00 0F 18 00 00 00 |U...............|
|00 00 01 00 40 00 00 00 00 00 80 00 00 00 00 00 |....@...........|
|00 00 00 00 00 00 00 00 01 00 01 00 00 00 00 00 |................|
|00 00 00 00 00 00 00 0C 54 68 65 20 4D 75 74 61 |........The Muta|
|74 69 6F 6E 00 00 00 00 00 00 23 41 20 74 72 6F |tion......#A tro|
|70 68 79 20 62 6F 75 67 68 74 20 77 69 74 68 20 |phy bought with |
|42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 73 01 01 |Bestial Tokens..|
|00 00 03 06 20 00 00 00 00 00 00 00 00 00 00 00 |.... ...........|
|00 00 00 00 00 00 00 00 00 01 00 01 38 81 01 EC |............8...|
|0D 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 00 01 |.Bestial Token..|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 09 01 00 00 00 00 00 00 03 22 B0 1A 17 00 |..........."....|
|00 00 00 00 00 00 00 0F 18 00 00 00 00 00 01 00 |................|
|06 00 00 00 00 08 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 01 00 01 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 0B 44 61 6E 67 6C 79 20 46 69 6E 67 00 |....Dangly Fing.|
|00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 62 |.....#A trophy b|
|6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 69 |ought with Besti|
|61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 20 |al Tokens...... |
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 74 |.......8....Best|
|69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 00 |ial Token.......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00       |..............  |
-------------------------------------------------------------------
[Server] packet : (0xBF) F_INIT_STORE  Size = 3724 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|0E 89 BF 00 00 03 13 00 01 02 00 03 00 00 00 00 |................|
|00 00 03 20 F5 10 F7 00 00 00 00 00 00 00 00 0F |... ............|
|18 00 00 00 00 00 01 00 40 00 00 00 00 08 00 00 |........@.......|
|00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 00 |................|
|00 00 00 00 00 00 00 00 00 00 00 14 54 68 65 20 |............The |
|43 72 69 6D 73 6F 6E 20 54 68 69 72 73 74 65 72 |Crimson Thirster|
|00 00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 |......#A trophy |
|62 6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 |bought with Best|
|69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 |ial Tokens......|
|20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | ...............|
|00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 |........8....Bes|
|74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 |tial Token......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 03 |................|
|00 00 00 00 00 00 03 21 56 15 C1 00 00 00 00 00 |.......!V.......|
|00 00 00 0F 18 00 00 00 00 00 01 00 40 00 00 00 |............@...|
|00 08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|01 00 01 00 00 00 00 00 00 00 00 00 00 00 00 12 |................|
|45 76 65 72 79 2D 73 68 69 66 74 69 6E 67 20 45 |Every-shifting E|
|79 65 00 00 00 00 00 00 23 41 20 74 72 6F 70 68 |ye......#A troph|
|79 20 62 6F 75 67 68 74 20 77 69 74 68 20 42 65 |y bought with Be|
|73 74 69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 |stial Tokens....|
|03 06 20 00 00 00 00 00 00 00 00 00 00 00 00 00 |.. .............|
|00 00 00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 |..........8....B|
|65 73 74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 |estial Token....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|02 03 00 00 00 00 00 00 03 21 57 15 C2 00 00 00 |.........!W.....|
|00 00 00 00 00 0F 18 00 00 00 00 00 01 00 40 00 |..............@.|
|00 00 00 08 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 01 00 01 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 11 4B 61 72 6C 27 73 20 4C 69 6D 70 20 57 72 |..Karl's Limp Wr|
|69 73 74 00 00 00 00 00 00 23 41 20 74 72 6F 70 |ist......#A trop|
|68 79 20 62 6F 75 67 68 74 20 77 69 74 68 20 42 |hy bought with B|
|65 73 74 69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 |estial Tokens...|
|00 03 06 20 00 00 00 00 00 00 00 00 00 00 00 00 |... ............|
|00 00 00 00 00 00 00 00 01 00 01 38 81 01 EC 0D |...........8....|
|42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 |Bestial Token...|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 03 03 00 00 00 00 00 00 03 21 58 15 C3 00 00 |..........!X....|
|00 00 00 00 00 00 0F 18 00 00 00 00 00 01 00 40 |...............@|
|00 00 00 00 00 80 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 01 00 01 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 13 54 68 65 20 46 65 61 72 66 75 6C 20 46 |...The Fearful F|
|6F 6F 6C 69 73 68 00 00 00 00 00 00 23 41 20 74 |oolish......#A t|
|72 6F 70 68 79 20 62 6F 75 67 68 74 20 77 69 74 |rophy bought wit|
|68 20 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 73 |h Bestial Tokens|
|01 01 00 00 03 06 20 00 00 00 00 00 00 00 00 00 |...... .........|
|00 00 00 00 00 00 00 00 00 00 00 01 00 01 38 81 |..............8.|
|01 EC 0D 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E |...Bestial Token|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 04 03 00 00 00 00 00 00 03 21 59 15 |.............!Y.|
|C4 00 00 00 00 00 00 00 00 0F 18 00 00 00 00 00 |................|
|01 00 40 00 00 00 00 00 80 00 00 00 00 00 00 00 |..@.............|
|00 00 00 00 00 00 01 00 01 00 00 00 00 00 00 00 |................|
|00 00 00 00 00 19 53 63 72 69 70 74 20 6F 66 20 |......Script of |
|54 7A 65 65 6E 74 63 68 27 73 20 57 69 6C 6C 00 |Tzeentch's Will.|
|00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 62 |.....#A trophy b|
|6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 69 |ought with Besti|
|61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 20 |al Tokens...... |
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 74 |.......8....Best|
|69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 00 |ial Token.......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 05 03 00 |................|
|00 00 00 00 00 03 21 5A 15 C5 00 00 00 00 00 00 |......!Z........|
|00 00 0F 18 00 00 00 00 00 01 00 40 00 00 00 00 |...........@....|
|08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 |................|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 12 53 |...............S|
|74 6F 6E 65 20 6F 66 20 55 6C 67 75 20 4B 68 61 |tone of Ulgu Kha|
|72 00 00 00 00 00 00 23 41 20 74 72 6F 70 68 79 |r......#A trophy|
|20 62 6F 75 67 68 74 20 77 69 74 68 20 42 65 73 | bought with Bes|
|74 69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 |tial Tokens.....|
|06 20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |. ..............|
|00 00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 |.........8....Be|
|73 74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 |stial Token.....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 06 |................|
|03 00 00 00 00 00 00 03 21 5B 15 C6 00 00 00 00 |........![......|
|00 00 00 00 0F 18 00 00 00 00 00 01 00 40 00 00 |.............@..|
|00 00 08 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 01 00 01 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|14 56 69 63 74 69 6D 20 6F 66 20 41 73 61 76 61 |.Victim of Asava|
|72 20 4B 75 6C 00 00 00 00 00 00 23 41 20 74 72 |r Kul......#A tr|
|6F 70 68 79 20 62 6F 75 67 68 74 20 77 69 74 68 |ophy bought with|
|20 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 73 01 | Bestial Tokens.|
|01 00 00 03 06 20 00 00 00 00 00 00 00 00 00 00 |..... ..........|
|00 00 00 00 00 00 00 00 00 00 01 00 01 38 81 01 |.............8..|
|EC 0D 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 00 |..Bestial Token.|
|01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 07 03 00 00 00 00 00 00 03 21 5C 15 C7 |............!\..|
|00 00 00 00 00 00 00 00 0F 18 00 00 00 00 00 01 |................|
|00 40 00 00 00 00 00 80 00 00 00 00 00 00 00 00 |.@..............|
|00 00 00 00 00 01 00 01 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 12 54 68 65 20 4B 68 61 64 68 75 72 |.....The Khadhur|
|20 56 69 73 69 6F 6E 00 00 00 00 00 00 23 41 20 | Vision......#A |
|74 72 6F 70 68 79 20 62 6F 75 67 68 74 20 77 69 |trophy bought wi|
|74 68 20 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E |th Bestial Token|
|73 01 01 00 00 03 06 20 00 00 00 00 00 00 00 00 |s...... ........|
|00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 38 |...............8|
|81 01 EC 0D 42 65 73 74 69 61 6C 20 54 6F 6B 65 |....Bestial Toke|
|6E 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 |n...............|
|00 00 00 00 00 08 03 00 00 00 00 00 00 03 21 5D |..............!]|
|15 C8 00 00 00 00 00 00 00 00 0F 18 00 00 00 00 |................|
|00 01 00 40 00 00 00 00 00 80 00 00 00 00 00 00 |...@............|
|00 00 00 00 00 00 00 01 00 01 00 00 00 00 00 00 |................|
|00 00 00 00 00 00 15 42 65 27 6C 61 6B 6F 72 27 |.......Be'lakor'|
|73 20 47 72 61 6E 64 20 50 6C 61 6E 00 00 00 00 |s Grand Plan....|
|00 00 23 41 20 74 72 6F 70 68 79 20 62 6F 75 67 |..#A trophy boug|
|68 74 20 77 69 74 68 20 42 65 73 74 69 61 6C 20 |ht with Bestial |
|54 6F 6B 65 6E 73 01 01 00 00 03 06 20 00 00 00 |Tokens...... ...|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 01 00 01 38 81 01 EC 0D 42 65 73 74 69 61 6C |....8....Bestial|
|20 54 6F 6B 65 6E 00 01 00 00 00 00 00 00 00 00 | Token..........|
|00 00 00 00 00 00 00 00 00 00 09 03 00 00 00 00 |................|
|00 00 03 21 61 15 CC 00 00 00 00 00 00 00 00 0F |...!a...........|
|18 00 00 00 00 00 01 00 40 00 00 00 00 00 80 00 |........@.......|
|00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 00 |................|
|00 00 00 00 00 00 00 00 00 00 00 19 48 75 6E 67 |............Hung|
|65 72 69 6E 67 20 45 79 65 20 6F 66 20 74 68 65 |ering Eye of the|
|20 56 61 72 67 00 00 00 00 00 00 23 41 20 74 72 | Varg......#A tr|
|6F 70 68 79 20 62 6F 75 67 68 74 20 77 69 74 68 |ophy bought with|
|20 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 73 01 | Bestial Tokens.|
|01 00 00 03 06 20 00 00 00 00 00 00 00 00 00 00 |..... ..........|
|00 00 00 00 00 00 00 00 00 00 01 00 01 38 81 01 |.............8..|
|EC 0D 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E 00 |..Bestial Token.|
|01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 0A 03 00 00 00 00 00 00 03 20 F6 10 F8 |............ ...|
|00 00 00 00 00 00 00 00 0F 18 00 00 00 00 00 01 |................|
|00 40 00 00 00 00 08 00 00 00 00 00 00 00 00 00 |.@..............|
|00 00 00 00 00 01 00 01 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 0A 47 6F 6C 64 65 6E 20 41 78 65 00 |.....Golden Axe.|
|00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 62 |.....#A trophy b|
|6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 69 |ought with Besti|
|61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 20 |al Tokens...... |
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 74 |.......8....Best|
|69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 00 |ial Token.......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 0B 03 00 |................|
|00 00 00 00 00 03 21 5E 15 C9 00 00 00 00 00 00 |......!^........|
|00 00 0F 18 00 00 00 00 00 01 00 40 00 00 00 00 |...........@....|
|08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 |................|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 13 41 |...............A|
|65 73 6C 69 6E 67 20 54 72 69 62 61 6C 20 47 69 |esling Tribal Gi|
|66 74 00 00 00 00 00 00 23 41 20 74 72 6F 70 68 |ft......#A troph|
|79 20 62 6F 75 67 68 74 20 77 69 74 68 20 42 65 |y bought with Be|
|73 74 69 61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 |stial Tokens....|
|03 06 20 00 00 00 00 00 00 00 00 00 00 00 00 00 |.. .............|
|00 00 00 00 00 00 00 01 00 01 38 81 01 EC 0D 42 |..........8....B|
|65 73 74 69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 |estial Token....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|0C 03 00 00 00 00 00 00 03 21 5F 15 CA 00 00 00 |.........!_.....|
|00 00 00 00 00 0F 18 00 00 00 00 00 01 00 40 00 |..............@.|
|00 00 00 08 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 01 00 01 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 0D 53 6E 61 65 67 72 27 73 20 47 69 66 74 00 |..Snaegr's Gift.|
|00 00 00 00 00 23 41 20 74 72 6F 70 68 79 20 62 |.....#A trophy b|
|6F 75 67 68 74 20 77 69 74 68 20 42 65 73 74 69 |ought with Besti|
|61 6C 20 54 6F 6B 65 6E 73 01 01 00 00 03 06 20 |al Tokens...... |
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 01 00 01 38 81 01 EC 0D 42 65 73 74 |.......8....Best|
|69 61 6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 00 |ial Token.......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 0D 03 00 |................|
|00 00 00 00 00 03 21 60 15 CB 00 00 00 00 00 00 |......!`........|
|00 00 0F 18 00 00 00 00 00 01 00 40 00 00 00 00 |...........@....|
|00 80 00 00 00 00 00 00 00 00 00 00 00 00 00 01 |................|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 18 4B |...............K|
|68 61 7A 61 67 69 20 52 61 69 64 69 6E 67 20 4F |hazagi Raiding O|
|72 6E 61 6D 65 6E 74 00 00 00 00 00 00 23 41 20 |rnament......#A |
|74 72 6F 70 68 79 20 62 6F 75 67 68 74 20 77 69 |trophy bought wi|
|74 68 20 42 65 73 74 69 61 6C 20 54 6F 6B 65 6E |th Bestial Token|
|73 01 01 00 00 03 06 20 00 00 00 00 00 00 00 00 |s...... ........|
|00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 38 |...............8|
|81 01 EC 0D 42 65 73 74 69 61 6C 20 54 6F 6B 65 |....Bestial Toke|
|6E 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 |n...............|
|00 00 00 00 00 0E 03 00 00 00 00 00 00 03 20 F7 |.............. .|
|10 F9 00 00 00 00 00 00 00 00 0F 18 00 00 00 00 |................|
|00 01 00 40 00 00 00 00 00 80 00 00 00 00 00 00 |...@............|
|00 00 00 00 00 00 00 01 00 01 00 00 00 00 00 00 |................|
|00 00 00 00 00 00 14 43 68 69 6C 64 72 65 6E 20 |.......Children |
|6F 66 20 6F 75 72 20 4C 6F 72 64 00 00 00 00 00 |of our Lord.....|
|00 23 41 20 74 72 6F 70 68 79 20 62 6F 75 67 68 |.#A trophy bough|
|74 20 77 69 74 68 20 42 65 73 74 69 61 6C 20 54 |t with Bestial T|
|6F 6B 65 6E 73 01 01 00 00 03 06 20 00 00 00 00 |okens...... ....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|01 00 01 38 81 01 EC 0D 42 65 73 74 69 61 6C 20 |...8....Bestial |
|54 6F 6B 65 6E 00 01 00 00 00 00 00 00 00 00 00 |Token...........|
|00 00 00 00 00 00 00 00 00 0F 03 00 00 00 00 00 |................|
|00 03 20 F8 10 FA 00 00 00 00 00 00 00 00 0F 18 |.. .............|
|00 00 00 00 00 01 00 40 00 00 00 00 00 80 00 00 |.......@........|
|00 00 00 00 00 00 00 00 00 00 00 01 00 01 00 00 |................|
|00 00 00 00 00 00 00 00 00 00 1D 52 65 64 20 46 |...........Red F|
|69 72 65 20 6F 66 20 41 6C 74 65 72 61 74 69 6F |ire of Alteratio|
|6E 20 53 63 72 69 70 74 00 00 00 00 00 00 23 41 |n Script......#A|
|20 74 72 6F 70 68 79 20 62 6F 75 67 68 74 20 77 | trophy bought w|
|69 74 68 20 42 65 73 74 69 61 6C 20 54 6F 6B 65 |ith Bestial Toke|
|6E 73 01 01 00 00 03 06 20 00 00 00 00 00 00 00 |ns...... .......|
|00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 |................|
|38 81 01 EC 0D 42 65 73 74 69 61 6C 20 54 6F 6B |8....Bestial Tok|
|65 6E 00 01 00 00 00 00 00 00 00 00 00 00 00 00 |en..............|
|00 00 00 00 00 00 10 03 00 00 00 00 00 00 03 21 |...............!|
|62 15 CD 00 00 00 00 00 00 00 00 0F 18 00 00 00 |b...............|
|00 00 01 00 40 00 00 00 00 08 00 00 00 00 00 00 |....@...........|
|00 00 00 00 00 00 00 00 01 00 01 00 00 00 00 00 |................|
|00 00 00 00 00 00 00 12 47 75 72 6B 68 61 6E 27 |........Gurkhan'|
|73 20 46 61 69 74 68 66 75 6C 00 00 00 00 00 00 |s Faithful......|
|23 41 20 74 72 6F 70 68 79 20 62 6F 75 67 68 74 |#A trophy bought|
|20 77 69 74 68 20 42 65 73 74 69 61 6C 20 54 6F | with Bestial To|
|6B 65 6E 73 01 01 00 00 03 06 20 00 00 00 00 00 |kens...... .....|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 |................|
|00 01 38 81 01 EC 0D 42 65 73 74 69 61 6C 20 54 |..8....Bestial T|
|6F 6B 65 6E 00 01 00 00 00 00 00 00 00 00 00 00 |oken............|
|00 00 00 00 00 00 00 00 11 03 00 00 00 00 00 00 |................|
|03 21 63 15 CE 00 00 00 00 00 00 00 00 0F 18 00 |.!c.............|
|00 00 00 00 01 00 40 00 00 00 00 00 80 00 00 00 |......@.........|
|00 00 00 00 00 00 00 00 00 00 01 00 01 00 00 00 |................|
|00 00 00 00 00 00 00 00 00 12 54 7A 65 65 6E 2C |..........Tzeen,|
|20 4E 75 72 67 68 2C 20 53 6C 61 61 00 00 00 00 | Nurgh, Slaa....|
|00 00 23 41 20 74 72 6F 70 68 79 20 62 6F 75 67 |..#A trophy boug|
|68 74 20 77 69 74 68 20 42 65 73 74 69 61 6C 20 |ht with Bestial |
|54 6F 6B 65 6E 73 01 01 00 00 03 06 20 00 00 00 |Tokens...... ...|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 01 00 01 38 81 01 EC 0D 42 65 73 74 69 61 6C |....8....Bestial|
|20 54 6F 6B 65 6E 00 01 00 00 00 00 00 00 00 00 | Token..........|
|00 00 00 00 00 00 00 00 00 00 12 03 00 00 00 00 |................|
|00 00 03 21 64 15 CF 00 00 00 00 00 00 00 00 0F |...!d...........|
|18 00 00 00 00 00 01 00 40 00 00 00 00 00 80 00 |........@.......|
|00 00 00 00 00 00 00 00 00 00 00 00 01 00 01 00 |................|
|00 00 00 00 00 00 00 00 00 00 00 11 54 68 65 20 |............The |
|46 6F 65 73 20 6F 66 20 45 6E 67 72 61 00 00 00 |Foes of Engra...|
|00 00 00 23 41 20 74 72 6F 70 68 79 20 62 6F 75 |...#A trophy bou|
|67 68 74 20 77 69 74 68 20 42 65 73 74 69 61 6C |ght with Bestial|
|20 54 6F 6B 65 6E 73 01 01 00 00 03 06 20 00 00 | Tokens...... ..|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 01 00 01 38 81 01 EC 0D 42 65 73 74 69 61 |.....8....Bestia|
|6C 20 54 6F 6B 65 6E 00 01 00 00 00 00 00 00 00 |l Token.........|
|00 00 00 00 00 00 00 00 00 00 00 00             |............    |
-------------------------------------------------------------------
[Server] packet : (0xC0) F_STORE_BUY_BACK  Size = 5 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 02 C0 00 00                                  |.....           |
-------------------------------------------------------------------
[Server] packet : (0xD7) F_INIT_EFFECTS  Size = 26 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 17 D7 01 00 00 00 02 0D 01 00 5D 17 E2 80 8D |...........]....|
|02 0D 02 02 00 B8 08 01 02 00                   |..........      |
-------------------------------------------------------------------
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 05 11 00 2B 00 00 00 0B 00 05 73 A1 43 08 |.....+......s.C.|
|05 93 4F 04 B4 68 00 00 00 00 00 00 00 00 20 83 |..O..h........ .|                                                
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 05 73 A1 52 75 8A CD 00 00 00 00 00 |.....s.Ru.......|
|00 05 12 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x5A) F_PING_DATAGRAM  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 01 05 12 00 2B FF FF 00 5A 00 69 B3          |.............   |
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 105 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 66 72 39 D8 00 00 0C FA 00 00 00 06 8E 42 00 |.fr9..........B.|
|02 00 3A 00 00 04 4A 37 27 85 00 00 00 00 00 00 |..:...J7'.......|
|00 01 00 03 E9 00 00 00 00 BA B8 00 04 00 00 01 |................|
|19 00 4D 6F 6E 6F 6C 69 74 68 20 53 63 72 65 61 |..Monolith Screa|
|6D 65 72 5E 6D 00 00 00 00 0F 01 0A 00 00 00 12 |mer^m...........|
|05 00 39 D8 4E 42 80 3A 00 00 64 00 A1 00 00 00 |..9.NB.:..d.....|
|00 0F FA 0C 39 D8 00 00 00                      |.........       |
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 05 16 00 2B 00 00 00 16 01 0C 00 00 F4 E7 |.....+..........|                                                
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 126 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 7B 72 01 0C 00 00 06 88 42 A6 00 06 9F 8C 00 |.{r......B......|
|02 1B FE 00 00 06 06 32 18 80 00 00 00 00 72 00 |.......2......r.|
|00 01 00 03 E8 00 00 00 00 B7 F3 00 08 00 00 01 |................|
|19 00 41 6E 63 69 65 6E 74 20 57 61 6C 6B 65 72 |..Ancient Walker|
|5E 6D 00 00 00 00 08 01 0A 00 00 00 12 1D 00 01 |^m..............|
|0C 5F 8C 9B FE 42 A6 64 00 A1 00 00 00 00 08 88 |._...B.d........|
|06 01 0C 00 06 00 14 11 5A 00 15 11 5C 00 16 11 |........Z...\...|
|5B 00 17 11 5E 00 18 11 5D 00 1C 11 59 00       |..............  |
-------------------------------------------------------------------