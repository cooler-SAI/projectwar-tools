﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_CREATE_PLAYER:MythicPacket 
    {
        public ushort ID { get; set; }
        public ushort ObjectID { get; set; }
        public ushort ModelID { get; set; }
        public ushort CareerLine { get; set; }
        public ushort Z { get; set; }
        public ushort ZoneID { get; set; }
        public ushort X { get; set; }
        public ushort Y { get; set; }
        public ushort Heading { get; set; }
        public byte Level { get; set; }
        public byte Level2 { get; set; }
        public byte ShowFlags { get; set; }
        public byte Unk2 { get; set; }
        public byte Unk3 { get; set; }
        public byte Faction { get; set; }
        public ushort Unk4 { get; set; }

        [ByteArray(8)]
        public byte[] Traits { get; set; }

        [ByteArray(12)]
        public byte[] Unk5 { get; set; }
        public byte Unk6 { get; set; }
        public byte Unk7 { get; set; }

        [ByteArray(8)]
        public byte[] Unk8 { get; set; }

        public byte RaceID { get; set; }

        public byte Health { get; set; }

        [PascalString]
        public string Name { get; set; }

        [PascalString]
        public string Title { get; set; }

        [PascalString]
        public string GuildName { get; set; }

        [ByteArray(4)]
        public byte[] Unk9 { get; set; }

        public override string ToString()
        {
            return Name + " " + Title + " " + GuildName;
        }

        public F_CREATE_PLAYER()
        {
        }
        public F_CREATE_PLAYER(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CREATE_PLAYER, ms, true)
        {
        }

        public override void AutoLoad(MemoryStream ms)
        {
            ID = FrameUtil.GetUint16(ms);
            ObjectID = FrameUtil.GetUint16(ms);
            ModelID = FrameUtil.GetUint16(ms);
            CareerLine = FrameUtil.GetUint16(ms);
            Z = FrameUtil.GetUint16(ms);
            ZoneID = FrameUtil.GetUint16(ms);
            X = FrameUtil.GetUint16(ms);
            Y = FrameUtil.GetUint16(ms);
            Heading = FrameUtil.GetUint16(ms);
            Level = FrameUtil.GetUint8(ms);
            Level2 = FrameUtil.GetUint8(ms);
            ShowFlags = FrameUtil.GetUint8(ms);
            Faction = FrameUtil.GetUint8(ms);
            Unk2 = FrameUtil.GetUint8(ms);
            Unk3 = FrameUtil.GetUint8(ms);
            Traits = FrameUtil.GetByteArray(ms, 15); //traits
            Unk5 = FrameUtil.GetByteArray(ms, 5); //unk
            RaceID = FrameUtil.GetUint8(ms);
            Unk6 = FrameUtil.GetUint8(ms);
            Unk7 = FrameUtil.GetUint8(ms);
            Health = FrameUtil.GetUint8(ms);
            Unk8 = FrameUtil.GetByteArray(ms, 8); //unk
            Name = FrameUtil.GetPascalString(ms);//.Replace("^M","").Replace("^F","");
            Title = FrameUtil.GetPascalString(ms);//.Replace("^M", "").Replace("^F", "");
            GuildName = FrameUtil.GetPascalString(ms);
            Unk9 = FrameUtil.GetByteArray(ms, 4); //unk
        }
    }
}
