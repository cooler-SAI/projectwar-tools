﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarShared;

namespace WarAdmin.Packets
{
    public class PacketProcessor
    {

        public List<MythicPacket> Packets = new List<MythicPacket>();
        private static Dictionary<ushort, string> _abilityNames;

        public static string GetAbilityName(ushort id)
        {
            if (_abilityNames == null)
            {
                _abilityNames = new Dictionary<ushort, string>();
                foreach (string line in File.ReadAllLines(@"c:\wardoc\abilitynames.txt"))
                {
                    string[] tokens = line.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    ushort abilityID = ushort.Parse(tokens[0]);
                    string abilityName = "";
                    if (tokens.Length > 1)
                        abilityName = tokens[1];
                    _abilityNames[abilityID] = abilityName;
                }
            }
            if (_abilityNames.ContainsKey(id))
                return _abilityNames[id];
            return id.ToString();
        }
        public PacketProcessor()
        {

        }

        public void Load(List<ParsedPacket> packets, bool client = false)
        {
            foreach (ParsedPacket data in packets)
            {
                MythicPacket packet = LoadPacket(data, client);
                if (packet != null)
                {
                    packet.Data = data.Data;
                    if (packet != null)
                    {
                        Packets.Add(packet);
                    }
                }
            }
            LoadCharacters();


        }


        public T LoadPacketTyped<T>(ParsedPacket data, bool client) where T : MythicPacket, new()
        {
            MemoryStream ms = new MemoryStream(data.Data);
            MythicPacket packet = null;
            int index = 2;
            if (client)
            {
                index = 9;
                ms.Position = 7;
            }
            WarShared.Protocol.GameOp op = (WarShared.Protocol.GameOp)data.Data[index];



            MythicPacket ret = LoadPacket(data, client);
            if (ret is T)
                return (T)ret;
            return null;
        }
        public static ushort ReverseBytes(ushort value)
        {
            return (ushort)((value & 0xFFU) << 8 | (value & 0xFF00U) >> 8);
        }
        private int _lastLIne = 0;

        public static MythicPacket LoadPacket(byte[] data, bool client)
        {
             if (!client)
                {
                    var op = ((WarShared.Protocol.GameOp)data[3]);
                    return LoadPacket(Util.ParseHex("[Server] packet : (0x" + ((int)op).ToString("X").PadLeft(2, '0') + ") " + op.ToString() + " Size = " + data.Length + "\r\n" + Util.Hex(data, 0, data.Length)).First(), false);
        
                }
                else
                {
                    var op = ((WarShared.Protocol.GameOp)data[7]);
                    return LoadPacket(Util.ParseHex("[Client] packet : (0x" + ((int)op).ToString("X").PadLeft(2, '0') + ") " + op.ToString() + " Size = " + data.Length + "\r\n" + Util.Hex(data, 0, data.Length)).First(), true);
                }
        }
        public static MythicPacket LoadPacket(ParsedPacket data, bool client)
        {
            MemoryStream ms = new MemoryStream(data.Data);
            MythicPacket packet = null;
            int index = 2;
            if (client)
            {
                index = 9;
                ms.Position = 7;
            }
            if (index < data.Data.Length)
            {
                WarShared.Protocol.GameOp op = (WarShared.Protocol.GameOp)data.Data[index];
                switch (op)
                {
                    case WarShared.Protocol.GameOp.F_HIT_PLAYER:
                        packet = new F_HIT_PLAYER(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CAST_PLAYER_EFFECT:
                        packet = new F_CAST_PLAYER_EFFECT(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAYER_STATE2:
                        packet = new F_PLAYER_STATE2(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_OBJECT_STATE:
                        packet = new F_OBJECT_STATE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_UPDATE_STATE:
                        packet = new F_UPDATE_STATE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_MOUNT_UPDATE:
                        packet = new F_MOUNT_UPDATE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CREATE_MONSTER:
                        packet = new F_CREATE_MONSTER(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAYER_INVENTORY:
                        packet = new F_PLAYER_INVENTORY(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAYER_HEALTH:
                        packet = new F_PLAYER_HEALTH(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CREATE_PLAYER:
                        packet = new F_CREATE_PLAYER(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_GFX_MOD:
                        packet = new F_GFX_MOD(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_DO_ABILITY:
                        packet = new F_DO_ABILITY(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_USE_ABILITY:
                        packet = new F_USE_ABILITY(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_DO_ABILITY_AT_POS:
                        packet = new F_DO_ABILITY_AT_POS(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_INTERACT:
                        packet = new F_INTERACT(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CREATE_STATIC:
                        packet = new F_CREATE_STATIC(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_INIT_EFFECTS:
                        packet = new F_INIT_EFFECTS(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CHARACTER_INFO:
                        packet = new F_CHARACTER_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_GRAPHICAL_REVISION:
                        packet = new F_GRAPHICAL_REVISION(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_SWITCH_ATTACK_MODE:
                        packet = new F_SWITCH_ATTACK_MODE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_ANIMATION:
                        packet = new F_ANIMATION(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAYER_STATS:
                        packet = new F_PLAYER_STATS(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_SET_ABILITY_TIMER:
                        packet = new F_SET_ABILITY_TIMER(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_KNOCKBACK:
                        packet = new F_KNOCKBACK(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CLIENT_DATA:
                        packet = new F_CLIENT_DATA(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_GET_ITEM:
                        packet = new F_GET_ITEM(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_ITEM_SET_DATA:
                        packet = new F_ITEM_SET_DATA(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CAREER_CATEGORY:
                        packet = new F_CAREER_CATEGORY(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CAREER_PACKAGE_INFO:
                        packet = new F_CAREER_PACKAGE_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_INTERACT_RESPONSE:
                        packet = new F_INTERACT_RESPONSE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_DEATHSPAM:
                        packet = new F_DEATHSPAM(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_BAG_INFO:
                        packet = new F_BAG_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_SCENARIO_INFO:
                        packet = new F_SCENARIO_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_SCENARIO_PLAYER_INFO:
                        packet = new F_SCENARIO_PLAYER_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_GROUP_STATUS_UPDATE:
                        packet = new F_GROUP_STATUS_UPDATE(ms);
                        break;
                    case WarShared.Protocol.GameOp.S_PLAYER_INITTED:
                        packet = new S_PLAYER_INITTED(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_REQUEST_CHAR_RESPONSE:
                        packet = new F_REQUEST_CHAR_RESPONSE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_OBJECTIVE_INFO:
                        packet = new F_OBJECTIVE_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_TOK_ENTRY_UPDATE:
                        packet = new F_TOK_ENTRY_UPDATE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_INTRO_CINEMA:
                        packet = new F_INTRO_CINEMA(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAYER_RENOWN:
                        packet = new F_PLAYER_RENOWN(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_INIT_STORE:
                        packet = new F_INIT_STORE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAYER_JUMP:
                        packet = new F_PLAYER_JUMP(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_LOCALIZED_STRING:
                        packet = new F_LOCALIZED_STRING(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_SWITCH_REGION:
                        packet = new F_SWITCH_REGION(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAY_EFFECT:
                        packet = new F_PLAY_EFFECT(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_SOCIAL_NETWORK:
                        packet = new F_SOCIAL_NETWORK(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAY_SOUND:
                        packet = new F_PLAY_SOUND(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_OBJECT_DEATH:
                        packet = new F_OBJECT_DEATH(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_SET_TARGET:
                        packet = new F_SET_TARGET(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_ACTIVE_EFFECTS:
                        packet = new F_ACTIVE_EFFECTS(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_UPDATE_HOT_SPOT:
                        packet = new F_UPDATE_HOT_SPOT(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_AUCTION_SEARCH_RESULT:
                        packet = new F_AUCTION_SEARCH_RESULT(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CATAPULT:
                        packet = new F_CATAPULT(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_ACTION_COUNTER_INFO:
                        packet = new F_ACTION_COUNTER_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_STORE_BUY_BACK:
                        packet = new F_STORE_BUY_BACK(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PET_INFO:
                        packet = new F_PET_INFO(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_OBJECT_EFFECT_STATE:
                        packet = new F_OBJECT_EFFECT_STATE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_FRIEND_ADD:
                        packet = new F_FRIENDADD(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_GROUP_STATUS:
                        packet = new F_GROUP_STATUS(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_CAREER_PACKAGE_UPDATE:
                        packet = new F_CAREER_PACKAGE_UPDATE(ms);
                        break;
                    case WarShared.Protocol.GameOp.F_PLAYER_LEVEL_UP:
                        packet = new F_PLAYER_LEVEL_UP(ms);
                        break;

                }
                if (packet != null)
                {

                    packet.Data = data.Data;
                    packet.OP = (WarShared.Protocol.GameOp)data.Data[2];
                    packet.OrigString = data.OrigString;
                    packet.Line = data.Line;
                    //   packet.PrevLine = _lastLIne;
                    packet.LineDiff = packet.Line - packet.PrevLine;
                    // _lastLIne = data.Line;
                }
            }
            return packet;
        }

        private void LoadCharacters()
        {
            //if(Packets.ContainsKey(WarShared.Protocol.GameOp.F_PLAYER_STATE2))
            //    foreach (F_PLAYER_STATE2 state in Packets[WarShared.Protocol.GameOp.F_PLAYER_STATE2])
            //    {
            //        if (!Characters.ContainsKey(state.Key))
            //        {
            //            Character c = new Character();
            //            c.ObjectID = state.Key;
            //            Characters[state.Key] = c;
            //        }
            //        Character character = Characters[state.Key];

            //        character.LocalX = state.X;
            //        character.LocalY = state.Y;
            //        character.Z = state.Z;
            //    }


        }
    }
}
