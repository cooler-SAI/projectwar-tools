﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{

    public class F_CHARACTER_INFO:MythicPacket
    {
        //public class AbilityInfo
        //{
        //    public byte A04_AbilityCount { get; set; }
        //    public ushort A05_Unk { get; set; }
        //    public byte A04_GroupType { get; set; }
        //    public List<AbilityData> Abilities { get; set; }

        //    public void DeSerialize(Stream ms)
        //    {
        //        Abilities = new List<AbilityData>();
        //        A04_AbilityCount = FrameUtil.GetUint8(ms);
        //        A05_Unk = FrameUtil.GetUint16(ms);

        //        for (int i = 0; i < A04_AbilityCount; i++)
        //        {
        //            AbilityData data = new AbilityData()
        //            {
        //                AbilityID = FrameUtil.GetUint16(ms),
        //                Level = FrameUtil.GetUint8(ms)
        //            };
        //            Abilities.Add(data);
        //        }
        //    }
        //}
        //public class CharacterData2
        //{

        //    public byte Level { get; set; }
        //    public byte Bolster { get; set; }
        //    public byte Unk1 { get; set; }
        //    public byte WoundsPercent { get; set; }
        //    public byte GetAPPercent { get; set; }
        //    public byte Unk2 { get; set; }
        //    public byte Unk3 { get; set; }
        //    public byte Unk4 { get; set; }
        //    public byte ModelID { get; set; }
        //    public ushort ObjectID { get; set; }
        //    public uint ID { get; set; }
        //    public ushort PetObjectID { get; set; }
        //    public byte PetHealth { get; set; }
        //    public ushort PetUnk { get; set; }
        //    public string Name { get; set; }
        //    public byte Unk5 { get; set; }
        //    public byte CareerLine { get; set; }

        //    public void DeSerialize(MemoryStream ms)
        //    {

        //        Level = FrameUtil.GetUint8(ms);
        //        Bolster = FrameUtil.GetUint8(ms);
        //        Unk1 = FrameUtil.GetUint8(ms);
        //        WoundsPercent = FrameUtil.GetUint8(ms);
        //        GetAPPercent = FrameUtil.GetUint8(ms);
        //        Unk2 = FrameUtil.GetUint8(ms);
        //        Unk3 = FrameUtil.GetUint8(ms);
        //        Unk4 = FrameUtil.GetUint8(ms);
        //        ModelID = FrameUtil.GetUint8(ms);
        //        ObjectID = FrameUtil.GetUint16(ms);
        //        ID = FrameUtil.GetUint32(ms);
        //        PetObjectID = FrameUtil.GetUint16(ms);
        //        PetHealth = FrameUtil.GetUint8(ms);
        //        PetUnk = FrameUtil.GetUint16(ms);
        //        Name = FrameUtil.GetPascalString(ms);
        //        Unk5 = FrameUtil.GetUint8(ms);
        //        CareerLine = FrameUtil.GetUint8(ms);
        //    }
        //}


        //public class CharacterData
        //{
        //    public uint CharacterID { get; set; }
        //    public byte Unk { get; set; }
        //    public byte ModelID { get; set; }
        //    public byte Race { get; set; }
        //    public byte Level { get; set; }
        //    public byte Bolster { get; set; }
        //    public byte Line { get; set; }
        //    public byte Realm { get; set; }
        //    public bool Leader { get; set; }
        //    public byte Unk1 { get; set; }
        //    public bool Online { get; set; }
        //    public byte NameLength { get; set; }
        //    public byte[] Unk2 { get; set; }
        //    public string Name { get; set; }
        //    public byte GuildNameLength { get; set; }
        //    public byte[] Unk3 { get; set; }
        //    public string GuildName { get; set; }
        //    public uint X { get; set; }
        //    public uint Y { get; set; }
        //    public uint Z { get; set; }
        //    public byte[] Unk4 { get; set; }
        //    public ushort ZoneID { get; set; }
        //    public byte Unk5 { get; set; }
        //    public byte PercentHealth { get; set; }
        //    public byte PercentAP { get; set; }
        //    public byte PercentMorale { get; set; }


        //    public void DeSerialize(MemoryStream ms)
        //    {
              

        //        try
        //        {

        //            return;
        //            CharacterID = 0;// (uint)CodedInputStream.ReadRawVarint32(ms);
        //            Unk = FrameUtil.GetUint8(ms);
        //            ModelID = FrameUtil.GetUint8(ms);
        //            Race = FrameUtil.GetUint8(ms);
        //            Level = FrameUtil.GetUint8(ms);
        //            Bolster = FrameUtil.GetUint8(ms);
        //            Line = FrameUtil.GetUint8(ms);
        //            Realm = FrameUtil.GetUint8(ms);
        //            Leader = FrameUtil.GetUint8(ms) > 0;
        //            Unk1 = FrameUtil.GetUint8(ms);
        //            Online = FrameUtil.GetUint8(ms) > 0;
        //            NameLength = FrameUtil.GetUint8(ms);
        //            Unk2 = FrameUtil.GetByteArray(ms, 3);
        //            Name = FrameUtil.GetString(ms, NameLength);
        //            GuildNameLength = FrameUtil.GetUint8(ms);
        //            Unk3 = FrameUtil.GetByteArray(ms, 3);
        //            GuildName = FrameUtil.GetString(ms, GuildNameLength);



        //            //X = CodedInputStream.ReadRawVarint32(ms);
        //            //Y = CodedInputStream.ReadRawVarint32(ms);
        //            //Z = CodedInputStream.ReadRawVarint32(ms);

        //            long pos = ms.Position;

        //            var a1 = FrameUtil.Unpack(ms, 1)[0];
        //            ms.Position = pos;
        //            if (a1 < 1000)
        //                Unk4 = FrameUtil.GetByteArray(ms, 9);
        //            else
        //                Unk4 = FrameUtil.GetByteArray(ms, 9);

        //            // var a2 = CodedInputStream.ReadRawVarint32(ms);
        //            //  var a3 = FrameUtil.GetUint8(ms);
        //            //  var a3 = CodedInputStream.ReadRawVarint32(ms);
        //            //   var a4 = CodedInputStream.ReadRawVarint32(ms);
        //            ZoneID = 0;// (byte)CodedInputStream.DecodeZigZag32(CodedInputStream.ReadRawVarint32(ms));
        //            var unk44 = FrameUtil.GetUint8(ms);
        //            PercentHealth = FrameUtil.GetUint8(ms);
        //            PercentAP = FrameUtil.GetUint8(ms);
        //            PercentMorale = FrameUtil.GetUint8(ms);

        //        }

        //        catch (Exception e)
        //        {
        //        }

        //    }
        //}

        //public class GroupData2
        //{
         
        //    public uint A05_GroupID { get; set; }
        //    public byte A06_GroupMemberCount { get; set; }
        //    public List<CharacterData> Characters { get; set; }

        //    public GroupData2()
        //    {
        //        Characters = new List<CharacterData>();
        //    }
        //    public void DeSerialize(MemoryStream ms)
        //    {
        //        A05_GroupID = (uint)FrameUtil.Unpack(ms, 1)[0];
        //        A06_GroupMemberCount = FrameUtil.GetUint8(ms);
        //        for (int i = 0; i < A06_GroupMemberCount; i++)
        //        {
        //            CharacterData c = new CharacterData();
        //            c.DeSerialize(ms);
        //            Characters.Add(c);
        //        }
        //    }

        //}

        //public class GroupData3
        //{

        //    public uint A05_GroupID { get; set; }
        //    public byte A06_GroupMemberCount { get; set; }
        //    public List<CharacterData> Characters { get; set; }

        //    public GroupData3()
        //    {
        //        Characters = new List<CharacterData>();
        //    }
        //    public void DeSerialize(MemoryStream ms)
        //    {
        //     //   A05_GroupID = (uint)FrameUtil.Unpack(ms, 1)[0];
        //        A06_GroupMemberCount = FrameUtil.GetUint8(ms);
        //        for (int i = 0; i < A06_GroupMemberCount; i++)
        //        {
        //            CharacterData c = new CharacterData();
        //            c.DeSerialize(ms);
        //            Characters.Add(c);
        //        }
        //    }

        //}
        //public class AbilityData
        //{
        //    public ushort AbilityID { get; set; }
        //    public byte Level { get; set; }
        //}
   

        //public byte A03_Type { get; set; }
        //public byte A04_GroupType { get; set; }
        //public AbilityInfo Abilities { get; set; }
        //public GroupData2 Group { get; set; }
      
        //public string AbilitiesString
        //{
        //    get
        //    {
        //        string result = "";
        //        //foreach (var a in Abilities)
        //        //{
        //        //    result += a.AbilityID + ": " + a.Level + "\r\n";
        //        //}
        //        return result;
        //    }
        //}



        //public class GroupData1
        //{

        //    public ushort A05_Unk { get; set; }
        //    public uint A07_LeaderID { get; set; }
        //    public byte A11_CharacterCount { get; set; }
        //    public List<object> Characters { get; set; }

        //    public GroupData1()
        //    {
              
        //    }
        //    public void DeSerialize(MemoryStream ms)
        //    {
        //        A05_Unk = FrameUtil.GetUint16(ms);
        //        A07_LeaderID = FrameUtil.GetUint32(ms);
        //        A11_CharacterCount = FrameUtil.GetUint8(ms);

        //        Characters = new List<object>();
        //        for (int i = 0; i < A11_CharacterCount; i++)
        //        {
        //            CharacterData2 c = new CharacterData2();
        //            c.DeSerialize(ms);
        //            Characters.Add(c);
        //        }
        //    }

        //}

   

    

        //public UInt32 Skills { get; set; }
        //public List<object> Data { get; set; }

        //public F_CHARACTER_INFO(MemoryStream ms)
        //    : base(WarShared.Protocol.GameOp.F_CHARACTER_INFO, ms, false)
        //{
        //    Group = new GroupData2();
        //}
        //public GroupData3 Warband { get; set; }
        //public override void Load(MemoryStream ms)
        //{
   
        //    ms.Position = 3;
        //    //base.Load(ms);
        //    A03_Type = FrameUtil.GetUint8(ms);
        //    Data = new List<object>();

        //    if (A03_Type == 1)
        //    {
        //        Abilities = new AbilityInfo();
        //        Abilities.DeSerialize(ms);
        //    }
        //    if (A03_Type == 6)
        //    {
        //        A04_GroupType = FrameUtil.GetUint8(ms);
        //     //   FrameUtil.GetUint8(ms);
      
        //        if (A04_GroupType == 1)
        //        {
        //            var data = new GroupData1();
        //           data.DeSerialize(ms);
        //            Data.Add(data);
        //        }
        //        if (A04_GroupType == 2)
        //        {
        //            Group = new GroupData2();
        //           // Group.DeSerialize(ms);
        //            Data.Add(Group);
        //        }
        //        if (A04_GroupType == 3)
        //        {
        //         //   while (ms.Position<  ms.Length)
        //            {
        //                byte[] data = { 0x01, 0x00, 0xD8, 0xA1, 0x01, 0x01, 0x9D, 0xE4, 0x6C, 0x0F };
        //                var groupID = FrameUtil.GetUint16R(ms);


        //                var u = FrameUtil.Unpack(ms, 1)[0];
        //             //   FrameUtil.GetUint8(ms);
        //           //     var vals = FrameUtil.UnpackUnsigned(ms, 4);
        //              //  return;
        //               Warband = new GroupData3();
        //               Warband.DeSerialize(ms);
        //            }
             
        //        }
        //    }
        //}

        public ushort Type { get; set; }
        public byte GroupType { get; set; }
        public List<object> Data { get; set; }

        public class Member
        {
            public uint A00_CharacterID { get; set; }
            public byte A04_Unk { get; set; }
            public byte A05_ModelID { get; set; }
            public Race A06_Race { get; set; }
            public byte A07_Level { get; set; }
            public byte A08_Bolster { get; set; }
            public CareerLine A09_CareerLine { get; set; }
            public byte A10_Realm { get; set; }
            public byte A11_Leader { get; set; }
            public byte A12_Unk { get; set; }
            public byte A12_Online { get; set; }
            public string A17_Name { get; set; }
            public string A22_GuildName { get; set; }
            public uint A23_X { get; set; }
            public uint A24_Y { get; set; }
            public uint A25_Z { get; set; }
            public uint A26_Unk { get; set; }
            public uint A27_Unk { get; set; }
            public int A28_Unk { get; set; }
            public ushort A32_ZoneID { get; set; }
            public uint A33_Unk { get; set; }
            public byte A34_Health { get; set; }
            public byte A35_AP { get; set; }
            public byte A36_Morale { get; set; }

            public override string ToString()
            {
                return A17_Name;
            }
            MemoryStream Output = new MemoryStream();

            public void Load(MemoryStream ms)
            {
                Output.Position = ms.Position;

                A00_CharacterID = (uint)FrameUtil.ReadVarUInt(ms);
                A04_Unk = (byte)FrameUtil.ReadVarUInt(ms);
                A05_ModelID = (byte)FrameUtil.ReadVarUInt(ms);
                A06_Race = (Race)FrameUtil.ReadVarUInt(ms);
                A07_Level = (byte)FrameUtil.ReadVarUInt(ms);
                A08_Bolster = (byte)FrameUtil.ReadVarUInt(ms);
                A09_CareerLine = (CareerLine)FrameUtil.ReadVarUInt(ms);
                A10_Realm = (byte)FrameUtil.ReadVarUInt(ms);
                A11_Leader = (byte)FrameUtil.ReadVarUInt(ms);
                A12_Unk = (byte)FrameUtil.ReadVarUInt(ms);   //main assist?
                A12_Online = (byte)FrameUtil.ReadVarUInt(ms);
                A17_Name = FrameUtil.GetPascalString32(ms);
                A22_GuildName = FrameUtil.GetPascalString32(ms);
                A23_X = (uint)FrameUtil.ReadVarUInt(ms);
                A24_Y = (uint)FrameUtil.ReadVarUInt(ms);
                A25_Z = (uint)FrameUtil.ReadVarUInt(ms);
                A26_Unk = FrameUtil.GetUint32(ms);
                A27_Unk = (uint)FrameUtil.ReadVarUInt(ms);
                A28_Unk = FrameUtil.ReadVarInt(ms);
                A32_ZoneID = (ushort)FrameUtil.ReadVarInt(ms);
                A33_Unk = (uint)FrameUtil.ReadVarUInt(ms);
                A34_Health = (byte)FrameUtil.ReadVarUInt(ms);
                A35_AP = (byte)FrameUtil.ReadVarUInt(ms);
                A36_Morale = (byte)FrameUtil.ReadVarUInt(ms);
            }
        }

        public class F_CHARACTER_INFO_GROUP2
        {
            public uint A00_GroupID { get; set; }
            public byte A04_MemberCount { get; set; }
            public List<Member> A05_Members { get; set; }
       
            public F_CHARACTER_INFO_GROUP2()
            {
            }
            public void Load(MemoryStream ms)
            {
                A05_Members = new List<Member>();
                A00_GroupID = (uint)FrameUtil.ReadVarUInt(ms);
                A04_MemberCount = (byte)FrameUtil.ReadVarUInt(ms);
                for (int i = 0; i < A04_MemberCount; i++)
                {
                    var member = new Member();
                    member.Load(ms);
                    A05_Members.Add(member);
                }
            }
        }




        public class WarbandParty
        {
            public uint Unk1 { get; set; }
            public uint Count { get; set; }
            public List<Member> Characters { get; set; }
            public void Load(MemoryStream ms)
            {
                 Unk1 = FrameUtil.ReadVarUInt(ms);
                 Characters = new List<Member>();
                 if (Unk1 > 0)
                 {
                     Count = FrameUtil.ReadVarUInt(ms);
     

                     if (Count <= 6)
                     {
                         for (int i = 0; i < Count; i++)
                         {
                             Member c = new Member();
                             c.Load(ms);
                             Characters.Add(c);
                         }
                     }
                 }
            }
        }

        public class ScenarioGroup
        {
            public byte Level { get; set; }
            public byte Bolster { get; set; }
            public byte v7 { get; set; }
            public byte WoundsPct { get; set; }
            public byte APPct { get; set; }
            public byte v6 { get; set; }
            public byte v32 { get; set; }
            public ushort ModelID { get; set; }
            public ushort ObjectID { get; set; }
            public uint CharacterID { get; set; }
            public ushort PetObjectID { get; set; }
            public byte PathWoundsPct { get; set; }
            public ushort PetModelID { get; set; }
            public string Name { get; set; }
            public CareerLine CareerLine { get; set; }

            public void Load(MemoryStream ms)
            {
                Level = FrameUtil.GetUint8(ms);
                Bolster = FrameUtil.GetUint8(ms);
                v7 = FrameUtil.GetUint8(ms);
                WoundsPct = FrameUtil.GetUint8(ms);
                APPct = FrameUtil.GetUint8(ms);
                v6 = FrameUtil.GetUint8(ms);
                v32 = FrameUtil.GetUint8(ms);
                ModelID = FrameUtil.GetUint16(ms);
                ObjectID = FrameUtil.GetUint16(ms);
                CharacterID = FrameUtil.GetUint32(ms);
                PetObjectID = FrameUtil.GetUint16(ms);
                PathWoundsPct = FrameUtil.GetUint8(ms);
                PetModelID = FrameUtil.GetUint16(ms);
                Name = FrameUtil.GetPascalString(ms);
                CareerLine = (CareerLine)FrameUtil.GetUint16(ms);
            }
        }

        public class F_CHARACTER_INFO_GROUP0 //scenario group
        {

            public byte v34 { get; set; }
            public ushort v3 { get; set; }
            public UInt32 LeaderID { get; set; }

            public List<ScenarioGroup> Members { get; set; }

            public F_CHARACTER_INFO_GROUP0()
            {
                Members = new List<ScenarioGroup>();
            }
            public void Load(MemoryStream ms)
            {
                v34 = FrameUtil.GetUint8(ms);
                v3 = FrameUtil.GetUint16(ms);

                LeaderID = FrameUtil.GetUint32(ms);
                var count = FrameUtil.GetUint8(ms);

                for (int i = 0; i < count; i++)
                {
                    var s = new ScenarioGroup();
                    s.Load(ms);
                    Members.Add(s);
                }
            }
        }

        public class F_CHARACTER_INFO_ABILITY1
        {
            public class Ability
            {
                public ushort ID { get; set; }
                public byte Level { get; set; }
                public string Name { get; set; }
                public void Load(MemoryStream ms)
                {
                    ID = FrameUtil.GetUint16(ms);
                    Level = FrameUtil.GetUint8(ms);
                    Name = frmMain.Abilities[ID].Name;
                }
                public override string ToString()
                {
                    return Name ?? ID.ToString();
                }
            }
            public byte AbilityCount { get; set; }
            public ushort Unk1 { get; set; }
            public List<Ability> Abilities { get; set; }
            public F_CHARACTER_INFO_ABILITY1()
            {

            }
            public void Load(MemoryStream ms)
            {
                AbilityCount = FrameUtil.GetUint8(ms);
                Unk1 = FrameUtil.GetUint16(ms);
                Abilities = new List<Ability>();
                for (int i = 0; i < AbilityCount; i++)
                {
                    var ab = new Ability();
                    ab.Load(ms);
                 
                  //  if (ab.ID == 14410 && AbilityCount < 10 )
                    {
                        Abilities.Add(ab);
                       
                    }
                }

            }
        }

        public class F_CHARACTER_INFO_GROUP1 //group member remove
        {

            public uint GroupID { get; set; }
            public uint CharacterID { get; set; }

            public F_CHARACTER_INFO_GROUP1()
            {

            }
            public void Load(MemoryStream ms)
            {
                GroupID = FrameUtil.ReadVarUInt(ms);
                CharacterID = FrameUtil.ReadVarUInt(ms);
            }
        }


           public class F_CHARACTER_INFO_GROUP3
           {

               public uint Unk1 { get; set; }

               public List<WarbandParty> Parties { get; set; }

               public F_CHARACTER_INFO_GROUP3()
               {
                   Parties = new List<WarbandParty>();
               }
               public void Load(MemoryStream ms)
               {
                   Unk1 = FrameUtil.ReadVarUInt(ms);
                   for (int r = 0; r < 4; r++)
                   {
                       var party = new WarbandParty();
                       party.Load(ms);
                       Parties.Add(party);
                   }
               }
           }

           public class F_CHARACTER_INFO_GROUP4
           {
               public uint PartyID { get; set; }
               public List<PartyMemberInfo> Members { get; set; }
               public class PartyMemberInfo
               {
                   public uint CharacterID { get; set; }
                   public uint ObjectID { get; set; }
                   public uint Unk1 { get; set; }
                   public uint Unk2 { get; set; }
                   public override string ToString()
                   {
                       return "CharID:" + CharacterID + " OID:" + ObjectID + " Unk1:" + Unk1 + " Unk2:" + Unk2;
                   }
               }

               public void Load(MemoryStream ms)
               {
                   Members = new List<PartyMemberInfo>();
                   PartyID = FrameUtil.ReadVarUInt(ms);
               
                   for (int i = 0; i < 6; i++)
                   {
                       var member = new PartyMemberInfo();
                       member.CharacterID = FrameUtil.ReadVarUInt(ms);

                       if (member.CharacterID > 0)
                       {
                           member.ObjectID = FrameUtil.ReadVarUInt(ms);

                           var more = FrameUtil.ReadVarUInt(ms);
                           if (more > 0)
                           {
                               member.Unk1 = FrameUtil.ReadVarUInt(ms);
                               member.Unk2 = FrameUtil.ReadVarUInt(ms);
                           }
                           Members.Add(member);
                       
                       }
                   }
               }
           }
        public F_CHARACTER_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CHARACTER_INFO, ms, false)
        {
          
        }


        public override void Load(MemoryStream ms)
        {
            Data = new List<Object>();
            ms.Position = 3;


            Type = FrameUtil.GetUint8(ms);

            if (Type == 6)
            {
                GroupType = FrameUtil.GetUint8(ms);

                 if (GroupType == 0)
                {
                    if (ms.Position < ms.Length)
                    {
                        var g = new F_CHARACTER_INFO_GROUP0();
                        g.Load(ms);
                        Data.Add(g);
                    }
                }
                 if (GroupType == 1)
                 {
                     var g = new F_CHARACTER_INFO_GROUP1();
                     g.Load(ms);
                     Data.Add(g);
                 }
                if (GroupType == 2)
                {
                    var g = new F_CHARACTER_INFO_GROUP2();
                    g.Load(ms);
                    Data.Add(g);
                }
                if (GroupType == 3)
                {
                    var g = new F_CHARACTER_INFO_GROUP3();
                    g.Load(ms);
                    Data.Add(g);

                }
                if (GroupType == 4)
                {
                    var g = new F_CHARACTER_INFO_GROUP4();
                    g.Load(ms);
                    Data.Add(g);
                }
            }
            if (Type == 1)
            {
                var g = new F_CHARACTER_INFO_ABILITY1();
                g.Load(ms);
                if (g.Abilities.Count > 0)
                {
                    Data.Add(g);
                    foreach (var a in g.Abilities)
                    {
                        var ab = frmMain.Abilities[a.ID];

                        if (ab.ID == 592 && a.Level > 40)
                        {
                           Console.WriteLine(ab.Name + " Level:" + a.Level);
                        }
                    }
                }


            }
            ZUnread = (uint)(ms.Length - ms.Position);

        }
        
    

    
    }
}
