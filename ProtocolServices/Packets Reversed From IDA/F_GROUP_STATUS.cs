﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_GROUP_STATUS : MythicPacket
    {
        public ushort GroupID { get; set; }
        public byte Type { get; set; }
        public byte Hide { get; set; }
        public byte LootMode { get; set; }
        public bool NoNeedOnGreed { get; set; }
        public bool AutoLootInRvR { get; set; }
        public byte LootThreshold { get; set; }
        public uint MasterLooterID { get; set; }
        public uint MainAssistID { get; set; }
        public uint LeaderID { get; set; }

        public bool IsPublic { get; set; }
        public bool IsGuildOnly { get; set; }
        public bool IsAllianceOnly { get; set; }
        public bool IsPasswordReq { get; set; }

        public List<uint> WarbandAssistants { get; set; }


        public F_GROUP_STATUS(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_GROUP_STATUS, ms, false)
        {

        }


        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            GroupID = FrameUtil.GetUint16(ms);
            Type = FrameUtil.GetUint8(ms);
            Hide = FrameUtil.GetUint8(ms);
            WarbandAssistants = new List<uint>();
            if (Type == 1)
            {
                LootMode = FrameUtil.GetUint8(ms);
                LootThreshold = FrameUtil.GetUint8(ms);
                MasterLooterID = FrameUtil.GetUint32(ms);
                NoNeedOnGreed = FrameUtil.GetUint8(ms) != 0;
                AutoLootInRvR = FrameUtil.GetUint8(ms) != 0;
                MainAssistID = FrameUtil.GetUint32(ms);
                LeaderID = FrameUtil.GetUint32(ms);
            }
            else if (Type == 2)
            {
                IsPublic = FrameUtil.GetUint8(ms) != 0;
                IsGuildOnly = FrameUtil.GetUint8(ms) != 0;
                IsAllianceOnly = FrameUtil.GetUint8(ms) != 0;
                IsPasswordReq = FrameUtil.GetUint8(ms) != 0;
            }
            else if (Type == 3)
            {
                var count = FrameUtil.ReadVarUInt(ms);
                if (Hide != 255)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var characterID = FrameUtil.ReadVarUInt(ms);
                        WarbandAssistants.Add(characterID);
                    }
                }
            }

        }




    }
}

