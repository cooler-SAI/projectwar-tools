﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_OBJECT_DEATH: MythicPacket
    {
        public ushort A03_TargetID { get; set; }
        public byte A05_Unk1 { get; set; }
        public byte A06_Unk2 { get; set; }
        public ushort A07_KilledBy { get; set; }
         public byte A09_Unk3 { get; set; }
         public byte A10_Unk4 { get; set; }
         public byte A11_Unk5 { get; set; }
         public byte A12_Unk6 { get; set; }
         public byte A13_Unk7 { get; set; }
         public byte A14_Unk8 { get; set; }



        public F_OBJECT_DEATH()
        {
        }
        public F_OBJECT_DEATH(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_OBJECT_DEATH, ms, true)
        {
        }

    }
}
