﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_CATAPULT : MythicPacket
    {
        public ushort A03_ObjectID { get; set; }
        public ushort A05_FromZoneID { get; set; }
        public ushort A07_CasterX { get; set; }
        public ushort A09_CasterY { get; set; }
        public ushort A11_CasterZ { get; set; }
        public ushort A13_TargetZoneID { get; set; }
        public ushort A15_TargetX { get; set; }
        public ushort A17_TargetY { get; set; }
        public ushort A19_TargetZ { get; set; }
        public ushort A21_Unk { get; set; }
        public ushort A23_Unk { get; set; }
        public ushort A25_Unk { get; set; }
        public ushort A27_Unk { get; set; }
        public ushort A29_Unk { get; set; }
        public ushort A31_Unk { get; set; }
        public ushort A33_Unk { get; set; }
        public ushort A35_Unk { get; set; }
        public ushort A47_Unk { get; set; }

        public F_CATAPULT(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CATAPULT, ms, true)
        {
        }
    }
}
