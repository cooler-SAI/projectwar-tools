﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_STORE_BUY_BACK : MythicPacket
    {
        public byte Count { get; set; }
        public uint Price { get; set; }

        public List<F_GET_ITEM.ItemData> Items { get; set; }
        public F_STORE_BUY_BACK(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_STORE_BUY_BACK, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            byte[] data = ms.ToArray();

            Items = new List<F_GET_ITEM.ItemData>();
            Count = FrameUtil.GetUint8(ms);
 
            for (int i = 0; i < Count; i++)
            {
                Price = FrameUtil.GetUint32(ms);
                F_GET_ITEM.ItemData t = new F_GET_ITEM.ItemData();
                t.DeSerialize(ms, true);
                Items.Add(t);
            }

        }
    }

}
