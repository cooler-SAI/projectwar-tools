﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_DO_ABILITY:MythicPacket 
    {
        public UInt32 Unk1 { get; set; }
        public UInt32 Unk2 { get; set; }
        public UInt32 Unk3 { get; set; }
        public UInt16 AbilityID { get; set; }

        public F_DO_ABILITY()
        {
        }
        public F_DO_ABILITY(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_DO_ABILITY, ms, true)
        {
        }
    }
}
