﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class F_ACTION_COUNTER_INFO : MythicPacket
    {
        public byte Flag { get; set; }
        public byte Unk0 { get; set; }
        public ushort Count { get; set; }
     

        public List<Region> Regions { get; set; }

        public class Encounter
        {
            public string Name { get; set; }
            public void Load(MemoryStream ms)
            {
                Name = FrameUtil.GetPascalString(ms);
                FrameUtil.GetUint8(ms);
            }
            public override string ToString()
            {
                return Name;
            }
        }
        public class Region
        {
            public ushort ID { get; set; }
            public uint Unk1 { get; set; }
            public uint LockoutMinutes { get; set; }
            public ushort Count { get; set; }
            public ushort Type { get; set; } //0 complete, 1 killed
            public string Name { get; set; }
            public List<Encounter> Encounters {get;set;}

            public void Load(MemoryStream ms)
            {


                ID = FrameUtil.GetUint16(ms);
                Unk1 = FrameUtil.GetUint32(ms);
                LockoutMinutes = FrameUtil.GetUint32(ms);
                Type = FrameUtil.GetUint16R(ms);
                Name = FrameUtil.GetPascalString(ms);
                FrameUtil.GetUint8(ms);
                Encounters = new List<Encounter>();

                for (int i = 0; i < 16; i++)
                {
                

                    var encounter = new Encounter();
                    encounter.Load(ms);
                    if(encounter.Name != "")
                    Encounters.Add(encounter);

                }
              //  FrameUtil.GetUint32(ms);
                FrameUtil.GetUint8(ms);
            }
            public override string ToString()
            {
                return Name;
            }
        }
        public F_ACTION_COUNTER_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_ACTION_COUNTER_INFO, ms, false)
        {
        }
        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray();
            ms.Position = 3;

            Flag = FrameUtil.GetUint8(ms);
            Unk0 = FrameUtil.GetUint8(ms);
            Count = FrameUtil.GetUint16(ms);
            FrameUtil.GetUint16(ms);
            Regions = new List<Region>();
            for (int i = 0; i < Count; i++)
            {
                var instance = new Region();
                instance.Load(ms);
                //if (instance.Count > 0 && instance.Count != 255)
                {
                    Regions.Add(instance);
                }
            }
        }
    }
}
