﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarShared;
using WarShared.Readers;
using static WarAdmin.Packets.F_GET_ITEM;

namespace WarAdmin.Packets
{
    public class F_BAG_INFO : MythicPacket
    {
        public byte Type { get; set; }
        public EventInfo EventInf { get; set; } = new EventInfo();


        public class EventInfo
        {
            public uint ID { get; set; }
            public string EventTitle { get; set; }
            public string EventSubTitle { get; set; }
            public string EventDescription { get; set; }
            public string TasksDescription { get; set; }
            public uint EventImageIndex { get; set; }
            public uint Unk2 { get; set; }
            public uint Unk3 { get; set; }
            public byte Eligible { get; set; }
            public uint Progress { get; set; }
            public byte RewardGroupCount { get; set; }
            public uint Unk23 { get; set; }

            public List<Reward> Rewards { get; set; } = new List<Reward>();
            public List<EventTask> Tasks { get; set; } = new List<EventTask>();

            public class Reward
            {
                public uint GroupIndex { get; set; }
                public uint DataLength { get; set; }
                public uint Unk6 { get; set; }
                public uint Unk7 { get; set; }
                public byte Unk8 { get; set; }
                public byte Unk9 { get; set; }
                public byte ItemCount { get; set; }
                public List<F_GET_ITEM.ItemData> Items { get; set; } = new List<ItemData>();

                public void DeSerialize(MemoryStream ms)
                {
                    GroupIndex = PacketUtil.GetUint32R(ms);
                    DataLength = PacketUtil.GetUint32R(ms);

                    var pos = ms.Position;
                    Unk6 = PacketUtil.GetUint32R(ms);
                    Unk7 = PacketUtil.GetUint32R(ms);

                    Unk8 = PacketUtil.GetUint8(ms);
                    Unk9 = PacketUtil.GetUint8(ms);
                    ItemCount = PacketUtil.GetUint8(ms);
                
                    for (int i = 0; i < ItemCount; i++)
                    {
                        var item = new F_GET_ITEM.ItemData();
                        item.DeSerialize(ms, true);
                        Items.Add(item);
                    }
                    var pos2 = ms.Position;
                    var len = pos2 - pos;
                    if (DataLength != len)
                        throw new Exception("Corruption");
                }
            }

            public class EventTask
            {
                public uint ID { get; set; }
                public string Name { get; set; }
                public string Description { get; set; }
                public uint Unk11 { get; set; }
                public uint Unk12 { get; set; }
                public uint TaskCompleted { get; set; }
                public uint TaskTotal { get; set; }
                public uint Unk15 { get; set; }
                public byte Unk9 { get; set; }
                public byte SubTaskCount { get; set; }
                public List<EventSubTask> SubTasks { get; set; } = new List<EventSubTask>();

                public void DeSerialize(MemoryStream ms)
                {
                    ID = PacketUtil.GetUint32R(ms);
                    Name = PacketUtil.GetPascalString(ms);
                    Description = PacketUtil.GetPascalString(ms);
                    Unk11 = PacketUtil.GetUint32R(ms);
                    Unk12 = PacketUtil.GetUint32R(ms);
                    TaskCompleted = PacketUtil.GetUint32R(ms);
                    TaskTotal = PacketUtil.GetUint32R(ms);
                    Unk15 = PacketUtil.GetUint32R(ms);
                    Unk9 = PacketUtil.GetUint8(ms);
                    SubTaskCount = PacketUtil.GetUint8(ms);

                    for (int s = 0; s < SubTaskCount; s++)
                    {
                        var subTask = new EventSubTask();
                        subTask.DeSerialize(ms);
                        SubTasks.Add(subTask);
                    }
                }
            }

            public class EventSubTask
            {
                public uint ID { get; set; }
                public string Name { get; set; }
                public uint Unk18 { get; set; }
                public byte Unk19 { get; set; }
                public uint Unk20 { get; set; }
                public uint TaskComplete { get; set; }
                public uint TaskTotal { get; set; }
                public uint Unk21 { get; set; }
                public ushort Unk22 { get; set; }


                public void DeSerialize(MemoryStream ms)
                {
                    ID = PacketUtil.GetUint32R(ms);
                    Name = PacketUtil.GetPascalString(ms);
                    Unk18 = PacketUtil.GetUint32R(ms);
                    Unk19 = PacketUtil.GetUint8(ms);
                    Unk20 = PacketUtil.GetUint32R(ms);
                    TaskComplete = PacketUtil.GetUint32R(ms);
                    TaskTotal = PacketUtil.GetUint32R(ms);
                    Unk21 = PacketUtil.GetUint32R(ms);
                    Unk22 = PacketUtil.GetUint16(ms);
                }
            }

            public void Load(MemoryStream ms)
            {
                var pos = ms.Position;
                ms.Position = pos;

                ID = PacketUtil.GetUint32R(ms);
                EventTitle = PacketUtil.GetPascalString(ms);
                EventSubTitle = PacketUtil.GetPascalString(ms);
                EventDescription = PacketUtil.GetPascalString(ms);
                TasksDescription = PacketUtil.GetPascalString(ms);

                EventImageIndex = PacketUtil.GetUint32R(ms);
                Unk2 = PacketUtil.GetUint32R(ms);
                Unk3 = PacketUtil.GetUint32R(ms);
                Eligible = PacketUtil.GetUint8(ms);
                Progress = PacketUtil.GetUint32R(ms);
                RewardGroupCount = PacketUtil.GetUint8(ms);

                for (int rewardGroups = 0; rewardGroups < RewardGroupCount; rewardGroups++)
                {
                    var reward = new Reward();
                    reward.DeSerialize(ms);
                    Rewards.Add(reward);
                }

                var tasks = PacketUtil.GetUint8(ms);
                for (int i = 0; i < tasks; i++)
                {
                    var task = new EventTask();
                    task.DeSerialize(ms);
                    Tasks.Add(task);
                }
                Unk23 = PacketUtil.GetUint32R(ms);
            }

        }

        public F_BAG_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_BAG_INFO, ms, true)
        {
           
        }
        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            Type = FrameUtil.GetUint8(ms);
            if (Type == 10)
            {
                EventInf = new EventInfo();
                EventInf.Load(ms);
            }
        }
   
    }
}
