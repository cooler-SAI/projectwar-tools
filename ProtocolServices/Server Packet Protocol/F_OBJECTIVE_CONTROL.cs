using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_OBJECTIVE_CONTROL, FrameType.Game)]
    public class F_OBJECTIVE_CONTROL : Frame
    {
        public F_OBJECTIVE_CONTROL() : base((int)GameOp.F_OBJECTIVE_CONTROL)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
