using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAY_TIME_STATS, FrameType.Game)]
    public class F_PLAY_TIME_STATS : Frame
    {
        public F_PLAY_TIME_STATS() : base((int)GameOp.F_PLAY_TIME_STATS)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
