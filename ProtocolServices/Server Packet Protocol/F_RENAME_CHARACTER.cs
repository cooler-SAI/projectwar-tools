using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_RENAME_CHARACTER, FrameType.Game)]
    public class F_RENAME_CHARACTER : Frame
    {
        public F_RENAME_CHARACTER() : base((int)GameOp.F_RENAME_CHARACTER)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
