using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_FRIEND_ADD, FrameType.Game)]
    public class F_FRIEND_ADD : Frame
    {
        public F_FRIEND_ADD() : base((int)GameOp.F_FRIEND_ADD)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
