using System.Collections.Generic;
using WarServer.Game.Entities;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_STATS, FrameType.Game)]
    public class F_PLAYER_STATS : Frame
    {
        private PlayerData Player;
        private Dictionary<BonusType, float> Stats = new Dictionary<BonusType, float>();

        public F_PLAYER_STATS() : base((int)GameOp.F_PLAYER_STATS)
        {
        }

        public static F_PLAYER_STATS Create(Player player, Dictionary<BonusType, float> stats)
        {
            return new F_PLAYER_STATS()
            {
                Player = player.Data,
                Stats = stats
            };
        }

		protected override void SerializeInternal()
        {
            WriteByte((byte)Stats.Count);//(byte)client.Character.Statistics.Count);
            WriteByte((byte)Player.GetTacticSlotFlags()); //tactic slot count
            WriteByte(0);
            WriteByte(0);
            WriteUInt16((ushort)Stats[BonusType.ARMOR]); //armor
            WriteByte((byte)0);//c.BolsterLevel);
            WriteByte((byte)Player.EffectiveLevel);

            foreach (var stat in Stats)
            {

                WriteByte((byte)stat.Key);
                WriteUInt16((ushort)(stat.Value));

            }
        }
    }
}
