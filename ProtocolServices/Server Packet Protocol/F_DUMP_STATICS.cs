using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DUMP_STATICS, FrameType.Game)]
    public class F_DUMP_STATICS : Frame
    {
        public F_DUMP_STATICS() : base((int)GameOp.F_DUMP_STATICS)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
