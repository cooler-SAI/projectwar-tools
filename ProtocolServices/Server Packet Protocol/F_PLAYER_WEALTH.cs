using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Game.Entities;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_WEALTH, FrameType.Game)]
    public class F_PLAYER_WEALTH : Frame
    {
        private  long Gold;
        public F_PLAYER_WEALTH() : base((int)GameOp.F_PLAYER_WEALTH)
        {
        }

        public static F_PLAYER_WEALTH Create(Player player)
        {
            return new F_PLAYER_WEALTH()
            {
                Gold = player.Data.Gold
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt64((long)Gold);
        }
    }
}
