using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_DEATH, FrameType.Game)]
    public class F_PLAYER_DEATH : Frame
    {
        public F_PLAYER_DEATH() : base((int)GameOp.F_PLAYER_DEATH)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
