using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_MYSTERY_BAG, FrameType.Game)]
    public class F_MYSTERY_BAG : Frame
    {
        public F_MYSTERY_BAG() : base((int)GameOp.F_MYSTERY_BAG)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
