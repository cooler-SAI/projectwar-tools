using System.Linq;
using System.Threading.Tasks;
using WarServer.Game.Entities;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CREATE_MONSTER, FrameType.Game)]
    public class F_CREATE_MONSTER : Frame
    {
        private MonsterData Monster;
        private ushort ObjectID;
        private ushort Following;
        private uint X;
        private uint Y;
        private ushort Z;
        private float Heading;
        private byte Faction;
        private bool IsDead;
        private F_OBJECT_STATE State;
        public F_PLAYER_INVENTORY Inventory { get; private set; }

        public enum MonsterFlags
        {
            Dormant = 1,
            UpdateState10 = 2,
            UpdateState21 = 4,
            UpdateState23 = 8,
            Animation = 16,
        }

        private F_CREATE_MONSTER() : base((int)GameOp.F_CREATE_MONSTER)
        {
        }

        public static async Task<F_CREATE_MONSTER> Create(Monster monster)
        {
            var result = new F_CREATE_MONSTER()
            {
                ObjectID = monster.ObjectID,
                Faction = monster.Data.Faction,
                Following = monster.Following?.ObjectID ?? 0,
                IsDead = monster.IsDead,
                X = (uint)monster.Location.X,
                Y = (uint)monster.Location.Y,
                Z = (ushort)monster.Location.Z,
                Heading = monster.Location.W,
                Monster = monster.Data,
                Inventory = F_PLAYER_INVENTORY.Create(monster),
                State = await monster.GetState(),
            };

            result.Inventory.Serialize();
            result.State.Serialize();
            return result;
        }

        protected override void SerializeInternal()
        {
            WriteUInt16((ushort)ObjectID);
            WriteUInt16(0);

            WriteUInt16((ushort)Util.ToHeading16(Heading));
            WriteUInt16((ushort)Z);
            WriteUInt32((uint)(X));
            WriteUInt32((uint)(Y));
            WriteUInt16((ushort)Monster.FallRate);
            WriteUInt16((ushort)Monster.ModelID);
            WriteByte((byte)Monster.Scale);
            WriteByte((byte)(Monster.Level));
            WriteByte((byte)Faction);

            WriteUInt16((ushort)Monster.Unk2);
            WriteUInt16((ushort)Monster.Unk3);

            WriteByte((byte)Monster.AnimScriptID);

            byte flags = 0;

            if (Monster.DormantInfo != 0)
                flags |= (byte)MonsterFlags.Dormant;
            if (Monster.UpdateState10 != 0)
                flags |= (byte)MonsterFlags.UpdateState10;
            if (Monster.UpdateState21 != 0)
                flags |= (byte)MonsterFlags.UpdateState21;
            if (Monster.UpdateState23 != 0)
                flags |= (byte)MonsterFlags.UpdateState23;
            if (Monster.AnimationID != 0)
                flags |= (byte)MonsterFlags.Animation;

            WriteByte((byte)Monster.Unk4);
            WriteByte((byte)Monster.Unk5);
            WriteByte((byte)flags);
            WriteByte((byte)Monster.Unk6);
            WriteUInt16((ushort)Monster.Wards);
            WriteUInt16((ushort)Monster.Unk8);
            WriteUInt16((ushort)Monster.Unk9);
            WriteUInt16((ushort)Monster.Unk10);
            WriteUInt16((ushort)Monster.Unk11);
            WriteUInt16((ushort)Monster.TitleID);

            if (Monster.IsDead && !Monster.Flags.Contains(3))
                Monster.Flags.Add(3);

            WriteByte((byte)Monster.Flags.Count); 
            Write(Monster.Flags.Select(e => (byte)e).ToArray(), 0, Monster.Flags.Count);
            WriteByte((byte)0); //questState

            WriteCString(Monster.Name + (Monster.Sex == Sex.MALE ? "^m" : Monster.Sex == Sex.FEMALE ? "^f" : "^n"));

            if (Monster.Flags.Contains(29))
            {
                for (int i = 0; i < 15; i++)
                {
                    if (i < Monster.Traits.Count)
                        WriteByte((byte)Monster.Traits[i]);
                    else
                        WriteByte(0);
                }
            }

            if ((flags & 1) == 1)
            {
                WriteUInt32((uint)Monster.DormantInfo);
            }

            if ((flags & 2) == 2)
            {
                WriteUInt16((ushort)Monster.UpdateState10);
            }

            if ((flags & 4) == 4)
            {
                WriteUInt16((ushort)Monster.UpdateState21);
            }

            if ((flags & 8) == 8)
            {
                WriteByte((byte)Monster.UpdateState23);
            }

            if ((flags & 16) == 16)
            {
                WriteUInt16((byte)Monster.AnimationID);
            }

            if (Monster.Flags.Contains(24))
            {
                WriteUInt16((byte)Monster.WeaponID);
            }

            if (Monster.Flags.Contains(25))
            {
                WriteByte((byte)Monster.Flag25Data.Count);
                if (Monster.Flag25Data.Count > 0)
                    foreach (var b in Monster.Flag25Data)
                        WriteByte((byte)b);
            }

            if ((Monster.Faction & 16) > 0)
            {
                WriteUInt32((uint)Monster.Faction);
            }

            WriteByte((byte)Monster.Unk12);
            WriteUInt16(Following);

            if (State != null)
                WriteByte((byte)(State.Offset - 1));

            if (Inventory != null)
                WriteUInt16R((byte)(Inventory.Offset - 1));

            if (State != null)
                Write(State.Data, 1, State.Offset - 1);

            if (Inventory != null)
                Write(Inventory.Data, 1, Inventory.Offset - 1);
        }
    }
}
