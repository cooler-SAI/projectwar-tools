using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INIT_PLAYER, FrameType.Game)]
    public class F_INIT_PLAYER : Frame
    {
        public F_INIT_PLAYER() : base((int)GameOp.F_INIT_PLAYER)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
