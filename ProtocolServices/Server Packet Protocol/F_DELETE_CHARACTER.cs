using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DELETE_CHARACTER, FrameType.Game)]
    public class F_DELETE_CHARACTER : Frame
    {
        public F_DELETE_CHARACTER() : base((int)GameOp.F_DELETE_CHARACTER)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
