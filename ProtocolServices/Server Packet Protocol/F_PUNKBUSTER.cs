using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PUNKBUSTER, FrameType.Game)]
    public class F_PUNKBUSTER : Frame
    {
        public F_PUNKBUSTER() : base((int)GameOp.F_PUNKBUSTER)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
