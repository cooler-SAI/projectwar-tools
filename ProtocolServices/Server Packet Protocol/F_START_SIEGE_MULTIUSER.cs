using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_START_SIEGE_MULTIUSER, FrameType.Game)]
    public class F_START_SIEGE_MULTIUSER : Frame
    {
        public F_START_SIEGE_MULTIUSER() : base((int)GameOp.F_START_SIEGE_MULTIUSER)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
