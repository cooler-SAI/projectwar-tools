using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_WORLD_LARGE, FrameType.Game)]
    public class F_REQUEST_WORLD_LARGE : Frame
    {
        public F_REQUEST_WORLD_LARGE() : base((int)GameOp.F_REQUEST_WORLD_LARGE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
