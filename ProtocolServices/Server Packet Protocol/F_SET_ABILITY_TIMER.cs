using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SET_ABILITY_TIMER, FrameType.Game)]
    public class F_SET_ABILITY_TIMER : Frame
    {
        private ushort AbilityID;
        private bool Morale;
        private uint Cooldown;

        public F_SET_ABILITY_TIMER() : base((int)GameOp.F_SET_ABILITY_TIMER)
        {
        }

        public static F_SET_ABILITY_TIMER Create(ushort abilityID, uint cooldown, bool morale)
        {
            return new F_SET_ABILITY_TIMER()
            {
                AbilityID = abilityID,
                Cooldown = cooldown,
                Morale = morale
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt16((ushort)AbilityID);
            WriteByte(0);
            if (Morale)
                WriteByte(2);
            else
                WriteByte(0);

            WriteUInt32(Cooldown);
            WriteByte(0);
            WriteByte(0);
            WriteByte(0);
            WriteByte(0);
        }
    }
}
