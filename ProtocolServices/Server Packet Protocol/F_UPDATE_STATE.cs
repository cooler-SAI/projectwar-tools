using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_UPDATE_STATE, FrameType.Game)]
    public class F_UPDATE_STATE : Frame
    {
        public enum StateType
        {
            ObjectState = 0x6,
            EnterCombat = 0x1A,
            SetupPQ = 17,
            ZoneInAlert = 1000,
            EndPqStage = 1001
        }

        private StateType Type;
        private ushort ObjectID;
        private ushort StageID;
        private bool BoolParam1;
        private byte ByteParam1;

        public F_UPDATE_STATE() : base((int)GameOp.F_UPDATE_STATE)
        {
        }
        public static F_UPDATE_STATE CreateEnterCombat(ushort objectID, bool enter)
        {
            var state = new F_UPDATE_STATE();
            state.ObjectID = objectID;
            state.BoolParam1 = enter;
            state.Type = StateType.EnterCombat;

            return state;
        }

        public static F_UPDATE_STATE ZoneInAlert(ushort zoneID)
        {
            var state = new F_UPDATE_STATE();
            state.Type = StateType.ZoneInAlert;
            state.ObjectID = zoneID;
            return state;
        }


        public static F_UPDATE_STATE EndStage(ushort pqID, ushort stageID)
        {
            var state = new F_UPDATE_STATE();
            state.Type = StateType.EndPqStage;
            state.ObjectID = pqID;
            state.StageID = stageID;
            return state;
        }


        public static F_UPDATE_STATE SetupPQ()
        {
            var state = new F_UPDATE_STATE();
            state.ObjectID = 2;
            state.Type = StateType.SetupPQ;

            return state;
        }

        public static F_UPDATE_STATE CreateObjectState(ushort objectID, byte vfxState)
        {
            var state = new F_UPDATE_STATE();
            state.ObjectID = objectID;
            state.ByteParam1 = vfxState;
            state.Type = StateType.ObjectState;

            return state;
        }
        protected override void SerializeInternal()
        {
   

            if (Type == StateType.EnterCombat)
            {
                WriteUInt16(ObjectID);
                WriteByte((byte)Type);
                WriteBool(BoolParam1);
                Fill(0, 7);
            }
            else if (Type == StateType.SetupPQ)
            {
                WriteUInt16(ObjectID);
                WriteByte((byte)Type);
                WriteByte(2);
                WriteByte(1);
                Fill(0, 5);
            }
            else if (Type == StateType.ZoneInAlert)
            {
                WriteUInt16(ObjectID);
                WriteByte((byte)0x11);
                WriteByte(1);
                WriteByte(1);
                WriteByte(6);
                Fill(0, 4);
            }

            else if (Type == StateType.EndPqStage)
            {
                WriteUInt16(0);
                WriteUInt16(ObjectID);
                WriteUInt32R(3);
                WriteUInt16(StageID);
                WriteByte(1);
                WriteUInt32(0);
            }

        }
    }
}
