using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_PONG, FrameType.Game)]
    public class S_PONG : Frame
    {
        public S_PONG() : base((int)GameOp.S_PONG)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
