using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_CHAR_RESPONSE, FrameType.Game)]
    public class F_REQUEST_CHAR_RESPONSE : Frame
    {
        private AccountData Account;

        public F_REQUEST_CHAR_RESPONSE() : base((int)GameOp.F_REQUEST_CHAR_RESPONSE)
        {
        }

        public static F_REQUEST_CHAR_RESPONSE Create(AccountData account)
        {
            return new F_REQUEST_CHAR_RESPONSE()
            {
                Account = account
            };
        }

		protected override void SerializeInternal()
        {
            var slotTorChar = Account.PlayersBySlot;

            FillString(Account.Username, 20);
            //WriteByte(0);
            //WriteByte(0);
            //WriteByte(0);
            //WriteByte(4);
            WriteUInt32(0); //lockout time in seconds
            WriteByte(0);
            WriteByte(0xFF); //allowed realm
            WriteByte(20);
            WriteByte(0); //rulesettype
            WriteByte(0); //lockout side
            WriteByte(0); //allow name change
            WriteByte(0);
            WriteByte(0);

            for (byte i = 0; i < 20; i++)
            {
                if (slotTorChar.ContainsKey(i))
                {
                    var c = slotTorChar[i];
                    FillString(c.Name, 48);
                    WriteByte((byte)c.Level);
                    WriteByte((byte)c.Career.ID);
                    WriteByte((byte)c.Career.Realm);
                    WriteByte((byte)c.Sex);
                    WriteUInt16R((ushort)c.ModelID); //from monsters.csv
                    WriteUInt16R((ushort)c.ZoneID);
                    Fill(0, 4);

                    for (ushort slotID = 19; slotID < 37; ++slotID)
                    {
                        if (c.Inventory.ContainsKey(slotID) && c.Inventory[slotID] != null)
                        {
                            if (c.Inventory[slotID].ModelIDAlternate != 0)
                                WriteUInt32R((uint)c.Inventory[slotID].ModelIDAlternate);
                            else
                                WriteUInt32R(c.Inventory[slotID].Item.ModelID);
                            WriteUInt16R((ushort)c.Inventory[slotID].Dye1); //primary dye
                            WriteUInt16R((ushort)c.Inventory[slotID].Dye2); //secondary

                        }
                        else
                        {
                            WriteUInt32R(0);
                            WriteUInt16R(0);
                            WriteUInt16R(0);
                        }
                    }

                    for (int d = 0; d < 4; d++)
                    {
                        Fill(0, 4);
                        WriteUInt16(0xFF00);
                        Fill(0, 2);
                    }

                    for (ushort slotID = 10; slotID < 13; ++slotID)
                    {
                        if (c.Inventory.ContainsKey(slotID))
                        {
                            if (c.Inventory[slotID].Item != null)
                            {
                                WriteUInt32R(c.Inventory[slotID].Item.ModelID);
                            }
                            else
                                WriteUInt32R(0);
                        }
                        else
                        {
                            WriteUInt32R(0);
                        }
                    }

                    WriteByte(0);
                    WriteByte(0);
                    WriteByte(0);
                    WriteByte(0);
                    WriteByte(0);
                    WriteByte(0);
                    WriteByte(0);
                    WriteByte(0);
                    WriteByte(1);
                    WriteByte(0);
                    WriteByte(0);

                    WriteByte((byte)c.Career.RaceID);
                    WriteUInt16(0); //titleID
                    for (int t = 0; t < 15; t++)
                    {
                        if (t < c.Traits.Count)
                        {
                            WriteByte((byte)c.Traits[t]);
                        }
                        else
                            WriteByte(0);
                    }
                    //  WriteUInt32(0); //traits
                    //  WriteUInt32(0); //traits
                    Fill(0, 7);
                }
                else
                    Fill(0, 284);
            }
        }

    }
}
