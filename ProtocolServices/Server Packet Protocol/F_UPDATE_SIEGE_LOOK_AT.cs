using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_UPDATE_SIEGE_LOOK_AT, FrameType.Game)]
    public class F_UPDATE_SIEGE_LOOK_AT : Frame
    {
        public F_UPDATE_SIEGE_LOOK_AT() : base((int)GameOp.F_UPDATE_SIEGE_LOOK_AT)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
