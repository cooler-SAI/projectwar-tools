using System.Collections.Generic;
using System.Linq;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CREATE_CHARACTER, FrameType.Game)]
    public class F_CREATE_CHARACTER : Frame
    {
        public byte SlotIndex;
        public Race Race;
        public CareerType CareerType;
        public Sex Sex;
        public byte ModelID;
        public int NameLength;
        public List<int> Traits;
        public string Charname;

        public F_CREATE_CHARACTER() : base((int)GameOp.F_CREATE_CHARACTER)
        {
        }

        protected override void DeserializeInternal()
        {
            SlotIndex = ReadByte();
            Race = (Race)ReadByte();
            CareerType = (CareerType)ReadByte();
            Sex = (Sex)ReadByte();
            ModelID = ReadByte();
            NameLength = ReadUInt16();
            Skip(2);

            Traits = ReadByteArray(15).Select(e => (int)e).ToList();
            Charname = ReadString(NameLength);
        }
    }
}
