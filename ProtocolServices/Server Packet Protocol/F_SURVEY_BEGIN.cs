using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SURVEY_BEGIN, FrameType.Game)]
    public class F_SURVEY_BEGIN : Frame
    {
        public F_SURVEY_BEGIN() : base((int)GameOp.F_SURVEY_BEGIN)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
