using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_UPDATE_ITEM_COOLDOWN, FrameType.Game)]
    public class F_UPDATE_ITEM_COOLDOWN : Frame
    {
        public F_UPDATE_ITEM_COOLDOWN() : base((int)GameOp.F_UPDATE_ITEM_COOLDOWN)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
