using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_CONNECTED, FrameType.Game)]
    public class S_CONNECTED : Frame
    {
        private uint Unk1 = 0;
        private uint Tag;
        private byte ServerID;
        private uint Unk2 = 0;
        private string Username;
        private string ServerName;
        private byte Unk3 = 0;
        private ushort Unk4 = 0;

        public S_CONNECTED() : base((int)GameOp.S_CONNECTED)
        {
        }

        public static S_CONNECTED Create(uint tag, byte serverID, string username, string serverName)
        {
            return new S_CONNECTED()
            {
                Tag = tag,
                ServerID = serverID,
                Username = username,
                ServerName = serverName
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt32(Unk1);
            WriteUInt32(Tag);
            WriteByte(ServerID);
            WriteUInt32(Unk2);
            WritePascalString(Username);
            WritePascalString(ServerName);
            WriteByte(Unk3);
            WriteUInt16(Unk4);
        }

    }
}
