using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_TOK_REWARD, FrameType.Game)]
    public class F_REQUEST_TOK_REWARD : Frame
    {
        public F_REQUEST_TOK_REWARD() : base((int)GameOp.F_REQUEST_TOK_REWARD)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
