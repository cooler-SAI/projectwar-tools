using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_STATE2, FrameType.Game)]
    public class F_PLAYER_STATE2 : Frame
    {
        public int PID;
        public int StateOnly;
        public int MoveVelocity;
        public int FallTime;
        public int HasEnemyTarget;
        public int FreeFall;
        public ActionType Action;
        public StrafeType Strafe;
        public int data32;
        public int Heartbeat;
        public int HasPosition;
        public float Heading;
        public int HasPositionChange;
        public int Grounded;
        public int X;
        public int Y;
        public int data60;
        public int ZoneID;
        public int Z;
        public int data70;
        public int data72;
        public int data76;
        public int data80;
        public int data84;
        public int TargetLos;
        public int data89;
        public int data90;
        public int data91;
        public int data92;
        public GroundType GroundType;
        public int data100;
        public int Floating2;
        public int data102;
        public int data103;
        public int data104;
        public int data105;
        public int data106;
        public int data107;

        public F_PLAYER_STATE2() : base((int)GameOp.F_PLAYER_STATE2)
        {
        }

        protected override void DeserializeInternal()
        {
            PID = (ushort)ReadBitInt(16);
            StateOnly = ReadBit();
            MoveVelocity = ReadBitIntClamped(-127, 325);
            FallTime = ReadBitIntClamped(-2000, 500);
            HasEnemyTarget = ReadBit();
            FreeFall = ReadBitInt(2);
            Action = (ActionType)ReadBitInt(3);
            Strafe = (StrafeType)ReadBitInt(3);

            if (HasEnemyTarget > 0 && StateOnly == 0)
                data32 = ReadBit();

            if (StateOnly == 0)
                Heartbeat = ReadBitInt(3);

            HasPosition = ReadBit();

            if (HasPosition > 0)
            {
                Heading = ReadBitAngleRad(12);
                HasPositionChange = ReadBit();
                Grounded = ReadBit();

                if (HasPositionChange == 0)
                {
                    X = ReadBitInt(16);
                    Y = ReadBitInt(16);
                }
            }

            if (HasEnemyTarget > 0 && StateOnly == 0)
                data60 = ReadBit();

            if (HasPosition > 0)
            {
                if (HasPositionChange == 0)
                {
                    ZoneID = ReadBitInt(9);
                    Z = ReadBitInt(16);
                }
                else
                {
                    data72 = ReadBitSigned(16);
                }
            }

            if (HasPosition > 0 && HasPositionChange > 0)
            {
                data76 = ReadBitSigned(16);
                data80 = ReadBitSigned(16);
                data84 = ReadBitInt(9);
            }

            if (HasEnemyTarget > 0)
            {
                if (StateOnly == 0)
                {
                    TargetLos = ReadBit();
                    data89 = ReadBit();
                    data90 = ReadBit();
                    data91 = ReadBit();
                    data92 = ReadBit();
                }
            }
            if (StateOnly == 0)
            {
                GroundType = (GroundType)ReadBitInt(3);
            }

            if (StateOnly > 0 || GroundType != GroundType.SOLID)
            {
                data100 = ReadBit();
                Floating2 = ReadBit();
                data102 = ReadBit();
                data103 = ReadBit();
            }

            data104 = ReadBit();

            if (StateOnly == 0)
                data105 = ReadBit();

            data106 = ReadBit();
            data107 = ReadBit();
        }

        protected override void SerializeInternal()
        {
            var data = new byte[100];
            data[0] = (byte)GameOp.F_PLAYER_STATE2;
            Buffer.BlockCopy(_orig, 0, data, 1, _orig.Length);
            //Offset = _orig.Length + 1;
            //  _data = data;
            //  return;

            _data = new byte[100];
            _data[0] = (byte)GameOp.F_PLAYER_STATE2;
            _currentBitIndex = 8;
            WriteBitInt(PID, 16);
            WriteBit(StateOnly);
            WriteIntClamped(MoveVelocity, -127, 325);
            WriteIntClamped(FallTime, -2000, 500);
            WriteBit(HasEnemyTarget);
            WriteBitInt(FreeFall, 2);
            WriteBitInt((int)Action, 3);
            WriteBitInt((int)Strafe, 3);

            if (HasEnemyTarget > 0 && StateOnly == 0)
                WriteBit(data32);

            if (StateOnly == 0)
                WriteBitInt(Heartbeat, 3);

            WriteBit(HasPosition);

            if (HasPosition > 0)
            {
                WriteAngleRad((float)Heading, 12);
                WriteBit(HasPositionChange);
                WriteBit(Grounded);

                if (HasPositionChange == 0)
                {
                    WriteBitInt(X, 16);
                    WriteBitInt(Y, 16);
                }
            }

            if (HasEnemyTarget > 0 && StateOnly == 0)
                WriteBit(data60);

            if (HasPosition > 0)
            {
                if (HasPositionChange == 0)
                {
                    WriteBitInt(ZoneID, 9);
                    WriteBitInt(Z, 16);
                }
                else
                {
                    WriteIntSigned(data72, 16);
                }
            }

            if (HasPosition > 0 && HasPositionChange > 0)
            {
                WriteIntSigned(data76, 16);
                WriteIntSigned(data80, 16);
                WriteBitInt(data84, 9);
            }

            if (HasEnemyTarget > 0)
            {
                if (StateOnly == 0)
                {
                    WriteBit(TargetLos);
                    WriteBit(data89);
                    WriteBit(data90);
                    WriteBit(data91);
                    WriteBit(data92);
                }
            }
            if (StateOnly == 0)
                WriteBitInt((int)GroundType, 3);

            if ((StateOnly > 0) || GroundType != GroundType.SOLID)
            {
                WriteBit(data100);
                WriteBit(Floating2);
                WriteBit(data102);
                WriteBit(data103);
            }

            WriteBit(data104);

            if (StateOnly == 0)
                WriteBit(data105);

            WriteBit(data106);
            WriteBit(data107);
            CommitBitIndex();

            Offset++;
            WriteByte(0);


        }
    }
}
