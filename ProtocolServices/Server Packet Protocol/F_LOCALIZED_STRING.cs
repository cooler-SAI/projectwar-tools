using System;
using System.Collections.Generic;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_LOCALIZED_STRING, FrameType.Game)]
    public class F_LOCALIZED_STRING : Frame
    {
        public class Localized
        {
            public LocalizedText Label;
            public List<object> Parameters = new List<object>();

            public void Write(Frame frame)
            {
                frame.WriteUInt32((uint)Label);
                frame.WriteUInt32((uint)(Parameters.Count));

                foreach (var p in Parameters)
                {
                    if (p is String)
                    {
                        frame.WriteByte(1);
                        frame.WritePascalString16(p.ToString());
                    }
                    else
                    {
                        frame.WriteByte(0);
                        ((Localized)p).Write(frame);
                    }
                }
            }
        }

        public byte Area;
        public byte Color;
        public Localized Section;


        public F_LOCALIZED_STRING() : base((int)GameOp.F_LOCALIZED_STRING)
        {
        }

        #region builders
        public static F_LOCALIZED_STRING TEXT_GROUP_INVITE_BEGIN(string inviteeName)
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_INVITE_BEGIN,
                    Parameters = new List<object>() { inviteeName }
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_YOU_WERE_INVITED(string invitedByName)
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_YOU_WERE_INVITED,
                    Parameters = new List<object>() { invitedByName }
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_INVITE_ERR_SELF()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_INVITE_ERR_SELF,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_PARTY_IS_FULL()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_PARTY_IS_FULL,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_INVITE_ERR_ENEMY()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_INVITE_ERR_ENEMY,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_INVITE_ERR_GROUPED(string inviteeName, string leader)
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_INVITE_ERR_GROUPED,
                    Parameters = new List<object>() { inviteeName, leader }
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_REFER_ERR_ENEMY()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_REFER_ERR_ENEMY,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_REFER_ERR_SELF()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_REFER_ERR_SELF,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_REFER_NOTICE_SELF(string inviteeName)
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_REFER_NOTICE_SELF,
                    Parameters = new List<object>() { inviteeName }
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_REFER_NOTICE_INVITEE(string invitedBy)
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_REFER_NOTICE_INVITEE,
                    Parameters = new List<object>() { invitedBy }
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_INVITE_ERR_NOINVITE()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_INVITE_ERR_NOINVITE,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_NOT_LEADER()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_NOT_LEADER,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_GROUP_KICK_ERR_INVALIDSLOT()
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_GROUP_KICK_ERR_INVALIDSLOT,
                }
            };
        }
        public static F_LOCALIZED_STRING TEXT_ERROR_INVITE_DECLINED(string declinedBy)
        {
            return new F_LOCALIZED_STRING()
            {
                Area = 0,
                Color = 0,
                Section = new Localized()
                {
                    Label = LocalizedText.TEXT_ERROR_INVITE_DECLINED,
                    Parameters = new List<object>() { declinedBy }
                }
            };
        }
        #endregion

        protected override void SerializeInternal()
        {
            WriteByte(Area);
            WriteByte(Color);

            Section.Write(this);
        }
    }
}
