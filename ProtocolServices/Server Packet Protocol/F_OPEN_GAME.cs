using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_OPEN_GAME, FrameType.Game)]
    public class F_OPEN_GAME : Frame
    {
        public F_OPEN_GAME() : base((int)GameOp.F_OPEN_GAME)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
