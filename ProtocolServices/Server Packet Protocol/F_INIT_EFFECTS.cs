using System;
using System.Collections.Generic;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INIT_EFFECTS, FrameType.Game)]
    public class F_INIT_EFFECTS : Frame
    {
        private List<Effect> Effects = new List<Effect>();
        private enum CommandType
        {
            List = 0,
            Add = 1,
            Remove = 2
        }

        public class Effect
        {
            public int BuffID;
            public ushort AbilityID;
            public int Time;
            public ushort CasterID;
            public Dictionary<int, int> Values = new Dictionary<int, int>();
        }

        private CommandType Type;
        private ushort ObjectID;

        private F_INIT_EFFECTS() : base((int)GameOp.F_INIT_EFFECTS)
        {
        }

        public static F_INIT_EFFECTS CreateAdd(ushort objectID, List<Effect> effects)
        {
            return new F_INIT_EFFECTS()
            {
                Type = F_INIT_EFFECTS.CommandType.Add,
                ObjectID = objectID,
                Effects = effects
            };
        }

        public static F_INIT_EFFECTS CreateRemove(ushort objectID, List<Effect> effects)
        {
            return new F_INIT_EFFECTS()
            {
                Type = F_INIT_EFFECTS.CommandType.Remove,
                ObjectID = objectID,
                Effects = effects,
            };
        }

        public static F_INIT_EFFECTS CreateList(ushort objectID, List<Effect> effects)
        {
            return new F_INIT_EFFECTS()
            {
                Type = F_INIT_EFFECTS.CommandType.List,
                ObjectID = objectID,
                Effects = effects
            };
        }

        protected override void SerializeInternal()
        {
            WriteByte((byte)Effects.Count);
            WriteByte((byte)Type);
            WriteUInt16(0);
            WriteUInt16(ObjectID);

            foreach (var effect in Effects)
            {
                Validate.IsTrue(effect.BuffID >= 0, nameof(effect.BuffID));

                WriteUInt16R((ushort)effect.BuffID);
                WriteUInt16R((ushort)effect.AbilityID);

                if (Type != CommandType.Remove)
                {
                    double time = (double)((double)effect.Time / (double)1000.0);
                    WriteZigZag((int)Math.Ceiling(time));
                    WriteUInt16R(effect.CasterID);

                    WriteByte((byte)effect.Values.Count);
                    foreach (var key in effect.Values.Keys)
                    {
                        WriteByte((byte)key);
                        WriteZigZag(effect.Values[key]);
                    }
                }
            }
            WriteByte(0);
        }

    }
}
