using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REMOVE_PLAYER, FrameType.Game)]
    public class F_REMOVE_PLAYER : Frame
    {
        public ushort ObjectID;

        public F_REMOVE_PLAYER() : base((int)GameOp.F_REMOVE_PLAYER)
        {
        }

        public static F_REMOVE_PLAYER Create(ushort objectID)
        {
            return new F_REMOVE_PLAYER()
            {
                ObjectID = objectID
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt16(ObjectID);
            WriteUInt16(0);
        }

    }
}
