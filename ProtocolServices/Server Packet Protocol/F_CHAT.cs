using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CHAT, FrameType.Game)]
    public class F_CHAT : Frame
    {
        private ushort SenderID;
        private LogFilter Filter;
        private string SenderName;
        private string Message;

        public F_CHAT() : base((int)GameOp.F_CHAT)
        {
        }
        public static F_CHAT Create(ushort senderID, LogFilter filter, string sender, string message)
        {
            return new F_CHAT()
            {
                SenderID = senderID,
                Filter = filter,
                SenderName = sender,
                Message = message
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16((ushort)SenderID);
            WriteByte((byte)Filter);
            Fill(0, 4);
            WritePascalString(SenderName);
            WriteUInt16((ushort)(Message.Length + 1));
            WriteString(Message, 0xFFFF);
            WriteByte(0);
        }
    }
}
