using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DELETE_NAME, FrameType.Game)]
    public class F_DELETE_NAME : Frame
    {
        public string Charname;
        public string Username;

        public F_DELETE_NAME() : base((int)GameOp.F_DELETE_NAME)
        {
        }

        protected override void DeserializeInternal()
        {
            Charname = Util.CamelCase(ReadString(30).Trim());
            Username = ReadString(30);
        }
    }
}
