﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WarServer.Game.Entities;
using WarServer.Game.Groups;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CHARACTER_INFO, FrameType.Game)]
    public class F_CHARACTER_INFO : Frame
    {
        public enum InfoType
        {
            CAREER,
            ABILITY_ADD,
            ABILITY_REMOVE,
            GROUP_INFO,
            GROUP_REMOVE_PLAYER,
            GROUP_UPDATE_OBJECTIDS
        }

        private InfoType Type;
        private PlayerData PlayerData;
        private List<AbilityBinData> Abilities;
        private int MasteryA;
        private int MasteryB;
        private int MasteryC;
        private PlayerGroup Group;
        private Player Player;
        private long PlayerID;
        private List<(float woundsPct, float apPct, float moralePct, Player player)> GroupPlayers;


        public F_CHARACTER_INFO() : base((int)GameOp.F_CHARACTER_INFO)
        {
        }

        public static F_CHARACTER_INFO CreateAbilityAdd(Player player, List<AbilityBinData> abilities, int masterA=0, int masteryB=0, int masteryC=0)
        {
            return new F_CHARACTER_INFO()
            {
                Type = F_CHARACTER_INFO.InfoType.ABILITY_ADD,
                PlayerData = player.Data,
                Abilities = abilities,
                MasteryA = masterA,
                MasteryB = masteryB,
                MasteryC = masteryC,
            };
        }

        public static F_CHARACTER_INFO CreateCareerInfo(Player player)
        {
            return new F_CHARACTER_INFO()
            {
                Type = F_CHARACTER_INFO.InfoType.CAREER,
                PlayerData = player.Data,
            };
        }

        public static F_CHARACTER_INFO CreateAbilityRemove(PlayerData player, List<AbilityBinData> abilities)
        {
            return new F_CHARACTER_INFO()
            {
                Type = InfoType.ABILITY_REMOVE,
                PlayerData = player,
                Abilities = abilities
            };
        }

        public static async Task<F_CHARACTER_INFO> CreateGroupInfo(PlayerGroup group)
        {
            return new F_CHARACTER_INFO()
            {
                Type = InfoType.GROUP_INFO,
                GroupPlayers = await group.GetPlayerVitals()
            };
        }

        public static F_CHARACTER_INFO CreateGroupRemovePlayer(PlayerGroup group, long playerID)
        {
            return new F_CHARACTER_INFO()
            {
                Type = InfoType.GROUP_REMOVE_PLAYER,
                Group = group,
                PlayerID = playerID
            };
        }

        public static async Task<F_CHARACTER_INFO> CreateGroupObjectIDs(PlayerGroup group, Player player)
        {
            return new F_CHARACTER_INFO()
            {
                Type = InfoType.GROUP_UPDATE_OBJECTIDS,
                GroupPlayers = await group.GetPlayerVitals(),
                Group = group,
                Player = player
            };
        }

        protected override void SerializeInternal()
        {
            if (Type == InfoType.CAREER)
                SerializeCareer();
            else if (Type == InfoType.ABILITY_ADD)
                SerializeAbilityAdd();
            else if (Type == InfoType.ABILITY_REMOVE)
                SerializeAbilityRemove();
            else if (Type == InfoType.GROUP_INFO)
                SerializeGroupInfo();
            else if (Type == InfoType.GROUP_UPDATE_OBJECTIDS)
                SerializeGroupObjectIDs();
            else if (Type == InfoType.GROUP_REMOVE_PLAYER)
                SerializeGroupRemovePlayer();
        }

        private void SerializeCareer()
        {
            WriteByte(3);
            WriteByte(0);
            WriteByte(0);
            WriteByte(0);
            WriteByte((byte)PlayerData.Career.CareerLineID);
            WriteByte((byte)PlayerData.Career.RaceID);
            WriteUInt32R((uint)PlayerData.Career.Skills);
            WriteByte(0);
        }

        private void SerializeGroupRemovePlayer()
        {
            WriteByte(6);
            WriteByte(1);

            WriteVarUInt32(Group.ID);
            WriteVarUInt32((uint)PlayerID);
        }

        private void SerializeGroupObjectIDs()
        {
            WriteByte(6);
            WriteByte(4);

            WriteVarUInt32(Group.ID);

            for (int i = 0; i < 6; i++)
            {
                if (i < GroupPlayers.Count && GroupPlayers[i].player.Zone.RegionID == Player.Zone.RegionID)
                {
                    WriteVarUInt32(GroupPlayers[i].player.ID);
                    WriteVarUInt32(GroupPlayers[i].player.ObjectID);
                    WriteVarUInt32(0); //true/false if more. write 2 more var ints (pets I assume)
                }
                else
                {
                    WriteVarUInt32(0);
                }
            }
        }

        private void SerializeGroupInfo()
        {
            WriteByte(6);
            WriteByte(2);

            WriteVarUInt32(Group.ID);
            WriteVarUInt32((uint)GroupPlayers.Count);

            foreach (var p in GroupPlayers)
            {
                WriteVarUInt32(p.player.ID);
                WriteVarUInt32((uint)p.player.ObjectID);
                WriteVarUInt32(p.player.Data.ModelID);
                WriteVarUInt32((uint)p.player.Data.Career.RaceID);
                WriteVarUInt32((uint)p.player.Level);
                WriteVarUInt32((uint)p.player.EffectiveLevel);
                WriteVarUInt32((uint)p.player.Data.Career.CareerLineID);
                WriteVarUInt32((uint)p.player.Data.Career.Realm);
                WriteVarUInt32((uint)(Group.Leader == p.player ? 1 : 0));
                WriteVarUInt32((uint)(Group.Leader == p.player ? 1 : 0));
                WriteVarUInt32((uint)(p.player.Offline ? 0 : 1));
                WritePascalString32(p.player.Name);
                WritePascalString32(p.player?.Guild?.Name??"");
                WriteVarUInt32((uint)p.player.X);
                WriteVarUInt32((uint)p.player.Y);
                WriteVarUInt32((uint)p.player.Z);
                WriteUInt32((uint)p.player.ID);
                WriteVarUInt32((uint)p.player.ID);
                WriteVarInt32((int)p.player.ID);
                WriteVarInt32((int)p.player.Zone.ID);
                WriteVarUInt32((uint)(uint)p.player.ObjectID);
                WriteVarUInt32((uint)p.woundsPct);
                WriteVarUInt32((uint)p.apPct);
                WriteVarUInt32((uint)p.moralePct);
            }
        }


        private void SerializeAbilityAdd()
        {
            WriteByte(1); // Action
            WriteByte((byte)Abilities.Count);//(byte)abilities.Count);
            WriteUInt16(0x300);


            foreach (var ability in Abilities)
            {

                int abilityLevel = Player.GetAbilityLevel(PlayerData.Level, ability, MasteryA, MasteryB, MasteryC);

                WriteUInt16((ushort)ability.ID);

                WriteByte((byte)(abilityLevel));
            }
        }

        private void SerializeAbilityRemove()
        {
            WriteByte(0x0B);
            WriteByte((byte)Abilities.Count);
            foreach (var ability in Abilities)
                WriteUInt16((ushort)ability.ID);
        }
    }
}
