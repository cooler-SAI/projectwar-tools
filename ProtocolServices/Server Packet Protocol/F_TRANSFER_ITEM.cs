using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_TRANSFER_ITEM, FrameType.Game)]
    public class F_TRANSFER_ITEM : Frame
    {
        public ushort ObjectID;
        public ushort To;
        public ushort From;
        public ushort Count;

        public F_TRANSFER_ITEM() : base((int)GameOp.F_TRANSFER_ITEM)
        {
        }

        protected override void DeserializeInternal()
        {
            ObjectID = ReadUInt16();
            To = ReadUInt16();
            From = ReadUInt16();
            Count = ReadUInt16();
        }
    }
}
