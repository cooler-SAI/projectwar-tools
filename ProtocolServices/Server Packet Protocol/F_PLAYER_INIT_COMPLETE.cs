using WarServer.Game.Entities;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_INIT_COMPLETE, FrameType.Game)]
    public class F_PLAYER_INIT_COMPLETE : Frame
    {
        private ushort ObjectID;
        public F_PLAYER_INIT_COMPLETE() : base((int)GameOp.F_PLAYER_INIT_COMPLETE)
        {
        }

        public static F_PLAYER_INIT_COMPLETE Create(Player player)
        {
            return new F_PLAYER_INIT_COMPLETE()
            {
                ObjectID = player.ObjectID
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt16R(ObjectID);
        }

    }
}
