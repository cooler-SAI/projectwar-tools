using System.Collections.Generic;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CAST_PLAYER_EFFECT, FrameType.Game)]
    public class F_CAST_PLAYER_EFFECT : Frame
    {
        private ushort abilityID;
        private ushort casterID;
        private ushort targetID;
        private AbilityResult result;
        private bool showEffect;
        private bool includeDamageMit;
        private bool includeAbsorb;
        private bool updateEffect;
        private bool showAsAttack;
        private byte ComponentIndex;
        private bool unk;
        private List<int> values;

        public F_CAST_PLAYER_EFFECT() : base((int)GameOp.F_CAST_PLAYER_EFFECT)
        {
        }

        public static F_CAST_PLAYER_EFFECT Create(ushort abilityID, ushort casterID, ushort targetID, AbilityResult result,
            bool showEffect, bool includeDamageMit, bool includeAbsorb, bool updateEffect, bool showAsAttack, byte compIndex,
            List<int> values, bool unk = false)
        {
            return new F_CAST_PLAYER_EFFECT()
            {
                abilityID = abilityID,
                targetID = targetID,
                casterID = casterID,
                ComponentIndex = compIndex,
                result = result,
                showEffect = showEffect, //only start effect on first tick, ticks to follow dont need to active effect
                updateEffect = updateEffect,
                includeDamageMit = includeDamageMit,
                includeAbsorb = includeAbsorb,
                showAsAttack = showAsAttack,
                unk = unk,
                values = values
            };
        }
        protected override void SerializeInternal()
        {
            WriteUInt16(casterID);
            WriteUInt16(targetID);
            WriteUInt16(abilityID);
            WriteByte((byte)ComponentIndex);
            WriteByte((byte)result); //parried, crit, dodged, etc

            byte effectInfo = 0;

            if (showEffect)
                effectInfo |= 0x01;
            if (includeDamageMit)
                effectInfo |= 0x02;
            if (includeAbsorb)
                effectInfo |= 0x20;
            if (updateEffect)
                effectInfo |= 0x08;
            if (showAsAttack)
                effectInfo |= 0x10;
            if (unk) //is on instant damage
                effectInfo |= 0x04;

            WriteByte((byte)effectInfo);

            if (values != null && values.Count > 0)
                foreach (var val in values)
                    WriteZigZag(val);

            WriteByte((byte)0);
        }
    }
}
