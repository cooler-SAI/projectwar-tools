﻿using System;
using System.Threading;

namespace AssetHashHunter
{
    public interface IUnifiedData
    {
        long Length { get; }

        int BufferSize { get; set; }

        void ForEachRead(Action<byte[], int, int> action, CancellationToken cancellationToken);

        void ForEachGroup(int groupSize, Action<byte[], int, int> action, Action<byte[], int, int> remainderAction, CancellationToken cancellationToken);

        byte[] ToArray(CancellationToken cancellationToken);
    }
}