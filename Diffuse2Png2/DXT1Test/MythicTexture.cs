﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace DXT1Test
{
    public class MythicTexture
    {
        public Byte[] Header = new Byte[4];
        public UInt32 ID;
        public UInt32 MaskID;
        public UInt16 Width;
        public UInt16 Height;
        public UInt32 ImageCount;
        public Byte[] Data;

        public List<MIPHeader> MipMapHeaders = new List<MIPHeader>();

        public class MIPHeader
        {
            public UInt32 DataOffset;
            public UInt16 Width;
            public UInt16 Height;
            public UInt16 ShiftX;
            public UInt16 ShiftY;

            public MIPHeader(BinaryReader reader)
            {
                Int64 dataOffset = reader.BaseStream.Position;

                DataOffset = reader.ReadUInt32() + (UInt32)dataOffset;
                Width = reader.ReadUInt16();
                Height = reader.ReadUInt16();
                ShiftX = reader.ReadUInt16();
                ShiftY = reader.ReadUInt16();
            }
        }

        public MythicTexture(Stream stream)
        {
            var reader = new BinaryReader(stream);

            Data = new Byte[stream.Length];
            stream.Read(Data, 0, (Int32)stream.Length);

            stream.Position = 0;
            Header = reader.ReadBytes(4);
            UInt32 unk = reader.ReadUInt32();
            ID = reader.ReadUInt32();
            MaskID = reader.ReadUInt32();
            Width = reader.ReadUInt16();
            Height = reader.ReadUInt16();
            ImageCount = reader.ReadUInt32();

            for (Int32 i = 0; i < ImageCount; i++)
            {
                var mip = new MIPHeader(reader);
                MipMapHeaders.Add(mip);
            }
        }
        public static void DecompressBlockAlphaDxt5(Byte[] blockStorage, Int32[,] output)
        {
            //// get the two alpha values
            //Int32 alpha0 = blockStorage[0];
            //Int32 alpha1 = blockStorage[1];

            //// compare the values to build the codebook
            //Byte[] codes = new Byte[8];
            //codes[0] = (Byte)alpha0;
            //codes[1] = (Byte)alpha1;

            //if (alpha0 <= alpha1)
            //{   /* use 5-alpha codebook */
            //    for (Int32 i = 1; i < 5; ++i)
            //    {
            //        codes[1 + i] = (Byte)((((5 - i) * alpha0) + (i * alpha1)) / 5);
            //    }
            //    codes[6] = 0;
            //    codes[7] = 255;
            //}
            //else
            //{   /* use 7-alpha codebook */
            //    for (Int32 i = 1; i < 7; ++i)
            //    {
            //        codes[1 + i] = (Byte)((((7 - i) * alpha0) + (i * alpha1)) / 7);
            //    }
            //}

            //Byte[] indices = new Byte[16];
            //Int32 src = 2;
            //Int32 dest = 0;

            //for (Int32 i = 0; i < 2; ++i) /* decode the indices */
            //{
            //    Int32 value = 0;
            //    for (Int32 j = 0; j < 3; ++j) /* grab 3 bytes */
            //    {
            //        Int32 byte_data = blockStorage[src++];
            //        value |= byte_data << (8 * j);
            //    }

            //    for (Int32 j = 0; j < 8; ++j)    /* unpack 8, 3-bit values from it */
            //    {
            //        Int32 index = (value >> (3 * j)) & 0x7;
            //        indices[dest++] = (Byte)index;
            //    }
            //}

            //for (Int32 i = 0; i < 4; ++i)
            //{
            //    for (Int32 j = 0; j < 4; ++j)   /* write out the indexed codebook values */
            //    {

            //        Byte gr = codes[indices[(i * 4) + j]];
            //        output[j,i] = gr;
            //    }
            //}

            output[0, 0] = blockStorage[0];
            output[1, 0] = blockStorage[0];
            output[2, 0] = blockStorage[1];
            output[3, 0] = blockStorage[1];
            output[0, 1] = blockStorage[2];
            output[1, 1] = blockStorage[2];
            output[2, 1] = blockStorage[3];
            output[3, 1] = blockStorage[3];
            output[0, 2] = blockStorage[4];
            output[1, 2] = blockStorage[4];
            output[2, 2] = blockStorage[5];
            output[3, 2] = blockStorage[5];
            output[0, 3] = blockStorage[6];
            output[1, 3] = blockStorage[6];
            output[2, 3] = blockStorage[7];
            output[3, 3] = blockStorage[7];

            //output[0, 0] = blockStorage[0];
            //output[1, 0] = Lerp(blockStorage[0], blockStorage[1], 0.33f);
            //output[2, 0] = Lerp(blockStorage[0], blockStorage[1], 0.66f);
            //output[3, 0] = blockStorage[1];
            //output[0, 1] = blockStorage[2];
            //output[1, 1] = Lerp(blockStorage[2], blockStorage[3], 0.33f);
            //output[2, 1] = Lerp(blockStorage[2], blockStorage[3], 0.66f);
            //output[3, 1] = blockStorage[3];
            //output[0, 2] = blockStorage[4];
            //output[1, 2] = Lerp(blockStorage[4], blockStorage[5], 0.33f);
            //output[2, 2] = Lerp(blockStorage[4], blockStorage[5], 0.66f);
            //output[3, 2] = blockStorage[5];
            //output[0, 3] = blockStorage[6];
            //output[1, 3] = Lerp(blockStorage[6], blockStorage[7], 0.33f);
            //output[2, 3] = Lerp(blockStorage[6], blockStorage[7], 0.66f);
            //output[3, 3] = blockStorage[7];
        }
        
        public static Byte Lerp(Byte a, Byte b, Single t)
        {
            Single tc = Clamp(t, 0.0f, 1.0f);
            return (Byte)Math.Abs((a* (1 - tc)) + (b* tc));
        }

        public static Single Clamp(Single v, Single a, Single b) => (v < a) ? a : ((v > b) ? b : v);

        public static void DecompressBlockDXT1(Byte[] blockStorage, Color[,] blockResult)
        {
            if (blockStorage.Length < 8)
                return;

            UInt16 color0 = (UInt16)(blockStorage[0] | (blockStorage[1] << 8));
            UInt16 color1 = (UInt16)(blockStorage[2] | (blockStorage[3] << 8));

            Int32 temp;

            temp = ((color0 >> 11) * 255) + 16;
            Byte r0 = (Byte)(((temp / 32) + temp) / 32);
            temp = (((color0 & 0x07E0) >> 5) * 255) + 32;
            Byte g0 = (Byte)(((temp / 64) + temp) / 64);
            temp = ((color0 & 0x001F) * 255) + 16;
            Byte b0 = (Byte)(((temp / 32) + temp) / 32);

            temp = ((color1 >> 11) * 255) + 16;
            Byte r1 = (Byte)(((temp / 32) + temp) / 32);
            temp = (((color1 & 0x07E0) >> 5) * 255) + 32;
            Byte g1 = (Byte)(((temp / 64) + temp) / 64);
            temp = ((color1 & 0x001F) * 255) + 16;
            Byte b1 = (Byte)(((temp / 32) + temp) / 32);

            UInt32 code = (UInt32)(blockStorage[4] | (blockStorage[5] << 8) | (blockStorage[6] << 16) | (blockStorage[7] << 24));

            for (Int32 j = 0; j < 4; j++)
            {
                for (Int32 i = 0; i < 4; i++)
                {
                    var finalColor = Color.FromArgb(0);
                    Byte positionCode = (Byte)((code >> (2 * ((4 * j) + i))) & 0x03);

                    if (color0 > color1)
                    {
                        switch (positionCode)
                        {
                            case 0:
                                finalColor = Color.FromArgb(255, r0, g0, b0);
                                break;
                            case 1:
                                finalColor = Color.FromArgb(255, r1, g1, b1);
                                break;
                            case 2:
                                finalColor = Color.FromArgb(255, ((2 * r0) + r1) / 3, ((2 * g0) + g1) / 3, ((2 * b0) + b1) / 3);
                                break;
                            case 3:
                                finalColor = Color.FromArgb(255, (r0 + (2 * r1)) / 3, (g0 + (2 * g1)) / 3, (b0 + (2 * b1)) / 3);
                                break;
                        }
                    }
                    else
                    {
                        switch (positionCode)
                        {
                            case 0:
                                finalColor = Color.FromArgb(255, r0, g0, b0);
                                break;
                            case 1:
                                finalColor = Color.FromArgb(255, r1, g1, b1);
                                break;
                            case 2:
                                finalColor = Color.FromArgb(255, (r0 + r1) / 2, (g0 + g1) / 2, (b0 + b1) / 2);
                                break;
                            case 3:
                                finalColor = Color.FromArgb(0, 0, 0, 0);
                                break;
                        }
                    }

                    blockResult[i, j] = finalColor;
                }
            }
        }

        public class Block
        {
            public Color[,] Diffuse;
            public Int32[,] Alpha;
        }

        public static Dictionary<Int32, Block> _blocks = new Dictionary<Int32, Block>();

        public static Byte[] CreateDiffuseDXT1(MythicTexture mask, MythicTexture diffuse, Int32 level, Int32 stride = 1024)
        {
            Byte[] resultTexture = new Byte[stride * stride * 8 * 4];
            var diffuseWriter = new BinaryWriter(new MemoryStream(resultTexture));

            //fill empty texture with transparent background
            for (Int32 i = 0; i < resultTexture.Length / 4; i++)
                diffuseWriter.Write((UInt32)0xFFFFFFFF);

            // if (i % 2 == 0)
            //    diffuseWriter.Write((UInt32)0xFFFFFFFF);
            //else
            //    diffuseWriter.Write((UInt32)0xFFFFFFFF);

            diffuseWriter.BaseStream.Position = 0;
            //  return resultTexture;
            using (var maskReader = new BinaryReader(new MemoryStream(mask.Data)))
            {
                using (var diffuseReader = new BinaryReader(new MemoryStream(diffuse.Data)))
                {
                    maskReader.BaseStream.Position = mask.MipMapHeaders[level].DataOffset;
                    diffuseReader.BaseStream.Position = diffuse.MipMapHeaders[level].DataOffset;

                    Int32 shiftBlocksX = diffuse.MipMapHeaders[level].ShiftX >> 2;
                    Int32 shiftBlocksY = diffuse.MipMapHeaders[level].ShiftY >> 2;
                    Int32 blocksWidthCount = diffuse.MipMapHeaders[level].Width >> 2;
                    Int32 blocksHeightCount = diffuse.MipMapHeaders[level].Height >> 2;
                    Int32 padRowBytes = stride - (8 * blocksWidthCount);
                    Int32 destTexturePtr = (shiftBlocksY * stride) + (8 * shiftBlocksX);
                    Int32 rowEnd = 8 * blocksWidthCount;
                    Int32 v23 = shiftBlocksX + (shiftBlocksY * (stride >> 2));
                    Int32 v124 = v23;
                    Int32 v98 = stride * (blocksWidthCount >> 2);
                    Int32 v107 = 0x80 - blocksWidthCount; //comes from textureParams->MaskWidthBlocks
                    Int32 v113 = 0;
                    Int32 v108 = 1;
                    Boolean decodedMask = false;

                    while (true)
                    {
                    READ_ROW:

                        if (maskReader.BaseStream.Position >= maskReader.BaseStream.Length)
                        {
                            break;
                        }

                        Byte controlCode = maskReader.ReadByte();

                        if (controlCode > 230)
                        {
                            v113 = controlCode - 229;
                            decodedMask = true;
                        }

                        if (!decodedMask)
                            v113 = controlCode & 1;

                        //decode mask/tint
                        if ((controlCode & 1) != 0 || decodedMask)
                        {
                            decodedMask = false;
                            while (true)
                            {
                                Int64 maskDxtBlock = maskReader.ReadInt64();
                                Int64 maskDiffuseBlock = diffuseReader.ReadInt64();

                                Color[,] b1 = new Color[4, 4];
                                Color[,] b2 = new Color[4, 4];
                                Int32[,] vals = new Int32[4, 4];

                                //DecompressBlockDXT1(BitConverter.GetBytes(maskDxtBlock), b1);
                                DecompressBlockDXT1(BitConverter.GetBytes(maskDiffuseBlock), b2);
                                DecompressBlockAlphaDxt5(BitConverter.GetBytes(maskDxtBlock), vals);

                                _blocks[(Int32)diffuseWriter.BaseStream.Position] = new Block()
                                {
                                    Alpha = vals,
                                    Diffuse = b2
                                };

                                //   diffuseWriter.Write(maskDiffuseBlock);
                                diffuseWriter.BaseStream.Position += 8;
                                v23 = v124 + 1;
                                v124++;

                                if (diffuseWriter.BaseStream.Position >= rowEnd)
                                {
                                    v23 += v107;
                                    diffuseWriter.BaseStream.Position += padRowBytes;
                                    v124 = v23;
                                    rowEnd += stride;
                                }

                                Boolean v33 = v113-- == 1;
                                if (v33)
                                    break;
                            }
                        }

                        Int32 v58 = controlCode >> 1;
                        if (controlCode >> 1 < 115)
                        {
                            if (v58 < 60)
                            {
                                Int32 v111 = v58 + 1;
                                while (true)
                                {
                                    Int32 v59 = v108;
                                    Int32 va = v59 & 3;

                                    if (va == 3)
                                    {
                                        //tinting
                                    }
                                    else
                                    {
                                        Int64 diffuseDxt1 = diffuseReader.ReadInt64();
                                        diffuseWriter.Write(diffuseDxt1);
                                        //diffuseWriter.BaseStream.Position += 8;
                                    }

                                    v23 = v124;
                                    ++v23;
                                    v124 = v23;

                                    if (diffuseWriter.BaseStream.Position >= rowEnd)
                                    {
                                        v23 += v107;
                                        diffuseWriter.BaseStream.Position += padRowBytes;
                                        rowEnd += stride;
                                        v124 = v23;
                                    }
                                    v111--;
                                    if (v111 == 0)
                                        goto READ_ROW;
                                }
                            }

                            Int32 v88 = v58 - 59;
                            diffuseWriter.BaseStream.Position += 8 * v88;
                            v23 += v88;
                            v124 = v23;
                            if (diffuseWriter.BaseStream.Position >= rowEnd)
                            {
                                do
                                {
                                    v23 += v107;
                                    diffuseWriter.BaseStream.Position += padRowBytes;
                                    rowEnd += stride;
                                    v124 = v23;
                                } while (diffuseWriter.BaseStream.Position >= rowEnd);
                            }
                        }
                    }
                }
            }

            return resultTexture;
        }

        public static Bitmap DecompressDXT1(Byte[] data, Int32 w, Int32 h)
        {

            var res = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (var g = Graphics.FromImage(res))
            {
                g.Clear(Color.Magenta);
            }
            res.MakeTransparent();

            using (var ms = new MemoryStream(data))
            {
                using (var r = new BinaryReader(ms, Encoding.ASCII, true))
                {
                    Int32 blockCountX = (w + 3) / 4;
                    Int32 blockCountY = (h + 3) / 4;
                    Int32 blockWidth = (w < 4) ? w : 4;
                    Int32 blockHeight = (h < 4) ? w : 4;

                    for (Int32 j = 0; j < blockCountY; j++)
                    {
                        for (Int32 i = 0; i < blockCountX; i++)
                        {
                            if (_blocks.ContainsKey((Int32)r.BaseStream.Position))
                            {
                                Block block = _blocks[(Int32)r.BaseStream.Position];

                                for (Int32 ja = 0; ja < 4; ja++)
                                {
                                    for (Int32 ia = 0; ia < 4; ia++)
                                    {
                                        res.SetPixel(ia + (i * 4), ja + (j * 4), Color.FromArgb(block.Alpha[ia, ja], block.Diffuse[ia, ja]));
                                        //res.SetPixel(ia + (i * 4), ja + (j * 4), Color.FromArgb(255, block.Alpha[ia, ja], block.Alpha[ia, ja], block.Alpha[ia, ja]));
                                        //res.SetPixel(ia + (i * 4), ja + (j * 4), Color.FromArgb(255, block.Diffuse[ia, ja]));
                                    }
                                }
                                r.BaseStream.Position += 8;
                            }
                            else
                            {
                                Byte[] blockStorage = r.ReadBytes(8);
                                DecompressBlockDXT1(i * 4, j * 4, w, blockStorage, res);
                            }
                        }
                    }
                }
            }

            return res;
        }
        public static void DecompressBlockDXT1(Int32 x, Int32 y, Int32 width, Byte[] blockStorage, Bitmap image)
        {
            if (blockStorage.Length < 8)
                return;

            UInt16 color0 = (UInt16)(blockStorage[0] | (blockStorage[1] << 8));
            UInt16 color1 = (UInt16)(blockStorage[2] | (blockStorage[3] << 8));

            Int32 temp;

            temp = ((color0 >> 11) * 255) + 16;
            Byte r0 = (Byte)(((temp / 32) + temp) / 32);
            temp = (((color0 & 0x07E0) >> 5) * 255) + 32;
            Byte g0 = (Byte)(((temp / 64) + temp) / 64);
            temp = ((color0 & 0x001F) * 255) + 16;
            Byte b0 = (Byte)(((temp / 32) + temp) / 32);

            temp = ((color1 >> 11) * 255) + 16;
            Byte r1 = (Byte)(((temp / 32) + temp) / 32);
            temp = (((color1 & 0x07E0) >> 5) * 255) + 32;
            Byte g1 = (Byte)(((temp / 64) + temp) / 64);
            temp = ((color1 & 0x001F) * 255) + 16;
            Byte b1 = (Byte)(((temp / 32) + temp) / 32);

            UInt32 code = (UInt32)(blockStorage[4] | (blockStorage[5] << 8) | (blockStorage[6] << 16) | (blockStorage[7] << 24));
            Int32 alpha = 255;
            for (Int32 j = 0; j < 4; j++)
            {
                for (Int32 i = 0; i < 4; i++)
                {
                    var finalColor = Color.FromArgb(0);
                    Byte positionCode = (Byte)((code >> (2 * ((4 * j) + i))) & 0x03);

                    if (color0 > color1)
                    {
                        switch (positionCode)
                        {
                            case 0:
                                finalColor = Color.FromArgb(alpha, r0, g0, b0);
                                break;
                            case 1:
                                finalColor = Color.FromArgb(alpha, r1, g1, b1);
                                break;
                            case 2:
                                finalColor = Color.FromArgb(alpha, ((2 * r0) + r1) / 3, ((2 * g0) + g1) / 3, ((2 * b0) + b1) / 3);
                                break;
                            case 3:
                                finalColor = Color.FromArgb(alpha, (r0 + (2 * r1)) / 3, (g0 + (2 * g1)) / 3, (b0 + (2 * b1)) / 3);
                                break;
                        }
                    }
                    else
                    {
                        switch (positionCode)
                        {
                            case 0:
                                finalColor = Color.FromArgb(alpha, r0, g0, b0);
                                break;
                            case 1:
                                finalColor = Color.FromArgb(alpha, r1, g1, b1);
                                break;
                            case 2:
                                finalColor = Color.FromArgb(alpha, (r0 + r1) / 2, (g0 + g1) / 2, (b0 + b1) / 2);
                                break;
                            case 3:
                                finalColor = Color.FromArgb(0, 0, 0, 0);
                                break;
                        }
                    }


                    //if (finalColor == Color.FromArgb(255, 0, 0, 0))
                    //{
                    //    finalColor = Color.FromArgb(0, 0, 0, 0);
                    //}
                    //else
                    //{
                    //    //finalColor = Color.White;
                    //}

                    if (x + i < width && y + j < image.Height)
                    {
                        image.SetPixel(x + i, y + j, finalColor);
                    }
                }
            }
        }

    }
}


