echo off
set source_folder=D:\Games\Warhammer Online - Age of Reckoning\myp\NO_DEHASH\geom
set destination_folder=D:\Games\Warhammer Online - Age of Reckoning\myp\NO_DEHASH\geom

for /f "usebackq delims=|" %%f in (`dir /b "%source_folder%\*.geom"`) do (
	Geom2Obj.exe "%source_folder%\%%~f" "%destination_folder%\%%~nf.obj"
echo[
)

set source_folder=D:\Games\Warhammer Online - Age of Reckoning\myp\assetdb\charmesh
set destination_folder=D:\Games\Warhammer Online - Age of Reckoning\myp\assetdb\charmesh

for /f "usebackq delims=|" %%f in (`dir /b "%source_folder%\*.geom"`) do (
	Geom2Obj.exe "%source_folder%\%%~f" "%destination_folder%\%%~nf.obj"
echo[
)

pause