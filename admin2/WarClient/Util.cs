﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace WarClient
{
    public static class TaskExtensions
    {
        //-----------------------------------------------------------------------------
        public static void RunInBackground(this Task task) => task.ContinueWith(t => { }, TaskContinuationOptions.OnlyOnFaulted);

    }

    public static class Util
    {
        public static void CompareArray(Byte[] arr1, Byte[] arr2, Int32 from = 0)
        {
            if (arr1.Length != arr2.Length)
            {
                Console.WriteLine("Difference in length Diff:" + Math.Abs(arr1.Length - arr2.Length));
                //throw new Exception("Invalid Length");
            }


            for (var i = from; i < arr1.Length; i++)
            {
                if (arr1[i] != arr2[i])
                    throw new Exception($"Invalid at:{i} ");
            }
        }

        public static String Hex(Byte[] dump, Int32 start, Int32 len, Int32? current = null, Int32 cols = 16, Boolean includeHeader = true)
        {
            var hexDump = new StringBuilder();

            if (includeHeader)
            {
                hexDump.AppendLine($"|{ new String('-', cols * 2 + cols)}|{ new String('-', cols)}|");
                for (var i = 0; i < cols; i++)
                {
                    var s1 = "";
                    var s2 = "";


                    s1 += i.ToString("X").PadLeft(2, '0') + " ";
                    s2 += (i % 16).ToString("X");

                    hexDump.AppendLine($"|{s1}|{s2}|");
                    hexDump.AppendLine($"|{ new String('-', cols * 2 + cols)}|{ new String('-', cols)}|");
                }
            }


            var end = start + len;
            for (var i = start; i < end; i += cols)
            {
                var text = new StringBuilder();
                var hex = new StringBuilder();


                for (var j = 0; j < cols; j++)
                {
                    if (j + i < end)
                    {
                        var val = dump[j + i];

                        if (current.HasValue && current.Value == j + i)
                            hex.Append("[" + dump[j + i].ToString("X2") + "]");
                        else
                            hex.Append(dump[j + i].ToString("X2"));

                        hex.Append(" ");
                        if (val >= 32 && val < 127)
                        {
                            if (current.HasValue && current.Value == j + i)
                                text.Append("[" + (Char)val + "]");
                            else
                                text.Append((Char)val);
                        }
                        else
                        {
                            if (current.HasValue && current.Value == j + i)
                                text.Append("[.]");
                            else
                                text.Append(".");
                        }
                    }
                }

                hexDump.AppendLine("|" + hex.ToString().PadRight(cols * 2 + cols) + "|" + text.ToString().PadRight(cols) + "|");
            }

            if (includeHeader)
                hexDump.AppendLine($"-{ new String('-', cols * 2 + cols)}-{ new String('-', cols)}-");
            return hexDump.ToString();
        }

    }
}
