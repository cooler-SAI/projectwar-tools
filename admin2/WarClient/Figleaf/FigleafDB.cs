﻿using System.Collections.Generic;
using System.IO;
using WarClient.Figleaf.Tables;

namespace WarClient.Figleaf
{
    public class FigleafDB
    {
        public FigStringTable TableStrings1 { get; private set; }
        public FigStringTable2 TableStrings2 { get; private set; }
        public CharacterArtTable TableCharacterArt { get; private set; }
        public FigurePartTable TableFigureParts { get; private set; }
        public ArtSwitchTable TableArtSwitches { get; private set; }
        public CharacterDecalTable TableCharacterDecals { get; private set; }
        public CharacterMeshesTable TableCharacterMeshes { get; private set; }
        public TextureTable TableTextures { get; private set; }
        public Unk8Table TableUnk8 { get; private set; }
        public FixtureTable TableFixtures { get; private set; }
        public LightmapTable TableLightmaps { get; private set; }
        public PatchTable TablePatches { get; private set; }
        public RegionTable TableRegions { get; private set; }
        public UnkVar16Table TableVAR_RECS1 { get; private set; }
        public UnkVar16Table TableVAR_RECS2 { get; private set; }
        public UnkVar16Table TableVAR_RECS3 { get; private set; }
        public UnkVar16Table TableVAR_RECS4 { get; private set; }
        public UnkVar16Table TableVAR_RECS5 { get; private set; }
        public List<IFigTable> Tables { get; private set; } = new List<IFigTable>();

        public FigleafDB()
        {
            TableStrings1 = new FigStringTable(this);
            TableStrings2 = new FigStringTable2(this);
            TableCharacterArt = new CharacterArtTable(this);
            TableFigureParts = new FigurePartTable(this);
            TableArtSwitches = new ArtSwitchTable(this);
            TableCharacterDecals = new CharacterDecalTable(this);
            TableCharacterMeshes = new CharacterMeshesTable(this);
            TableTextures = new TextureTable(this);
            TableUnk8 = new Unk8Table(this);
            TableFixtures = new FixtureTable(this);
            TableLightmaps = new LightmapTable(this);
            TablePatches = new PatchTable(this);
            TableRegions = new RegionTable(this);
            TableVAR_RECS1 = new UnkVar16Table(this, 0xa8);
            TableVAR_RECS2 = new UnkVar16Table(this, 0xb4);
            TableVAR_RECS3 = new UnkVar16Table(this, 0xc0);
            TableVAR_RECS4 = new UnkVar16Table(this, 0xcc);
            TableVAR_RECS5 = new UnkVar16Table(this, 0xd8);

            Tables = new List<IFigTable>(){
                 TableStrings1,
                 TableStrings2,
                 TableCharacterArt,
                 TableFigureParts,
                 TableArtSwitches,
                 TableCharacterDecals,
                 TableCharacterMeshes,
                 TableTextures,
                 TableUnk8,
                 TableFixtures,
                 TableLightmaps,
                 TablePatches,
                 TableRegions,
                 TableVAR_RECS1,
                 TableVAR_RECS2,
                 TableVAR_RECS3,
                 TableVAR_RECS4,
                 TableVAR_RECS5 };
        }

        public void Load(Stream stream)
        {
            using (var reader = new BinaryReader(stream))
            {
                var header = reader.ReadBytes(4);
                var tocSize = reader.ReadUInt32();
                var fileSize = reader.ReadUInt32();

                TableStrings1.Load(reader);
                TableStrings2.Load(reader);
                TableCharacterArt.Load(reader);
                TableFigureParts.Load(reader);
                TableArtSwitches.Load(reader);
                TableCharacterDecals.Load(reader);
                TableCharacterMeshes.Load(reader);
                TableTextures.Load(reader);
                TableUnk8.Load(reader);
                TableFixtures.Load(reader);
                TableLightmaps.Load(reader);
                TablePatches.Load(reader);
                TableRegions.Load(reader);
                TableVAR_RECS1.Load(reader);
                TableVAR_RECS2.Load(reader);
                TableVAR_RECS3.Load(reader);
                TableVAR_RECS4.Load(reader);
                TableVAR_RECS5.Load(reader);
            }
        }

        public void Save(MemoryStream stream)
        {
            var writer = new BinaryWriter(stream);

            writer.Write(new System.Char[] { 'b', 'd', 'L', 'F' });
            writer.Write((System.UInt32)0x20);
            writer.Write((System.UInt32)0);

            TableStrings1.Save(writer);
            TableStrings2.Save(writer);
            TableCharacterArt.Save(writer);
            TableFigureParts.Save(writer);
            TableArtSwitches.Save(writer);
            TableCharacterDecals.Save(writer);
            TableCharacterMeshes.Save(writer);
            TableTextures.Save(writer);
            TableUnk8.Save(writer);
            TableFixtures.Save(writer);
            TableLightmaps.Save(writer);
            TablePatches.Save(writer);
            TableRegions.Save(writer);
            TableVAR_RECS1.Save(writer);
            TableVAR_RECS2.Save(writer);
            TableVAR_RECS3.Save(writer);
            TableVAR_RECS4.Save(writer);
            TableVAR_RECS5.Save(writer);

            stream.Position = 0x8;
            writer.Write((System.UInt32)stream.Length);
        }
    }
}
