﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class CharacterMeshesTable : FigTable<CharacterMesh>
    {
        private const System.Int32 HeaderPosition = 0x54;
        private const System.Int32 RecordSize = 0x14;

        public CharacterMeshesTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var mesh = new CharacterMesh(_db, i);
                mesh.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                mesh.A02a = reader.ReadUInt32();
                mesh.A02c = reader.ReadUInt32();
                mesh.A04 = reader.ReadUInt32();
                mesh.GeomID = reader.ReadInt32();

                Records.Add(mesh);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (var i = 0; i < Records.Count; i++)
            {
                CharacterMesh mesh = Records[i];

                writer.Write((System.UInt32)mesh.SourceIndex);
                writer.Write(mesh.A02a);
                writer.Write(mesh.A02c);
                writer.Write(mesh.A04);
                writer.Write(mesh.GeomID);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((System.UInt32)Records.Count);
            writer.Write((System.UInt32)(Records.Count > 0 ? pos : 0));
            var size = (System.UInt32)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class CharacterMesh : FigRecord
    {
        public System.Int32 Index { get; }
        public FigStringRef SourceIndex { get; set; }
        public System.UInt32 A02a { get; set; }
        public System.UInt32 A02c { get; set; }
        public System.UInt32 A04 { get; set; }
        public System.Int32 GeomID { get; set; }
        public CharacterMesh(FigleafDB db, System.Int32 index) : base(db) => Index = index;
    }


    public class CharacterMeshRef
    {
        private readonly FigleafDB _db;

        public static implicit operator System.Int32(CharacterMeshRef r) => r.Index;

        public override System.String ToString()
        {
            if (Index >= 0 && Index <= _db.TableCharacterMeshes.Records.Count)
                return _db.TableCharacterMeshes.Records[Index].SourceIndex.ToString() + " [" + Index + "]";
            return "";
        }

        public System.Int32 Index { get; set; }
        public CharacterMeshRef(FigleafDB db, System.Int32 value)
        {
            _db = db;
            Index = value;
        }
    }
}

