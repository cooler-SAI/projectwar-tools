﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class CharacterDecalTable : FigTable<CharacterDecal>
    {
        private const System.Int32 HeaderPosition = 0x48;
        private const System.Int32 RecordSize = 0x3C;

        public CharacterDecalTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var decal = new CharacterDecal(_db, i);
                decal.Type = (DecalType)reader.ReadUInt32();
                decal.FileID = reader.ReadInt32();

                decal.AlphaBits = reader.ReadByte();
                decal.TintType = reader.ReadByte();
                decal.Dye1 = reader.ReadByte();
                decal.Dye2 = reader.ReadByte();

                decal.MaskIndex = new FigStringRef(_db, reader.ReadInt32());
                decal.MaskHash = reader.ReadInt64();
                decal.MaskHashB = reader.ReadInt32();
                decal.DiffuseNormalGlowIndex = new FigStringRef(_db, reader.ReadInt32());
                decal.DiffuseNormalHash = reader.ReadInt64();
                decal.DiffuseNormalHashB = reader.ReadInt32();
                decal.TintSpecularIndex = new FigStringRef(_db, reader.ReadInt32());
                decal.TintSpecularHash = reader.ReadInt64();
                decal.TintSpecularHashB = reader.ReadInt32();


                Records.Add(decal);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (var i = 0; i < Records.Count; i++)
            {
                CharacterDecal decal = Records[i];

                writer.Write((System.UInt32)decal.Type);
                writer.Write(decal.FileID);
                writer.Write(decal.AlphaBits);
                writer.Write(decal.TintType);
                writer.Write(decal.Dye1);
                writer.Write(decal.Dye2);
                writer.Write((System.UInt32)decal.MaskIndex);
                writer.Write(decal.MaskHash);
                writer.Write(decal.MaskHashB);
                writer.Write((System.UInt32)decal.DiffuseNormalGlowIndex);
                writer.Write(decal.DiffuseNormalHash);
                writer.Write(decal.DiffuseNormalHashB);
                writer.Write((System.UInt32)decal.TintSpecularIndex);
                writer.Write(decal.TintSpecularHash);
                writer.Write(decal.TintSpecularHashB);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((System.UInt32)Records.Count);
            writer.Write((System.UInt32)(Records.Count > 0 ? pos : 0));
            var size = (System.UInt32)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public enum DecalType : System.Int32
    {
        Mask = 1179413857,
        Diffuse = 1179411561,
        Tint = 1179415662,
        Glow = 1179412332,
        Normal = 1179414127,
        Specular = 1179415408
    }

    public class CharacterDecal : FigRecord
    {
        public System.Int32 Index { get; }
        public DecalType Type { get; set; }
        public System.Int32 FileID { get; set; }

        public FigStringRef MaskIndex { get; set; }

        public System.Byte AlphaBits { get; set; }
        public System.Byte TintType { get; set; }
        public System.Byte Dye1 { get; set; }
        public System.Byte Dye2 { get; set; }


        public System.Int64 MaskHash { get; set; }
        public System.Int32 MaskHashB { get; set; }

        public FigStringRef DiffuseNormalGlowIndex { get; set; }
        public System.Int64 DiffuseNormalHash { get; set; }
        public System.Int32 DiffuseNormalHashB { get; set; }

        public FigStringRef TintSpecularIndex { get; set; }
        public System.Int64 TintSpecularHash { get; set; }
        public System.Int32 TintSpecularHashB { get; set; }

        public CharacterDecal(FigleafDB db, System.Int32 index) : base(db) => Index = index;
    }

    public class CharacterDecalRef
    {
        private readonly FigleafDB _db;

        public static implicit operator System.Int32(CharacterDecalRef r) => r.Index;

        public override System.String ToString()
        {
            if (Index >= 0 && Index <= _db.TableCharacterDecals.Records.Count)
                return _db.TableCharacterDecals.Records[Index].MaskIndex.ToString() + " [" + Index + "]";
            return Index.ToString();
        }

        public System.Int32 Index { get; set; }
        public CharacterDecalRef(FigleafDB db, System.Int32 value)
        {
            _db = db;
            Index = value;
        }
    }
}

