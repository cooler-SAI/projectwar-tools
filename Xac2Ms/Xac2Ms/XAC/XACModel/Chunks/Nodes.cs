﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{

    public class Nodes
    {
        public uint mNodesCount;
        public uint mRootNodesCount; //(number of nodes with parentId = -1)
        public Vector3F mStaticBoxMin;
        public Vector3F mStaticBoxMax;

        public List<Node> mNodes;

        public Nodes()
        {
            mNodesCount = 0;
            mRootNodesCount = 0;
            mStaticBoxMin = new Vector3F();
            mStaticBoxMax = new Vector3F();
            mNodes = new List<Node>();
        }

        public void ReadIn(BinaryReader iStream, Chunk iChunk)
        {
            mNodesCount = iStream.ReadUInt32();
            mRootNodesCount = iStream.ReadUInt32();

            // mStaticBoxMin.ReadIn(iStream);
            //mStaticBoxMax.ReadIn(iStream);

            mNodes.Clear();
            for (int i = 1; i <= mNodesCount; i++)
            {
                Node node = new Node();
                node.ReadIn(iStream, iChunk);

                Console.WriteLine($"Rotation           {node.mRotation}");
                Console.WriteLine($"Bind Pose Rotation {node.mBindPoseRotation}");
                Console.WriteLine($"Position           {node.mPosition}");
                Console.WriteLine($"Scale              {node.mScale}");
                Console.WriteLine($"Bind Pose Position {node.mBindPosePosition}");
                Console.WriteLine($"LOD Mask           {Convert.ToString(node.mLODMask, 2)}");
                Console.WriteLine($"Parent             {node.mParentID}");
                Console.WriteLine($"Child node count   {node.mChildNodeCount}");
                Console.WriteLine($"Node Flags         {Convert.ToString(node.mNodeFlags, 2)}");
                Console.WriteLine($"Transform         {node.mTransform}");
                Console.WriteLine($"Name               '{node.mName}'");
                Console.WriteLine($"");


                mNodes.Add(node);
            }
        }

        public void WriteOut(BinaryWriter iStream, Chunk iChunk)
        {
            iStream.Write(mNodesCount);
            iStream.Write(mRootNodesCount);
            mStaticBoxMin.WriteOut(iStream);
            mStaticBoxMax.WriteOut(iStream);
            foreach (Node node in mNodes)
            {
                node.WriteOut(iStream, iChunk);
            }
        }

        public long GetSize(Chunk iChunk)
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mNodesCount);
            vSize += Marshal.SizeOf(mRootNodesCount);
            vSize += mStaticBoxMin.GetSize();
            vSize += mStaticBoxMax.GetSize();
            foreach (Node node in mNodes)
            {
                vSize += node.GetSize(iChunk);
            }
            return vSize;
        }

        public override string ToString()
        {
            string vTheString = "XACNodeTree NodesCount: " + mNodesCount.ToString() +
                                "  RotationRootNodesCount: " + mRootNodesCount.ToString();
            return vTheString;
        }
    }
}
