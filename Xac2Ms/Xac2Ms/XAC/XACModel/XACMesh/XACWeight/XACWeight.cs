﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class XACWeight
    {
        public float mWeight1;
        public float mWeight2;
        public float mWeight3;
        public float mWeight4;

        public XACWeight(float iWeight1, float iWeight2, float iWeight3, float iWeight4)
        {
            mWeight1 = iWeight1;
            mWeight2 = iWeight2;
            mWeight3 = iWeight3;
            mWeight4 = iWeight4;
        }

        public override string ToString()
        {
            string vTheString = "XACWeights w1:" + mWeight1.ToString() + " w2:" + mWeight2.ToString() + " w3:" + mWeight3.ToString() + " w4:" + mWeight4.ToString();
            return vTheString;
        }
    }

}
