﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace WarClient.Services
{
    public abstract class ServerRPC
    {
        private string _address;
        private int _port;

        public ServerRPC(string address, int port)
        {
        }

        protected async Task<TResult> RequestAsync<TResult>(string endpoint, params (string, object)[] parameters)
        {
            var path = $@"http://{_address}:{_port}/{endpoint}?";
            foreach (var p in parameters)
            {
                path += p.Item1 + "=" + Uri.EscapeUriString(p.Item1.ToString()) + "&";
            }
            var request = HttpWebRequest.Create(path);

            request.Proxy = null;


            request.ContentType = "application/json";
            request.Method = "POST";

            var response = await request.GetResponseAsync();

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<TResult>(reader.ReadToEnd());
            }
        }

    }
}
