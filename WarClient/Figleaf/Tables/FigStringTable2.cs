﻿using System;
using System.IO;
using System.Text;

namespace WarClient.Figleaf.Tables
{
    public class FigString2:FigRecord
    {
        public int Index { get; }
        public String Value { get; set; }
        public override string ToString()
        {
            return Value;
        }
        public FigString2(FigleafDB db, int index) : base(db) { Index = index; }
    }

    public class FigStringTable2 : FigTable<FigString2>
    {
        public uint Unk1 { get; set; }
        private const int HeaderPosition = 0x14;

        public FigStringTable2(FigleafDB db) : base(db)
        {
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;

            foreach (var entry in Records)
            {
                if (!String.IsNullOrEmpty(entry.Value))
                    writer.Write(Encoding.GetEncoding(437).GetBytes(entry.Value), 0, entry.Value.Length);
                writer.Write((byte)0);
            }
            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Unk1);
            writer.Write((uint)(Records.Count));
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            writer.Write((uint)(endPos - pos));
            writer.BaseStream.Position = endPos;
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            Unk1 = reader.ReadUInt32();
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();
            byte[] str = new byte[0xFFFF];

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var offset = reader.BaseStream.Position;

                int size = 0;
                for (size = 0; size < 0xFFFF; size++)
                {
                    byte c = (byte)reader.ReadByte();
                    if (c == '\0')
                        break;
                    str[size] = c;
                }

                Records.Add(new FigString2(_db, i)
                {
                    Value = Encoding.GetEncoding(437).GetString(str, 0, size),
                });
            }
        }
    }
}
