﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class Unk8Table : FigTable<Unk8>
    {
        private const int HeaderPosition = 0x6c;
        private const int RecordSize = 0x3c;

        public Unk8Table(FigleafDB db) : base(db)
        {

        }

        public override void Load(BinaryReader reader)
        {
              reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var texture = new Unk8(_db, i);
                var pos = reader.BaseStream.Position;
                var data = reader.ReadBytes(RecordSize);
                reader.BaseStream.Position = pos;
                texture.DiffuseSourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                texture.DiffuseHash = reader.ReadInt64();
                texture.DiffuseHashA = reader.ReadInt32();
                texture.SpecularSourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                texture.SpecularHash = reader.ReadInt64();
                texture.SpecularHashA = reader.ReadInt32();
                texture.TintSourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                texture.TintHash = reader.ReadInt64();
                texture.TintHashA = reader.ReadInt32();
                texture.A13 = reader.ReadInt32();
                texture.A14 = reader.ReadInt32();
                texture.A15 = reader.ReadInt32();
                texture.RawHex = Util.Hex(data, 0, data.Length, null, 16, false);
                Records.Add(texture);
            }
        }

        public override void Save(BinaryWriter writer)
        {
              var pos = writer.BaseStream.Position;
            for (int i = 0; i < Records.Count; i++)
            {
                var texture = Records[i];
                writer.Write((uint)texture.DiffuseSourceIndex);
                writer.Write(texture.DiffuseHash);
                writer.Write(texture.DiffuseHashA);
                writer.Write((uint)texture.SpecularSourceIndex);
                writer.Write(texture.SpecularHash);
                writer.Write(texture.SpecularHashA);
                writer.Write((uint)texture.TintSourceIndex);
                writer.Write(texture.TintHash);
                writer.Write(texture.TintHashA);
                writer.Write(texture.A13);
                writer.Write(texture.A14);
                writer.Write(texture.A15);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }


    public class Unk8:FigRecord
    {
        public int Index { get; }
        public FigStringRef DiffuseSourceIndex{ get; set; }
        public long DiffuseHash{ get; set; }
        public int DiffuseHashA{ get; set; }


        public FigStringRef SpecularSourceIndex{ get; set; }
        public long SpecularHash{ get; set; }
        public int SpecularHashA{ get; set; }


        public FigStringRef TintSourceIndex{ get; set; }
        public long TintHash{ get; set; }
        public int TintHashA{ get; set; }


        public int A13{ get; set; }
        public int A14{ get; set; }
        public int A15{ get; set; }
        public Unk8(FigleafDB db, int index) : base(db) { Index = index; }
        public string RawHex { get; set; }
    }
}

