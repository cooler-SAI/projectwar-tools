﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WarClient.Figleaf.Tables
{
    public class FigurePartTable : FigTable<Figure>
    {
        private const int HeaderPosition = 0x30;
        private const int RecordSize = 0x30;

        public FigurePartTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var figure = new Figure(_db, i);

                figure.SourceIndex = new FigStringRef(_db, reader.ReadInt32());
                figure.Unk01 = reader.ReadByte();
                figure.Unk02 = reader.ReadByte();
                figure.Unk03 = reader.ReadByte();
                figure.Unk04 = reader.ReadByte();
                figure.Unk05 = reader.ReadByte();
                figure.Unk06 = reader.ReadByte();
                figure.Unk07 = reader.ReadByte();
                figure.Unk08 = reader.ReadByte();
                figure.Unk09 = reader.ReadByte();
                figure.Unk10 = reader.ReadByte();
                figure.Unk11 = reader.ReadByte();
                figure.Unk12 = reader.ReadByte();
                figure.Unk13 = reader.ReadByte();
                figure.Unk14 = reader.ReadByte();
                figure.Unk15 = reader.ReadByte();
                figure.Unk16 = reader.ReadByte();
                figure.Unk17 = reader.ReadByte();
                figure.Unk18 = reader.ReadByte();
                figure.Unk19 = reader.ReadByte();
                figure.Unk20 = reader.ReadByte();

                figure.DecalCount = reader.ReadInt32();
                figure.DecalStart = reader.ReadInt32();
                figure.GeometryCount = reader.ReadInt32();
                figure.GeometryStart = reader.ReadInt32();
                figure.AttachmentCount = reader.ReadInt32();
                figure.AttachmentStart = reader.ReadInt32();

                Records.Add(figure);
            }

            for (int i = 0; i < EntryCount; i++)
            {
                var record = Records[i];

                if (record.DecalCount > 0)
                {
                    reader.BaseStream.Position = Offset + record.DecalStart + (i * RecordSize);
                    for (int c = 0; c < record.DecalCount; c++)
                    {
                        var a2 = new FigureDecal();
                        a2.Unk01 = reader.ReadInt32();
                        a2.MaleDiffuse1 = new FigStringRef(_db, reader.ReadInt32());
                        a2.FemaleDiffuse1 = new FigStringRef(_db, reader.ReadInt32());
                        a2.MaleDiffuse2 = new FigStringRef(_db, reader.ReadInt32());
                        a2.FemaleDiffuse2 = new FigStringRef(_db, reader.ReadInt32());
                        a2.MaleSpecular1 = new FigStringRef(_db, reader.ReadInt32());
                        a2.FemaleSpecular2 = new FigStringRef(_db, reader.ReadInt32());
                        a2.MaleTint1 = new FigStringRef(_db, reader.ReadInt32());
                        a2.FemaleTint1 = new FigStringRef(_db, reader.ReadInt32());
                        a2.MaleTint2 = new FigStringRef(_db, reader.ReadInt32());
                        a2.FemaleTint2 = new FigStringRef(_db, reader.ReadInt32());
                        a2.Unk12 = reader.ReadInt32();
                        a2.Unk13 = reader.ReadInt32();
                        a2.Unk14 = reader.ReadInt32();
                        a2.Unk15 = reader.ReadInt32();
                        a2.Unk16 = reader.ReadInt32();
                        a2.Unk17 = reader.ReadInt32();
                        a2.Unk18 = reader.ReadInt32();
                        a2.Unk19 = reader.ReadInt32();
                        a2.MaleGlow1 = new FigStringRef(_db, reader.ReadInt32());
                        a2.FemaleGlow1 = new FigStringRef(_db, reader.ReadInt32());
                        a2.Unk22 = new FigStringRef(_db, reader.ReadInt32());
                        a2.Unk23 = new FigStringRef(_db, reader.ReadInt32());
                        a2.Unk24 = new FigStringRef(_db, reader.ReadInt32());

                        record.Decals.Add(a2);
                    }
                }
                if (record.GeometryCount > 0)
                {
                    reader.BaseStream.Position = Offset + record.GeometryStart + (i * RecordSize);
                    for (int c = 0; c < record.GeometryCount; c++)
                    {
                        var a2 = new FigureGeometry();
                        a2.MaleCharacterMeshID =  new CharacterMeshRef(_db, reader.ReadInt32());
                        a2.FemaleCharacterGeometryID = new CharacterMeshRef(_db, reader.ReadInt32());
                        a2.Unk03 = reader.ReadInt32();
                        a2.Unk04 = reader.ReadInt32();

                        record.Geometry.Add(a2);
                    }
                }

                if (record.AttachmentCount > 0)
                {
                    reader.BaseStream.Position = Offset + record.AttachmentStart + (i * RecordSize);
                    for (int c = 0; c < record.AttachmentCount; c++)
                    {
                        var a2 = new FigureAttachment();
                        a2.Unk01 = reader.ReadInt64();
                      //  a2.Unk02 = reader.ReadInt32();
                        record.Attachments.Add(a2);
                    }
                }
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;

            writer.BaseStream.Position += RecordSize * Records.Count;
            var DataStart = writer.BaseStream.Position;
            var ExtStart = RecordSize * Records.Count;
          
            for (int i = 0; i < Records.Count; i++)
            {
                var record = Records[i];

                // record.Part1Start = 0;
                if (record.Decals.Count > 0)
                {
                    record.DecalStart = (int)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (var decal in record.Decals)
                    {
                        writer.Write(decal.Unk01);
                        writer.Write(decal.MaleDiffuse1);
                        writer.Write(decal.FemaleDiffuse1);
                        writer.Write(decal.MaleDiffuse2);
                        writer.Write(decal.FemaleDiffuse2);
                        writer.Write(decal.MaleSpecular1);
                        writer.Write(decal.FemaleSpecular2);
                        writer.Write(decal.MaleTint1);
                        writer.Write(decal.FemaleTint1);
                        writer.Write(decal.MaleTint2);
                        writer.Write(decal.FemaleTint2);
                        writer.Write(decal.Unk12);
                        writer.Write(decal.Unk13);
                        writer.Write(decal.Unk14);
                        writer.Write(decal.Unk15);
                        writer.Write(decal.Unk16);
                        writer.Write(decal.Unk17);
                        writer.Write(decal.Unk18);
                        writer.Write(decal.Unk19);
                        writer.Write(decal.MaleGlow1);
                        writer.Write(decal.FemaleGlow1);
                        writer.Write(decal.Unk22);
                        writer.Write(decal.Unk23);
                        writer.Write(decal.Unk24);

                    }
                }

               // record.Part2Start = 0;
                if (record.Geometry.Count > 0)
                {
                    record.GeometryStart = (int)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (var geometry in record.Geometry)
                    {
                        writer.Write(geometry.MaleCharacterMeshID);
                        writer.Write(geometry.FemaleCharacterGeometryID);
                        writer.Write(geometry.Unk03);
                        writer.Write(geometry.Unk04);
                    }
                }

               // record.Part3Start = 0;
                if (record.Attachments.Count > 0)
                {
                    record.AttachmentStart = (int)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (var part in record.Attachments)
                    {
                        writer.Write(part.Unk01);
                     //   writer.Write(part.Unk02);
                    }
                }
            }
            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = pos;

            for (int i = 0; i < Records.Count; i++)
            {
                var figure = Records[i];

                writer.Write((uint)figure.SourceIndex);
                writer.Write(figure.Unk01);
                writer.Write(figure.Unk02);
                writer.Write(figure.Unk03);
                writer.Write(figure.Unk04);
                writer.Write(figure.Unk05);
                writer.Write(figure.Unk06);
                writer.Write(figure.Unk07);
                writer.Write(figure.Unk08);
                writer.Write(figure.Unk09);
                writer.Write(figure.Unk10);
                writer.Write(figure.Unk11);
                writer.Write(figure.Unk12);
                writer.Write(figure.Unk13);
                writer.Write(figure.Unk14);
                writer.Write(figure.Unk15);
                writer.Write(figure.Unk16);
                writer.Write(figure.Unk17);
                writer.Write(figure.Unk18);
                writer.Write(figure.Unk19);
                writer.Write(figure.Unk20);
                writer.Write(figure.DecalCount);
                writer.Write(figure.DecalStart);
                writer.Write(figure.GeometryCount);
                writer.Write(figure.GeometryStart);
                writer.Write(figure.AttachmentCount);
                writer.Write(figure.AttachmentStart);
            }

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)((endPos - DataStart) + (Records.Count * RecordSize));
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class Figure : FigRecord
    {
        public int Index { get; }
        public FigStringRef SourceIndex { get; set; }

        public byte Unk01 { get; set; }
        public byte Unk02 { get; set; }
        public byte Unk03 { get; set; }
        public byte Unk04 { get; set; }

        public byte Unk05 { get; set; }
        public byte Unk06 { get; set; }
        public byte Unk07 { get; set; }
        public byte Unk08 { get; set; }

        public byte Unk09 { get; set; }
        public byte Unk10 { get; set; }
        public byte Unk11 { get; set; }
        public byte Unk12 { get; set; }

        public byte Unk13 { get; set; }
        public byte Unk14 { get; set; }
        public byte Unk15 { get; set; }
        public byte Unk16 { get; set; }

        public byte Unk17 { get; set; }
        public byte Unk18 { get; set; }
        public byte Unk19 { get; set; }
        public byte Unk20 { get; set; }

        public int DecalCount { get; set; }
        public int DecalStart { get; set; }
        public List<FigureDecal> Decals = new List<FigureDecal>();

        public int GeometryCount { get; set; }
        public int GeometryStart { get; set; }
        public List<FigureGeometry> Geometry = new List<FigureGeometry>();

        public int AttachmentCount { get; set; }
        public int AttachmentStart { get; set; }
        public List<FigureAttachment> Attachments = new List<FigureAttachment>();

        public Figure(FigleafDB db, int index): base(db) { Index = index; }
    }

    public class FigureRef
    {
        private FigleafDB _db;

        public static implicit operator int(FigureRef r)
        {
            return r.Index;
        }

        public override string ToString()
        {
            if (Index >= 0 && Index <= _db.TableFigureParts.Records.Count)
                return _db.TableFigureParts.Records[Index].SourceIndex.ToString() + " [" + Index + "]";
            return "";
        }

        public int Index { get; set; }
        public FigureRef(FigleafDB db, int value)
        {
            _db = db;
            Index = value;
        }
    }

    public class FigureDecal
    {

        public int Unk01 { get; set; }
        public FigStringRef MaleDiffuse1 { get; set; }
        public FigStringRef FemaleDiffuse1 { get; set; }
        public FigStringRef MaleDiffuse2 { get; set; }
        public FigStringRef FemaleDiffuse2 { get; set; }
        public FigStringRef MaleSpecular1 { get; set; }
        public FigStringRef FemaleSpecular2 { get; set; }
        public FigStringRef MaleTint1 { get; set; }
        public FigStringRef FemaleTint1 { get; set; }
        public FigStringRef MaleTint2 { get; set; }
        public FigStringRef FemaleTint2 { get; set; }
        public int Unk12 { get; set; }
        public int Unk13 { get; set; }
        public int Unk14 { get; set; }
        public int Unk15 { get; set; }
        public int Unk16 { get; set; }
        public int Unk17 { get; set; }
        public int Unk18 { get; set; }
        public int Unk19 { get; set; }
        public FigStringRef MaleGlow1 { get; set; }
        public FigStringRef FemaleGlow1 { get; set; }
        public FigStringRef Unk22 { get; set; }
        public FigStringRef Unk23 { get; set; }
        public FigStringRef Unk24 { get; set; }
    }

    public class FigureGeometry
    {
        public CharacterMeshRef MaleCharacterMeshID { get; set; }
        public CharacterMeshRef FemaleCharacterGeometryID { get; set; }
        public int Unk03 { get; set; }
        public int Unk04 { get; set; }
    }


    public class FigureAttachment
    {
        public long Unk01 { get; set; }
      //  public int Unk02 { get; set; }
    }
}
