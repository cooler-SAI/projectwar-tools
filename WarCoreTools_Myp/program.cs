﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Text;


namespace WarClientTool
{
    public static class Program
    {
        private static AssemblyFileVersionAttribute version;
        private static AssemblyTitleAttribute title;
        private static String exe;

        public static Dictionary<Int64, String> AssetHash2Path = new Dictionary<Int64, String>();
        public static Dictionary<String, Int64> AssetPath2Hash = new Dictionary<String, Int64>();

        static void Main(String[] args)
        {
            version = Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyFileVersionAttribute>();
            title = Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyTitleAttribute>();
            exe = System.AppDomain.CurrentDomain.FriendlyName;

            //args = new string[] { @"-e", @"D:\Games\Warhammer Online - Age of Reckoning\data.Myp", @"D:\Games\Warhammer Online - Age of Reckoning\Myps\data" };

            Console.WriteLine($"{title.Title} {version.Version}");

            if (File.Exists("asset_hash_path.tab"))
            {
                Byte[] hashes_slim = File.ReadAllBytes("asset_hash_path.tab");
                Byte[] hashes_slim_dat = Compress(hashes_slim);
                File.WriteAllBytes("hashes_slim.dat", hashes_slim_dat);

                Console.WriteLine("Compressed asset_hash_path.tab into hashes_slim.dat");
            }

            if (args == null || args.Length == 0 || String.IsNullOrWhiteSpace(args[0]) || args[0].Length < 3)
            {
                Console.WriteLine("Invalid arguments");
                return;
            }

            LoadDehashes();

            String cmd = args[0].ToLower();

            Int32 prefixLength = 0;

            if (cmd[0] == '-' && cmd[1] == '-')
                prefixLength = 2;
            else if (cmd[0] == '-' || cmd[0] == '\\' || cmd[0] == '/' || cmd[0] == '+')
                prefixLength = 1;

            String prefix = cmd.Substring(0, prefixLength);
            String suffix = cmd.Substring(prefixLength, cmd.Length - prefixLength);

            switch (suffix)
            {
                case @"cap":
                case @"compress-asset-paths":
                    CompressPathList(args);
                    break;

                case @"e":
                case @"extract":
                    Extract(args);
                    break;

                case @"pf":
                case @"pack-file":
                    PackFile(args);
                    break;

                case @"pd":
                case @"pack-directory":
                case @"pack-folder":
                    PackFolder(args);
                    break;

                case @"hf":
                case @"hash-file":
                    HashFile(args);
                    break;

                case @"hd":
                case @"hash-directry":
                case @"hash-folder":
                    HashFolder(args);
                    break;

                case @"h":
                case @"help":
                    Help();
                    break;

                default:
                    Help();
                    break;
            }

            Console.WriteLine("");
        }

        static void CompressPathList(String[] args)
        {
            if (File.Exists("hashes_slim.tab"))
            {
                Byte[] hashes_slim = File.ReadAllBytes("hashes_slim.tab");
                Byte[] hashes_slim_dat = Compress(hashes_slim);
                File.WriteAllBytes("hashes_slim.dat", hashes_slim_dat);

                Console.WriteLine("Compressed hashes_slim.tab into hashes_slim.dat");
            }
        }

        static void HashFile(String[] args)
        {

        }


        static void HashFolder(String[] args)
        {
            if (!Directory.Exists(args[1]))
            {
                Console.WriteLine($"Folder {args[1].ToLower()} does not exist.");
                return;
            }
        }

        static void Extract(String[] args)
        {
            if (args.Length < 3)
            {
                Help();
                return;
            }

            if (Directory.Exists(args[1]))
            {
                Console.WriteLine($"Extracting to {args[1]}");
                foreach (String MypName in Enum.GetNames(typeof(MythicPackage)))
                {
                    String name = Path.Combine(args[1], MypName + ".Myp");
                    var p = (MythicPackage)Enum.Parse(typeof(MythicPackage), MypName);
                    ExtractMyp(p, name, args[2]);
                }
            }
            else if (File.Exists(args[1]))
            {
                String name = args[1].ToLower();
                var p = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(name).ToUpper());
                ExtractMyp(p, name, args[2]);
            }
        }

        static void PackFolder(String[] args)
        {
            if (args.Length < 2)
            {
                Help();// Console.WriteLine("Usage: PACKFOLDER [Myps folder] [dest Myp file]");
                return;
            }

            if (!Directory.Exists(args[1]))
            {
                Console.WriteLine($"Myps folder {args[1].ToLower()} does not exist.");
                return;
            }

            String MypName = Path.GetFileName(args[2]).ToUpper();

            Myp Myp = null;

            if (!File.Exists(args[2]))
            {
                Console.WriteLine($"Creating {MypName.ToLower()}.");
                _ = null ?? new Myp(args[2], true);
            }
            else
            {
                Console.WriteLine($"Processing {MypName.ToLower()}.");
            }

            Myp = null ?? new Myp(args[2]);
            var watch = System.Diagnostics.Stopwatch.StartNew();
            {
                Boolean verbose = args.Length == 3 ? (args[3].Substring(2) == "verbose" || args[3].Substring(1) == "verbose" || args[3] == "verbose") : false;
                // Parallel.ForEach(Directory.GetFiles(args[1]), (file) =>
                //   {
                foreach (String file in Directory.GetFiles(args[1]))
                {
                    String assetName = file.Replace(args[1], "").Replace("\\", "/").Substring(1).ToLower();
                    if (verbose)
                        Console.WriteLine("Packing " + Path.GetFileName(file));

                    if (!assetName.Contains("unknown_hashes"))
                    {
                        Myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                    }
                    else
                    {
                        Int64 hash = Convert.ToInt64(Path.GetFileNameWithoutExtension(file).ToLower(), 16); //(long)ulong.Parse(Path.GetFileNameWithoutExtension(file));
                        Myp.UpdateAsset(hash, File.ReadAllBytes(file), CanCompress(file));
                    }

                    //  Myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }//);

                // Parallel.ForEach(Directory.GetFiles(args[1]), (folder) =>
                // {
                foreach (String folder in Directory.GetDirectories(args[1]))
                {
                    PackFolder(args[1], folder, Myp, verbose);
                }//);
                //Console.WriteLine("Writing " + args[2]);
                Myp.Save();
            }
            watch.Stop();

            Double elapsed_seconds = Math.Round(watch.ElapsedMilliseconds / 1000.0f, 2);
            Console.WriteLine($"Compression completed in {elapsed_seconds} seconds.");


        }

        static void PackFile(String[] args)
        {
            if (args.Length < 3)
            {
                Help();
                return;
            }

            if (!File.Exists(args[1]))
            {
                Console.WriteLine($"Asset to pack not found {args[1]}");
                return;
            }

            String MypName = Path.GetFileName(args[2]).ToUpper();

            MythicPackage p = MythicPackage.NONE;
            if (Enum.TryParse<MythicPackage>(MypName, out p))
            {
                Console.WriteLine($"Destination .Myp file name is limited to known .Myp archive names.");
                return;
            }

            if (!File.Exists(args[2]))
            {
                Console.WriteLine($"Creating Myp ({MypName})");
                using (var Myp = new Myp(args[2], true))
                { }
            }

            using (var Myp = new Myp(args[3], true))
            {
                String assetName = args[3].Replace("\\", "/").ToLower();

                Console.WriteLine("Packing [" + assetName + "]");
                Myp.UpdateAsset(assetName, File.ReadAllBytes(args[1]), CanCompress(args[1]));

                Console.WriteLine("Writing " + args[3]);
                Myp.Save();

                Console.WriteLine("Done");
            }
        }

        private static void PackFolder(String root, String currentFolder, Myp Myp, Boolean verbose)
        {
            foreach (String folder in Directory.GetDirectories(currentFolder))
            {
                PackFolder(root, folder, Myp, verbose);
            }

            // Parallel.ForEach(Directory.GetFiles(currentFolder), (file) =>
            // {
            foreach (String file in Directory.GetFiles(currentFolder))
            {
                if (verbose)
                    Console.WriteLine("Packing " + Path.GetFileName(file));
                String assetName = file.ToLower().Replace(root, "").Replace("\\", "/").Substring(1).Replace(".stx.dds", ".stx");

                if (assetName.Contains("no_dehash"))
                {
                    // long hash = (long)ulong.Parse(Path.GetFileNameWithoutExtension(file));
                    Int64 hash = Convert.ToInt64(Path.GetFileNameWithoutExtension(file).ToLower(), 16);
                    Myp.UpdateAsset(hash, File.ReadAllBytes(file), CanCompress(file));
                }
                else
                {
                    Myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }
            }//);
        }

        static Boolean CanCompress(String filename, Boolean defaultValue = true)
        {
            FileAttributes attributes = File.GetAttributes(filename);

            //Console.WriteLine($"{filename} C[{attributes.HasFlag(FileAttributes.Archive)}]");

            return attributes.HasFlag(FileAttributes.Archive);
        }

        static void ExtractMyp(MythicPackage p, String name, String dest)
        {
            if (File.Exists(name))
            {
                try
                {
                    using (var Myp = new Myp(name, false))
                    {
                        Console.WriteLine($"Processing {name} containing {Myp.Assets.Count} assets.");

                        Int32 count = 0;

                        Task.Run(() =>
                        {
                            while (true)
                            {
                                unchecked
                                {
                                    Single delta_progress = 100.0f / Myp.Assets.Count;
                                    Int32 percent = (Int32)(delta_progress * count);

                                    Console.Title = $"WAaaAGHhhH TOolz WOT iZ 4 MeKAniks =>< {count}/{Myp.Assets.Count} assets {percent}% extracted from {name.ToLower()}";
                                }

                                if (count == Myp.Assets.Count)
                                    break;

                                Thread.Yield();
                                Thread.Sleep(1000);
                            }
                        });

                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        Parallel.ForEach(Myp.Assets, (asset) =>
                        {
                            Int64 hash = asset.Key;
                            Byte[] data = Myp.GetAssetData(hash);
                            String path = String.Empty;
                            if (AssetHash2Path.ContainsKey(hash))
                            {
                                path = Path.Combine(dest, AssetHash2Path[hash].ToLower().Replace("/", "\\").Replace(".stx", ".stx.dds"));
                                String folder = Path.GetDirectoryName(path);
                                if (!Directory.Exists(folder))
                                    Directory.CreateDirectory(folder);
                            }
                            else
                            {
                                String type = Myp.GetExtension(data);

                                String folder = Path.Combine(dest, "unknown_hashes", type);
                                if (!Directory.Exists(folder))
                                    Directory.CreateDirectory(folder);

                                UInt64 uhash = (UInt64)hash;
                                uhash = (uhash >> 32) | (uhash << 32); // swap adjacent 32-bit blocks
                                uhash = ((uhash & 0xFFFF0000FFFF0000) >> 16) | ((uhash & 0x0000FFFF0000FFFF) << 16); // swap adjacent 16-bit blocks
                                uhash = ((uhash & 0xFF00FF00FF00FF00) >> 8) | ((uhash & 0x00FF00FF00FF00FF) << 8); // swap adjacent 8-bit blocks

                                Byte[] ubytes = BitConverter.GetBytes(uhash);
                                var ustringBuilder = new StringBuilder(ubytes.Length);

                                foreach (Byte byteValue in ubytes)
                                    ustringBuilder.Append(byteValue.ToString("x2"));

                                path = Path.Combine(folder, ustringBuilder.ToString() + "." + type.ToLower());
                            }
                            File.WriteAllBytes(path, data);


                            //using (BinaryWriter outputFile = new BinaryWriter9(Path.Combine(docPath, "WriteLines.txt"))) {
                            //    foreach (string line in lines)
                            //        outputFile.write.WriteLine(line);
                            //}



                            //Shell32.Shell shell = new Shell32.ShellClass();
                            ////set the namespace to file path
                            //Shell32.Folder folder = shell.NameSpace(filePath);
                            ////get ahandle to the file
                            //Shell32.FolderItem folderItem = folder.ParseName(fileName);

                            //BinaryWriter Writer = new BinaryWriter()

                            //
                            //File


                            //FileAttributes attributes = File.GetAttributes(path);

                            //// Console.WriteLine($"{path} C[{asset.Value.Compressed}]");

                            //if(asset.Value.Compressed == 1)
                            //    attributes |= FileAttributes.Archive;
                            //else
                            //    attributes &= ~FileAttributes.Archive;

                            //File.SetAttributes(path, attributes);

                            ++count;

                        });
                        watch.Stop();

                        Double elapsed_seconds = Math.Round(watch.ElapsedMilliseconds / 1000.0f, 2);
                        Console.WriteLine($"Extraction completed in {elapsed_seconds} seconds.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static Byte[] Compress(Byte[] data)
        {
            var outB = new MemoryStream();

            using (var archive = new DeflateStream(outB, CompressionLevel.Optimal, true))
            {
                archive.Write(data, 0, (Int32)data.Length);
            }
            outB.Position = 0;
            return outB.ToArray();
        }

        public static Byte[] DeCompress(Byte[] data, Boolean gzip = false)
        {
            var outB = new MemoryStream(data);
            if (gzip)
                outB.Position += 2;

            using (var archive = new DeflateStream(outB, CompressionMode.Decompress, true))
            {
                var dest = new MemoryStream();
                archive.CopyTo(dest);
                return dest.ToArray();
            }
        }

        static void LoadDehashes()
        {
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WarClientTool.hashes_slim.dat"))
            {
                Byte[] data = new Byte[stream.Length];
                stream.Read(data, 0, data.Length);
                data = DeCompress(data);
                LoadDehashes(data);

                Int32 hash_count = AssetHash2Path.Count;
                Int32 new_hash_count = 0;
                Console.WriteLine($"Using {hash_count} built in asset path(s) and hashe(s).");

                if (File.Exists($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\hashes_slim.tab"))
                {
                    LoadDehashes(File.ReadAllBytes($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\hashes_slim.tab"));
                    new_hash_count = AssetHash2Path.Count - hash_count;
                    Console.WriteLine($"Loaded {new_hash_count} extra asset path(s) and hashe(s) from hashes_slim.tab");
                }
                hash_count = AssetHash2Path.Count;

                if (File.Exists($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_hash_path.tab"))
                {
                    LoadDehashes(File.ReadAllBytes($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_hash_path.tab"));
                    new_hash_count = AssetHash2Path.Count - hash_count;
                    Console.WriteLine($"Loaded {new_hash_count} extra asset path(s) and hashe(s) from asset_hash_path.tab");
                }
                hash_count = AssetHash2Path.Count;

                if (File.Exists($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_path.tab"))
                {
                    LoadAndHashPaths(File.ReadAllBytes($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_path.tab"));
                    new_hash_count = AssetHash2Path.Count - hash_count;
                    Console.WriteLine($"Loaded {new_hash_count} extra asset path(s) from asset_path.tab");
                }
            }
        }

        static void LoadAndHashPaths(Byte[] data)
        {
            try
            {
                using (var reader = new StreamReader(new MemoryStream(data)))
                {
                    while (!reader.EndOfStream)
                    {
                        String line = reader.ReadLine();
                        Int64 hash = Myp.HashWAR(line);
                        if (!AssetHash2Path.ContainsKey(hash))
                        {
                            AssetHash2Path[hash] = line;
                            AssetPath2Hash[line] = hash;
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error loading asset hashes");
            }
        }

        static void LoadDehashes(Byte[] data)
        {
            try
            {
                using (var reader = new StreamReader(new MemoryStream(data)))
                {
                    while (!reader.EndOfStream)
                    {
                        String[] items = reader.ReadLine().Split(new Char[] { '\t' });
                        Int64 hash = Int64.Parse(items[0]);
                        if (!AssetHash2Path.ContainsKey(hash))
                        {
                            AssetHash2Path[hash] = items[1];
                            AssetPath2Hash[items[1]] = hash;
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error loading asset hashes");
            }
        }

        static void Help()
        {
            Console.WriteLine($"{title.Title} {version.Version}");
            Console.WriteLine("  ");
            Console.WriteLine($" Usage: {exe.ToLower()} <command> <source> <destination> <hash> (optional) --verbose");
            Console.WriteLine("  ");
            Console.WriteLine("  The command can be formatted in the following ways.");
            Console.WriteLine("  --command, -command, /command");
            Console.WriteLine("  ");
            Console.WriteLine("  The following commands are possible.");
            Console.WriteLine("  extract, e             : Extracts either all .Myp files in the <source> directory or extract the .Myp <source> file to the <destination> directory.");
            Console.WriteLine("  pack-directory, pd     : Recursively packs all files below the <source> directory in to the <destination> .Myp file creating or modifying as requried.");
            Console.WriteLine("  pack-file, pf          : Packs a single <source> file in to the <destination> .Myp file optionally providing an <asset string or hash(override)>");
            Console.WriteLine("  csv  : Unpacks a csv file from the <source> .Myp file and a <asset string or hash> to be edited and re-saved.");
            Console.WriteLine("  h    : Displays this message.");

            //Console.WriteLine("  DELETE [dest Myp file] [asset name]");
        }
    }
}

//case "-cpl":
//goto case "--compress-path-list";
//case "--compress-path-list":
//CompressPathList(args);
//break;

//case "-e":
//goto case "--extract";
//case "--extract":
//Extract(args);
//break;

//case "-pf":
//goto case "pack-file";
//case "pack-file":
//PackFile(args);
//break;

//case "-pd":
//goto case "--pack-directory";
//case "--pack-directory":
//goto case "--pack-folder";
//case "--pack-folder":
//PackFolder(args);
//break;

//case "-ecf":
//goto case "--edit-csv-file";
//case "--edit-csv-file":
//EditCSVFile(args);
//break;

//case "-hf":
//goto case "--hash-file";
//case "--hash-file":
//HashFile(args);
//break;

//case "-hd":
//goto case "--hash-directry";
//case "--hash-directry":
//goto case "--hash-folder";
//case "--hash-folder":
//HashFolder(args);
//break;

//case "-h":
//goto case "help";
//case "help":
//Help();
//break;
