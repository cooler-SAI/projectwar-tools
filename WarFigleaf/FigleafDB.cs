﻿
using System;
using System.Collections.Generic;
using System.IO;
using WarFigleaf.Tables;

namespace WarFigleaf
{
    public class FigleafDB
    {
        public byte[] FigData;
        public byte[] OrigData;

        public FigStringTable TableStrings1 { get; private set; }
        public FigStringTable2 TableStrings2 { get; private set; }
        public CharacterArtTable TableCharacterArt { get; private set; }
        public FigurePartTable TableFigureParts { get; private set; }
        public ArtSwitchTable TableArtSwitches { get; private set; }
        public CharacterDecalTable TableCharacterDecals { get; private set; }
        public CharacterMeshesTable TableCharacterMeshes { get; private set; }
        public TextureTable TableTextures { get; private set; }
        public Unk8Table TableUnk8 { get; private set; }
        public FixtureTable TableFixtures { get; private set; }
        public LightmapTable TableLightmaps { get; private set; }
        public PatchTable TablePatches { get; private set; }
        public RegionTable TableRegions { get; private set; }
        public UnkVar16Table TableVAR_RECS1 { get; private set; }
        public UnkVar16Table TableVAR_RECS2 { get; private set; }
        public UnkVar16Table TableVAR_RECS3 { get; private set; }
        public UnkVar16Table TableVAR_RECS4 { get; private set; }
        public UnkVar16Table TableVAR_RECS5 { get; private set; }
        public Dictionary<TableType, FigTable> Tables { get; private set; } = new Dictionary<TableType, FigTable>();

        public void AddTable(FigTable table)
        {
            Tables[table.Type] = table;
        }

        public void Save(Stream stream)
        {
            BinaryWriter writer = new BinaryWriter(stream);
            stream.Write(new byte[0x33c], 0, 0x33c);
            stream.Position = 0;

            writer.Write(new char[] { 'b', 'd', 'L', 'F' });
            writer.Write((uint)0x20); //block count?
            writer.Write((uint)0); //filesize

            int startPos = (int)writer.BaseStream.Position;


            foreach (var table in Tables.Values)
            {
                table.SaveToc(writer);
            }

            stream.Position = 0x33c;
            foreach (var table in Tables.Values)
            {
                table.SaveData(writer);
            }
            stream.Position = 0x8;
            writer.Write((uint)stream.Length);
        }

        public void CompareStreams(Stream streamA, Stream streamB, int offset, int length)
        {
            byte[] data1 = new byte[length];
            byte[] data2 = new byte[length];

            var pos1 = streamA.Position;
            var pos2 = streamB.Position;

            streamA.Position = offset;
            streamB.Position = offset;

            streamA.Read(data1, 0, length);
            streamB.Read(data2, 0, length);

            streamA.Position = pos1;
            streamB.Position = pos2;

            for (int i = 0; i < length; i++)
            {
                if (data1[i] != data2[i])
                {
                    throw new Exception("Not equal at " + i);
                }
            }
        }
        public void Load(byte[] data)
        {
            FigData = data;
            OrigData = new byte[data.Length];
            Buffer.BlockCopy(data, 0, OrigData, 0, data.Length);

            using (var reader = new BinaryReader(new MemoryStream(data)))
            {
                var header = reader.ReadBytes(4);
                var tocSize = reader.ReadUInt32();
                var fileSize = reader.ReadUInt32();

                TableStrings1 = new FigStringTable(this, data, reader);
                TableStrings2 = new FigStringTable2(this, data, reader);
                TableCharacterArt = new CharacterArtTable(this, 0x48, data, reader);
                TableFigureParts = new FigurePartTable(this, 0x30, data, reader);
                TableArtSwitches = new ArtSwitchTable(this, 0xA4, data, reader);
                TableCharacterDecals = new CharacterDecalTable(this, 0x3C, data, reader);
                TableCharacterMeshes = new CharacterMeshesTable(this, 0x14, data, reader);
                TableTextures = new TextureTable(this, 0x3C, data, reader);
                TableUnk8 = new Unk8Table(this, 0x3C, data, reader);
                TableFixtures = new FixtureTable(this, 0x48, data, reader);
                TableLightmaps = new LightmapTable(this, 0x28, data, reader);
                TablePatches = new PatchTable(this, 0xC, data, reader);
                TableRegions = new RegionTable(this, 0x20, data, reader);
                TableVAR_RECS1 = new UnkVar16Table(this, reader, data, TableType.VAR_RECS1);
                TableVAR_RECS2 = new UnkVar16Table(this, reader, data, TableType.VAR_RECS2);
                TableVAR_RECS3 = new UnkVar16Table(this, reader, data, TableType.VAR_RECS3);
                TableVAR_RECS4 = new UnkVar16Table(this, reader, data, TableType.VAR_RECS4);
                TableVAR_RECS5 = new UnkVar16Table(this, reader, data, TableType.VAR_RECS5);

                foreach (var table in Tables.Values)
                {
                    table.Load();
                }
            }
        }
    }
}
