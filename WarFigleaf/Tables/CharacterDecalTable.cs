﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class CharacterDecalTable : FigTable
    {
        public List<CharacterDecalView> Decals;
        public override List<object> Records => Decals.Select(e => (object)e).ToList();
        public override int RecordSize => Marshal.SizeOf(typeof(CharacterDecal));

        public CharacterDecalTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.CharacterDecals, db, data, reader)
        {
        }

        protected override int LoadInternal()
        {
            Decals = ReadStructs<CharacterDecal>(_fileData, Offset, (int)EntryCount).Select(e => new CharacterDecalView(e)).ToList();

            int i = 0;
            StringBuilder s = new StringBuilder();

            Decals.ForEach(e =>
                {
                    e.Index = i;
                    if (e.A04_MaskIndex > -1 && e.A04_MaskIndex < _db.TableStrings1.Strings.Count)
                        e.MaskName = _db.TableStrings1.Strings[(int)e.A04_MaskIndex].Value;
                    else
                        e.MaskName = "!INVALID_INDEX!";

                    if (e.A08_DiffuseNormalGlowIndex > -1 && e.A08_DiffuseNormalGlowIndex < _db.TableStrings1.Strings.Count)
                        e.DiffuseNormalGlowName = _db.TableStrings1.Strings[(int)e.A08_DiffuseNormalGlowIndex].Value;
                    else
                        e.DiffuseNormalGlowName = "!INVALID_INDEX!";

                    if (e.A12_TintSpecularIndex > -1 && e.A12_TintSpecularIndex < _db.TableStrings1.Strings.Count)
                        e.TintSpecularName = _db.TableStrings1.Strings[(int)e.A12_TintSpecularIndex].Value;
                    else
                        e.TintSpecularName = "!INVALID_INDEX!";

                    i++;
                }
            );

            return RecordSize * (int)EntryCount;
        }
    }
}

