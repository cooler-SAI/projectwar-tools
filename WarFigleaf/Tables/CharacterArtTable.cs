﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class CharacterArtTable : FigTable
    {
        public override List<object> Records => CharacterArt.Select(e => (object)e).ToList();
        public List<CharacterArtView> CharacterArt;
        public Dictionary<int, CharacterArtData[]> Parts = new Dictionary<int, CharacterArtData[]>();
        public override int RecordSize => Marshal.SizeOf(typeof(CharacterArtData));

        public CharacterArtTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.CharacterArt, db, data, reader)
        {
        }

        public override void SaveData(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            var recSize = Marshal.SizeOf(typeof(CharacterArt));
            var extSize = Marshal.SizeOf(typeof(CharacterArtData));
            var size = Marshal.SizeOf(typeof(CharacterArt)) * (int)EntryCount;


            //write out primary data
            writer.BaseStream.Position = pos;
            foreach (var st in CharacterArt)
            {
                var data = WriteStructs(st.CharacterArt);
                writer.Write(data, 0, data.Length);
            }

            writer.Write(_extData, 0, _extData.Length);

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = TocOffset;
            writer.Write((uint)CharacterArt.Count);
            writer.Write((uint)pos);
            writer.Write((uint)(endPos - pos));

            writer.BaseStream.Position = endPos;
        }

        protected override bool LoadExtData()
        {
            _reader.BaseStream.Position = Offset + (Marshal.SizeOf(typeof(CharacterArt)) * (int)EntryCount);
            int extSize = (int)(DataSize - (_reader.BaseStream.Position - Offset));
            _extData = new byte[extSize];
            _reader.BaseStream.Read(_extData, 0, extSize);
            return true;
        }

        protected override int LoadInternal()
        {
            CharacterArt = ReadStructs<CharacterArt>(_fileData, Offset, (int)EntryCount).Select(e => new CharacterArtView(e)).ToList();
            int i = 0;
            foreach (var art in CharacterArt)
            {
                if (art.SourceIndex < _db.TableStrings1.Strings.Count)
                    art.Name = _db.TableStrings1.Strings[(int)art.SourceIndex].Value;
                else
                    art.Name = "!INALID_INDEX!";


                art.Index = i;
                i++;
            }
            return RecordSize * (int)EntryCount;
        }
    }
   
}

