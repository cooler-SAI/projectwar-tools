﻿using System.Runtime.InteropServices;

namespace WarFigleaf.Structs
{

    [StructLayout(LayoutKind.Sequential)]
    public struct Fixture
    {
        public uint SourceIndex;
        public uint Unk1;
        public uint Unk2;
        public uint Unk3;
        public uint Unk4;
        public uint Unk5;
        public uint Unk6;
        public uint Unk7;
        public uint Unk8;
        public uint Unk9;
        public uint Unk10;
        public uint Unk11;
        public uint Unk12;
        public uint Unk13;
        public uint CollisionSourceIndex;
        public uint Unk15;
        public uint Unk16;
        public uint Unk17;
    }


    public class FixtureView
    {
        public int Index { get; set; }
        public Fixture Fixture;
        public FixtureView(Fixture fixture)
        {
            Fixture = fixture;
        }
        public string Name { get; set; }
        public string NameCollision { get; set; }
        public uint SourceIndex { get => Fixture.SourceIndex; set => Fixture.SourceIndex = value; }
        public uint Unk1 { get => Fixture.Unk1; set => Fixture.Unk1 = value; }
        public uint Unk2 { get => Fixture.Unk2; set => Fixture.Unk2 = value; }
        public uint Unk3 { get => Fixture.Unk3; set => Fixture.Unk3 = value; }
        public uint Unk4 { get => Fixture.Unk4; set => Fixture.Unk4 = value; }
        public uint Unk5 { get => Fixture.Unk5; set => Fixture.Unk5 = value; }
        public uint Unk6 { get => Fixture.Unk6; set => Fixture.Unk6 = value; }
        public uint Unk7 { get => Fixture.Unk7; set => Fixture.Unk7 = value; }
        public uint Unk8 { get => Fixture.Unk8; set => Fixture.Unk8 = value; }
        public uint Unk9 { get => Fixture.Unk9; set => Fixture.Unk9 = value; }
        public uint Unk10 { get => Fixture.Unk10; set => Fixture.Unk10 = value; }
        public uint Unk11 { get => Fixture.Unk11; set => Fixture.Unk11 = value; }
        public uint Unk12 { get => Fixture.Unk12; set => Fixture.Unk12 = value; }
        public uint Unk13 { get => Fixture.Unk13; set => Fixture.Unk13 = value; }
        public uint CollisionSourceIndex { get => Fixture.CollisionSourceIndex; set => Fixture.CollisionSourceIndex = value; }
        public uint Unk15 { get => Fixture.Unk15; set => Fixture.Unk15 = value; }
        public uint Unk16 { get => Fixture.Unk16; set => Fixture.Unk16 = value; }
        public uint Unk17 { get => Fixture.Unk17; set => Fixture.Unk17 = value; }
    }
}
