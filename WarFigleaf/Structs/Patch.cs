﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
  
    [StructLayout(LayoutKind.Sequential)]
    public struct PatchData
    {
        public ushort A01a;
        public ushort A01b;
        public ushort A02a;
        public ushort A02b;
        public ushort A03a;
        public ushort A03b;
        public byte A04aa;
        public byte A04ab;
        public ushort A04b;
    }


    public struct PatchDataView
    {
        public PatchData Data;

        public PatchDataView(PatchData data)
        {
            Data = data;
        }
        public ushort A01a { get => Data.A01a; set => Data.A01a = value; }
        public ushort A01b { get => Data.A01b; set => Data.A01b = value; }
        public ushort A02a { get => Data.A02a; set => Data.A02a = value; }
        public ushort A02b { get => Data.A02b; set => Data.A02b = value; }
        public ushort A03a { get => Data.A03a; set => Data.A03a = value; }
        public ushort A03b { get => Data.A03b; set => Data.A03b = value; }

        public byte A04aa { get => Data.A04aa; set => Data.A04aa = value; }
        public byte A04ab { get => Data.A04ab; set => Data.A04ab = value; }

        public ushort A04b { get => Data.A04b; set => Data.A04b = value; }

        public void Save(BinaryWriter dataWriter)
        {
            dataWriter.Write((UInt16)A01a);
            dataWriter.Write((UInt16)A01b);
            dataWriter.Write((UInt16)A02a);
            dataWriter.Write((UInt16)A02b);
            dataWriter.Write((UInt16)A03a);
            dataWriter.Write((UInt16)A03b);
            dataWriter.Write((byte)A04aa);
            dataWriter.Write((byte)A04ab);
            dataWriter.Write((UInt16)A04b);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Patch
    {
        public ushort Count { get; set; }
        public ushort Unk2 { get; set; }
        public uint DataStart { get; set; }
        public ushort ZoneID { get; set; }
        public byte X { get; set; }
        public byte Y { get; set; }
  

        public override string ToString()
        {
            return ZoneID.ToString();
        }
    }

    public class PatchView
    {
        public Patch Patch;
        public List<PatchDataView> PatchData;
        public PatchView(Patch patch)
        {
            Patch = patch;
        }

        public ushort Count { get => Patch.Count; set => Patch.Count = value; }
        public ushort Unk2 { get => Patch.Unk2; set => Patch.Unk2 = value; }
        public uint DataStart { get => Patch.DataStart; set => Patch.DataStart = value; }
        public ushort ZoneID { get => Patch.ZoneID; set => Patch.ZoneID = value; }
        public byte X { get => Patch.X; set => Patch.X = value; }
        public byte Y { get => Patch.Y; set => Patch.Y = value; }

        public override string ToString()
        {
            return ZoneID.ToString();
        }
    }
}
