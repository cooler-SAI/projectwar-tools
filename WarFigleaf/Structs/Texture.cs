﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
       public enum Format : uint
    {
        UNKNOWN = 0,
        R8G8B8 = 20,
        A8R8G8B8 = 21,
        X8R8G8B8 = 22,
        R5G6B5 = 23,
        X1R5G5B5 = 24,
        A1R5G5B5 = 25,
        A4R4G4B4 = 26,
        R3G3B2 = 27,
        A8 = 28,
        A8R3G3B2 = 29,
        X4R4G4B4 = 30,
        A2B10G10R10 = 31,
        A8B8G8R8 = 32,
        X8B8G8R8 = 33,
        G16R16 = 34,
        A2R10G10B10 = 35,
        A16B16G16R16 = 36,
        L8 = 50,
        A8L8 = 51,
        A4L4 = 52,
        D16_LOCKABLE = 70,
        D32 = 71,
        D24X8 = 77,
        D16 = 80,
        D32F_LOCKABLE = 82,
        L16 = 81,
        R16F = 111,
        G16R16F = 112,
        A16B16G16R16F = 113,
        R32F = 114,
        G32R32F = 115,
        A32B32G32R32F = 116,
        DXT1 = 0x31545844,
        DXT2 = 0x32545844,
        DXT3 = 0x33545844,
        DXT4 = 0x34545844,
        DXT5 = 0x35545844,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Texture
    {
        public uint SourceIndex;
        public uint A02;
        public uint A03;
        public uint Size;
        public ushort Width;
        public ushort Height;
        public Format A07;
        public uint MipMaps;

        public uint A09_A;
        public uint A09_B;
        public uint A10_A;
        public uint A10_B;

        public uint A11;
        public uint A12;
        public uint A13;
        public uint A14;
    }

    public class TextureView
    {
        public Texture Texture;
        public string Name { get; set; }

        public TextureView(Texture texture)
        {
            Texture = texture;
        }

        public uint SourceIndex { get => Texture.SourceIndex; set => Texture.SourceIndex = value; }
        public uint A02 { get => Texture.A02; set => Texture.A02 = value; }
        public uint A03 { get => Texture.A03; set => Texture.A03 = value; }
        public uint Size { get => Texture.Size; set => Texture.Size = value; }
        public ushort Width { get => Texture.Width; set => Texture.Width = value; }
        public ushort Height { get => Texture.Height; set => Texture.Height = value; }
        public Format A07 { get => Texture.A07; set => Texture.A07 = value; }
        public uint MipMaps { get => Texture.MipMaps; set => Texture.MipMaps = value; }
    }
}
