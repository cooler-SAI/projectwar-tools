#pragma once

#define WIN32_LEAN_AND_MEAN

#include <sdkddkver.h>

#include <WinSock2.h>
#include <MSWSock.h>
#include <websocket.h>

#include <Windows.h>

#include <algorithm>
#include <any>
#include <array>
#include <bitset>
#include <chrono>
#include <deque>
#include <exception>
#include <execution>
#include <experimental/filesystem>
#include <filesystem>
#include <forward_list>
#include <fstream>
#include <functional>
#include <future>
#include <initializer_list>
#include <iomanip>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <memory_resource>
#include <mutex>
#include <new>
#include <numeric>
#include <optional>
#include <ostream>
#include <random>
#include <ratio>
#include <regex>
#include <set>
#include <shared_mutex>
#include <sstream>
#include <stack>
#include <string>
#include <string_view>
#include <thread>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include <tlhelp32.h>
#include <ole2.h>
#include <fileapi.h>

#define fclose(_Stream)                                           _fclose_nolock(_Stream)
#define fflush(_Stream)                                           _fflush_nolock(_Stream)
#define fgetc(_Stream)                                            _fgetc_nolock(_Stream)
#define fputc(_Ch, _Stream)                                       _fputc_nolock(_Ch, _Stream)
#define fread(_DstBuf, _ElementSize, _Count, _Stream)             _fread_nolock(_DstBuf, _ElementSize, _Count, _Stream)
#define fread_s(_DstBuf, _DstSize, _ElementSize, _Count, _Stream) _fread_nolock_s(_DstBuf, _DstSize, _ElementSize, _Count, _Stream)
#define fseek(_Stream, _Offset, _Origin)                          _fseek_nolock(_Stream, _Offset, _Origin)
#define _fseeki64(_Stream, _Offset, _Origin)                      _fseeki64_nolock(_Stream, _Offset, _Origin)
#define ftell(_Stream)                                            _ftell_nolock(_Stream)
#define _ftelli64(_Stream)                                        _ftelli64_nolock(_Stream)
#define fwrite(_SrcBuf, _ElementSize, _Count, _Stream)            _fwrite_nolock(_SrcBuf, _ElementSize, _Count, _Stream)
#define getc(_Stream)                                             _getc_nolock(_Stream)
#define putc(_Ch, _Stream)                                        _putc_nolock(_Ch, _Stream)
#define ungetc(_Ch, _Stream)                                      _ungetc_nolock(_Ch, _Stream)

#define rot(x, k) (((x)<<(k)) | ((x)>>(32-(k))))

#define mix(a, b, c) { \
		a -= c; a ^= rot(c,4);  c += b; \
		b -= a; b ^= rot(a,6);  a += c; \
		c -= b; c ^= rot(b,8);  b += a; \
		a -= c; a ^= rot(c,16); c += b; \
		b -= a; b ^= rot(a,19); a += c; \
		c -= b; c ^= rot(b,4);  b += a; }

#define final(a, b, c) { \
		c ^= b; c -= rot(b,14); \
		a ^= c; a -= rot(c,11); \
		b ^= a; b -= rot(a,25); \
		c ^= b; c -= rot(b,16); \
		a ^= c; a -= rot(c,4);  \
		b ^= a; b -= rot(a,14); \
		c ^= b; c -= rot(b,24); }

static auto to_upper = [](std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned int c) {
        return static_cast<char>(std::toupper(c)); }
    );
    return s;
};

enum LogLevel : uint32_t
{
	LogLevel_VERBOSE = 1,
	LogLevel_INFO = 2,
	LogLevel_WARNING = 4,
	LogLevel_ERROR = 8
};

enum LogType : uint32_t
{
	LogType_NETWORK = 0,
	LogType_SYSTEM = 1,
	LogType_PATCHER = 2,
	LogType_Myp = 3,
	LogType_UI = 4,
	LogType_CONSOLE = 5,
	LogType_FTP = 6,
	LogType_HTTP = 7,
};

enum Archive : uint32_t
{
	ARCHIVE_NONE = 0,
	ARCHIVE_MFT = 1,
	ARCHIVE_ART = 2,
	ARCHIVE_ART2 = 3,
	ARCHIVE_ART3 = 4,
	ARCHIVE_AUDIO = 5,
	ARCHIVE_DATA = 6,
	ARCHIVE_WORLD = 7,
	ARCHIVE_INTERFACE = 8,
	ARCHIVE_VIDEO = 9,
	ARCHIVE_BLOODHUNT = 10,
	ARCHIVE_PATCH = 11,
	ARCHIVE_VO_ENGLISH = 12,
	ARCHIVE_VO_FRENCH = 13,
	ARCHIVE_VIDEO_FRENCH = 14,
	ARCHIVE_VO_GERMAN = 15,
	ARCHIVE_VIDEO_GERMAN = 16,
	ARCHIVE_VO_ITALIAN = 17,
	ARCHIVE_VIDEO_ITALIAN = 18,
	ARCHIVE_VO_SPANISH = 19,
	ARCHIVE_VIDEO_SPANISH = 20,
	ARCHIVE_VO_KOREAN = 21,
	ARCHIVE_VIDEO_KOREAN = 22,
	ARCHIVE_VO_CHINESE = 23,
	ARCHIVE_VIDEO_CHINESE = 24,
	ARCHIVE_VO_JAPANESE = 25,
	ARCHIVE_VIDEO_JAPANESE = 26,
	ARCHIVE_VO_RUSSIAN = 27,
	ARCHIVE_VIDEO_RUSSIAN = 28,
	ARCHIVE_WARTEST = 29,
	ARCHIVE_NDA = 30,
	ARCHIVE_DEV = 31
};

enum FigTableType : uint32_t
{
	STRINGS1 = 0,
	STRINGS2 = 1,
	CHARACTER_ART = 2,
	FIGURE_PARTS = 3,
	ART_SWITCHES = 4,
	CHARACTER_DECALS = 5,
	CHARACTER_MESHES = 6,
	TEXTURES = 7,
	UNK_8 = 8,
	FIXTURES = 9,
	UNK_10 = 10,
	LIGHTMAPS = 11,
	REGIONS = 12,
	VAR_RECTS_13 = 13,
	VAR_RECTS_14 = 14,
	VAR_RECTS_15 = 15,
	VAR_RECTS_16 = 16,
};

typedef void(_cdecl* LogCallback)(uint32_t level, std::string message);
typedef void(_cdecl* ProgressCallback)(int32_t index, int32_t total);
