#include "platform.h"

#include "myp.h"
#include "zlib.h"

Myp::Myp(LogCallback logger) {
    _changed = false;
    _archiveHash = 0;
    _archive = ARCHIVE_NONE;
    _header = { 0 };
    _filesize = 0;
    _logger = logger;
    _file = nullptr;
    _loaded = false;
    _tempFile = nullptr;
    _changed = false;
    _totalFileSize = 0;
    _entryCount = 0;
}

Myp::Myp(LogCallback logger, const char* filename) {
    _filename = filename;
    _changed = false;
    _archiveHash = 0;
    _archive = ARCHIVE_NONE;
    _header = { 0 };
    _filesize = 0;
    _logger = logger;
    _file = nullptr;
    _loaded = false;
    _tempFile = nullptr;
    _changed = false;
    _totalFileSize = 0;
    _entryCount = 0;
    _loaded = Load(filename);
}

Myp::Myp(LogCallback logger, const char* filename, bool create) {
    _filename = filename;
    _changed = false;
    _archiveHash = 0;
    _archive = ARCHIVE_NONE;
    _header = { 0 };
    _filesize = 0;
    _logger = logger;
    _file = nullptr;
    _loaded = false;
    _tempFile = nullptr;
    _changed = false;
    _totalFileSize = 0;
    _entryCount = 0;

    if (create)
        Create(filename);
}

void Myp::WriteNewMypHeader() {

    _changed = true;

    _fseeki64(_file, 0, SEEK_SET);

    const unsigned char tag[4] = { 'M','Y','P','\0' };

    uint8_t padding[0x1DC] = { 0 };
    uint32_t data32[1] = { 0 };
    _header.EntriesPerFile = 1000;
    _header.FileOffset1 = 0x200;
    _header.FileOffset2 = 0;

    fwrite(tag, 4, 1, _file);

    data32[0] = 0x05;
    fwrite(data32, 4, 1, _file);

    data32[0] = 0xfd23ec43;
    fwrite(data32, 4, 1, _file);

    data32[0] = 0x200;
    fwrite(data32, 4, 1, _file);

    data32[0] = 0;
    fwrite(data32, 4, 1, _file);

    data32[0] = 1000;
    fwrite(data32, 4, 1, _file);

    data32[0] = 0;
    fwrite(data32, 4, 1, _file);

    data32[0] = 0x5;
    fwrite(data32, 4, 1, _file);

    data32[0] = 0x5;
    fwrite(data32, 4, 1, _file);

    fwrite(padding, 0x1DC, 1, _file);

    FindInsertFreeFileEntry(); //insert frist mft table in linked list
    fflush(_file);
}

bool Myp::MarkUsedBlock(int64_t pos, int64_t length, int type, void* recordPtr) {

    UsedBlock block;
    block.Offset = pos;
    block.Length = length;
    block.Type = type;
    block.RecordPtr = recordPtr;
    _usedBlocks.push_back(block);

    return true;
}

void Myp::CalculateFreeBlocks() {
    _freeBlocks.clear();

    std::sort(_usedBlocks.begin(), _usedBlocks.end(), [](const UsedBlock & b1, const UsedBlock & b2) {
        return (b1.Offset < b2.Offset);
        });

    OutputDebugStringA(std::string("Calculating free blocks " + std::to_string(_usedBlocks.size())).c_str());

    for (size_t i = 0; i < _usedBlocks.size() - 1; i++) {
        UsedBlock& b1 = _usedBlocks[i];
        UsedBlock& b2 = _usedBlocks[i + static_cast<size_t>(1)];
        UsedBlock b3;

        b3.Offset = b1.Offset + b1.Length;
        b3.Length = b2.Offset - b3.Offset;

        b3.Type = BLOCK_FREE;
        b3.RecordPtr = nullptr;

        if (b3.Length > 0) {
            _freeBlocks.push_back(b3);
        }
    }

    //check if last block takes up to end of file
    if (_usedBlocks.size()) {
        UsedBlock b3;
        UsedBlock& lastBlock = _usedBlocks[_usedBlocks.size() - 1];

        b3.Offset = lastBlock.Offset + lastBlock.Length;
        b3.Length = _filesize - b3.Offset;
        b3.Type = BLOCK_FREE;
        b3.RecordPtr = nullptr;

        if (b3.Length > 0) {
            _freeBlocks.push_back(b3);
        }
    }

    std::sort(_freeBlocks.begin(), _freeBlocks.end(), [](const UsedBlock & b1, const UsedBlock & b2) {
        return (b1.Length < b2.Length);
        });
}

bool Myp::LoadMyp() {
    _entryCount = 0;

    OutputDebugStringA(std::string("Loading " + _filename + " ...").c_str());

    _filesize = std::filesystem::file_size(_filename);
    _changed = false;
    memset(&_header, 0, sizeof(MypHeader));

    //_tempMypName = std::string(std::tmpnam_s(NUL  ()) + std::string(".Myp");

   // _tempMypName.reserve(32);
   // _tempMypName.resize(32);

    //char buf[64];
  //  errno_t err = tmpnam_s(buf, 8);


    Open();

    if (fread(&_header, sizeof(MypHeader), 1, _file) != 1) {
        OutputDebugStringA(std::string("Unable to read header in " + _filename + " file").c_str());
        return false;
    }

    MarkUsedBlock(0, _ftelli64(_file), BLOCK_MYP_HEADER, &_header);

    if (_header.FileTableHeader[0] != 'M' || _header.FileTableHeader[1] != 'Y' || _header.FileTableHeader[2] != 'P') {
        OutputDebugStringA(std::string("Malfromed header in " + _filename + " file.").c_str());
        return false;
    }

    //FileTableHeader* mftHeader = new FileTableHeader();
    //memset(mftHeader, 0, sizeof(FileTableHeader));

    //mftHeader->FileCount = _header.EntriesPerFile;
    //mftHeader->NextFileOffset1 = _header.FileOffset1;
    //mftHeader->NextFileOffset2 = _header.FileOffset2;

    int64_t mftOffset = (int64_t)((int64_t)_header.FileOffset2 << 32 | (int64_t)_header.FileOffset1);
    //uint32_t mftOffset1 = _header.FileOffset1;
    //uint32_t mftOffset2 = _header.FileOffset2;

    int page = 0;

    while (1) {
        _fseeki64(_file, 0, SEEK_SET);
        if (_fseeki64(_file, mftOffset, SEEK_SET)) {
            OutputDebugStringA(std::string("FileTable Header offset " + std::to_string(mftOffset) + " out of range.").c_str());
            return false;
        }

        FileTableHeader* mftHeader2 = new FileTableHeader();

        if (fread(mftHeader2, sizeof(FileTableHeader), 1, _file) != 1) {
            OutputDebugStringA(std::string(std::string("Failed to read file table header.")).c_str());
            return false;
        }

        _fileHeaderToc[mftHeader2] = _ftelli64(_file);

        if (mftHeader2->FileCount > _header.EntriesPerFile) {
            OutputDebugStringA(std::string("FileTable Header asset count (" + std::to_string(mftHeader2->FileCount) + ") does not match (" + std::to_string(_header.EntriesPerFile) + ") in the MasterFileTable Header.").c_str());
            return false;
        }

        _fileTables.push_back(mftHeader2);

        auto pos = _ftelli64(_file);

        //read file entries as a block	
        //allocate 1000 
        auto size = sizeof(FileEntry) * mftHeader2->FileCount;
        void* mftBlocks = malloc(size);
        memset(mftBlocks, 0, size);
        _fileBlockMemory.push_back(mftBlocks);

        if (fread(mftBlocks, sizeof(FileEntry), mftHeader2->FileCount, _file) != mftHeader2->FileCount) {
            OutputDebugStringA(std::string("FileTable Header offset " + std::to_string(mftOffset) + " out of range.").c_str());
            OutputDebugStringA(std::string(std::string("Failed to read file table entry block.")).c_str());
            return false;
        }

        //add mft data block usage
        MarkUsedBlock(_fileHeaderToc[mftHeader2], _ftelli64(_file) - _fileHeaderToc[mftHeader2], BLOCK_MFT_ENTRY, mftHeader2);

        for (int i = 0; i < (int)mftHeader2->FileCount; i++) {
            int ptr = i * sizeof(FileEntry);
            FileEntry* entry = ((FileEntry*)mftBlocks + i);
            int64_t hash = (int64_t)entry->Hash2 << 32 | entry->Hash1;
            _fileEntries.push_back(entry);
            _fileEntryToc[entry] = pos + ptr; //file location of file entry

            int64_t entryOffset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

            if (entryOffset < 0 || entryOffset > _filesize) {
                OutputDebugStringA(std::string(std::string("Corrupt asset. Offset:" + std::to_string(entryOffset) + " " + std::to_string(hash))).c_str());
            }
            if (entryOffset != 0) {
                MarkUsedBlock(entryOffset, static_cast<int64_t>(entry->CompressedSize) + static_cast<int64_t>(entry->HeaderSize), BLOCK_ASSET_ENTRY, entry);
            }

            if (hash != 0) {
                if (entry->Offset1 == 0 && entry->Offset2 == 0) {
                    OutputDebugStringA(std::string(std::string("Corrupt asset." + std::to_string(hash))).c_str());
                }
                _fileEntriesHash[hash] = entry;
                _totalFileSize += entry->UnCompressedSize;
                _entryCount++;
            }
        }

        mftOffset = (int64_t)mftHeader2->NextFileOffset2 << 32 | mftHeader2->NextFileOffset1;
        page++;
        if (mftOffset == 0) {
            break;
        }
    }
    CalculateFreeBlocks();

    _loaded = true;

    return true;
}

uint32_t Myp::getHashHash() {
    uint32_t hashash = 0;
    std::vector<uint32_t> hashes;

    for (auto& asset : _fileEntries) {
        if (asset->Offset1 != 0 || asset->Offset2 != 0) {
            hashes.push_back(asset->CRC32);
            hashes.push_back(asset->UnCompressedSize);
        }
    }
    if (hashes.size()) {
        std::sort(hashes.begin(), hashes.end());
        for (int i = 0; i < (int)hashes.size(); i++) {
            hashash = adler32(hashash, (const uint8_t*)& hashes[i], 4);
            //cout << "CRC32: " << hashes[i] << " HH:" << hashash << endl;
        }
    }
    return hashash;
}

int64_t Myp::HashWARv1(const char* s) {
    uint32_t edx = 0, eax, esi, ebx = 0, ph = 0, sh = 0;
    uint32_t edi, ecx;
    static uint32_t seed = 0xDEADBEEF;
    int len = (uint32_t)std::strlen(s);

    eax = ecx = edx = ebx = esi = edi = 0;
    ebx = edi = esi = (uint32_t)std::strlen((s)+seed);

    int i = 0;

    for (i = 0; i + 12 < len; i += 12) {
        edi = (uint32_t)((s[i + 7] << 24) | (s[i + 6] << 16) | (s[i + 5] << 8) | s[i + 4]) + edi;
        esi = (uint32_t)((s[i + 11] << 24) | (s[i + 10] << 16) | (s[i + 9] << 8) | s[i + 8]) + esi;
        edx = (uint32_t)((s[i + 3] << 24) | (s[i + 2] << 16) | (s[i + 1] << 8) | s[i]) - esi;

        edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
        esi += edi;
        edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
        edx += esi;
        esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
        edi += edx;
        ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
        esi += edi;
        edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
        ebx += esi;
        esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
        edi += ebx;
    }

    if (len - i > 0) {
        switch (len - i) {
        case 12:
            esi += (uint32_t)s[i + 11] << 24;
        case 11:
            esi += (uint32_t)s[i + 10] << 16;
        case 10:
            esi += (uint32_t)s[i + 9] << 8;
        case 9:
            esi += (uint32_t)s[i + 8];
        case 8:
            edi += (uint32_t)s[i + 7] << 24;
        case 7:
            edi += (uint32_t)s[i + 6] << 16;
        case 6:
            edi += (uint32_t)s[i + 5] << 8;
        case 5:
            edi += (uint32_t)s[i + 4];
        case 4:
            ebx += (uint32_t)s[i + 3] << 24;
        case 3:
            ebx += (uint32_t)s[i + 2] << 16;
        case 2:
            ebx += (uint32_t)s[i + 1] << 8;
        case 1:
            ebx += (uint32_t)s[i];
            break;
        }

        esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
        ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
        edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
        esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
        edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
        edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
        eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

        ph = edi;
        sh = eax;
        return ((int64_t)ph << 32) + sh;
    }
    ph = esi;
    sh = eax;
    return ((int64_t)ph << 32) + sh;
}

int64_t Myp::HashWARv3b(const void* key, size_t length) {
    uint32_t a;
    uint32_t b;
    uint32_t c = b = a = 0xdeadbeef + ((uint32_t)length);				/* Set up the internal state */

    uint32_t* k = (uint32_t*)key;										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
    while (length > 12) {
        a += k[0]; b += k[1]; c += k[2];
        mix(a, b, c);
        length -= 12;
        k += 3;
    }

    switch (length) {													/* handle the last (probably partial) block */
    case 12: { c += k[2]; b += k[1]; a += k[0]; break; }
    case 11: { c += k[2] & 0xffffff; b += k[1]; a += k[0]; break; }
    case 10: { c += k[2] & 0xffff; b += k[1]; a += k[0]; break; }
    case 9: { c += k[2] & 0xff; b += k[1]; a += k[0]; break; }
    case 8: { b += k[1]; a += k[0]; break; }
    case 7: { b += k[1] & 0xffffff; a += k[0]; break; }
    case 6: { b += k[1] & 0xffff; a += k[0]; break; }
    case 5: { b += k[1] & 0xff; a += k[0]; break; }
    case 4: { a += k[0]; break; }
    case 3: { a += k[0] & 0xffffff; break; }
    case 2: { a += k[0] & 0xffff; break; }
    case 1: { a += k[0] & 0xff; break; }
    case 0: { return (((int64_t)b) << 32) | c; }						/* zero length strings require no mixing */
    }

    final(a, b, c);
    return (((int64_t)b) << 32) | c;
}

int64_t Myp::HashWARv4(std::string s) {
    size_t length = s.length();

    uint32_t a;
    uint32_t b;
    uint32_t c = b = a = 0xdeadbeef + ((uint32_t)length);				/* Set up the internal state */

    uint32_t* k = (uint32_t*)s.c_str();										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
    while (length > 12) {
        a += k[0]; b += k[1]; c += k[2];
        mix(a, b, c);
        length -= 12;
        k += 3;
    }

    switch (length) {													/* handle the last (probably partial) block */
    case 12: { c += k[2]; b += k[1]; a += k[0]; break; }
    case 11: { c += k[2] & 0xffffff; b += k[1]; a += k[0]; break; }
    case 10: { c += k[2] & 0xffff; b += k[1]; a += k[0]; break; }
    case 9: { c += k[2] & 0xff; b += k[1]; a += k[0]; break; }
    case 8: { b += k[1]; a += k[0]; break; }
    case 7: { b += k[1] & 0xffffff; a += k[0]; break; }
    case 6: { b += k[1] & 0xffff; a += k[0]; break; }
    case 5: { b += k[1] & 0xff; a += k[0]; break; }
    case 4: { a += k[0]; break; }
    case 3: { a += k[0] & 0xffffff; break; }
    case 2: { a += k[0] & 0xffff; break; }
    case 1: { a += k[0] & 0xff; break; }
    case 0: { return (((int64_t)b) << 32) | c; }						/* zero length strings require no mixing */
    }

    final(a, b, c);
    return (((int64_t)b) << 32) | c;
}

void Myp::Open() {
    if (!_file)
        if (0 != fopen_s(&_file, _filename.c_str(), "rb+"))
            return;

    _filesize = std::filesystem::file_size(_filename);

    if (!_file)
        return;
}

bool Myp::Create(std::string filename) {

    auto folder = std::filesystem::path(filename).make_preferred().remove_filename();

    if (!std::filesystem::exists(folder))
        std::filesystem::create_directories(folder);

    if (!std::filesystem::exists(filename)) {
        std::ofstream new_file(filename, std::ofstream::binary | std::ofstream::trunc, 0x80);
        if (new_file.good()) {

            static const MypHeader header = MypHeader{ { 'M', 'Y', 'P', '\0' }, 0x5ul, 0xfd23ec43ul, 0x200ul, 0x0ul, 0x3E8ul, 0x0ul, 0x5ul, 0x5, { '\0' } };
            //reinterpret_cast<uint32_t*>((uint8_t*)&header)[0] = 0x0050594ul;

            try {
                new_file.seekp(std::ofstream::beg);
                new_file.write(reinterpret_cast<const char*>(&header), sizeof(MypHeader));

                FileTableHeader ftHeader = { header.FileCount, 0, 0 };

                new_file.write(reinterpret_cast<const char*>(&ftHeader), sizeof(FileTableHeader));

                FileEntry ftEntry = { 0, 0, 0, 0, 0, 0, 0, 0 };

                for (uint32_t i = 0; i < header.EntriesPerFile; i++) {
                    new_file.write(reinterpret_cast<const char*>(&ftEntry), sizeof(FileEntry));
                }
            }
            catch (std::fstream::failure & e) {
                OutputDebugStringA(std::string("Myp Create '" + filename + "' : " + e.what()).c_str());
                return false;
            }
            catch (std::system_error & e) {
                OutputDebugStringA(std::string("Myp Create '" + filename + "' : " + e.what()).c_str());
                return false;
            }
            catch (std::runtime_error & e) {
                OutputDebugStringA(std::string("Myp Create '" + filename + "' : " + e.what()).c_str());
                return false;
            }

            new_file.flush();
            new_file.close();

            return true;
        }
    }
    return false;
}

bool Myp::Load(std::string filename) {

    if (!std::filesystem::exists(filename)) {
        OutputDebugStringA(std::string("Myp archive '" + filename + "' does not exist").c_str());
        return false;
    }

    _filename = std::filesystem::path(filename, std::filesystem::path::generic_format).string();

    Open();

    if (!_file) {
        OutputDebugStringA(std::string("Error opening Myp '" + std::string(filename) + "'").c_str());
        Close();
        return false;
    }

    if (!LoadMyp()) {
        Close();
        return false;
    }

    return true;
}

bool Myp::HasAsset(int64_t hash) {
    return GetAsset(hash) != nullptr;
}

bool Myp::HasAsset(const char* name) {
    return GetAsset(name) != nullptr;
}

FileEntry* Myp::GetAsset(const char* name) {
    if (!_loaded) {
        OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
        return nullptr;
    }

    auto it = _fileEntriesHash.find(HashWARv1(name));

    if (it != _fileEntriesHash.end())
        return it->second;

    return nullptr;
}

const std::map<int64_t, FileEntry*>& Myp::GetAssets() {
    if (!_loaded)
        LoadMyp();

    return _fileEntriesHash;
}

FileEntry* Myp::GetAsset(int64_t hash) {
    if (!_loaded) {
        OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
        return nullptr;
    }

    auto it = _fileEntriesHash.find(hash);
    if (it != _fileEntriesHash.end()) {
        return _fileEntriesHash[hash];
    }
    return nullptr;
}

bool Myp::UpdateAsset(const char* name, const uint8_t * data, uint32_t dataLength, bool compressed, uint8_t * headerData, uint32_t headerDataLength, uint32_t overrideCrc32) {
    if (!_loaded) {
        OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
        return false;
    }

    _changed = true;
    auto hash = HashWARv1(name);

    return UpdateAsset(hash, data, dataLength, compressed, headerData, headerDataLength, overrideCrc32);
}

bool Myp::UpdateAsset(const int64_t hash, const uint8_t * data, uint32_t dataLength, bool compressed, uint8_t * headerData, uint32_t headerDataLength, uint32_t overrideCrc32) {
    if (!_loaded) {
        OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
        return false;
    }

    FileEntryChange entry;
    entry.Compressed = compressed ? 0 : 1;
    entry.Hash1 = hash & 0xFFFFFFFF;
    entry.Hash2 = hash >> 32;
    entry.crc32 = overrideCrc32;

    entry.DataLength = dataLength;

    if (headerData != 0) {
        entry.HeaderData = headerData;
        entry.HeaderDataLength = headerDataLength;
    }
    else {
        const uint8_t _defaultMetaData[] = { 0x04,0x00,0x84,0x00,0x00,0x02,0x00,0x80,0x3C,0x53,0x2D,0x71,0xDC,0x0E,0xE0,0x94,0x70,0xC1,0x12,0x2B,
            0x29,0xA1,0xFA,0x02,0x7F,0x9C,0x2D,0x51,0xA3,0x89,0x0E,0xFC,0x1B,0x3C,0x42,0xA5,0x18,0x98,0x93,0xA7,0xC4,0xD7,0xCF,0xE1,0x4B,0x1A,0x4B,
            0x41,0x3B,0x35,0x14,0xE8,0x45,0x98,0x70,0x77,0xB2,0x9E,0x0C,0x98,0xDE,0x0A,0xE6,0x88,0xF2,0x1F,0x39,0x01,0x47,0xB1,0x93,0x77,0xF8,0xCD,
            0x65,0x6B,0xDB,0xDB,0xF0,0xB6,0x02,0x90,0x6A,0xDE,0x18,0xAE,0x2E,0x4A,0xFD,0x2F,0x38,0x36,0xA7,0x0E,0x13,0x09,0x4E,0xAF,0x7C,0x87,0xAF,
            0x99,0xDF,0x1E,0x41,0xCA,0x79,0x1E,0x14,0x11,0xB0,0xB7,0x13,0x5C,0x78,0xC7,0xE5,0x8A,0x82,0xC6,0xC9,0x5E,0x51,0xD0,0x9E,0xE7,0xD6,0x58,
            0xF1,0xBD,0xBA,0xC6,0x73,0xB5,0x0E,0xF4 };

        const uint32_t _defaultMetaDataSize = 0x88;

        entry.HeaderData = _defaultMetaData;
        entry.HeaderDataLength = _defaultMetaDataSize;
    }

    if (nullptr == _tempFile) {
        errno_t err = tmpfile_s(&_tempFile);
        if (err)
            return false;
    }

    entry._tempFilePos = _ftelli64(_tempFile);
    if (entry.HeaderDataLength)
        fwrite(entry.HeaderData, entry.HeaderDataLength, 1, _tempFile);

    fwrite(data, dataLength, 1, _tempFile);
    _changed = true;

    auto existing = GetAsset(hash);
    if (existing == nullptr) {
        OutputDebugStringA(std::string("Inserting asset [" + std::to_string(hash) + "] " + std::to_string(dataLength) + " bytes" + (compressed ? "." : std::to_string(compressed)) + " compressed.").c_str());
        _inserted[hash] = entry;
    }
    else {
        OutputDebugStringA(std::string("Updating asset " + std::to_string(hash) + ", " + std::to_string(dataLength) + " bytes" + (compressed ? "." : std::to_string(compressed)) + " compressed.").c_str());
        _updated[hash] = entry;
    }

    return true;
}

void Myp::LoadFrom(Myp * Myp) {
    for (int i = 0; i < (int)Myp->_fileEntries.size(); i++) {
        FileEntry* entry = Myp->_fileEntries[i];

        if (entry->Hash1 != 0 || entry->Hash2 != 0) {
            int64_t hash = (int64_t)entry->Hash2 << 32 | entry->Hash1;

            uint8_t* assetData = new uint8_t[entry->UnCompressedSize];
            Myp->GetAssetData(entry, assetData);
            UpdateAsset(hash, assetData, entry->UnCompressedSize, entry->Compressed);

        }

    }
    Save();
}

std::map<int64_t, std::string> Myp::GetDeHash(const char* dehash) {
    UNREFERENCED_PARAMETER(dehash);
    /*   int count = 0;
       std::map<int64_t, std::string> _dehash;
       if (_filesize = std::filesystem::file_size(dehash) > 0) {
           if (dehash) {
               FILE* file = nullptr;
               errno_t err = fopen_s(&file, dehash, "r");
               if (err) {
                   return _dehash;
               }
               int len = 8000;
               char line[8000];
               char* tok;

               while (fgets(line, len, file) != 0) {
                   if (line[std::strlen((line) - 1] == '\n') {
                       line[std::strlen((line) - 1] = '\0';
                   }
                   tok = strtok_s(line, "#", &tok);
                   int index = 0;
                   int64_t high = 0;
                   int64_t hash = 0;

                   while (tok != 0) {
                       if (index == 0)
                           high = (int64_t)_strtoui64(tok, nullptr, 16) << (int64_t)32;
                       else if (index == 1)
                           hash = high | _strtoui64(tok, nullptr, 16);
                       else if (index == 2) {
                           hash = Myp::HashWARv1(tok);
                           _dehash[Myp::HashWARv1(tok)] = std::string(tok);
                           break;
                       }
                       tok = strtok_s(nullptr, "#", &tok);

                       if (count == 202070) {
                           index = index;
                       }
                       index++;
                   }
                   count++;
               }

           }
           auto f = _dehash[0xe0f506f382b99902];
       }
       return _dehash;*/
}

FileEntry* Myp::FindInsertFreeFileEntry() {
    for (int i = 0; i < (int)_fileEntries.size(); i++) {
        if (_fileEntries[i]->Hash1 == 0 && _fileEntries[i]->Hash2 == 0)
            return _fileEntries[i];
    }
    //auto pos = _ftelli64(_file);

    //update last file entry to point to new table
    if (_fileTables.size() > 0) {
        FileTableHeader* lastHeader = _fileTables[_fileTables.size() - 1];
        _filesize = std::filesystem::file_size(_filename);
        lastHeader->NextFileOffset1 = (uint32_t)(_filesize);
        lastHeader->NextFileOffset2 = (_filesize >> 32);

        auto tocLoc = _fileHeaderToc[lastHeader];

        //jump to file header
        _fseeki64(_file, tocLoc, SEEK_SET);

        fwrite(lastHeader, sizeof(FileTableHeader), 1, _file);
    }
    else {
        //no tables, create first entry
        _fseeki64(_file, 0x200, SEEK_SET);
    }

    _fseeki64(_file, 0, SEEK_END);

    //auto pos1 = _ftelli64(_file);
    //create new file header entry
    FileTableHeader* newHeader = new FileTableHeader();
    newHeader->FileCount = _header.FileCount;
    newHeader->NextFileOffset1 = 0;
    newHeader->NextFileOffset2 = 0;

    _fileTables.push_back(newHeader);
    _fileHeaderToc[newHeader] = _ftelli64(_file);
    fwrite(newHeader, sizeof(FileTableHeader), 1, _file);

    FileEntry* entry = nullptr;
    FileEntry* first = nullptr;
    int64_t mft_start_pos = _ftelli64(_file);

    //write empty file entries
    for (int i = 0; i < (int)_header.EntriesPerFile; i++) {
        entry = new FileEntry();
        memset(entry, 0, sizeof(FileEntry));

        if (i == 0)
            first = entry;

        int ptr = i * sizeof(FileEntry);

        _fileEntryToc[entry] = mft_start_pos + ptr; //mftEntry->mftEntryHeader
        fwrite(&entry, sizeof(FileEntry), 1, _file);

        _fileEntries.push_back(entry);
        _insertedFileEntries.push_back(entry);
    }
    return first;
}

bool Myp::FreeBlock(int64_t offset, int64_t size) {
    for (int i = 0; i < (int)_usedBlocks.size(); i++) {
        if (_usedBlocks[i].Offset == offset) {
            if (_usedBlocks[i].Length == size) {
                _usedBlocks.erase(_usedBlocks.begin() + i, _usedBlocks.begin() + i + 1);

                //reorder free blocks TODO: replace with heap
                std::sort(_usedBlocks.begin(), _usedBlocks.end(), [](const UsedBlock & b1, const UsedBlock & b2) {
                    return (b1.Offset < b2.Offset);
                    });
            }
            return true;
        }
    }
    return false;
}

void Myp::Delete(FileEntry * entry) {
    Open();
    int64_t hash = (int64_t)entry->Hash2 << 32 | entry->Hash1;

    if (_fileEntriesHash.find(hash) != _fileEntriesHash.end()) {
        int64_t mftEntryLoc = _fileEntryToc[entry];
        int64_t offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

        FreeBlock(offset, static_cast<int64_t>(entry->CompressedSize) + static_cast<int64_t>(entry->HeaderSize));
        _fseeki64(_file, mftEntryLoc, SEEK_SET);

        memset(entry, 0, sizeof(FileEntry));

        fwrite(entry, sizeof(FileEntry), 1, _file);

        //_fileEntriesHash.erase(hash);

        _header.FileCount--;

        _fseeki64(_file, 0x18, SEEK_SET);
        fwrite(&_header.FileCount, sizeof(uint32_t), 1, _file);

        fflush(_file);
    }
}

UsedBlock Myp::GetFreeBlock(int64_t size) {
    int index = -1;

    for (int i = 0; i < (int)_freeBlocks.size(); i++) {
        if ((int64_t)_freeBlocks[i].Length >= size) {
            index = i;
            break;
        }
    }
    if (index > -1) {
        UsedBlock found = _freeBlocks[index];
        _freeBlocks.erase(_freeBlocks.begin() + index, _freeBlocks.begin() + index + 1);

        //insert back remainder free space
        if ((int64_t)found.Length > size) {
            UsedBlock block;
            block.Offset = found.Offset + size;
            block.Length = found.Length - size;
            _freeBlocks.push_back(block);
        }
        found.Length = size;

        //reorder free blocks TODO: replace with heap
        std::sort(_freeBlocks.begin(), _freeBlocks.end(), [](const UsedBlock & b1, const UsedBlock & b2) {
            return (b1.Length < b2.Length);
            });

        //verify block is free
        for (int i = 0; i < (int)_usedBlocks.size(); i++) {
            auto& block = _usedBlocks[i];
            if (found.Offset >= block.Offset && found.Offset <= block.Offset + block.Length && (found.Offset + found.Length) >= block.Offset && (found.Offset + found.Length) <= block.Offset + block.Length) {
                OutputDebugStringA(std::string("Invalid free block").c_str());
            }
        }

        return found;
    }

    UsedBlock block;
    block.Offset = 0;
    block.Length = 0;
    return block;
}

bool Myp::Save() {
    Open();

    if (!_loaded)
        Load();

    if (!_changed)
        return true;

    OutputDebugStringA(std::string("Flushing " + std::to_string(_updated.size()) + " bytes to disk.").c_str());
    fflush(_tempFile);

    //write out all updates
    for (auto iterator = _updated.begin(); iterator != _updated.end(); iterator++) {
        FileEntry* entry = _fileEntriesHash[iterator->first];
        size_t mftEntryLoc = _fileEntryToc[entry];
        size_t compressedLen = iterator->second.DataLength;
        uint8_t* compressedData = nullptr;

        _filesize = std::filesystem::file_size(_filename);

        int result = 0;

        size_t len = static_cast<uint64_t>(iterator->second.DataLength) + static_cast<uint64_t>(iterator->second.HeaderDataLength);

        uint8_t* data = new uint8_t[len];
        _fseeki64(_tempFile, iterator->second._tempFilePos, SEEK_SET);
        fread(data, len, 1, _tempFile);

        if (iterator->second.Compressed) {
            compressedData = new uint8_t[static_cast<uint64_t>(iterator->second.DataLength) + static_cast<uint64_t>(512)];
            compressedLen = iterator->second.DataLength + 512;

            result = compress2(static_cast<Bytef*>(compressedData), reinterpret_cast<uLongf*>(&compressedLen), data + iterator->second.HeaderDataLength, static_cast<uLong>(iterator->second.DataLength), -1);
            entry->Compressed = 1;

            if (result != 0) {
                OutputDebugStringA(std::string("Error compressing asset.").c_str());
            }
        }
        else {
            compressedData = data + iterator->second.HeaderDataLength;
            entry->Compressed = 0;
        }

        //move to file entry header to update contents location
        _fseeki64(_file, mftEntryLoc, SEEK_SET);

        int64_t offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

        //file is bigger then original record, write file contents ptr to point at end of file, also if file is last file or free block, overwrite data, othewrise append
        if ((iterator->second.HeaderDataLength + iterator->second.DataLength) > (static_cast<uint64_t>(entry->HeaderSize) + static_cast<uint64_t>(entry->CompressedSize))) {
            UsedBlock freeBlock = GetFreeBlock(static_cast<int64_t>(iterator->second.HeaderDataLength) + static_cast<int64_t>(compressedLen));
            if (freeBlock.Length > 0) {
                entry->Offset1 = (uint32_t)(freeBlock.Offset);
                entry->Offset2 = (uint32_t)(freeBlock.Offset >> 32);
            }
            else {
                entry->Offset1 = (uint32_t)(_filesize);
                entry->Offset2 = (uint32_t)(_filesize >> 32);
            }
        }

        // offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

        entry->CompressedSize = static_cast<uint32_t>(compressedLen);
        entry->UnCompressedSize = static_cast<uint32_t>(iterator->second.DataLength);
        if (iterator->second.crc32 != 0)
            entry->CRC32 = iterator->second.crc32;
        else
            entry->CRC32 = adler32(0, data + iterator->second.HeaderDataLength, static_cast<uInt>(iterator->second.DataLength));
        entry->HeaderSize = static_cast<uint32_t>(iterator->second.HeaderDataLength);

        //	entry->HeaderSize = iterator->second.HeaderDataLength;


        fwrite(entry, sizeof(entry), 1, _file);

        /*fwrite(&offset, sizeof(int64_t), 1, _file);
        fwrite(&entry->HeaderSize, sizeof(uint32_t), 1, _file);
        fwrite(&entry->CompressedSize, sizeof(uint32_t), 1, _file);
        fwrite(&entry->UnCompressedSize, sizeof(uint32_t), 1, _file);
        fwrite(&entry->Hash1, sizeof(uint32_t), 1, _file);
        fwrite(&entry->Hash2, sizeof(uint32_t), 1, _file);
        fwrite(&entry->CRC32, sizeof(uint32_t), 1, _file);
        fwrite(&entry->Compressed, sizeof(uint8_t), 1, _file);
*/
//jump to file contents, skip header, write compressed/uncompressed data
        _fseeki64(_file, offset, SEEK_SET);
        fwrite(data, entry->HeaderSize, 1, _file);

        //auto pos1 = _ftelli64(_file);
        fwrite(compressedData, compressedLen, 1, _file);
        //		auto pos2 = _ftelli64(_file);

        if (iterator->second.Compressed) {
            delete[] compressedData;
        }

        delete[] data;

        //update file size
        _filesize = std::filesystem::file_size(_filename);
    }

    //write out all new records
    std::vector<FileEntryChange> newEntries;

    for (auto iterator = _inserted.begin(); iterator != _inserted.end(); iterator++)
        newEntries.push_back(iterator->second);

    //	int count = newEntries.size();
    size_t mftFileCount = 0;
    std::vector<FileEntryChange> toPack;
    int tableIndex = 0;
    while (newEntries.size() > 0) {
        OutputDebugStringA(std::string("Packing file table[" + std::to_string(tableIndex) + "]").c_str());
        tableIndex++;
        mftFileCount = newEntries.size();

        if (mftFileCount > static_cast<size_t>(_header.EntriesPerFile))
            mftFileCount = static_cast<size_t>(_header.EntriesPerFile);

        _header.FileCount += static_cast<uint32_t>(mftFileCount);


        int i = 0;
        for (i = 0; i < mftFileCount; i++) //write up to 1000 entries per file
        {
            FileEntryChange chng = newEntries[i];
            FileEntry* entry = FindInsertFreeFileEntry(); //find empty entry or insert new one
            entry->Offset1 = 0;
            entry->Offset2 = 0;
            entry->HeaderSize = static_cast<uint32_t>(chng.HeaderDataLength);
            entry->Hash1 = chng.Hash1;
            entry->Hash2 = chng.Hash2;
            entry->Compressed = chng.Compressed;
            entry->Unk1 = 0;

            int64_t hash = (int64_t)chng.Hash2 << 32 | chng.Hash1;
            _fileEntriesHash[hash] = entry;

            chng.Entry = entry;
            //			auto toc = _fileEntryToc[entry];
            _fseeki64(_file, _fileEntryToc[entry], SEEK_SET); //jump to new entry's mftentry location file

            fwrite(entry, sizeof(entry), 1, _file);
            //fwrite(&entry->Offset2, sizeof(uint32_t), 1, _file);
            //fwrite(&entry->HeaderSize, sizeof(uint32_t), 1, _file);
            //fwrite(&entry->CompressedSize, sizeof(uint32_t), 1, _file);
            //fwrite(&entry->HeaderSize, sizeof(uint32_t), 1, _file);
            //fwrite(&entry->Hash1, sizeof(uint32_t), 1, _file);
            //fwrite(&entry->Hash2, sizeof(uint32_t), 1, _file);
            //fwrite(&entry->CRC32, sizeof(uint32_t), 1, _file);
           // fwrite(&chng.Compressed, sizeof(uint8_t), 1, _file);

            toPack.push_back(chng);
        }
        newEntries.erase(newEntries.begin(), newEntries.begin() + mftFileCount);
    }

    fflush(_file);
    for (int i = 0; i < (int)toPack.size(); i++) {
        FileEntryChange change = toPack[i];
        _filesize = std::filesystem::file_size(_filename);

        uint8_t* compressedData = nullptr;
        uLong compressedLen = static_cast<uLong>(change.DataLength);


        uint8_t* data = new uint8_t[static_cast<uint64_t>(change.DataLength) + static_cast<uint64_t>(change.HeaderDataLength)];
        memset(data, 0, change.DataLength + change.HeaderDataLength);

        _fseeki64(_tempFile, change._tempFilePos, SEEK_SET);
        fread(data, change.DataLength + change.HeaderDataLength, 1, _tempFile);


        if (change.Compressed) {
            compressedData = new uint8_t[change.DataLength + static_cast<uint32_t>(512)];
            compressedLen = static_cast<uLong>(change.DataLength + static_cast<size_t>(512));

            compress2(compressedData, &compressedLen, data + change.HeaderDataLength, static_cast<uLong>(change.DataLength), -1);
            change.Entry->Compressed = 1;
        }
        else {
            compressedData = (uint8_t*)data + change.HeaderDataLength;
            change.Entry->Compressed = 0;
        }

        change.Entry->CompressedSize = compressedLen;
        change.Entry->UnCompressedSize = static_cast<uint32_t>(change.DataLength);

        UsedBlock freeBlock = GetFreeBlock(static_cast<int64_t>(change.Entry->HeaderSize) + static_cast<int64_t>(change.Entry->CompressedSize));
        if (freeBlock.Length > 0) {
            change.Entry->Offset1 = (uint32_t)(freeBlock.Offset);
            change.Entry->Offset2 = (uint32_t)(freeBlock.Offset >> 32);
        }
        else {
            change.Entry->Offset1 = (uint32_t)(_filesize);
            change.Entry->Offset2 = (uint32_t)(_filesize >> 32);
            freeBlock.Offset = _filesize;
        }

        if (change.crc32 != 0)
            change.Entry->CRC32 = change.crc32;
        else
            change.Entry->CRC32 = static_cast<uint32_t>(adler32(0, data + change.HeaderDataLength, static_cast<uInt>(change.DataLength)));

        change.Entry->HeaderSize = static_cast<uint32_t>(change.HeaderDataLength);

        //		auto toc = _fileEntryToc[chng.Entry];
                //jump back to file entry, and update offset where file data begins
        _fseeki64(_file, _fileEntryToc[change.Entry], SEEK_SET);

        fwrite(change.Entry, sizeof(FileEntry), 1, _file);

        //jump to freeblock area and write file contents
        _fseeki64(_file, freeBlock.Offset, SEEK_SET);

        if (change.HeaderDataLength > 0)
            fwrite(data, change.HeaderDataLength, 1, _file);

        fwrite(compressedData, compressedLen, 1, _file);
        if (change.Compressed) {
            delete[] compressedData;
        }

        delete[] data;
    }

    _fseeki64(_file, 0x18, SEEK_SET);
    fwrite(&_header.FileCount, sizeof(uint32_t), 1, _file);

    fflush(_file);
    Close();
    Load(_filename.c_str());
    return true;
}

//uint32_t Myp::GetFileSize(const char* name)
//{
//	if (!_loaded)
//	{
//		OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
//		return nullptr;
//	}
//
//	FileEntry* entry = GetAsset(name);
//	if (name != nullptr)
//		return entry->UnCompressedSize;
//
//	return -1;
//}

bool Myp::GetAssetMetaData(FileEntry * entry, uint8_t * dstBuffer, int destOffset) {
    if (!_loaded) {
        OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
        return false;
    }

    int64_t offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;
    if (offset > 0) {
        if (offset > _filesize) {
            OutputDebugStringA(std::string(std::string("Invalid file offset.")).c_str());
            return false;
        }

        if (_fseeki64(_file, offset, SEEK_SET)) {
            OutputDebugStringA(std::string(std::string("Error extracting file. FileEntry offset is out of range.")).c_str());
            return false;
        }

        uint8_t* data = (uint8_t*)malloc(entry->HeaderSize);

        if (nullptr == data)
            return false;

        memset(data, 0, entry->HeaderSize);

        Open();

        if (fread(data, entry->HeaderSize, 1, _file) != 1) {
            CloseFile();
            OutputDebugStringA(std::string(std::string("Failed to extract metadata from Myp.")).c_str());
            return false;
        }

        memcpy(dstBuffer + destOffset, data, entry->HeaderSize);
        free(data);
        return true;
    }
    return true;
}

bool Myp::GetAssetData(FileEntry * entry, uint8_t * dstBuffer, int destOffset, bool decompress) {
    if (!_loaded) {
        OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
        return false;
    }


    int64_t offset = (int64_t)entry->Offset2 << 32 | entry->Offset1;

    if (offset > 0) {
        if (offset > _filesize) {
            OutputDebugStringA(std::string(std::string("Invalid file offset.")).c_str());
            return false;
        }

        if (offset + entry->HeaderSize + entry->CompressedSize > _filesize) {
            OutputDebugStringA(std::string(std::string("Asset truncated")).c_str());
            return false;
        }

        Open();
        if (_fseeki64(_file, offset + entry->HeaderSize, SEEK_SET)) {
            CloseFile();
            OutputDebugStringA(std::string(std::string("Error extracting file. FileEntry offset is out of range.")).c_str());
            return false;
        }

        uint8_t* data = (uint8_t*)malloc(entry->CompressedSize);

        if (nullptr == data)
            return false;

        memset(static_cast<void*>(data), 0, entry->CompressedSize);


        if (fread(data, entry->CompressedSize, 1, _file) != 1) {
            CloseFile();
            OutputDebugStringA(std::string(std::string("Failed to extract file from Myp.")).c_str());
            return false;
        }

        if (decompress) {
            if (entry->Compressed > 0) {
                int result = uncompress(dstBuffer + destOffset, (uLongf*)& entry->UnCompressedSize, data, (uLongf)entry->CompressedSize);

                if (result != 0) {
                    int64_t hash = ((int64_t)entry->Offset2 << 32 | entry->Offset1);
                    OutputDebugStringA(std::string(std::string("Error decompressing asset hash=" + std::to_string(hash) + " zlib=" + std::to_string(result))).c_str());
                }
            }
            else {
                memcpy(dstBuffer + destOffset, data, entry->UnCompressedSize);
            }
        }
        else {
            memcpy(dstBuffer + destOffset, data, entry->CompressedSize);
        }

        free(data);
        return true;
    }
    return true;
}

std::string Myp::GetAssetDataString(const char* file) {

    if (!_loaded) {
        OutputDebugStringA(std::string("The " + (_filename.empty() ? (_filename + " ") : " ") + std::string("file appears not to have loaded.")).c_str());
        return "";
    }

    auto entry = GetAsset(file);
    if (entry == nullptr) {
        OutputDebugStringA(std::string("Asset '" + std::string(file) + "' not found in " + _filename).c_str());
        return "";
    }
    size_t size = entry->UnCompressedSize;

    uint8_t* dst = new uint8_t[size + 1];
    memset(dst, 0, size + 1);
    GetAssetData(entry, dst);

    std::string str((const char*)dst);

    delete[] dst;
    return str;
}

std::string Myp::GetAssetDataString(int64_t hash) {
    if (!_loaded) {
        OutputDebugStringA(std::string(std::string("Myp has not been loaded")).c_str());
        return "";
    }

    auto entry = GetAsset(hash);
    if (entry == nullptr) {
        OutputDebugStringA(std::string(std::string("Asset '" + std::to_string(hash) + "' not found in " + _filename)).c_str());
        return "";
    }
    auto size = entry->UnCompressedSize;

    uint8_t* dst = new uint8_t[size + 1];
    memset(dst, 0, size + 1);
    GetAssetData(entry, dst);

    std::string str((const char*)dst);
    delete[] dst;

    return str;
}

Myp::~Myp() {
    Close();
}

bool Myp::Load() {
    return Load(_filename);
}

void Myp::CloseFile() {

}

void Myp::Close() {

    if (_file) {
        fflush(_file);
        fclose(_file);
        _file = nullptr;
    }

    _filesize = 0;
    _updated.clear();
    _inserted.clear();
    _fileEntryToc.clear();

    if (_tempFile != nullptr) {
        fclose(_tempFile);
        _tempFile = nullptr;
    }

    for (int i = 0; i < (int)_fileBlockMemory.size(); i++)
        free(_fileBlockMemory[i]);

    _loaded = false;
    _fileEntriesHash.clear();

    for (int i = 0; i < (int)_fileTables.size(); i++)
        delete _fileTables[i];

    for (int i = 0; i < (int)_insertedFileEntries.size(); i++)
        delete _insertedFileEntries[i];

    _insertedFileEntries.clear();
    _fileTables.clear();
    _fileBlockMemory.clear();
    _fileEntries.clear();
    _fileHeaderToc.clear();
}
