
#include "Buffer.h"

#include <crtdbg.h>

/*******************************************************************************
* Buffer::Buffer
******************************************************************************/
Buffer::Buffer() noexcept
	: _capacity(0), _level(0), _offset(0), _data(nullptr), _packetSize(0), _offsetStart(0), _currentOp(0)
{
}

/*******************************************************************************
* Buffer::Buffer
******************************************************************************/
Buffer::Buffer(unsigned int capacity, unsigned int align) noexcept
	: _capacity(capacity), _level(0), _offset(0), _packetSize(0), _offsetStart(0), _currentOp(0)
{
	_data = static_cast<unsigned char*>(_aligned_malloc(_capacity, align));
}

/*******************************************************************************
* Buffer::~Buffer
******************************************************************************/
Buffer::~Buffer()
{
	_aligned_free(static_cast<void*>(_data));
}

/*******************************************************************************
* Buffer::write
******************************************************************************/
bool Buffer::write(const void *data, unsigned int size)
{
	_VERIFY(_offset < _capacity);
	_VERIFY(_level <= _capacity);

	if (_level + size > _capacity)
		return false;

	unsigned int pos = (_offset + _level) % _capacity;

	if (pos + size <= _capacity)
	{
		memcpy(_data + pos, data, size);
	}
	else
	{
		void *p0 = &_data[pos];
		unsigned int s0 = _capacity - pos;

		void *p1 = &_data[0];
		unsigned int s1 = size - s0;

		memcpy(p0, data, s0);
		memcpy(p1, static_cast<const char *>(data) + s0, s1);
	}

	_level += size;
	return true;
}

/*******************************************************************************
* Buffer::read
******************************************************************************/
bool Buffer::read(void *data, unsigned int size)
{
	if (!const_cast<Buffer *>(this)->peek(data, size))
		return false;

	_offset = (_offset + size) % _capacity;

	// TODO: reset _offset to 0 when _level is 0
	_level -= size;

	return true;
}

/*******************************************************************************
* Buffer::read
******************************************************************************/
bool Buffer::read(Buffer &buf, unsigned int size)
{
	if (level() < size)
	{
		return false;
	}
	if (buf.level() + size > buf.capacity())
	{
		return false;
	}

	for (unsigned int i = 0; i < size; i++)
	{
		unsigned int si = (_offset + i) % _capacity;
		unsigned int di = (buf._offset + buf._level + i) % buf._capacity;
		buf._data[di] = _data[si];
	}

	_offset = (_offset + size) % _capacity;
	_level -= size;
	buf._level += size;

	_VERIFY(_offset < _capacity);
	return true;
}

/*******************************************************************************
* Buffer::peek
******************************************************************************/
bool Buffer::peek(void *data, unsigned int size, bool frameRead) const
{
	_VERIFY(_offset < _capacity); // FIXME: this assertion is sometimes failing
	_VERIFY(_level <= _capacity);

	if (size > _level)
		return false;

	if (frameRead && size + _offset > _offsetStart + _packetSize)
	{
		return false;
	}

	if (_offset + size <= _capacity)
	{
		memcpy(data, &_data[_offset], size);
		auto d = ((char*)data)[3];
		d = d;
	}
	else
	{
		const void *p0 = &_data[_offset];
		unsigned int s0 = _capacity - _offset;

		const void *p1 = &_data[0];
		unsigned int s1 = size - s0;

		memcpy(data, p0, s0);
		memcpy(static_cast<char *>(data) + s0, p1, s1);
	}

	return true;
}

/*******************************************************************************
* Buffer::discard
******************************************************************************/
bool Buffer::discard(unsigned int size) noexcept
{
	if (size > _level)
		return false;
	_offset = (_offset + size) % _capacity;
	_level -= size;
	return true;
}

/*******************************************************************************
* Buffer::operator[]
******************************************************************************/
unsigned char &Buffer::operator[](unsigned int index) noexcept
{
	_VERIFY(index < _level);
	return _data[(_offset + index) % _capacity];
}

/*******************************************************************************
* Buffer::operator[]
******************************************************************************/
unsigned char Buffer::operator[](unsigned int index) const
{
	_VERIFY(index < _level);
	return this->operator[index];// const_cast<Buffer *>(this)->operator[](index);
}

 Buffer &Buffer::operator=(Buffer &&buf) noexcept
{
	_aligned_free(_data);
	_data = buf._data;
	_capacity = buf._capacity;
	_level = buf._level;
	_offset = buf._offset;
	buf._data = 0;
	return *this;
}