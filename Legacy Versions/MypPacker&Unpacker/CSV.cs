﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarClientTool
{
    public class CSV
    {
        public List<List<String>> Lines = new List<List<String>>();
        public Int32 RowIndex { get; set; }
        private String _commentsection = null;
        public Dictionary<String, List<String>> _keyedRows = new Dictionary<String, List<String>>();
        public List<String> Row => ReadRow();
        public Boolean EOF => RowIndex == Lines.Count - 1;
        public CSV() => RowIndex = 0;

        public String GetKeyedValue(Object rowID, Int32 colIndex, String defaultValue = "")
        {
            String key = rowID.ToString();
            return _keyedRows.ContainsKey(key) && colIndex < _keyedRows[key].Count ? _keyedRows[key][colIndex] : defaultValue;
        }
        public static List<String> QuotedCSVToList(String line, Char quote = '"', Char splitter = ',')
        {
            var list = new List<String>();

            Char[] chars = line.ToCharArray();
            Boolean inQuote = false;
            String current = "";
            for (Int32 i = 0; i < chars.Length; i++)
            {
                if (chars[i] == quote && !inQuote)
                {

                    inQuote = true;
                    //    continue;
                }
                else if (chars[i] == quote && inQuote)
                {
                    inQuote = false;
                    // continue;
                }
                else if (chars[i] == splitter && !inQuote)
                {
                    list.Add(current);
                    current = "";
                }
                else if (inQuote)
                {
                    current += chars[i];
                }
                else
                {
                    current += chars[i];
                }

            }
            if (current.Length > 0)
                list.Add(current);

            return list;
        }


        public CSV(String text, String commentSection = null, Boolean quoted = false, Int32 idCol = -1)
        {
            _commentsection = commentSection;
            String comment = "";
            foreach (String row in text.Split(new Char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList())
            {
                if (_commentsection != null && row.StartsWith(_commentsection))
                {
                    comment = row.Replace(",", "").Replace(";", "");

                }


                if (row.StartsWith(";"))
                    continue;

                var cols = new List<String>();
                if (quoted)
                {
                    cols = QuotedCSVToList(row);
                }
                else
                {
                    foreach (String col in row.Split(new Char[] { ',' }))
                    {
                        cols.Add(col);
                    }
                }

                if (_commentsection != null && comment != "")
                    cols.Add(comment);
                Lines.Add(cols);
                if (idCol > -1 && idCol < cols.Count)
                    _keyedRows[cols[idCol]] = cols;
            }
            RowIndex = 0;
        }


        public Dictionary<Int32, Int32> ToDictionary(Int32 IDCol, Int32 valueCol)
        {
            var results = new Dictionary<Int32, Int32>();
            while (!EOF)
            {
                List<String> row = ReadRow();
                results[ReadInt32(IDCol)] = ReadInt32(valueCol);
                NextRow();
            }
            return results;
        }

        public void MoveToEnd() => RowIndex = Lines.Count - 1;

        public void NewRow() => Lines.Insert(RowIndex + 1, new List<String>());

        public void RemoveRow() => Lines.RemoveAt(RowIndex);
        public void Remove(List<Int32> rows)
        {
            var lines = new List<List<String>>();
            for (Int32 i = 0; i < Lines.Count; i++)
            {
                if (!rows.Contains(i))
                    lines.Add(Lines[i]);
            }
            Lines = lines;
            if (RowIndex > Lines.Count - 1)
                RowIndex = lines.Count - 1;
        }

        public void WriteCol(Int32 col, String value)
        {
            List<String> line = Lines[RowIndex];
            if (line.Count - 1 < col)
            {
                Int32 count = col - line.Count;
                for (Int32 i = 0; i <= count; i++)
                    line.Add("");
            }
            line[col] = value;
        }


        public List<String> ReadRow() => Lines[RowIndex];

        public void NextRow() => RowIndex++;

        public Int32 ReadInt32(Int32 col, Int32 defaultValue = 0) => col < Lines[RowIndex].Count && Int32.TryParse(Lines[RowIndex][col], out Int32 result) ? result : defaultValue;

        public Int32 PeakInt32(Int32 row, Int32 col, Int32 defaultValue = -1)
        {
            if (row < Lines.Count)
            {
                Int32.TryParse(Lines[row][col], out Int32 result);
                return result;
            }
            return defaultValue;
        }
        public Single ReadFloat(Int32 col) => col < Lines[RowIndex].Count && Single.TryParse(Lines[RowIndex][col], out Single result) ? result : 0;

        public String ReadString(Int32 col) => Lines[RowIndex][col];

        public String ToText()
        {
            var builder = new StringBuilder();
            foreach (List<String> row in Lines)
            {
                for (Int32 i = 0; i < row.Count; i++)
                {
                    if (i < row.Count - 1)
                        builder.Append(row[i] + ",");
                    else
                        builder.Append(row[i]);
                }
                builder.Append("\n");
            }
            return builder.ToString();
        }

    }
}
