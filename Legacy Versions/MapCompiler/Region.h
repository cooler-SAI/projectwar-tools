#pragma once

#include "NIF.h"
#include "Fixture.h"
#include "Terrain.h"
#include "Collision.h"
#include "Config.h"

ref class Fixture;
ref class Zone;

ref class Region
{
public:

	Region()
		: ErrorCount(0)
	{
	}

	int ID;
	Dictionary<int, Zone^>^ Zones;
	int ErrorCount;
	Config ^Config;
};