
#include "Serializer.h"
#include "Writer.h"
#include "CompilerException.h"

#include "obj/NiImage.h"
#include "obj/NiTexturingProperty.h"
#include "obj/NiSourceTexture.h"
#include "obj/NiLODNode.h"
#include "obj/NiCollisionObject.h"
#include "obj/NiCollisionData.h"
#include "obj/NiTriShape.h"
#include "obj/NiTriShapeData.h"

MemoryStream ^Serializer::Serialize(Fixture ^fixture)
{
	MemoryStream ^stream = gcnew MemoryStream();
	Writer ^writer = gcnew Writer(stream);

	writer->Write((UInt32)fixture->ID);
	writer->Write((UInt32)fixture->NIF);
	writer->Write(fixture->Name);
	writer->Write((float)fixture->X);
	writer->Write((float)fixture->Y);
	writer->Write((float)fixture->Z);
	writer->Write((float)fixture->A);
	writer->Write((float)fixture->Scale);
	writer->Write((UInt32)fixture->Collide);
	writer->Write((float)fixture->Angle3D);
	writer->Write((float)fixture->Axis3D.X);
	writer->Write((float)fixture->Axis3D.Y);
	writer->Write((float)fixture->Axis3D.Z);

	return stream;
}

MemoryStream ^Serializer::Serialize(Zone^ zone, Terrain ^terrain)
{
	MemoryStream ^stream = gcnew MemoryStream();
	Writer ^writer = gcnew Writer(stream);

	writer->Write((UInt32)zone->ID);
	writer->Write((UInt32)terrain->Width);
	writer->Write((UInt32)terrain->Height);
	for (int y = 0; y < terrain->Height; y++)
		for (int x = 0; x < terrain->Width; x++)
			writer->Write((UInt32)terrain->GetValue(x, y));

	/* MemoryStream ^vertexData = terrain->GetVertexData();
	int numV = terrain->NumVertices;
	int vbSize = (int) vertexData->Length;
	writer->Write((UInt32) numV);
	writer->Write((UInt32) vbSize);
	writer->Write(vertexData->GetBuffer(), 0, (int) vertexData->Length);

	MemoryStream ^indexData = terrain->GetIndexData();
	writer->Write((UInt32) terrain->NumTriangles);
	writer->Write((UInt32) indexData->Length);
	writer->Write(indexData->GetBuffer(), 0, (int) indexData->Length);
	*/
	return stream;
}

MemoryStream ^Serializer::Serialize(NIF ^nif)
{
	MemoryStream ^stream = gcnew MemoryStream();
	Writer ^writer = gcnew Writer(stream);

	writer->Write((UInt32)nif->ID);
	writer->Write(nif->Name);
	writer->Write(nif->Filename);
	writer->Write((float)nif->MinAngle);
	writer->Write((float)nif->MaxAngle);

	String ^nifPath = "assetdb/fixtures/fi.0.0." + nif->Filename;

	Niflib::NiObjectRef root;
	Niflib::NiAVObjectRef object;

	auto nifData = MYPManager::Instance->GetAsset(nifPath);


	if (nifData == nullptr)
	{
		throw gcnew CompilerException("NifLoadFailure", nif->Name, "Path does not exist: " + nifPath);
		//WriteEmptyNIFNode(writer);
	}

	try
	{
		array<Byte>^ b = gcnew array<Byte>(nifData->Length);

		Buffer::BlockCopy(nifData, 0, b, 0, nifData->Length);

		//GCHandle hdl = GCHandle::Alloc(b, GCHandleType::Pinned);
		//char* ptr = (char*)(void*)hdl.AddrOfPinnedObject();

		pin_ptr<System::Byte> p = &b[0];
		unsigned char* pby = p;
		char* pch = reinterpret_cast<char*>(pby);


		std::stringstream ss;
		ss.write(pch, nifData->Length);
		ss.seekg(0);

		root = Niflib::ReadNifTree(ss);
	}
	catch (...)
	{
	}

	if (!root)
		throw gcnew CompilerException("NifLoadFailure", nif->Name, "Niflib::ReadNifTree failed to load '" + nifPath + "'");

	object = Niflib::DynamicCast<Niflib::NiAVObject>(root);

	if (!object)
		throw gcnew CompilerException("NifLoadFailure", nif->Name, "Malformed NIF at '" + nifPath + "'");

	WriteNIFNode(writer, object);
	return stream;
}

void Serializer::WriteNIFNode(Writer ^writer, Niflib::NiAVObjectRef object)
{
	writer->WriteStdString(object->GetName());
	writer->WriteStdString(object->GetType().GetTypeName());

	// Transformation
	Niflib::Vector3 t(object->GetLocalTranslation());
	writer->Write(object->GetLocalTranslation());
	writer->Write(object->GetLocalRotation());
	writer->Write((float)object->GetLocalScale());

	Niflib::NiLODNodeRef lodNode = Niflib::DynamicCast<Niflib::NiLODNode>(object);

	if (lodNode)
	{
		// TODO
		writer->Write((UInt32)1);
		WriteNIFNode(writer, lodNode->GetChildren()[0]);
	}
	else
	{
		Niflib::NiNodeRef node = Niflib::DynamicCast<Niflib::NiNode>(object);

		if (node)
		{
			std::vector<Niflib::NiAVObjectRef> children = node->GetChildren();

			writer->Write((UInt32)children.size());
			for (size_t i = 0; i < children.size(); i++)
				WriteNIFNode(writer, children[i]);
		}
		else
		{
			// No children
			writer->Write((UInt32)0);
		}
	}

	if (!WriteTriShapeData(writer, object))
	{
		// No vertices, triangles, or normals
		writer->Write((UInt32)0);
		writer->Write((UInt32)0);
		writer->Write((UInt32)0);
	}
}

MemoryStream ^Serializer::SerializeEmptyNIF(NIF ^nif)
{
	MemoryStream ^stream = gcnew MemoryStream();
	Writer ^writer = gcnew Writer(stream);
	writer->Write((UInt32)nif->ID);
	writer->Write(nif->Name);
	writer->Write(nif->Filename);
	writer->Write((float)nif->MinAngle);
	writer->Write((float)nif->MaxAngle);
	WriteEmptyNIFNode(writer);
	return stream;
}

void Serializer::WriteEmptyNIFNode(Writer ^writer)
{
	writer->Write("");
	writer->Write("");

	// Translation
	writer->Write((float)0);
	writer->Write((float)0);
	writer->Write((float)0);

	// Rotation
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			writer->Write((float)0);

	// Scaling
	writer->Write((float)0);

	// No children
	writer->Write((UInt32)0);

	// No vertices, triangles, or normals
	writer->Write((UInt32)0);
	writer->Write((UInt32)0);
	writer->Write((UInt32)0);
}

bool Serializer::WriteTriShapeData(Writer ^writer, Niflib::NiAVObjectRef object)
{
	Niflib::NiTriShapeRef triShape = Niflib::DynamicCast<Niflib::NiTriShape>(object);

	if (triShape)
	{
		Niflib::NiGeometryDataRef geomData = triShape->GetData();

		if (geomData)
		{
			Niflib::NiTriShapeDataRef triShapeData = Niflib::DynamicCast<Niflib::NiTriShapeData>(geomData);

			if (triShapeData)
			{
				std::string objectName = object->GetName();
				size_t pos = writer->BaseStream->Position;

				// Vertices
				std::vector<Niflib::Vector3> vertices = triShapeData->GetVertices();
				UInt32 numVertices = (UInt32)vertices.size();
				writer->Write(numVertices);
				for (size_t i = 0; i < numVertices; i++)
				{
					Niflib::Vector3 v(vertices[i]);

					writer->Write((float)v.x);
					writer->Write((float)v.y);
					writer->Write((float)v.z);
				}

				// Normals
				std::vector<Niflib::Vector3> normals = triShapeData->GetNormals();
				writer->Write((UInt32)normals.size());
				if (normals.size() > 0 && normals.size() == vertices.size())
				{
					for (size_t i = 0; i < normals.size(); i++)
					{
						Niflib::Vector3 n(normals[i]);

						writer->Write((float)n.x);
						writer->Write((float)n.y);
						writer->Write((float)n.z);
					}
				}

				// Triangles
				std::vector<Niflib::Triangle> triangles = triShapeData->GetTriangles();
				writer->Write((UInt32)triangles.size());
				for (size_t i = 0; i < triangles.size(); i++)
				{
					Niflib::Triangle t(triangles[i]);

					writer->Write((UInt32)t.v1);
					writer->Write((UInt32)t.v2);
					writer->Write((UInt32)t.v3);
				}

				return true;
			}
		}
	}

	return false;
}

MemoryStream ^Serializer::Serialize(int zoneID, Collision ^collision)
{
	MemoryStream ^stream = gcnew MemoryStream();
	Writer ^writer = gcnew Writer(stream);

	writer->Write((UInt32)zoneID);
	writer->Write((UInt32)collision->GetVertexCount());
	for (unsigned int i = 0; i < collision->GetVertexCount(); i++)
	{
		Vector3 v(collision->GetVertex(i));
		writer->Write((float)v.X);
		writer->Write((float)v.Y);
		writer->Write((float)v.Z);
	}

	writer->Write((UInt32)collision->GetTriangleCount());
	writer->Write((UInt32)4); // Index size
	for (unsigned int i = 0; i < collision->GetTriangleCount(); i++)
	{
		Triangle t = collision->GetTriangle(i);
		writer->Write((UInt32)t.i0);
		writer->Write((UInt32)t.i1);
		writer->Write((UInt32)t.i2);
		writer->Write((UInt32)t.fixture);
	}
	return stream;
}

MemoryStream ^Serializer::Serialize(int zoneID, BSPNode ^node)
{
	MemoryStream ^stream = gcnew MemoryStream();
	Writer ^writer = gcnew Writer(stream);
	writer->Write((UInt32)zoneID);
	writer->Write((UInt32)Config->BSPDepthCutoff);
	writer->Write((UInt32)Config->BSPTrianglesCutoff);
	WriteBSPNode(writer, node);
	return stream;
}

void Serializer::WriteBSPNode(Writer ^writer, BSPNode ^node)
{
	if (node->Back == nullptr)
	{
		writer->Write((Byte)1);
		if (node->Triangles != nullptr)
		{
			writer->Write((UInt32)node->Triangles->Count);
			for each (int i in node->Triangles)
				writer->Write(i);
		}
		else
			writer->Write((UInt32)0);

	}
	else
	{
		writer->Write((Byte)0);
		writer->Write(node->P.N.X);
		writer->Write(node->P.N.Y);
		writer->Write(node->P.N.Z);
		writer->Write(node->P.D);
		WriteBSPNode(writer, node->Back);
		WriteBSPNode(writer, node->Front);
	}
}