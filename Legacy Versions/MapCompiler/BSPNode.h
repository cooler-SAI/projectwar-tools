
#pragma once

#include "Triangle.h"

using namespace System::Collections::Generic;
using namespace WarZoneLib;

ref class BSPNode
{
public:

	BSPNode()
	{
	}

	property bool IsLeaf
	{
		bool get()
		{
			return _back == nullptr;
		}
	};

	property BSPNode ^Front
	{
		BSPNode ^get()
		{
			return _front;
		}
	}

	property BSPNode ^Back
	{
		BSPNode ^get()
		{
			return _back;
		}
	}

	void Expand()
	{
		if (IsLeaf)
		{
			_front = gcnew BSPNode();
			_back = gcnew BSPNode();
		}
	}

	Plane P;
	List<int> ^Triangles;

private:

	BSPNode ^_front, ^_back;
};