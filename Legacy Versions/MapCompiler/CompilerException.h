
#pragma once

using namespace System;

ref class CompilerException : public System::Exception
{
private:

	String ^_code, ^_asset;

public:

	CompilerException(String ^code, String ^asset, String ^message)
		: Exception(message)
	{
		_code = code;
		_asset = asset;
	}

	property String ^Code
	{
		String ^get() { return _code; }
	}

	property String ^Asset
	{
		String ^get() { return _asset; }
	}
};