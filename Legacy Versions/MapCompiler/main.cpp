
#include "MapCompiler.h"

using namespace System;

int main(array<String ^> ^args)
{
    if (args->Length != 3)
    {
        Console::WriteLine("Usage: MapCompiler <config-file> <output-file> <region-id>");
        return -1;
    }
    else
    {
        MapCompiler ^mapCompiler = gcnew MapCompiler();
        return mapCompiler->Compile(args[0], args[1], int::Parse(args[2]));
    }
}
