
#pragma once

#include "NIF.h"
#include "Fixture.h"
#include "Terrain.h"
#include "Collision.h"
#include "Config.h"
#include "Region.h"

ref class Fixture;

ref class Zone
{
public:

	Zone()
		: ErrorCount(0)
	{
	}

	int ID;
	String ^Name;
	array<NIF ^> ^NIFS;
	array<Fixture ^> ^Fixtures;
	Collision ^Collision;
	Terrain ^Terrain;
	Region^ Region;
	int OffsetX, OffsetY;
	List<int>^ FixtureFilter;

	NIF ^GetNIFByID(int nifId)
	{
		for (int i = 0; i < NIFS->Length; i++)
			if (NIFS[i]->ID == nifId)
				return NIFS[i];
		return nullptr;
	}

	int ErrorCount;
};