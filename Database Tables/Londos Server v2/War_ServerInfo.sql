CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ServerInfo`
--

DROP TABLE IF EXISTS `ServerInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ServerInfo` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EnvironmentID` bigint(20) NOT NULL,
  `Label` longtext,
  `Name` varchar(500) DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  `Port` int(11) DEFAULT NULL,
  `ControlPort` int(11) DEFAULT NULL,
  `Type` bigint(20) DEFAULT NULL,
  `State` bigint(20) DEFAULT NULL,
  `Autostart` tinyint(1) DEFAULT NULL,
  `StatusMsg` longtext,
  `Retry` int(11) DEFAULT NULL,
  `ProcessID` int(11) DEFAULT NULL,
  `ExecPath` longtext,
  `ConfigPath` longtext,
  `Clients` int(11) DEFAULT NULL,
  `Notes` longtext,
  `MOTD` longtext,
  PRIMARY KEY (`ID`),
  KEY `IX_ServerInfo_EnvironmentID` (`EnvironmentID`),
  CONSTRAINT `FK_ServerInfo_Environment_EnvironmentID` FOREIGN KEY (`EnvironmentID`) REFERENCES `environment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServerInfo`
--

LOCK TABLES `ServerInfo` WRITE;
/*!40000 ALTER TABLE `ServerInfo` DISABLE KEYS */;
INSERT INTO `ServerInfo` VALUES (1,1,'DEV-GameServer','Avelorn','10.10.1.141',8044,9044,4,0,0,'',0,11304,'C:\\Users\\Administrator\\Documents\\WarServer\\WarServer\\bin\\Debug\\netcoreapp2.0\\WarGameServer.dll','C:\\Users\\Administrator\\Documents\\WarServer\\WarServer\\game_settings.json',0,NULL,NULL),(7,1,'DEV-PatcherServer','','10.10.1.141',8045,9045,2,0,0,'',0,20924,'C:\\Users\\Administrator\\Documents\\WarServer\\WarPatcherServer\\bin\\Debug\\netcoreapp2.0\\WarPatcherServer.dll','C:\\Users\\Administrator\\Documents\\WarServer\\WarPatcherServer\\patcher_settings.json',0,NULL,NULL),(8,1,'DEV-Lobby',NULL,'10.10.1.141',8046,9046,3,0,0,'',0,2844,'C:\\Users\\Administrator\\Documents\\WarServer\\WarLobbyServer\\bin\\Debug\\netcoreapp2.0\\WarLobbyServer.dll','C:\\Users\\Administrator\\Documents\\WarServer\\WarLobbyServer\\lobby_settings.json',1,NULL,NULL),(12,1,'DEV-Admin',NULL,'10.10.1.141',9047,9047,1,0,0,NULL,0,0,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `ServerInfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:17
