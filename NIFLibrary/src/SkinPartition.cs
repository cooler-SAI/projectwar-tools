/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

namespace NIFLibrary
{
	using System;
	using System.IO;

    /// <summary>
    /// Class SkinPartition.
    /// </summary>
    public class SkinPartition
	{
        /// <summary>
        /// The number vertices
        /// </summary>
        public UInt16 NumVertices;

        /// <summary>
        /// The number triangles
        /// </summary>
        public UInt16 NumTriangles;

        /// <summary>
        /// The number bones
        /// </summary>
        public UInt16 NumBones;

        /// <summary>
        /// The number strips
        /// </summary>
        public UInt16 NumStrips;

        /// <summary>
        /// The number weights per vertex
        /// </summary>
        public UInt16 NumWeightsPerVertex;

        /// <summary>
        /// The bones
        /// </summary>
        public UInt16[] Bones;

        /// <summary>
        /// The has vertex map
        /// </summary>
        public Boolean HasVertexMap;

        /// <summary>
        /// The vertex map
        /// </summary>
        public UInt16[] VertexMap;

        /// <summary>
        /// The has vertex weights
        /// </summary>
        public Boolean HasVertexWeights;

        /// <summary>
        /// The vertex weights
        /// </summary>
        public Single[][] VertexWeights;

        /// <summary>
        /// The strip lengths
        /// </summary>
        public UInt16[] StripLengths;

        /// <summary>
        /// The has faces
        /// </summary>
        public Boolean HasFaces;

        /// <summary>
        /// The strips
        /// </summary>
        public UInt16[][] Strips;

        /// <summary>
        /// The triangles
        /// </summary>
        public Triangle[] Triangles;

        /// <summary>
        /// The has bone indicies
        /// </summary>
        public Boolean HasBoneIndicies;

        /// <summary>
        /// The bone indicies
        /// </summary>
        public Byte[][] BoneIndicies;

        /// <summary>
        /// The unkown short
        /// </summary>
        public UInt16 UnkownShort;

        /// <summary>
        /// The unkown short2
        /// </summary>
        public UInt16 UnkownShort2;

        /// <summary>
        /// The unkown short3
        /// </summary>
        public UInt16 UnkownShort3;

        /// <summary>
        /// The number vertices2
        /// </summary>
        public UInt16 NumVertices2;

        /// <summary>
        /// The unkown short4
        /// </summary>
        public UInt16 UnkownShort4;

        /// <summary>
        /// The unkown short5
        /// </summary>
        public UInt16 UnkownShort5;

        /// <summary>
        /// The unkown short6
        /// </summary>
        public UInt16 UnkownShort6;

        /// <summary>
        /// The unkown arr
        /// </summary>
        public SkinPartitionUnkownItem1[] UnkownArr;

        /// <summary>
        /// Initializes a new instance of the <see cref="SkinPartition"/> class.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="reader">The reader.</param>
        public SkinPartition(NiFile file, BinaryReader reader)
		{
			this.NumVertices = reader.ReadUInt16();
			this.NumTriangles = reader.ReadUInt16();
			this.NumBones = reader.ReadUInt16();
			this.NumStrips = reader.ReadUInt16();
			this.NumWeightsPerVertex = reader.ReadUInt16();
			this.Bones = reader.ReadUInt16Array((Int32)this.NumBones);
			this.HasVertexMap = true;
			this.HasVertexWeights = true;
			this.HasFaces = true;
			if (file.Version >= eNifVersion.v10_1_0_0)
			{
				this.HasVertexMap = reader.ReadBoolean(file.Version);
			}
			if (this.HasVertexMap)
			{
				this.VertexMap = reader.ReadUInt16Array((Int32)this.NumVertices);
			}
			if (file.Version >= eNifVersion.v10_1_0_0)
			{
				this.HasVertexWeights = reader.ReadBoolean(file.Version);
			}
			if (this.HasVertexWeights)
			{
				this.VertexWeights = new Single[(Int32)this.NumVertices][];
				for (var i = 0; i < (Int32)this.NumVertices; i++)
				{
					this.VertexWeights[i] = reader.ReadFloatArray((Int32)this.NumWeightsPerVertex);
				}
			}
			this.StripLengths = reader.ReadUInt16Array((Int32)this.NumStrips);
			if (file.Version >= eNifVersion.v10_1_0_0)
			{
				this.HasFaces = reader.ReadBoolean(file.Version);
			}
			if (this.HasFaces && this.NumStrips != 0)
			{
				this.Strips = new UInt16[(Int32)this.NumStrips][];
				for (var j = 0; j < (Int32)this.NumStrips; j++)
				{
					this.Strips[j] = reader.ReadUInt16Array((Int32)this.StripLengths[j]);
				}
			}
			else if (this.HasFaces && this.NumStrips == 0)
			{
				this.Triangles = new Triangle[(Int32)this.NumTriangles];
				for (var k = 0; k < this.Triangles.Length; k++)
				{
					this.Triangles[k] = new Triangle(reader);
				}
			}
			this.HasBoneIndicies = reader.ReadBoolean(file.Version);
			if (this.HasBoneIndicies)
			{
				this.BoneIndicies = new Byte[(Int32)this.NumVertices][];
				for (var l = 0; l < this.BoneIndicies.Length; l++)
				{
					this.BoneIndicies[l] = new Byte[(Int32)this.NumWeightsPerVertex];
					for (var m = 0; m < (Int32)this.NumWeightsPerVertex; m++)
					{
						this.BoneIndicies[l][m] = reader.ReadByte();
					}
				}
			}
			if (file.Header.UserVersion >= 12u)
			{
				this.UnkownShort = reader.ReadUInt16();
			}
			if (file.Version == eNifVersion.v10_2_0_0 && file.Header.UserVersion == 1u)
			{
				this.UnkownShort2 = reader.ReadUInt16();
				this.UnkownShort3 = reader.ReadUInt16();
				this.NumVertices2 = reader.ReadUInt16();
				this.UnkownShort4 = reader.ReadUInt16();
				this.UnkownShort5 = reader.ReadUInt16();
				this.UnkownShort6 = reader.ReadUInt16();
				this.UnkownArr = new SkinPartitionUnkownItem1[(Int32)this.NumVertices2];
				for (var n = 0; n < (Int32)this.NumVertices2; n++)
				{
					this.UnkownArr[n] = new SkinPartitionUnkownItem1(file, reader);
				}
			}
		}
	}
}
