﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarClientTool
{
    public class PatcherAsset 
    {
       
        public long EnvironmentID { get; set; }

     
        public long PatcherFileID { get; set; }

     
         public MythicPackage Package { get; set; }


         public long Size { get; set; }
         public long CompressedSize { get; set; }
         public long Hash { get; set; }
         public uint CRC32 { get; set; }
     
       public string Name { get; set; }
       public bool Compressed { get; set; }
        public byte[] Data;
        public override string ToString()
        {
            return Name;
        }
        public PatcherAsset()
        {
        }
    }
    public class StringTable
    {
        public List<string> List = new List<string>();
        public Dictionary<string, string> _keyed = new Dictionary<string, string>();
        public StringTable()
        {
        }

        public void Load(byte[] data)
        {
            var str = System.Text.UnicodeEncoding.Unicode.GetString(data);
            int row = 0;
            foreach (var line in str.Split(new string[] { "\r\n" }, StringSplitOptions.None))
            {
                var index = line.IndexOf('\t');
                string key = line;

                if (index > 0)
                {
                    key = line.Substring(0, index).ToLower().Trim();
                    var value = line.Substring(index + 1);
                    _keyed[key] = value;
                    List.Add(value);
                }
                else
                {
                    _keyed[key] = "";
                    List.Add("");
                }
                row++;
            }
        }

        public string GetKeyedValue(string row)
        {
            if (_keyed.ContainsKey(row))
                return _keyed[row];
            return "";
        }

        public string GetValue(int row)
        {
            if (row < List.Count)
                return List[row];
            return "";
        }

        public void SetValue(int row, string value)
        {
            if (row > List.Count)
            {
                int count = row - List.Count + 1;

                for (int i = 0; i < count; i++)
                    List.Add("");
            }
            _keyed[row.ToString()] = value;
            List[row] = value;

        }

        public string ToDocument()
        {
            var builder = new StringBuilder();

            for (int i = 0; i < List.Count; i++)
            {
                builder.Append(i + "\t" + List[i] + "\r\n");
            }


            return builder.ToString();
        }
    }
    public enum Language
    {
        english = 1,
        french = 2,
        german = 3,
        italian = 4,
        spanish = 5,
        korean = 6,
        s_chinese = 7,
        t_chinese = 8,
        japanese = 9,
        russian = 10,
    }

    public class Folder
    {
        public MythicPackage Package { get; internal set; }
        public string Name { get { if (Path == null) return Package.ToString().ToLower(); else return System.IO.Path.GetFileName(Path); } }
        public Folder Parent { get; internal set; }
        public MYPManager Manager { get; internal set; }
        public long PatcherFileID = 0;
        public Folder Root
        {
            get
            {
                Folder r = Parent;
                while (r != null)
                {
                    if (r.Parent == null)
                        return r;
                    r = r.Parent;
                }
                return this;
            }
        }

        public Dictionary<string, Folder> Folders = new Dictionary<string, Folder>();
        public Dictionary<long, PatcherAsset> Files = new Dictionary<long, PatcherAsset>();
        public bool Dehashed = true;
        public string Path { get; internal set; }

        public override string ToString()
        {
            return Name;
        }

        public Folder(MYPManager manager)
        {
            Manager = manager;
        }

        public List<PatcherAsset> GetAssets(MythicPackage packageFilter = MythicPackage.NONE)
        {
            var list = new List<PatcherAsset>();
            GetAssetsRecursive(list, packageFilter);
            return list;
        }

        public List<MythicPackage> GetPackages()
        {
            var list = new List<PatcherAsset>();
            GetAssetsRecursive(list);
            return list.GroupBy(e => e.Package).Select(e => e.Key).ToList();
        }
        private void GetAssetsRecursive(List<PatcherAsset> assets, MythicPackage packageFilter = MythicPackage.NONE)
        {

            assets.AddRange(Files.Values.Where(e => packageFilter == MythicPackage.NONE || e.Package == packageFilter));
            foreach (var folder in Folders.Values)
                folder.GetAssetsRecursive(assets);
        }
    }

    public class MYPManager: IDisposable
    {
        private Dictionary<MythicPackage, MYP> Packages = new Dictionary<MythicPackage, MYP>();
        private string _mypFolder;
        public readonly Dictionary<long, string> HashToString = null;
        public Dictionary<MythicPackage, Folder> PackageFolders = new Dictionary<MythicPackage, Folder>();
        private Dictionary<Language, Dictionary<string, StringTable>> _strings = new Dictionary<Language, Dictionary<string, StringTable>>();

        public bool Updated
        {
            get
            {

                foreach (var package in Packages.Values)
                    if (package.Changed)
                    {
                        return true;
                    }
                return false;
            }
        }


        public void Close()
        {
            foreach (var archive in Packages.Values)
            {
                archive.Dispose();
            }
        }

        public MYPManager(String mypFolder, Dictionary<long, string> dehashes)
        {
            //HashToString = dehashes;
            _mypFolder = mypFolder;
        }


        protected async Task LoadInternal(bool overlay, params MythicPackage[] include)
        {
            var tasks = new List<Task>();

            foreach (MythicPackage archive in Enum.GetValues(typeof(MythicPackage)))
            {
                if (archive == MythicPackage.MFT)
                    continue;

                if (include == null || !include.Contains(archive))
                    continue;

                string file = Path.Combine(_mypFolder, archive + ".myp");
                if (File.Exists(file))
                    tasks.Add(Task.Run(() => LoadPackage(file)));
            }
            await Task.WhenAll(tasks);
        }

        public Dictionary<long, PatcherAsset> GetAssets(MythicPackage package)
        {
            return Packages[package].Assets;
        }

        public List<MythicPackage> GetPackages()
        {
            return Packages.Keys.ToList();
        }

        public PatcherAsset GetAsset(MythicPackage package, long hash)
        {
            if (Packages.ContainsKey(package) && Packages[package].Assets.ContainsKey(hash))
            {
                if (Packages[package].Assets[hash].Data == null)
                    Packages[package].Assets[hash].Data = Packages[package].GetAssetData(hash);
                return Packages[package].Assets[hash];
            }
            return null;
        }

        private void LoadPackage(string file)
        {
            var package = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(file).ToUpper());

            if (File.Exists(file))
            {
                try
                {
                    var m = new MYP(package);
                    m.Manager = this;
                    Packages[package] = m;
                }
                catch (Exception e)
                {
                  Console.WriteLine(e.ToString());
                }
            }
        }

        public  void Save()
        {
            foreach (var myp in Packages.Values.ToList())
            {
                if (myp.Changed)
                {
                    Console.WriteLine($"Saving archive '{myp.Filename}'");
                    myp.Save();
                }
            }

        }

        public void UpdateAsset(PatcherAsset asset)
        {
            Packages[asset.Package].UpdateAsset(asset);
        }

        public void InsertAsset(PatcherAsset asset)
        {
            Packages[asset.Package].UpdateAsset(asset);
        }

        public void DeleteAsset(PatcherAsset asset)
        {
            Packages[asset.Package].Delete(asset);
        }

        public bool HasAsset(MythicPackage package, long hash)
        {
            return Packages.ContainsKey(package) && Packages[package].Assets.ContainsKey(hash);
        }

        public void Dispose()
        {
            foreach (var myp in Packages.Values.ToList())
            {
                myp.Dispose();
            }
        }


        public string GetAssetStringASCII(string assetName, MythicPackage? archive = null)
        {
            long hash = MYP.HashWAR(assetName);
            if (archive == null)
            {
                foreach (var package in GetPackages())
                {

                    if (HasAsset(package, hash))
                        return System.Text.Encoding.ASCII.GetString(GetAsset(package, hash).Data);
                }
            }
            else if (GetPackages().Contains(archive.Value))
            {
                return System.Text.Encoding.ASCII.GetString(GetAsset(archive.Value, hash).Data);
            }

            return "";
        }
        public string GetAssetStringUnicode(string assetName, MythicPackage? archive = null)
        {
            long hash = MYP.HashWAR(assetName);
            if (archive == null)
            {
                foreach (var package in GetPackages())
                {

                    if (HasAsset(package, hash))
                        return System.Text.Encoding.Unicode.GetString(GetAsset(package, hash).Data);
                }
            }
            else if (GetPackages().Contains(archive.Value))
            {
                return System.Text.Encoding.Unicode.GetString(GetAsset(archive.Value, hash).Data);
            }

            return "";
        }

        public PatcherAsset GetAsset(string assetName)
        {
            return GetAsset(MYP.HashWAR(assetName));
        }

        public PatcherAsset GetAsset(PatcherAsset asset)
        {
            asset.Data = GetAsset(asset.Package, asset.Hash).Data;
            return asset;
        }

        public PatcherAsset FindAsset(string assetName, MythicPackage package)
        {
            var asset = GetAsset(assetName, package);
            if (asset == null)
            {
                return GetAsset(MYP.HashWAR(assetName));
            }
            return asset;
        }

        public PatcherAsset GetAsset(string assetName, MythicPackage package)
        {
            var hash = MYP.HashWAR(assetName);
            if (HasPackage(package) && HasAsset(package, hash))
            {
                return GetAsset(package, hash);
            }
            return null;
        }

        public PatcherAsset GetAsset(long hash)
        {
            foreach (var package in GetPackages())
            {
                var asset = GetAsset(package, hash);
                if (asset != null)
                    return asset;
            }
            return null;
        }

        public bool HasPackage(MythicPackage package)
        {
            return GetPackages().Contains(package);
        }

        public async Task Load(bool overlay, params MythicPackage[] include)
        {
            await LoadInternal(overlay, include);
            foreach (var package in GetPackages())
            {
                foreach (var asset in GetAssets(package).Values)
                    AddToFolder(package, asset);
            }
        }

        protected void AddToFolder(MythicPackage package, PatcherAsset entry)
        {
            string packagename = package.ToString().ToLower();
            if (!PackageFolders.ContainsKey(package))
            {
                PackageFolders[package] = new Folder(this)
                {
                    Package = package,
                    Path = ""
                };
            }
            Folder folder = PackageFolders[package];

            if (!string.IsNullOrEmpty(entry.Name))
            {
                var folders = entry.Name.Split(new char[] { '/' });
                string path = "";


                for (int i = 0; i < folders.Length - 1; i++)
                {
                    path += folders[i];
                    if (!folder.Folders.ContainsKey(path))
                    {
                        folder.Folders[path] = new Folder(this)
                        {
                            Package = package,
                            Path = path,
                            PatcherFileID = entry.PatcherFileID,
                            Parent = folder,
                        };
                    }

                    folder = folder.Folders[path];
                    path += '/';
                }
                path += folders[folders.Length - 1];

                folder.Files[entry.Hash] = entry;
            }
            else
            {

                if (!folder.Folders.ContainsKey("NO_DEHASH"))
                {
                    folder.Folders["NO_DEHASH"] = new Folder(this)
                    {
                        Package = package,
                        Path = "NO_DEHASH",
                        Dehashed = false,
                        PatcherFileID = entry.PatcherFileID,
                    };
                }
                folder.Folders["NO_DEHASH"].Files[entry.Hash] = entry;
            }
        }

        public void UpdateAsset(MythicPackage package, string path, byte[] data)
        {
            var asset = GetAsset(path, package);
            asset.Data = data;
            UpdateAsset(asset);
        }

        public string GetStringByKey(Language language, string tableName, string key, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string path = tableName.Replace($"data/strings/{language}/", "");
            string result = "";
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<string, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                var asset = GetAsset("data/strings/" + language + "/" + path, defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].GetKeyedValue(key);

            return result;
        }

        public string GetString(Language language, string tableName, int row, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string path = tableName.Replace($"data/strings/{language}/", "");
            string result = "";
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<string, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                var asset = GetAsset("data/strings/" + language + "/" + path, defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].GetValue(row);

            return result;
        }

        public string GetString(Language language, string tableName)
        {
            string path = tableName.Replace($"data/strings/{language}/", "");
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<string, StringTable>();
            if (!_strings[language].ContainsKey(path))
            {
                GetStringTable(language, path);
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].ToDocument();

            return "";
        }

        public byte[] GetStringUnicode(Language language, string tableName)
        {
            string path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<string, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                GetStringTable(language, path);
            }

            if (_strings[language].ContainsKey(path))
                return System.Text.UnicodeEncoding.Unicode.GetPreamble().Concat(System.Text.UnicodeEncoding.Unicode.GetBytes(_strings[language][path].ToDocument())).ToArray();

            return System.Text.UnicodeEncoding.Unicode.GetPreamble().Concat(System.Text.UnicodeEncoding.Unicode.GetBytes("")).ToArray();
        }

        public List<string> GetStringTable(Language language, string tableName, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<string, StringTable>();


            if (!_strings[language].ContainsKey(path))
            {
                var asset = GetAsset($"data/strings/{language}/{path}", defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].List;

            return new List<string>();

        }

        public StringTable GetStringTable2(Language language, string tableName, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<string, StringTable>();


            if (!_strings[language].ContainsKey(path))
            {
                var asset = GetAsset($"data/strings/{language}/{path}", defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path];

            return null;

        }

        public void SetString(Language language, string tableName, int label, string value, MythicPackage defaulPackage = MythicPackage.DEV)
        {
            string path = tableName.Replace($"data/strings/{language}/", "");
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<string, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                var asset = GetAsset(path, defaulPackage);
                if (asset == null)
                    asset = GetAsset(path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
            {
                _strings[language][path].SetValue(label, value);
            }

        }

        public string GetStringFormat(Language language, string label, params object[] vals)
        {
            string str = GetStringByKey(language, "/default.txt", label);

            for (int i = 0; i < vals.Length; i++)
            {
                str = str.Replace("<<" + (i + 1).ToString() + ">>", vals[i].ToString());
            }
            return str.Replace("<BR>", "\r\n");
        }

        //public bool HasDehash(string path)
        //{
        //    return HashToString.ContainsKey(MYP.HashWAR(path));
        //}
    }

}
