echo off
set source_folder=D:\Games\Warhammer Online - Age of Reckoning\myp\art\skeletons
set destination_folder=D:\Games\Warhammer Online - Age of Reckoning\myp\art\skeletons

for /f "usebackq delims=|" %%f in (`dir /b "%source_folder%\*.xac"`) do (
	Xac2Ms.exe "%source_folder%\%%~f" "%destination_folder%\%%~nf.ms"
echo[
)

pause