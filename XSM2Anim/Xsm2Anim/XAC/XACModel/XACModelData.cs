﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class XACData
    {
        public Info mMetaData;
        public Nodes mNodeTree;
        public List<Mesh> mMeshesList;
        public List<XACAnimation> mAnimationList;
        public List<XACTexture> mTextureList;
        public List<XACUnknownOfChunkID2> mUnknownOfChunkID2List;
        public MaterialInfo mMaterialInfo;
        public List<StandardMaterial> mMaterialListChunkID3;
        public List<StandardMaterial> mMaterialListChunkID5;
        public List<Node> mBonesList;

        public XACData()
        {
            mMetaData = new Info();
            mNodeTree = new Nodes();
            mMeshesList = new List<Mesh>();
            mAnimationList = new List<XACAnimation>();
            mTextureList = new List<XACTexture>();
            mUnknownOfChunkID2List = new List<XACUnknownOfChunkID2>();
            mMaterialInfo = new MaterialInfo();
            mMaterialListChunkID3 = new List<StandardMaterial>();
            mMaterialListChunkID5 = new List<StandardMaterial>();
            mBonesList = new List<Node>();
        }
    }

}