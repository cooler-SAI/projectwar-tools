﻿using System.IO;

namespace UnexpectedBytes
{
    public class MaterialInfo
    {
        private uint mTotalMaterialsCount;
        private uint mStandardMaterialsCount;
        private uint mFxMaterialsCount;


        public MaterialInfo()
        {
            mTotalMaterialsCount = 0;
            mStandardMaterialsCount = 0;
            mFxMaterialsCount = 0;
        }

        public void ReadIn(BinaryReader iStream, Chunk iChunk)
        {
            mTotalMaterialsCount = iStream.ReadUInt32();
            mStandardMaterialsCount = iStream.ReadUInt32();
            mFxMaterialsCount = iStream.ReadUInt32();
        }

        public void WriteOut(BinaryWriter iStream, Chunk iChunk)
        {
            iStream.Write(mTotalMaterialsCount);
            iStream.Write(mStandardMaterialsCount);
            iStream.Write(mFxMaterialsCount);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += sizeof(uint);
            vSize += sizeof(uint);
            vSize += sizeof(uint);
            return vSize;
        }
    }
}
