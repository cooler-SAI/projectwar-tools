﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{
  public class XACUnknownOfChunkID2
  {
    public UInt32 mValue1;
    public UInt32 mValue2;
    public List<XACUnknownOfChunkID2Entry> mUnknownOfChunkID2List;

    public XACUnknownOfChunkID2(UInt32 iValue1, UInt32 iValue2, List<XACUnknownOfChunkID2Entry> iUnknownOfChunkID2List)
    {
      mValue1 = iValue1;
      mValue2 = iValue2;
      mUnknownOfChunkID2List = new List<XACUnknownOfChunkID2Entry>(iUnknownOfChunkID2List);
    }

    public override string ToString()
    {
      int vUnknownOfChunkID2ListCount = 0;
      if (null != mUnknownOfChunkID2List)
      {
        vUnknownOfChunkID2ListCount = mUnknownOfChunkID2List.Count;
      }
      string vTheString = "XACUnknownOfChunkID2 Value1(UInt32):" + mValue1.ToString() + " Value2(UInt32):" + mValue2.ToString() + "  Entries:" + vUnknownOfChunkID2ListCount.ToString();
      return vTheString;
    }
  }

}