﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class XACFace
    {
        //The List will always contain 3 VerticesIdnexes and represents a triangle Face
        //Seems XAC only knows triangles?
        List<UInt32> mVerticesIndexs;
        public XACFace(List<UInt32> iVerticesIndexs)
        {
            mVerticesIndexs = new List<UInt32>(iVerticesIndexs);
        }

        public override string ToString()
        {
            string vTheString = "XACFace";
            if ((null != mVerticesIndexs) && (0 < mVerticesIndexs.Count))
            {
                vTheString += " Vertices";
                foreach (UInt32 vVerticeIndex in mVerticesIndexs)
                {
                    vTheString += " " + vVerticeIndex.ToString();
                }
            }
            else
            {
                vTheString += " No Vertcies";
            }
            return vTheString;
        }
    }

}
